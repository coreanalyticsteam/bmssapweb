﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
using System.Web;

namespace Wrapper
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "HealthEx" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select HealthEx.svc or HealthEx.svc.cs at the Solution Explorer and start debugging.
    public class HealthEx : IHealthEx
    {
        public List<LeadDetails> GetAllExLead(ReqMarkExSearch objReqMarkExSearch)
        {
            
            IHealthExBL bll = new HealthExBL();
            return bll.GetAllExLead(objReqMarkExSearch);
        }

        public int MarkLeadForHealthEx(ReqMarkHealthEx objReqMarkHealthEx)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.MarkLeadForHealthEx(objReqMarkHealthEx);
        }

        public int AddSupplierPlanForEx(PostPlanSelectionData objPostPlanSelectionData)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AddSupplierPlanForEx(objPostPlanSelectionData);
        }

        public int AddMemberDouments(PostMemberDocumentDetails objPostMemberDocumentDetails)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AddMemberDouments(objPostMemberDocumentDetails);
        }

        public List<MasterSupplier> GetSupplierList()
        {
            //var token = HttpContext.Current.Request.Headers["Token"];
            IHealthExBL bll = new HealthExBL();
            return bll.GetSupplierList();
        }


        public ResUploadCaseToInsurer GetExLeadDetailsForCaseSend(string LeadID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetExLeadDetailsForCaseSend(Convert.ToInt64(LeadID));
        }

        public List<LeadDetails> GetLeadsForPBDashboard(ReqMarkExSearch objReqMarkExSearch)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetLeadsForPBDashboard(objReqMarkExSearch);
        }

        public ResInsurerUnderwriting GetExLeadDetailsForInsurer(string LeadID, string InsurerID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetExLeadDetailsForInsurer(Convert.ToInt64(LeadID), Convert.ToInt16(InsurerID),0,0);
        }

        public int UpdateInsurerAction(PostInsurerActionData objPostInsurerActionData)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.UpdateInsurerAction(objPostInsurerActionData);
        }

        public ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(string LeadID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetPBInsurerUnderwritingDetails(Convert.ToInt64(LeadID));
        }

        public UserDetails Login(UserLoginRequest objUserLoginRequest)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.Login(objUserLoginRequest);

        }
    }
}
