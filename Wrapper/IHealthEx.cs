﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;

namespace Wrapper
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHealthEx" in both code and config file together.
    [ServiceContract]
    public interface IHealthEx
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "GetAllExLead")]
        List<LeadDetails> GetAllExLead(ReqMarkExSearch objReqMarkExSearch);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "MarkLeadForHealthEx")]
        int MarkLeadForHealthEx(ReqMarkHealthEx objReqMarkHealthEx);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AddSupplierPlanForEx")]
        int AddSupplierPlanForEx(PostPlanSelectionData objPostPlanSelectionData);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AddMemberDouments")]
        int AddMemberDouments(PostMemberDocumentDetails objPostMemberDocumentDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetSupplierList")]
        List<MasterSupplier> GetSupplierList();

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetExLeadDetailsForCaseSend/{LeadID}")]
        ResUploadCaseToInsurer GetExLeadDetailsForCaseSend(string LeadID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "GetLeadsForPBDashboard")]
        List<LeadDetails> GetLeadsForPBDashboard(ReqMarkExSearch objReqMarkExSearch);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetExLeadDetailsForInsurer/{LeadID}/{InsurerID}")]
        ResInsurerUnderwriting GetExLeadDetailsForInsurer(string LeadID, string InsurerID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateInsurerAction")]
        int UpdateInsurerAction(PostInsurerActionData objPostInsurerActionData);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetPBInsurerUnderwritingDetails/{LeadID}")]
        ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(string LeadID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Login")]
        UserDetails Login(UserLoginRequest objUserLoginRequest);
    }
}
