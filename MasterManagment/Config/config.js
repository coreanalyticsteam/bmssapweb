﻿myApp.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("IssueTypeMaster");
    $stateProvider
        .state('IssueTypeMaster', {
            name: 'IssueTypeMaster',
            url: '/IssueTypeMaster',
            templateUrl: 'View/IssueTypeMaster.html',
            controller: 'IssueTypeMasterController'
        }).state('SubIssueTypeMaster', {
            name: 'SubIssueTypeMaster',
            url: '/SubIssueTypeMaster',
            templateUrl: 'View/SubIssueTypeMaster.html',
            controller: 'SubIssueTypeMasterController'
        });
});
