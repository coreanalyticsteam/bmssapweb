﻿myApp.controller('SubIssueTypeMasterController', function ($scope, AjaxFactory, $stateParams) {
   
    //$scope.newIssue = { IssueTypeId: 0, IssueName: '', IsActive: true, SequenceID: 0, ParentID: 0 };
    
    AjaxFactory.post(ServiceURL + 'GetAllIssues', function (data) {
        $scope.AllIssues = JSON.parse(data.data.result.Success);
        //console.log($scope.ProductList);
    });
    $scope.GetIssues = function (data) {
        AjaxFactory.postData(ServiceURL + 'GetTicketSubIssueMaster', { Json: data}, function (data) {
            $scope.SubIssueList = JSON.parse(data.data.result.Success).Table;
            $scope.ProductList = JSON.parse(data.data.result.Success).Table1;
          //  console.log($scope.ProductList);
        });
    }
    $scope.AddNew = function () {

        var newSet = { IssueTypeId: 0, IssueName: '', IsActive: true, SequenceID: 0,TAT:0 }
        $scope.SubIssueList.push(newSet);
    }
    $scope.SaveIssue = function (data, dataindex) {
        debugger;
        $scope.RowIndex = dataindex;
        if (data.IssueName == '') {
            alert("Please Add Issue Name first")
            return false;
        }
        if (data.Product == undefined) {
            alert("Please Add Product first")
            return false;
        }
        
        data.ProductName = JSON.parse(data.Product).ProductName;
        $scope.SubIssueList[dataindex].ProductName = JSON.parse(data.Product).ProductName;
        for (var i in $scope.SubIssueList) {
            if ($scope.SubIssueList[i].ProductID == JSON.parse(data.Product).ProductID && $scope.SubIssueList[i].IssueName == data.IssueName) {
                alert(data.ProductName + " and " + data.IssueName + " Already exists");
                return false;
            }
        }
        data.ProductID = JSON.parse(data.Product).ProductID;
        data.ParentID = JSON.parse(data.Product).IssueTypeId
        AjaxFactory.postData(ServiceURL + 'SaveTicketIssueTypeMaster', { Json: data }, function (data) {
            //debugger;
            $scope.NewId = JSON.parse(data.data.result.Success);
            if ($scope.NewId > 0) {
                $scope.SubIssueList[$scope.RowIndex].IssueTypeId = parseInt($scope.NewId);
                alert("Successfully saved");
            }
            else {
                alert("Some error occured");
            }
           // console.log($scope.ProductList);
        });
    }
    $scope.EditIssue = function (data, dataindex) {
        debugger;
        //$scope.RowIndex = dataindex;
        //$scope.parentIndex = parentIndex;
        AjaxFactory.postData(ServiceURL + 'SaveTicketIssueTypeMaster', { Json: data }, function (data) {
            //debugger;
            $scope.NewId = JSON.parse(data.data.result.Success);
            if ($scope.NewId > 0) {
                alert("Successfully saved");
            }
            else {
                alert("Some error occured");
            }
        });
    }
    //$scope.SaveNewIssue = function (data) {
    //    debugger;
    //    if (data.IssueName == '') {
    //        alert("Please Add Issue Name first")
    //        return false;
    //    }
    //    if (data.Product == undefined) {
    //        alert("Please Add Product first")
    //        return false;
    //    }

    //    data.ProductName = JSON.parse(data.Product).Product;
    //    for (var i in $scope.IssueMaster) {
    //        if ($scope.IssueMaster[i].IssueName.toLowerCase() == data.IssueName.toLowerCase()) {
    //            alert(data.IssueName + " Already exists");
    //            return false;
    //        }
    //    }
    //    data.ProductID = JSON.parse(data.Product).ProductID;
    //    AjaxFactory.postData(ServiceURL + 'SaveTicketIssueTypeMaster', { Json: data }, function (data) {
    //        $scope.NewId = JSON.parse(data.data.result.Success);
    //        if ($scope.NewId > 0) {
    //            alert("Successfully saved");
    //            location.reload();
    //        }
    //        else {
    //            alert("Some error occured");
    //        }
    //    });
    //}
});