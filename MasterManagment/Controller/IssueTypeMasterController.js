﻿myApp.controller('IssueTypeMasterController', function ($scope, AjaxFactory, $stateParams) {
   
    $scope.newIssue = { IssueTypeId: 0, IssueName: '', IsActive: true, SequenceID: 0, ParentID: 0,TAT:0 };
    AjaxFactory.post(ServiceURL + 'GetTicketIssueMaster', function (data) {
        $scope.IssueMaster =  JSON.parse(data.data.result.Success);
        
    });
    AjaxFactory.post(ServiceURL + 'GetProducts', function (data) {
        $scope.ProductList = JSON.parse(data.data.result.Success);
        console.log($scope.ProductList);
    });

    $scope.AddNew = function (data, dataindex) {

        var newSet = { IssueTypeId: 0, IssueName: data.IssueName, IsActive: true, SequenceID: 0,ParentID:0,TAT:0 }
        $scope.IssueMaster[dataindex].TicketIssueTypeMaster.push(newSet);
    }
    $scope.SaveIssue = function (data, dataindex, parentIndex) {
        debugger;
        $scope.RowIndex = dataindex;
        $scope.parentIndex = parentIndex;
        
        data.ProductName = JSON.parse(data.Product).Product;
        for (var i in $scope.IssueMaster[$scope.parentIndex].TicketIssueTypeMaster) {
            if ($scope.IssueMaster[$scope.parentIndex].TicketIssueTypeMaster[i].ProductID == JSON.parse(data.Product).ProductID) {
                alert(data.ProductName + " Already exists");
                return false;
            }
        }
        data.ProductID = JSON.parse(data.Product).ProductID;
        AjaxFactory.postData(ServiceURL + 'SaveTicketIssueTypeMaster', { Json: data }, function (data) {
            //debugger;
            $scope.NewId = JSON.parse(data.data.result.Success);
            if ($scope.NewId > 0) {
                $scope.IssueMaster[$scope.parentIndex].TicketIssueTypeMaster[$scope.RowIndex].IssueTypeId = parseInt($scope.NewId);
                alert("Successfully saved");
            }
            else {
                alert("Some error occured");
            }
           // console.log($scope.ProductList);
        });
    }
    $scope.EditIssue = function (data, dataindex, parentIndex) {
        debugger;
        //$scope.RowIndex = dataindex;
        //$scope.parentIndex = parentIndex;
        AjaxFactory.postData(ServiceURL + 'SaveTicketIssueTypeMaster', { Json: data }, function (data) {
            //debugger;
            $scope.NewId = JSON.parse(data.data.result.Success);
            if ($scope.NewId > 0) {
                alert("Successfully saved");
            }
            else {
                alert("Some error occured");
            }
        });
    }
    $scope.SaveNewIssue = function (data) {
        debugger;
        if (data.IssueName == '') {
            alert("Please Add Issue Name first")
            return false;
        }
        if (data.Product == undefined) {
            alert("Please Add Product first")
            return false;
        }

        data.ProductName = JSON.parse(data.Product).Product;
        for (var i in $scope.IssueMaster) {
            if ($scope.IssueMaster[i].IssueName.toLowerCase() == data.IssueName.toLowerCase()) {
                alert(data.IssueName + " Already exists");
                return false;
            }
        }
        data.ProductID = JSON.parse(data.Product).ProductID;
        AjaxFactory.postData(ServiceURL + 'SaveTicketIssueTypeMaster', { Json: data }, function (data) {
            $scope.NewId = JSON.parse(data.data.result.Success);
            if ($scope.NewId > 0) {
                alert("Successfully saved");
                location.reload();
            }
            else {
                alert("Some error occured");
            }
        });
    }
});