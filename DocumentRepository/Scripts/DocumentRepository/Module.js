﻿var DocumentRepository = angular.module("DocumentRepository");

DocumentRepository.directive('fileModel', ['$parse', 'DocumentRepositoryService', function ($parse, DocumentRepositoryService) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    debugger;
                    DocumentRepositoryService.FileData = element[0].files[0];
                    modelSetter(scope.$parent.$parent.myFile, element[0].files[0]);
                });
            });
        }
    };
}]);


DocumentRepository.directive('andyDraggable', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            var options = scope.$eval(attrs.andyDraggable); //allow options to be passed in
            elm.draggable(options);
        }
    };
});