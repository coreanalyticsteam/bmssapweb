﻿DocumentRepository.controller("DocumentRepositoryCtrl", function ($scope, DocumentRepositoryService, $routeParams, $uibModal) {

    $scope.LeadID = $routeParams.LeadID;
    $scope.userID = $routeParams.UserID;
    $scope.CustomerID = $routeParams.CustomerID;
    $scope.Type = $routeParams.Type;
    $scope.CheckDocumetStatus = 0;
    $scope.selectedDoc = undefined;

    $scope.MailToList = [];
    $scope.GetDocuments = function () {
        DocumentRepositoryService.GETRequiredDocumentsForBooking($scope.LeadID).success(function (data) {
            debugger;
            $scope.Documentsdetails = data;
            $scope.DocStatusList = CommonData.DocStatus;
            $scope.DocStatusListWait = CommonData.DocStatusWiting;
            // $scope.MailToList = CommonData.MailTo; 
            $scope.MailToList.push({ "MailToID": 0, "Name": "Select" });
            if ($scope.Documentsdetails.ProductID == 2) {
                $scope.MailToList.push({ "MailToID": 3, "Name": "Proposal Form" });
            }
            else {
                $scope.MailToList.push({ "MailToID": 1, "Name": "Customer" });
                $scope.MailToList.push({ "MailToID": 2, "Name": "Insurer" });
            }
            $scope.MailToList.Selected = { "MailToID": 0, "Name": "Select" };
            for (var i = 0; i < $scope.Documentsdetails.ReqDocCategoryDetails.length; i++) {

                if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus == null || ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID == null || $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID == 0)) {
                    $scope.Documentsdetails.ReqDocCategoryDetails[i].Checked = true;
                }
            }
            if ($scope.Documentsdetails.ProductID == 115) {
                if ($scope.Documentsdetails.StatusID == 37 && ($scope.Documentsdetails.SubStatusID == 295 || $scope.Documentsdetails.SubStatusID == 1469)) {
                    $scope.CheckDocumetStatus = 0;
                }
                else if ($scope.Documentsdetails.StatusID == 39 && $scope.Documentsdetails.SubStatusID == 303) {
                    $scope.CheckDocumetStatus = 0;
                }
                else {
                    $scope.CheckDocumetStatus = 1;
                }
            }
            else if ($scope.Documentsdetails.ProductID == 7) {
                if ($scope.Documentsdetails.StatusID == 55 &&
                        (
                            $scope.Documentsdetails.SubStatusID == 467
                            || $scope.Documentsdetails.SubStatusID == 468
                    || $scope.Documentsdetails.SubStatusID == 470
                    || $scope.Documentsdetails.SubStatusID == 471
                    || $scope.Documentsdetails.SubStatusID == 473
                    || $scope.Documentsdetails.SubStatusID == 474
                    || $scope.Documentsdetails.SubStatusID == 476
                    || $scope.Documentsdetails.SubStatusID == 477
                        )
                    ) {
                    $scope.CheckDocumetStatus = 0;
                }
                else if ($scope.Documentsdetails.StatusID == 39
                    &&
                    (
                    $scope.Documentsdetails.SubStatusID == 303
                    || $scope.Documentsdetails.SubStatusID == 1539
					                    || $scope.Documentsdetails.SubStatusID == 1538
                    || $scope.Documentsdetails.SubStatusID == 1540
                    )
                    ) {
                    $scope.CheckDocumetStatus = 0;
                }
                else {
                    $scope.CheckDocumetStatus = 1;
                }
            }
            else {
                $scope.CheckDocumetStatus = 0;
            }
            //$scope.CheckDocumetStatus=
        });


    };
    $scope.DocumentDataList = [];
    $scope.GetDocuments();
    $scope.isEmpty = function (str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };

    $scope.SaveDocumentsStatus = function () {
        if ($scope.CheckDocumetStatus == 0) {
            var DocumentPostData = {
                "UserID": $routeParams.UserID,
                "BookingID": $routeParams.LeadID,
                "DocumentDetailIDsList": []
            };
            var DocumentDetailID = {
                "DocCategoryID": "",
                "DocumentID": "",
                "StatusID": "",
                "DocName": "",
                "File": "",
                "FileString": ""
            };
            debugger;
            var msg = '';
            var DocCategoryID = [];
            for (var i = 0; i < $scope.Documentsdetails.ReqDocCategoryDetails.length; i++) {

                if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus != null && $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID >= 0) {
                    for (var j = 0; j < $scope.DocumentDataList.length; j++) {
                        if ($scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID == $scope.DocumentDataList[j].DocCategoryID) {
                            if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc != null && $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc.DocumentID > 0) {
                                if (!$scope.isEmpty($scope.Documentsdetails.ReqDocCategoryDetails[i].NewDocName)) {
                                    DocCategoryID.push($scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID);
                                    DocumentDetailID = {
                                        "DocCategoryID": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID,
                                        "DocumentID": $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc.DocumentID,
                                        "StatusID": 1,
                                        "DocName": $scope.Documentsdetails.ReqDocCategoryDetails[i].NewDocName,
                                        "File": $scope.DocumentDataList[j].FileName,
                                        "FileString": $scope.DocumentDataList[j].FileStringData,
                                        "DocEmailRemarks": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment

                                    };
                                    DocumentPostData.DocumentDetailIDsList.push(DocumentDetailID);
                                }
                                else {
                                    msg = msg + ' Please Enter Document Name for ' + $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryName;
                                }
                            }
                            else {
                                msg = msg + ' Please select Document for ' + $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryName;
                            }
                        }

                    }
                    //alert(DocCategoryID.indexOf($scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID));
                    debugger;
                    if (DocCategoryID.indexOf($scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID) < 0) {
                        if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc != null && $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc.DocumentID >= 0) {
                            if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc.DocumentID != $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.DocumentID
                                || $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID != $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.StatusID
                                || $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc.DocEmailRemarks != $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment) {
                                DocCategoryID.push($scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID);
                                DocumentDetailID = {
                                    "DocCategoryID": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID,
                                    "DocumentID": $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc.DocumentID,
                                    "StatusID": $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID,
                                    "DocName": "",
                                    "File": "",
                                    "FileString": "",
                                    "DocEmailRemarks": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment

                                };
                                DocumentPostData.DocumentDetailIDsList.push(DocumentDetailID);
                            }
                        }
                        else {
                            if (DocCategoryID.indexOf($scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID) < 0) {
                                if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID != $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.StatusID
                                    || $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedDoc.DocEmailRemarks != $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment) {
                                    DocCategoryID.push($scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID);
                                    DocumentDetailID = {
                                        "DocCategoryID": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID,
                                        "DocumentID": 0,
                                        "StatusID": $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID,
                                        "DocName": "",
                                        "File": "",
                                        "FileString": "",
                                        "DocEmailRemarks": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment
                                    };
                                    DocumentPostData.DocumentDetailIDsList.push(DocumentDetailID);
                                }
                            }
                        }
                    }


                }
                else {
                    msg = msg + ' Please select Status for ' + $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryName;
                }


            }
            debugger;
            if (msg.trim() == '') {
                DocumentRepositoryService.InsertUpdateDocumentsForBooking(DocumentPostData).success(function (response) {
                    if (response.Return == 0) {
                        $scope.Documentsdetails = response;
                        $scope.DocumentDataList = [];
                        //for (var i = 0; i < $scope.Documentsdetails.ReqDocCategoryDetails.length; i++) {                        

                        //    //if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus == null || ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID == null || $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID == 0)) {
                        //    //   // $scope.Documentsdetails.ReqDocCategoryDetails[i].Checked = true;
                        //    //}
                        //}
                        if (($scope.Documentsdetails.ProductID == 7 || $scope.Documentsdetails.ProductID == 115) && $scope.Documentsdetails.BlockAutoCommunication != 1)
                        {

                            var TriggerName = "";
                            var toRec = $scope.Documentsdetails.CustomerEmailID;

                            var CommunicationType = "1";
                            if (($scope.Documentsdetails.ProductID == 7 && $scope.Documentsdetails.StatusID == 55)
                                || ($scope.Documentsdetails.ProductID == 115 && $scope.Documentsdetails.StatusID == 37))
                            {
                                var objEmailData = {
                                    "CommunicationDetails": {
                                        "LeadID": $scope.LeadID,
                                        "ProductID": $scope.Documentsdetails.ProductID,
                                        //"DocumentDetails": DocumentDetails,

                                        "Conversations": [
                                          {
                                              "From": "communication@policybazaar.com",
                                              "ToReceipent": [
                                              toRec
                                               //ToEmailId
                                              ],
                                              "CreatedBy": "BMS",
                                              "TriggerName": TriggerName,
                                              "UserID": $scope.userID,
                                              "AutoTemplate": true, "LeadStatusID": $scope.Documentsdetails.StatusID, "SubStatusID": $scope.Documentsdetails.SubStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                          }
                                        ],
                                        "BookingDetail": { "BookingType": "DIRECT" },
                                        "CommunicationType": CommunicationType,
                                        "IsBooking": true
                                    }
                                };
                                DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                    // alert(response.SendResult.Description);
                                });

                                toRec = $scope.Documentsdetails.CustomerMobile;
                                CommunicationType = "2";
                                objEmailData = {
                                    "CommunicationDetails": {
                                        "LeadID": $scope.LeadID,
                                        "ProductID": $scope.Documentsdetails.ProductID,
                                        //"DocumentDetails": DocumentDetails,

                                        "Conversations": [
                                          {
                                              "From": "communication@policybazaar.com",
                                              "ToReceipent": [
                                              toRec
                                               //ToEmailId
                                              ],
                                              "CreatedBy": "BMS",
                                              "TriggerName": TriggerName,
                                              "UserID": $scope.userID,
                                              "AutoTemplate": true, "LeadStatusID": $scope.Documentsdetails.StatusID, "SubStatusID": $scope.Documentsdetails.SubStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                          }
                                        ],
                                        "BookingDetail": { "BookingType": "DIRECT" },
                                        "CommunicationType": CommunicationType,
                                        "IsBooking": true
                                    }
                                };
                                DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                    // alert(response.SendResult.Description);
                                });
                            }
                            else if (( $scope.Documentsdetails.ProductID == 7 ||  $scope.Documentsdetails.ProductID == 115)
                                && $scope.Documentsdetails.StatusID == 39 && $scope.Documentsdetails.SubStatusID == 320)
                            {
                                var _statusID = 0; var _subStatusID = 0;
                                
                                    _subStatusID = $scope.Documentsdetails.SubStatusID;
                                    _statusID = $scope.Documentsdetails.StatusID;
                               
                                var objEmailData = {
                                    "CommunicationDetails": {
                                        "LeadID": $scope.LeadID,
                                        "ProductID": $scope.Documentsdetails.ProductID,
                                        //"DocumentDetails": DocumentDetails,

                                        "Conversations": [
                                          {
                                              "From": "communication@policybazaar.com",
                                              "ToReceipent": [
                                              toRec
                                               //ToEmailId
                                              ],
                                              "CreatedBy": "BMS",
                                              "TriggerName": TriggerName,
                                              "UserID": $scope.userID,
                                              "AutoTemplate": true, "LeadStatusID": _statusID, "SubStatusID": _subStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                          }
                                        ],
                                        "BookingDetail": { "BookingType": "DIRECT" },
                                        "CommunicationType": CommunicationType,
                                        "IsBooking": true
                                    }
                                };
                                DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                    // alert(response.SendResult.Description);
                                });

                                toRec = $scope.Documentsdetails.CustomerMobile;
                                CommunicationType = "2";
                                objEmailData = {
                                    "CommunicationDetails": {
                                        "LeadID": $scope.LeadID,
                                        "ProductID": $scope.Documentsdetails.ProductID,
                                        //"DocumentDetails": DocumentDetails,

                                        "Conversations": [
                                          {
                                              "From": "communication@policybazaar.com",
                                              "ToReceipent": [
                                              toRec
                                               //ToEmailId
                                              ],
                                              "CreatedBy": "BMS",
                                              "TriggerName": TriggerName,
                                              "UserID": $scope.userID,
                                              "AutoTemplate": true, "LeadStatusID": _statusID, "SubStatusID": _subStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                          }
                                        ],
                                        "BookingDetail": { "BookingType": "DIRECT" },
                                        "CommunicationType": CommunicationType,
                                        "IsBooking": true
                                    }
                                };
                                DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                    // alert(response.SendResult.Description);
                                });
                            }
                        }
                        alert('Document updated successfully');
                    }
                    else {
                        alert('Please enter unique file name');
                    }
                });
            }
            else {
                alert(msg);
            }
        }
        else {
            alert('Doc changes can not be done for these status.');
        }
    };


    $scope.SaveExistingDocs = function (doc) {

        if (doc.Checked != false) {
            debugger;
            var r = confirm("Are you sure to upload the document??");
            if (r == true) {
                var objExistingDocumentDetailsRequest = {

                    "UserID": $routeParams.UserID,
                    "BookingID": $routeParams.LeadID,
                    "ParentID": doc.DocPointsID
                };
                DocumentRepositoryService.UpdateDocumentsForBooking(objExistingDocumentDetailsRequest).success(function (response) {
                    if (response.Return == 0) {
                        $scope.Documentsdetails = response;
                        //for (var i = 0; i < $scope.Documentsdetails.ReqDocCategoryDetails.length; i++) {

                        //    if ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus == null || ($scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID == null || $scope.Documentsdetails.ReqDocCategoryDetails[i].SelectedStatus.StatusID == 0)) {
                        //        $scope.Documentsdetails.ReqDocCategoryDetails[i].Checked = true;
                        //    }
                        //}
                        alert('Document updated successfully');
                    }
                    else {
                        doc.Checked = false;
                        alert('Can not upload document with same name.');
                    }
                });
            }
        }
    };
    $scope.file_changed = function (element) {
        debugger;
        $scope.$apply(function (scope) {

            var photofile = element.files[0];

            var fileReader = new FileReader();
            var DocumentData = {
                "DocCategoryID": undefined,
                "DocumentID": undefined,
                "FileName": undefined,
                "DocName": undefined,
                "Size": undefined,
                "FileStringData": undefined
            };
            DocumentData.DocCategoryID = element.id.split(':')[0];
            DocumentData.DocumentID = element.id.split(':')[1];
            DocumentData.DocName = element.id.split(':')[2];

            fileReader.onload = function (fileLoadedEvent) {
                var chkIndex = undefined;
                DocumentData.FileName = element.files[0].name;
                DocumentData.Size = element.files[0].size
                DocumentData.FileStringData = btoa(fileLoadedEvent.target.result);
                if ($scope.DocumentDataList.length > 0) {
                    for (var i = 0; i < $scope.DocumentDataList.length; i++) {
                        if ($scope.DocumentDataList[i].DocCategoryID == element.id.split(':')[0]) {
                            chkIndex = i;
                        }
                    }
                }
                if (chkIndex != undefined) {
                    $scope.DocumentDataList.splice(chkIndex, 1);
                }
                $scope.DocumentDataList.push(DocumentData);
                //
            };
            if (element.files.length > 0) {
                fileReader.readAsBinaryString(element.files[0]);
            }
            else {
                if ($scope.DocumentDataList.length > 0) {
                    for (var i = 0; i < $scope.DocumentDataList.length; i++) {
                        if ($scope.DocumentDataList[i].DocCategoryID == element.id.split(':')[0]) {
                            $scope.DocumentDataList.splice(i, 1);
                        }
                    }
                }
            }

        });
    };


    $scope.uploadExistingFile = function (doc) {
        if (doc.Checked != false) {
            debugger;
            var r = confirm("Are you sure to upload the document??");
            if (r == true) {
                var PayLoad = {
                    "CustomerID": $scope.Documentsdetails.CustomerID, "LeadID": $scope.LeadID,
                    "ParentCustDocumentID": doc.CustDocumentID, "ProductID": $scope.Documentsdetails.ProductID,
                    "ProductName": $scope.Documentsdetails.ProductName, "UserID": $scope.userID, "HostName": ''
                };
                DocumentRepositoryService.UploadCustomerDocuments(PayLoad).success(function (response) {
                    debugger;
                    if (parseInt(response) > 0) {
                        alert('Document updated successfully');
                        $scope.GetDocuments();
                    }
                    else {
                        doc.Checked = false;
                        alert('Please try again.');
                    }
                });
            }
            else { doc.Checked = false; }
        }
    };



    $scope.SendEmail = function () {
        debugger;
        var DocumentDetails = [];
        var TriggerName = "";
        var ToEmailId = "343";
        var objEmailData;
        var DocID, DocName;
        if ($scope.MailToList.Selected.MailToID == 1 || $scope.MailToList.Selected.MailToID == 2 || $scope.MailToList.Selected.MailToID == 3) {
            for (var i = 0; i < $scope.Documentsdetails.ReqDocCategoryDetails.length; i++) {
                if ($scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails != undefined) {
                    if ($scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.DocumentID > 0) {
                        DocID = $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.DocumentID;
                        DocName = $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.DocumentName;
                    }
                    else {
                        DocID = 0;
                        DocName = '';
                    }
                }
                else {
                    DocID = 0;
                    DocName = '';
                }
                if ($scope.MailToList.Selected.MailToID == 1 && $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.StatusID == 0) {
                    ToEmailId = $scope.Documentsdetails.CustomerEmailID;
                    TriggerName = "DocPointsEmail";
                    DocumentDetails.push({
                        "DocCategoryID": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID,
                        "DocCategoryName": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryName,
                        "DocumentID": DocID,
                        "DocumentName": DocName,
                        "DocComment": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment
                    });
                }
                else if ($scope.MailToList.Selected.MailToID == 2 &&
                            $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.StatusID == 1
                             && $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.DocURL != '') {
                    for (var j = 0; j < CommonData.SupplierEmailIDs.length; j++) {
                        if (CommonData.SupplierEmailIDs[j].ProductID == $scope.Documentsdetails.ProductID && CommonData.SupplierEmailIDs[j].SupplierID == $scope.Documentsdetails.SupplierID) {
                            ToEmailId = CommonData.SupplierEmailIDs[j].EmailIDl;
                        }
                    }
                    TriggerName = "DocPointsEmailToInsurer";
                    DocumentDetails.push({
                        "DocCategoryID": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID,
                        "DocCategoryName": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryName,
                        "DocumentID": DocID,
                        "DocumentName": DocName,
                        "DocComment": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment,
                        "DocURL": $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.DocURL
                    });
                }
                else if ($scope.MailToList.Selected.MailToID == 3 &&
                            $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.StatusID == 1 &&
                            $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID == 24) {
                    ToEmailId = $scope.Documentsdetails.CustomerEmailID;
                    TriggerName = "ProposalFormEmail";
                    if (DocID != 0 && DocName != '')
                        DocumentDetails.push({
                            "DocCategoryID": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryID,
                            "DocCategoryName": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocCategoryName,
                            "DocumentID": DocID,
                            "DocumentName": DocName,
                            "DocComment": $scope.Documentsdetails.ReqDocCategoryDetails[i].DocComment,
                            "DocURL": $scope.Documentsdetails.ReqDocCategoryDetails[i].CurStatusDetails.DocURL
                        });
                }

            }
        }
        else {
            //alert('Please select any email ');

            return false;
        }
        //$scope.Documentsdetails.CustomerEmailID
        objEmailData = {
            "CommunicationDetails": {
                "LeadID": $scope.LeadID,
                "ProductID": $scope.Documentsdetails.ProductID,
                "DocumentDetails": DocumentDetails,

                "Conversations": [
                  {
                      "From": "communication@policybazaar.com",
                      "ToReceipent": [
                       //"neerajr@policybazaar.com", "lalan@policybazaar.com", "manojv@policybazaar.com", "asmit@policybazaar.com"
                       ToEmailId
                      ],
                      "CreatedBy": "BMS",
                      "TriggerName": TriggerName,
                      "UserID": $scope.userID,
                      "AutoTemplate": true
                  }
                ],
                "BookingDetail": { "BookingType": "DIRECT" },
                "CommunicationType": 1,
                "IsBooking": true
            }
        };
        if (ToEmailId != "") {
            if (DocumentDetails.length > 0) {
                DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                    alert(response.SendResult.Description);
                });
            }
            else {
                alert('No document found.');
            }
        }
        else {
            alert("EmailID is not available.");
        }
    };
    var modalInstanceSameCtrl;
    $scope.GetDocumentAuditTrail = function () {
        debugger;
        DocumentRepositoryService.GetDocumentAuditTrail($scope.LeadID).success(function (data) {
            $scope.DocumentAuditTrail = data;
            modalInstanceSameCtrl = $uibModal.open({
                animation: true,
                size: 'lg',
                scope: $scope,
                templateUrl: 'views/DocAuditTrial.htm'

            });
        });
    };
    $scope.cancel = function () {
        modalInstanceSameCtrl.close();
    };




    $scope.UpdateDocStatus = function () {

        var DocumentPostData = {
            "UserID": $routeParams.UserID,
            "BookingID": $routeParams.LeadID,
            "DocumentDetailIDsList": []
        };
        var DocumentDetailID = {
            "DocCategoryID": "",
            "DocumentID": "",
            "StatusID": "",
            "DocName": "",
            "File": "",
            "FileString": "",
            "DocEmailRemarks": ""
        };
        debugger;
        var msg = '';
        var DocCategoryID = [];
        var countSelectedStatus = 0;
        for (var i = 0; i < $scope.Documentsdetails.WaitDocumentsForBooking.length; i++) {

            if ($scope.Documentsdetails.WaitDocumentsForBooking[i].SelectedStatus != null && $.inArray($scope.Documentsdetails.WaitDocumentsForBooking[i].SelectedStatus.StatusID, [1,4]) != -1) {
                countSelectedStatus = countSelectedStatus + 1;
                DocumentDetailID = {
                    "DocPointsID": $scope.Documentsdetails.WaitDocumentsForBooking[i].DocPointsID,
                    "DocCategoryID": $scope.Documentsdetails.WaitDocumentsForBooking[i].DocCategoryID,
                    "DocumentID": $scope.Documentsdetails.WaitDocumentsForBooking[i].DocumentID,
                    "StatusID": $scope.Documentsdetails.WaitDocumentsForBooking[i].SelectedStatus.StatusID,
                    "DocEmailRemarks": $scope.Documentsdetails.WaitDocumentsForBooking[i].DocComment
                };
                DocumentPostData.DocumentDetailIDsList.push(DocumentDetailID);
            }


        }

        debugger;
        if (countSelectedStatus == $scope.Documentsdetails.WaitDocumentsForBooking.length) {
            DocumentRepositoryService.UpdateDocStatus(DocumentPostData).success(function (response) {
                if (response.Return == 0) {
                    $scope.Documentsdetails = response;
                    $scope.DocumentDataList = [];
                    if (($scope.Documentsdetails.ProductID == 7 || $scope.Documentsdetails.ProductID == 115) && $scope.Documentsdetails.BlockAutoCommunication != 1) {

                        var TriggerName = "";
                        var toRec = $scope.Documentsdetails.CustomerEmailID;

                        var CommunicationType = "1";
                        if (($scope.Documentsdetails.ProductID == 7 && $scope.Documentsdetails.StatusID == 55)
                            || ($scope.Documentsdetails.ProductID == 115 && $scope.Documentsdetails.StatusID == 37)) {
                            var objEmailData = {
                                "CommunicationDetails": {
                                    "LeadID": $scope.LeadID,
                                    "ProductID": $scope.Documentsdetails.ProductID,
                                    //"DocumentDetails": DocumentDetails,

                                    "Conversations": [
                                      {
                                          "From": "communication@policybazaar.com",
                                          "ToReceipent": [
                                          toRec
                                           //ToEmailId
                                          ],
                                          "CreatedBy": "BMS",
                                          "TriggerName": TriggerName,
                                          "UserID": $scope.userID,
                                          "AutoTemplate": true, "LeadStatusID": $scope.Documentsdetails.StatusID, "SubStatusID": $scope.Documentsdetails.SubStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                      }
                                    ],
                                    "BookingDetail": { "BookingType": "DIRECT" },
                                    "CommunicationType": CommunicationType,
                                    "IsBooking": true
                                }
                            };
                            DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                // alert(response.SendResult.Description);
                            });

                            toRec = $scope.Documentsdetails.CustomerMobile;
                            CommunicationType = "2";
                            objEmailData = {
                                "CommunicationDetails": {
                                    "LeadID": $scope.LeadID,
                                    "ProductID": $scope.Documentsdetails.ProductID,
                                    //"DocumentDetails": DocumentDetails,

                                    "Conversations": [
                                      {
                                          "From": "communication@policybazaar.com",
                                          "ToReceipent": [
                                          toRec
                                           //ToEmailId
                                          ],
                                          "CreatedBy": "BMS",
                                          "TriggerName": TriggerName,
                                          "UserID": $scope.userID,
                                          "AutoTemplate": true, "LeadStatusID": $scope.Documentsdetails.StatusID, "SubStatusID": $scope.Documentsdetails.SubStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                      }
                                    ],
                                    "BookingDetail": { "BookingType": "DIRECT" },
                                    "CommunicationType": CommunicationType,
                                    "IsBooking": true
                                }
                            };
                            DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                // alert(response.SendResult.Description);
                            });
                        }
                        else if (($scope.Documentsdetails.ProductID == 7 || $scope.Documentsdetails.ProductID == 115)
                            && $scope.Documentsdetails.StatusID == 39 && $scope.Documentsdetails.SubStatusID == 320) {
                            var _statusID = 0; var _subStatusID = 0;

                            _subStatusID = $scope.Documentsdetails.SubStatusID;
                            _statusID = $scope.Documentsdetails.StatusID;

                            var objEmailData = {
                                "CommunicationDetails": {
                                    "LeadID": $scope.LeadID,
                                    "ProductID": $scope.Documentsdetails.ProductID,
                                    //"DocumentDetails": DocumentDetails,

                                    "Conversations": [
                                      {
                                          "From": "communication@policybazaar.com",
                                          "ToReceipent": [
                                          toRec
                                           //ToEmailId
                                          ],
                                          "CreatedBy": "BMS",
                                          "TriggerName": TriggerName,
                                          "UserID": $scope.userID,
                                          "AutoTemplate": true, "LeadStatusID": _statusID, "SubStatusID": _subStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                      }
                                    ],
                                    "BookingDetail": { "BookingType": "DIRECT" },
                                    "CommunicationType": CommunicationType,
                                    "IsBooking": true
                                }
                            };
                            DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                // alert(response.SendResult.Description);
                            });

                            toRec = $scope.Documentsdetails.CustomerMobile;
                            CommunicationType = "2";
                            objEmailData = {
                                "CommunicationDetails": {
                                    "LeadID": $scope.LeadID,
                                    "ProductID": $scope.Documentsdetails.ProductID,
                                    //"DocumentDetails": DocumentDetails,

                                    "Conversations": [
                                      {
                                          "From": "communication@policybazaar.com",
                                          "ToReceipent": [
                                          toRec
                                           //ToEmailId
                                          ],
                                          "CreatedBy": "BMS",
                                          "TriggerName": TriggerName,
                                          "UserID": $scope.userID,
                                          "AutoTemplate": true, "LeadStatusID": _statusID, "SubStatusID": _subStatusID, "SupplierId": [$scope.Documentsdetails.SupplierID]
                                      }
                                    ],
                                    "BookingDetail": { "BookingType": "DIRECT" },
                                    "CommunicationType": CommunicationType,
                                    "IsBooking": true
                                }
                            };
                            DocumentRepositoryService.SendEmail(objEmailData).success(function (response) {
                                // alert(response.SendResult.Description);
                            });
                        }
                    }
                    alert('Document updated successfully');
                }
                else {
                    alert('Please enter unique file name');
                }
            });
        }
        else {
            alert('Please select status for all pending documents');
        }

    };

});


DocumentRepository.controller("DocpointfortataCtrl", function ($scope, DocumentRepositoryService, $routeParams, $uibModal) {

    $scope.LeadID = $routeParams.LeadID;
    $scope.UserID = $routeParams.UserID;
    $scope.AppNo = $routeParams.AppNo;
    $scope.CustomerID = $routeParams.CustomerID;
    $scope.Type = $routeParams.Type;
    $scope.CheckDocumetStatus = 0;
    $scope.selectedDoc = undefined;

    $scope.MailToList = [];
    $scope.GetPendingDocuments = function () {
        DocumentRepositoryService.GetPendingDocuments($scope.LeadID, $scope.AppNo).success(function (data) {
            debugger;
            $scope.Documentsdetails = data;
            $scope.TermLifeUploadURL = TermLifeUploadURL + data.fid;
            if ($scope.Documentsdetails == null || $scope.Documentsdetails == '' || $scope.Documentsdetails == undefined) {
                $scope.ErrorMsg = 'Something went wrong in merging files';
            }
        });


    };
    var modalInstance;
    $scope.GetPendingDocuments();
    //ClickPopupYes, ClickPopupNo, ClickPopupOk
    $scope.ClickPopupYesSaveDocStatus = function () {
        debugger;
        if ($scope.ActionType == 1) {
            var dataDetails = $scope.dataReq.dataDetails, status = $scope.dataReq.status, DocType = $scope.dataReq.DocType;
            if (status != 10) {
                //if (dataDetails.isName) {
                if (true) {
                    var r = true;

                    if (r == true) {
                        $scope.DisplayMsg = 'Please wait...';
                        $scope.ModalType = 0;//1-confirm,2-Succes/Error Msg
                        var objUpdateDocStatusReq;
                        objUpdateDocStatusReq = {
                            "BookingID": $scope.LeadID,
                            "DocPointsID": dataDetails.DocPointsID,
                            "StatusID": status,
                            "UserID": $scope.UserID,
                            "AppNo": $scope.AppNo,
                            "DocType": dataDetails.docTypeId,
                            "DocumentID": dataDetails.docTypeId,
                            "lastModified": dataDetails.LastModified,
                            "DocCategoryID": dataDetails.docCategoryId,
                            "DocumentType": DocType,
                            "Type": "1"
                        };

                        DocumentRepositoryService.UpdateSingleDocStatus(objUpdateDocStatusReq).success(function (response) {
                            debugger;
                            //modalInstance.close();

                            if (!response.IsErrorInDocSubmit) {
                                // alert('Document updated successfully');
                                $scope.ModalType = 2;//1-confirm,2-Succes/Error Msg
                                $scope.DisplayMsg = 'Document updated successfully.';
                                $scope.Documentsdetails = response;
                            } else {
                                $scope.ModalType = 3;//1-confirm,2-Succes/Error Msg
                                $scope.DisplayMsg = response.ErrorMsgInDocSubmit;
                            }
                        });
                    }
                }
                else {
                    // alert('There is a error in naming convention, Please try again.');
                }
            }
        }
        else if ($scope.ActionType == 2) {            
            $scope.SubmitDocument($scope.dataReq.DocList, $scope.dataReq.DocType);
        }
      
    };
    $scope.ClickPopupNo = function (type) {
        modalInstance.close();
    };
    $scope.ClickPopupOk = function (type) {
        modalInstance.close();
    };
    $scope.SaveDocStatus = function (dataDetails, status, DocType) {
        debugger;
        $scope.dataReq = { "dataDetails": dataDetails, "status": status, "DocType": DocType };
        $scope.DisplayMsg = 'Do you want to save the status?';
        $scope.ModalType = 1;//1-confirm,2-Succes/Error Msg
        $scope.ActionType = 1;
        modalInstance = $uibModal.open({
            animation: true,
            size: 'sl',
            scope: $scope,
            templateUrl: 'views/alert.html'

        });
        //if (status != 10) {
        //    //if (dataDetails.isName) {
        //    if (true) {
        //        var r = confirm("Do you want to save the status.");

        //        if (r == true) {

        //            var objUpdateDocStatusReq;
        //            objUpdateDocStatusReq = {
        //                "BookingID": $scope.LeadID,
        //                "DocPointsID": dataDetails.DocPointsID,
        //                "StatusID": status,
        //                "UserID": $scope.UserID,
        //                "AppNo": $scope.AppNo,
        //                "DocType": dataDetails.docTypeId,
        //                "DocumentID": dataDetails.docTypeId,
        //                "lastModified": dataDetails.LastModified,
        //                "DocCategoryID": dataDetails.docCategoryId,
        //                "DocumentType": DocType,
        //                "Type": "1"
        //            };

        //            DocumentRepositoryService.UpdateSingleDocStatus(objUpdateDocStatusReq).success(function (response) {
        //                debugger;
        //                if (response != "" && response != null && response != undefined) {
        //                    alert('Document updated successfully');
        //                    $scope.Documentsdetails = response;
        //                } else {
        //                    alert('There is some issue in docs, please refresh and try again.');
        //                }
        //            });
        //        }
        //    }
        //    else {
        //        alert('There is a error in naming convention, Please try again.');
        //    }
        //}
       

    };

    $scope.SubmitDocumentToInsu = function (DocList, DocType) {
        $scope.dataReq = { "DocList": DocList, "DocType": DocType };
        $scope.DisplayMsg = 'Do you want to submit docs to Insurer?';
        $scope.ModalType = 1;//1-confirm,2-Succes/Error Msg
        $scope.ActionType = 2;
        modalInstance = $uibModal.open({
            animation: true,
            size: 'sl',
            scope: $scope,
            templateUrl: 'views/alert.html'

        });
    };
    $scope.SubmitDocument = function (DocList, DocType) {
        debugger;
       
        var objListUpdateDocStatusReq = [];
       
        if (DocType == 3 || DocType == 4 || DocType == 2) {
            //var r = confirm("Do you want to save the status.");
            $scope.DisplayMsg = 'Please wait...';
            $scope.ModalType = 0;//1-confirm,2-Succes/Error Msg
            if (true == true) {
                var objUpdateDocStatusReq;
                if (DocType == 3 || DocType == 4) {
                    objUpdateDocStatusReq = {
                        "BookingID": $scope.LeadID,
                        "DocPointsID": DocList.DocPointsID,
                        "StatusID": 7,
                        "UserID": $scope.UserID,
                        "AppNo": $scope.AppNo,
                        "DocType": DocList.docTypeId,
                        "DocumentID": DocList.docTypeId,
                        "FileUrl":DocList.MergedFile,
                        "lastModified": DocList.LastModified,
                        "DocCategoryID": DocList.docCategoryId,
                        "DocumentType": DocType,

                        "DocCatTypeName": DocList.docCategoryName,
                        "DocTypeName": DocList.docType,

                        "Type": "2"
                    };
                    objListUpdateDocStatusReq.push(objUpdateDocStatusReq);
                }
                else if (DocType == 2) {
                    for (var i = 0; i < DocList.length; i++) {
                        if (DocList[i].statusID != 7) {

                            objUpdateDocStatusReq = {
                                "BookingID": $scope.LeadID,
                                "DocPointsID": DocList[i].DocPointsID,
                                "StatusID": 7,
                                "UserID": $scope.UserID,
                                "AppNo": $scope.AppNo,
                                "DocType": DocList[i].docTypeId,
                                "DocumentID": DocList[i].docTypeId,
                                "FileUrl": DocList[i].MergedFile,
                                "lastModified": DocList[i].LastModified,
                                "DocCategoryID": DocList[i].docCategoryId,
                                "DocumentType": DocType,
                                "DocCatTypeName": DocList[i].docCategoryName,
                                "DocTypeName": DocList[i].docType,
                                "Type": "2"
                            };
                            objListUpdateDocStatusReq.push(objUpdateDocStatusReq);
                        }
                    }
                }
                var objUpdateDocReq = {
                    "leadId": $scope.Documentsdetails.leadId,
                    "BookingID": $scope.Documentsdetails.BookingID,
                    "applicationNumber": $scope.Documentsdetails.applicationNumber,
                    "sisid": $scope.Documentsdetails.sisid,
                    "suppliertag": $scope.Documentsdetails.suppliertag,
                    "proposalid": $scope.Documentsdetails.proposalid,
                    "customerid": $scope.Documentsdetails.customerid,
                    "quotationid": $scope.Documentsdetails.quotationid,
                    "supplierid": $scope.Documentsdetails.supplierid,
                    "UpdateDocStatusReqList": objListUpdateDocStatusReq
                };
                DocumentRepositoryService.SubmitDoc(objUpdateDocReq).success(function (response) {
                    debugger;
                    if (!response.IsErrorInDocSubmit) {
                        //alert('Document updated successfully');
                        $scope.ModalType = 2;//1-confirm,2-Succes/Error Msg
                        $scope.DisplayMsg = 'Document updated successfully.';
                        $scope.Documentsdetails = response;
                    } else {
                        //alert('There is some issue in docs, please refresh and try again.');
                        $scope.ModalType = 3;//1-confirm,2-Succes/Error Msg
                        $scope.DisplayMsg = response.ErrorMsgInDocSubmit;
                    }
                });
            }
        }
        //else {

        //}

    };
   
});