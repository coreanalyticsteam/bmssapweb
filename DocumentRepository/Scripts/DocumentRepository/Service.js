﻿//http://localhost:50366/api/EndorsementApi/GetEndorsementFieldList
DocumentRepository.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
});

DocumentRepository.service("DocumentRepositoryService", function ($http) {

    this.FileData = {};  

    this.GETRequiredDocumentsForBooking = function (LeadID) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "DocRepositoryService.svc/GETRequiredDocumentsForBooking/" + LeadID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    }

    this.InsertUpdateDocumentsForBooking = function (objDocumentDetailsRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "DocRepositoryService.svc/InsertUpdateDocumentsForBooking",
            data: JSON.stringify(objDocumentDetailsRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.UpdateDocumentsForBooking = function (objExistingDocumentDetailsRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "DocRepositoryService.svc/UpdateDocumentsForBooking",
            data: JSON.stringify(objExistingDocumentDetailsRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }


    this.SendEmail = function (CommunicationDetails) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.commService + "send",
            data: JSON.stringify(CommunicationDetails)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.GetDocumentAuditTrail = function (LeadID) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "DocRepositoryService.svc/GetDocumentAuditTrail/" + LeadID
        }).success(function (data) {
            console.log(data);
        }).error(function (data) {
            console.log(data);
        });
    }
   

    this.UpdateDocStatus = function (objDocumentDetailsRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "DocRepositoryService.svc/UpdateDocStatus",
            data: JSON.stringify(objDocumentDetailsRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.UpdateSingleDocStatus = function (objUpdateDocStatusReq) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "DocRepositoryService.svc/UpdateSingleDocStatus",
            data: JSON.stringify(objUpdateDocStatusReq)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.SubmitDoc = function (objUpdateDocReq) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "DocRepositoryService.svc/SubmitDoc",
            data: JSON.stringify(objUpdateDocReq)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.GetPendingDocuments = function (leadId, applicationNumber) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "DocRepositoryService.svc/GetPendingDocuments/" + leadId + "/" + applicationNumber
        }).success(function (data) {
            console.log(data);
        }).error(function (data) {
            console.log(data);
        });
    }

});

