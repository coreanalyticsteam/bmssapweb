﻿

angular.module("DocumentRepository", ["ngRoute", "ui.bootstrap"]).config(["$routeProvider",
  function ($routeProvider) {
      $routeProvider.
        when('/DocumentRepository/Details/:UserID/:LeadID', {
            templateUrl: 'views/DocumentRepository.html',
            controller: 'DocumentRepositoryCtrl'
        }).
        when('/DocumentRepository/Online/:UserID/:LeadID/:AppNo', {
            templateUrl: 'views/Docpointfortata.html',
            controller: 'DocpointfortataCtrl'
        })

      ;

  }]);

