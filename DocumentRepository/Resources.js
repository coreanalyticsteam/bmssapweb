﻿//This will be used to prvide the config
//QA-http://10.0.29.223:5556
//Live-http://10.72.179.218:8850
//Dev-http://localhost:55235/
//http://qamatrixapi.policybazaar.com/CommBox/
var config = {
    serviceURL: "http://localhost:55235/",
   // serviceURL: "http://10.0.10.29:6650/DocRepositoryService.svc/",
    commService: "http://localhost/Communication/Communication.svc/"
};
var TermLifeUploadURL = "http://devlife.policybazaar.com/uat/index.php/moredetails/index/apicalls?fid=";
var CommonData = {
    "DocStatus": [{ "StatusID": 0, "StatusName": "Pending" },
        { "StatusID": 1, "StatusName": "Received" },
        { "StatusID": 2, "StatusName": "Not Required" }
    ],
    "DocStatusWiting": [
        { "StatusID": 3, "StatusName": "Waiting For Approval" },
        { "StatusID": 1, "StatusName": "Received" },
        { "StatusID": 4, "StatusName": "Resubmit" }
    ]
    , "MailTo": [{ "MailToID": 0, "Name": "Select" }, { "MailToID": 1, "Name": "Customer" }, { "MailToID": 2, "Name": "Insurer" }]
    , "SupplierEmailIDs": [
            { "ProductID": 115, "SupplierID": 1, "EmailID": "anita.rao@aegonreligare.com" },
            { "ProductID": 115, "SupplierID": 11, "EmailID": "online@maxlifeinsurance.com" },
            { "ProductID": 115, "SupplierID": 7, "EmailID": "buyonline@iciciprulife.com" },
            { "ProductID": 115, "SupplierID": 2, "EmailID": "onlinepartnerhelpdesk@avivaindia.com" },
            { "ProductID": 115, "SupplierID": 6, "EmailID": "onlinedocuments@hdfclife.com" },
            { "ProductID": 115, "SupplierID": 23, "EmailID": "support@idbifederal.com" },
            { "ProductID": 115, "SupplierID": 14, "EmailID": "online.cell@sbilife.co.in" },
            { "ProductID": 115, "SupplierID": 13, "EmailID": "rlife.customerservice@relianceadda.com" },
            { "ProductID": 115, "SupplierID": 12, "EmailID": "online@pnbmetlife.co.in" },
            { "ProductID": 115, "SupplierID": 3, "EmailID": "websaleslife@bajajallianz.co.in" },
            { "ProductID": 115, "SupplierID": 18, "EmailID": "service@bharti-axalife.com" },
            { "ProductID": 115, "SupplierID": 17, "EmailID": "websales@edelweisstokio.in" },
            { "ProductID": 7, "SupplierID": 17, "EmailID": "anita.rao@aegonreligare.com" },
            { "ProductID": 7, "SupplierID": 10, "EmailID": "online@maxlifeinsurance.com" },
            { "ProductID": 7, "SupplierID": 5, "EmailID": "buyonline@iciciprulife.com" },
            { "ProductID": 7, "SupplierID": 8, "EmailID": "onlinepartnerhelpdesk@avivaindia.com" },
            { "ProductID": 7, "SupplierID": 16, "EmailID": "onlinedocuments@hdfclife.com" },
            { "ProductID": 7, "SupplierID": 14, "EmailID": "support@idbifederal.com" },
            { "ProductID": 7, "SupplierID": 32, "EmailID": "online.cell@sbilife.co.in" },
            { "ProductID": 7, "SupplierID": 40, "EmailID": "rlife.customerservice@relianceadda.com" },
            { "ProductID": 7, "SupplierID": 31, "EmailID": "online@pnbmetlife.co.in" },
            { "ProductID": 7, "SupplierID": 11, "EmailID": "websaleslife@bajajallianz.co.in" },
            { "ProductID": 7, "SupplierID": 4, "EmailID": "customerservice@birlasunlife.com" },
            { "ProductID": 7, "SupplierID": 20, "EmailID": "service@bharti-axalife.com" },
            { "ProductID": 7, "SupplierID": 24, "EmailID": "websales@edelweisstokio.in" }
        ]
    //,"Portability": [{ "id": -1, "label": "No Filter" }, { "id": 1, "label": "Yes" }, { "id": 0, "label": "No" }],
    //"MedicalOrInspection": [{ "id": -1, "label": "No Filter" }, { "id": 1, "label": "Yes" }, { "id": 0, "label": "No" }]
};
