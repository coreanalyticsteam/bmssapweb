﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PropertyLayers
{

    public class FieldVerification {    
        public int FieldID
        { get; set; }

        public int IsDocRec
        { get; set; }
    }

    [DataContract]
    public class EndorsementFieldUpdateReq
    {
        [DataMember]
        public long BookingID
        { get; set; }

        [DataMember]
        public long DetailsID
        { get; set; }

        [DataMember]
        public int FieldID
        { get; set; }

        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public int TYPE
        { get; set; }

    }

    [DataContract]
    public class EndorsementAddFieldResponse
    {
        [DataMember]
        public long DetailsID
        { get; set; }

        [DataMember]
        public bool IsDocReceived
        { get; set; }

        
        [DataMember]
        public List<EndorsementDocReq> EndorsementDocReqList { get; set; }

        [DataMember]
        public List<EndorsementDocReq> EndorsementExistngDocList { get; set; }

    }
    [DataContract]
    public class EndorsementRollBackRequest
    {
        [DataMember]
        public long BookingID
        { get; set; }

        [DataMember]
        public long DetailsID
        { get; set; }

        [DataMember]
        public int StatusMasterID
        { get; set; }

        [DataMember]
        public int UserID
        { get; set; }
    }
    [DataContract]
    public class EndorsementStatusdetailsResponse
    {
        [DataMember]
        public string StatusMasterName
        { get; set; }

        [DataMember]
        public string CreatedBy
        { get; set; }

        [DataMember]
        public int IsRollBack
        { get; set; }

        [DataMember]
        public int StatusMasterID
        { get; set; }

        [DataMember]
        public int IsActive
        { get; set; }

        [DataMember]
        public string CreatedOn
        { get; set; }
    }
    [DataContract]
    public class ReqDocUploadRequest
    {
        [DataMember]
        public long DetailsID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public int FieldID
        { get; set; }

        [DataMember]
        public int UserID
        { get; set; }
        
        [DataMember]
        public List<ReqDocRequest> ReqDocRequestList { get; set; }

    }
    [DataContract]
    public class ReqDocRequest
    {            

        [DataMember]
        public int DocumentID
        { get; set; }

        [DataMember]
        public int DocCategoryID
        { get; set; }

        [DataMember]
        public string HostName
        { get; set; }

        [DataMember]
        public string FileName
        { get; set; }

        [DataMember]
        public string FileStream
        { get; set; }

        [DataMember]
        public string FileUrl
        { get; set; }

        [DataMember]
        public long ParentCustDocumentID
        { get; set; }
        
      

    }
    [DataContract]
    public class EndorsementDocReq
    {        

        [DataMember]
        public long RuleID
        { get; set; }

        [DataMember]
        public int DocumentID
        { get; set; }

        [DataMember]
        public int DocCategoryID
        { get; set; }

        [DataMember]
        public string Document
        { get; set; }

        [DataMember]
        public string DocOperator
        { get; set; }

        [DataMember]
        public string RuleOperator
        { get; set; }

        [DataMember]
        public string HostName
        { get; set; }

        [DataMember]
        public string DocumentUrl
        { get; set; }

        [DataMember]
        public long CustDocumentID
        { get; set; }
    }

    [DataContract]
    public class EndorsementListDetails
    {
        [DataMember]
        public List<BookingEndorsementList> EndorsementList { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public string CustName
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }
       
        [DataMember(EmitDefaultValue = false)]
        public int ProductID
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProductName
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SupplierID
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PlanId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SupplierName
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PlanName
        { get; set; }

        [DataMember]
        public int IsEligible
        { get; set; }
    }
    [DataContract]
    public class EndorsementDetail
    {
        [DataMember]
        public List<EndorsementFieldMaster> EndorsementFieldMasterList { get; set; }

        [DataMember]
        public List<EndorsementFieldMaster> EndorsementFieldListDetails { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public int IsDocReceived
        { get; set; }

        [DataMember]
        public string CustName
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public long UserID
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ProductID
        { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string ProductName
        { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public int SupplierID
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int PlanId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SupplierName
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PlanName
        { get; set; }



        [DataMember]
        public EndorsementStatusDetails EndorsementStatus { get; set; }
       
        [DataMember]
        public EndorsementAddtionalInfo EndorsementAddtionalInformation { get; set; }
        [DataMember]
        public List<EndorsementStatusDetails> RejectionReasonList { get; set; }

        [DataMember]
        public List<EndorsementDocumentCopy> EndorsementDocumentCopyList { get; set; }
    }
    
    [DataContract]
    public class EndorsementFieldMaster
    {
        [DataMember(EmitDefaultValue = false)]
        public int SNo
        { get; set; }       
        [DataMember(EmitDefaultValue = false)]
        public int FieldID
        { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string FieldName
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string OldValue
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NewValue
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DocReq
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FieldType
        { get; set; }

        [DataMember]
        public int IsDocReceived
        { get; set; }

        [DataMember]
        public int MasterType
        { get; set; }

        [DataMember]
        public List<FieldMasterData> FieldMasterDataList
        { get; set; }
    }

    [DataContract]
    public class EndorsementFieldAddRequest
    {
        [DataMember]
        public int UserID
        { get; set; }
        
        [DataMember]
        public long DetailsID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public int FieldID
        { get; set; }        

        [DataMember]
        public string OldValue
        { get; set; }

        [DataMember]
        public string NewValue
        { get; set; }

    }
    [DataContract]
    public class BookingEndorsementList
    {
        [DataMember]
        public long DetailsID
        { get; set; }

        [DataMember]
        public string CreatedDate
        { get; set; }

        [DataMember]
        public string StatusMasterName
        { get; set; }

        [DataMember]
        public string ReasonName
        { get; set; }

        [DataMember]
        public string NextStatus
        { get; set; }
    }

    [DataContract]
    public class FieldMasterData
    {
        [DataMember]
        public int Value
        { get; set; }

        [DataMember]
        public string Text
        { get; set; }
    }
    [DataContract]
    public class EndorsementDocumentCopy
    {
        [DataMember]
        public string Document
        { get; set; }

        [DataMember]
        public string DocumentUrl
        { get; set; }
    }
    [DataContract]
    public class EndorsementStatusDetails
    {        

        [DataMember]
        public long StatusMasterID
        { get; set; }
                
        [DataMember]
        public string StatusMasterName
        { get; set; }

        [DataMember]
        public long ReasonId
        { get; set; }

        [DataMember]
        public string ReasonName
        { get; set; }
      
    }

    

    [DataContract]
    public class EndorsementAddtionalInfo
    {
        [DataMember]
        public long DetailId
        { get; set; }

        [DataMember]
        public int IsPremiumChange
        { get; set; }

        [DataMember]
        public Decimal PremiumChange
        { get; set; }

        
        [DataMember]        
        public int PremiumCollectedRefunded
        { get; set; }

        [DataMember]
        public int SCVerified
        { get; set; }

        [DataMember]
        public int SCReceived
        { get; set; }

    }

    [DataContract]
    public class DocCategory
    {
        [DataMember(EmitDefaultValue = false)]
        public int CategoryId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CategoryName
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Documents> DocumentsList
        { get; set; }
    }
    [DataContract]
    public class Documents
    {
        [DataMember(EmitDefaultValue = false)]
        public int DocumentId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long CustDocumentId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DocumentName
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string DocumentUrl
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DocVerificationStatus SelectedStatus
        { get; set; }

    }
    [DataContract]
    public class DocVerificationStatus
    {
        [DataMember(EmitDefaultValue = false)]
        public int VerificationStatusId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VerificationStatusName
        { get; set; }
    }

    [DataContract]
    public class RequestEndorsementDocList
    {
        [DataMember(EmitDefaultValue = false)]
        public long LeadID
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int UserID
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long DetailId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<RequestEndorsementDocStatus> RequestEndorsementDocStatusList
        { get; set; }

    }

    [DataContract]
    public class RequestEndorsementDocStatus
    {
        [DataMember(EmitDefaultValue = false)]
        public long CustDocumentId
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int VerificationStatusId
        { get; set; }
    }


    [DataContract]
    public class EndorsementDocList
    {       

        [DataMember(EmitDefaultValue = false)]
        public List<DocCategory> DocCategoryList
        { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<DocVerificationStatus> DocVerificationStatusList
        { get; set; }

    }
    [DataContract]
    public class File_Attachment
    {
        
        [DataMember(EmitDefaultValue = false)]
        public string FileName
        {
            get;
            set;
        }
        
        [DataMember(EmitDefaultValue = false)]
        public Int32 FileSize
        {
            get;
            set;
        }
        
        [DataMember(EmitDefaultValue = false)]
        public string AttachemntContent
        {
            get;
            set;
        }
    }
}
