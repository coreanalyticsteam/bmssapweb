﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace PropertyLayers
{
    [DataContract]
    public class TokenEntity
    {
        [DataMember]
        public int UserId
        { get; set; }

        [DataMember]
        public string LoginID
        { get; set; }

        [DataMember]
        public DateTime IssuedOn
        { get; set; }

        [DataMember]
        public DateTime ExpiresOn
        { get; set; }

        [DataMember]
        public string AuthToken
        { get; set; }

    }
    // class TokenEntity
    //{    UserId = userId,
    //                               IssuedOn = issuedOn,
    //                               ExpiresOn = expiredOn,
    //                               AuthToken = token
    //}
}
