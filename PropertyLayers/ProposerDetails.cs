﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PropertyLayers
{
    [DataContract]
    public class ProposerDetails
    {
        [DataMember]
        public int Adults { get; set; }
        [DataMember]
        public int Age { get; set; }
       
        [DataMember]
        public Annualincome AnnualIncome { get; set; }
       
        [DataMember]
        public int Childern { get; set; }
       
        
        [DataMember]
        public Coveredmember[] CoveredMembers { get; set; }
       
        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public int CustomerMemeberID { get; set; }
        [DataMember]
        public string DateOfBirth { get; set; }
        
       
        [DataMember]
        public int EnquiryID { get; set; }
        [DataMember]
        public int Gender { get; set; }
       
        [DataMember]
        public int LeadID { get; set; }
        [DataMember]
        public string LeadSource { get; set; }
      
        [DataMember]
        public Maritalstatus MaritalStatus { get; set; }
        [DataMember]
        public int MatrixLeadID { get; set; }
        [DataMember]
        public string Name { get; set; }
      
       
       
     
        [DataMember]
        public string PanNo { get; set; }
        
        [DataMember]
        public int ProductID { get; set; }
        [DataMember]
        public int ProposerId { get; set; }
        [DataMember]
        public string SumInsured { get; set; }
        [DataMember]
        public int Title { get; set; }
       
       

    }

    [DataContract]
    public class Annualincome
    {
        [DataMember]
        public int AnnualIncomeId { get; set; }
        [DataMember]
        public string AnnualIncomeText { get; set; }
    }
    [DataContract]
    public class Contactinformation
    {
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public int AltMobileNo { get; set; }
        [DataMember]
        public string AreaCode { get; set; }
        [DataMember]
        public Citystate CityState { get; set; }
        [DataMember]
        public string CommunicationAddress { get; set; }
        [DataMember]
        public int ContactInfoId { get; set; }
        [DataMember]
        public string ContactNo { get; set; }
        [DataMember]
        public int CountryId { get; set; }
        [DataMember]
        public string DateOfBirth { get; set; }
        [DataMember]
        public int HealthProposerId { get; set; }
        [DataMember]
        public long MobileNo { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PermanentAddress { get; set; }
        [DataMember]
        public string PrimaryEmail { get; set; }
        [DataMember]
        public string SecondryEmail { get; set; }
        [DataMember]
        public string StarCityID { get; set; }

    }
    [DataContract]
    public class Citystate
    {
        [DataMember]
        public int CityId { get; set; }
        [DataMember]
        public string CityName { get; set; }
        [DataMember]
        public int Pincode { get; set; }
        [DataMember]
        public int StateId { get; set; }
        [DataMember]
        public string StateName { get; set; }
    }
    [DataContract]
    public class Maritalstatus
    {
        [DataMember]
        public int MaritalStatusId { get; set; }
        [DataMember]
        public string MaritalStatusText { get; set; }
    }
    [DataContract]
    public class Coveredmember
    {
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string DateOfBirth { get; set; }
       
     
        [DataMember]
        public int Gender { get; set; }
        [DataMember]
        public int HealthProposerId { get; set; }
        [DataMember]
        public int Height { get; set; }
       
       
     
       
        [DataMember]
        public int MemberId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int NomineeId { get; set; }
        [DataMember]
        public string NomineeName { get; set; }
        [DataMember]
        public string NomineeRelationship { get; set; }
       
     
      
      
        [DataMember]
        public int Title { get; set; }

        [DataMember]
        public Relation Relation { get; set; }


        [DataMember]
        public int Weight { get; set; }

        [DataMember]
        public bool IsExcluded { get; set; }
        [DataMember]
        public string ExclusionReason { get; set; }

        [DataMember]
        public PEDDetails[] PEDDetails { get; set; }

    }
    [DataContract]
    public class Relation
    {
        [DataMember]
        public int RelationShipId { get; set; }
        [DataMember]
        public string RelationShipName { get; set; }
    }

    [DataContract]
    public class PEDDetails
    {
        [DataMember]
        public int PEDId { get; set; }
        [DataMember]
        public string PEDName { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }

}
