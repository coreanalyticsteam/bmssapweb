﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PropertyLayers
{
    class DocPoints
    {
    }

    //public class FTPFilePush
    //{

    //    public string ProposalNumber
    //    { get; set; }


    //    public List<FTPFileDetails> FTPFileDetailList
    //    { get; set; }

    //}

    //public class FTPFileDetails
    //{

    //    public string FTPFile
    //    { get; set; }


    //    public string FileURL
    //    { get; set; }

    //}
    [DataContract]
    public class FTPFilePush
    {
        [DataMember]
        public string ProposalNumber
        { get; set; }

        [DataMember]
        public List<FTPFileDetails> FTPFileDetailList
        { get; set; }

    }
    [DataContract]
    public class FTPFileDetails
    {
        [DataMember]
        public string FTPFile
        { get; set; }

        [DataMember]
        public string FileURL
        { get; set; }

    }
    [DataContract]
    public class DocumentDetailIDs
    {
        [DataMember]
        public int DocumentType
        { get; set; }

        [DataMember]
        public int DocCategoryID
        { get; set; }

        [DataMember]
        public long DocPointsID
        { get; set; }


        [DataMember]
        public int DocumentID
        { get; set; }

        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public string DocName
        { get; set; }

        [DataMember]
        public string File
        { get; set; }

        [DataMember]
        public string FileString
        { get; set; }

        [DataMember]
        public string FileUrl
        { get; set; }

        [DataMember]
        public string DocEmailRemarks
        { get; set; }
        
    }

    [DataContract]
    public class ExistingDocumentDetailsRequest
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public long BookingID
        { get; set; }

        [DataMember]
        public long ParentID
        { get; set; }

    }
    [DataContract]
    public class DocumentDetailsRequest
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public long BookingID
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public List<DocumentDetailIDs> DocumentDetailIDsList
        { get; set; }
    }

    [DataContract]
    public class DocumentAuditTrailResponse
    {
        [DataMember]
        public string  Documentcategory
        { get; set; }

        [DataMember]
        public int DocCategoryId
        { get; set; }

        [DataMember]
        public int DocumentId
        { get; set; }

        [DataMember]
        public string Document
        { get; set; }

        [DataMember]
        public string CreatedOn
        { get; set; }

        [DataMember]
        public string CreatedBy
        { get; set; }

        [DataMember]
        public string DocStatus
        { get; set; }

        [DataMember]
        public string DocEmailRemarks
        { get; set; }

    }
    [DataContract]
    public class DocumentDetailsResponse
    {
        [DataMember]
        public long BookingID
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int  SupplierID
        { get; set; }

        [DataMember]
        public string CustomerName
        { get; set; }

        [DataMember]
        public string CustomerEmailID
        { get; set; }

        [DataMember]
        public string CustomerMobile
        { get; set; }

        [DataMember]
        public string ProductName
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public string SupplierName
        { get; set; }


        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public int SubStatusID
        { get; set; }


        [DataMember]
        public int BlockAutoCommunication
        { get; set; }

        [DataMember]
        public int IsMedInspReq
        { get; set; }
        

        [DataMember]
        public string PlanName
        { get; set; }

        [DataMember]
        public List<CustDocumentDetails> RecDocumentsForBooking
        { get; set; }

        [DataMember]
        public List<CustDocumentDetails> WaitDocumentsForBooking
        { get; set; }

        [DataMember]
        public List<CustDocumentDetails> RecDocumentsForCustomer
        { get; set; }

        [DataMember]
        public List<DocCategoryDetails> ReqDocCategoryDetails
        { get; set; }

        [DataMember]
        public int Return
        { get; set; }
    }

    [DataContract]
    public class CustDocumentDetails
    {
        [DataMember]
        public int DocumentID
        { get; set; }

        [DataMember]
        public string DocumentName
        { get; set; }

        [DataMember]
        public string DocUrl
        { get; set; }

        [DataMember]
        public string DocName
        { get; set; }

        [DataMember]
        public int DocCategoryID
        { get; set; }

        [DataMember]
        public string DocCategoryName
        { get; set; }

        [DataMember]
        public long DocPointsID
        { get; set; }

        [DataMember]
        public long ParentID
        { get; set; }

        [DataMember]
        public string DocEmailRemarks
        { get; set; }

        [DataMember]
        public StatusDetails SelectedStatus
        { get; set; }
        
    }

    [DataContract]
    public class DocCategoryDetails
    {
        [DataMember]
        public int DocCategoryID
        { get; set; }

        [DataMember]
        public string DocCategoryName
        { get; set; }

        [DataMember]
        public bool CustDocWait
        { get; set; }

        [DataMember]
        public CustDocumentDetails SelectedDoc
        { get; set; }

        [DataMember]
        public StatusDetails SelectedStatus
        { get; set; }

        [DataMember]
        public int DocOwner
        { get; set; }

        [DataMember]
        public int DocPointsID
        { get; set; }

        [DataMember]
        public List<CustDocumentDetails> DocumentDetailsList
        { get; set; }

        [DataMember]
        public CurStatusDetails CurStatusDetails
        { get; set; }

        [DataMember]
        public Boolean CheckEmail
        { get; set; }

        
    }

    [DataContract]
    public class StatusDetails
    {
        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public string StatusName
        { get; set; }

    }

    [DataContract]
    public class CurStatusDetails
    {
        [DataMember]
        public int DocumentID
        { get; set; }

        [DataMember]
        public string DocumentName
        { get; set; }

        [DataMember]
        public string DocURL
        { get; set; }

        [DataMember]
        public int StatusID
        { get; set; }   
      
    }

}
