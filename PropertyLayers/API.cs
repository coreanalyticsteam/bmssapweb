﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropertyLayers
{
    [DataContract]
    public class ICICIPRUMob
    {
        [DataMember]
        public string InputKey { get; set; }
        [DataMember]
        public string webservicename { get; set; }
    }

    [DataContract]
    public class ICICIPRUMobRes
    {
        [DataMember]
        public Int64 BookingId { get; set; }
        [DataMember]
        public int ProductID { get; set; }
        [DataMember]
        public int SupplierId { get; set; }
        [DataMember]
        public string ApplicationNo { get; set; }
        [DataMember]
        public string POLICYKEY { get; set; }
        [DataMember]
        public string APPLICATIONKEY { get; set; }
        [DataMember]
        public string FILLER_OWNERNAME { get; set; }
        [DataMember]
        public string SUBMISSIONDATE { get; set; }
        [DataMember]
        public string FILLER_PRODUCTCODE { get; set; }
        [DataMember]
        public string PLAN_NAME { get; set; }
        [DataMember]
        public string FILLER_SUMINSURED { get; set; }
        [DataMember]
        public string POLICYSTATUS { get; set; }
        [DataMember]
        public string EFFDATE { get; set; }
        [DataMember]
        public string FOLLOWUPCODE { get; set; }
        [DataMember]
        public string RECEIVEDDATE { get; set; }
        [DataMember]
        public string REQUESTDATE { get; set; }
        [DataMember]
        public string PREMIUM_TERM { get; set; }
        [DataMember]
        public string RISK_TERM { get; set; }
        [DataMember]
        public string MED_FOLLOWUPCODE { get; set; }
        [DataMember]
        public string MED_REQUESTDATE { get; set; }
        [DataMember]
        public string BRANCHRECDDATE { get; set; }
        [DataMember]
        public string COPSRECDDATE { get; set; }
        [DataMember]
        public string EXAMDATE { get; set; }
        [DataMember]
        public string FLAG { get; set; }
        [DataMember]
        public string DUMMY_2 { get; set; }
        [DataMember]
        public bool __hashCodeCalc { get; set; }
    }
}
