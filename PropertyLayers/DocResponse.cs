﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace PropertyLayers
{
    [DataContract]
    public class DocReqResp
    {
        [DataMember]
        public string BookingID { get; set; }
        [DataMember]
        public string DocumentType { get; set; }
        [DataMember]
        public string DocumentTypeName { get; set; }

        [DataMember]
        public string docCategoryName { get; set; }
        [DataMember]
        public string docType { get; set; }


        [DataMember]
        public string DocCategoryID { get; set; }
        [DataMember]
        public string Documentcategory { get; set; }
        [DataMember]
        public string DocumentID { get; set; }
        [DataMember]
        public string Document { get; set; }
        [DataMember]
        public string DocPointsID { get; set; }
        [DataMember]
        public string StatusID { get; set; }
        [DataMember]
        public string MergedName { get; set; }
        [DataMember]
        public string MergedURL { get; set; }
        [DataMember]
        public string DocName { get; set; }
        [DataMember]
        public string DocUrl { get; set; }
        [DataMember]
        public string DocExt { get; set; }
        //[DataMember]
        //public string StatusID { get; set; }
        [DataMember]
        public string StatusName { get; set; }
        [DataMember]
        public string CreatedOn { get; set; }
        [DataMember]
        public byte IsMerged { get; set; }
        [DataMember]
        public string MergingMsg { get; set; }
    }

    [DataContract]
    public class MergeFile
    {
        [DataMember]
        public List<string> FileList { get; set; }
        [DataMember]
        public string FileName { get; set; }

    }
    

    [DataContract]
    public class DocResponse
    {
        [DataMember]
        public string leadId { get; set; }
        [DataMember]
        public string applicationNumber { get; set; }
        [DataMember]
        public string fid { get; set; }
        [DataMember]
        public string sisid { get; set; }
        [DataMember]
        public string AgentCode { get; set; }

        [DataMember]
        public string suppliertag { get; set; }
        [DataMember]
        public string proposalid { get; set; }
        [DataMember]
        public string customerid { get; set; }
        [DataMember]
        public string quotationid { get; set; }
        [DataMember]
        public string supplierid { get; set; }   

        [DataMember]
        public KYCDocuments KYC_Documents { get; set; }
        [DataMember]
        public NonKYCDocuments Non_KYC_Documents { get; set; }
        [DataMember]
        public OtherDocuments Other_Documents { get; set; }

        [DataMember]
        public bool IsErrorInDocSubmit { get; set; }
        [DataMember]
        public string ErrorMsgInDocSubmit { get; set; } 
    }

    [DataContract]
    public class DocResponseForService
    {
        [DataMember]
        public string leadId { get; set; }
        [DataMember]
        public string applicationNumber { get; set; }    
        [DataMember]
        public string fid { get; set; } 
        [DataMember]
        public string sisid { get; set; }          
        [DataMember]
        public KYCDocuments KYC_Documents { get; set; }
        [DataMember]
        public NonKYCDocuments Non_KYC_Documents { get; set; }
        [DataMember]
        public OtherDocuments Other_Documents { get; set; }
    }

     [DataContract]
    public class UpdateDocReq
    {
        [DataMember]
         public string leadId { get; set; }
        
        [DataMember]
        public string BookingID { get; set; }
        [DataMember]
        public string applicationNumber { get; set; }
        [DataMember]
        public string sisid { get; set; }
        [DataMember]
        public string suppliertag { get; set; }
        [DataMember]
        public string proposalid { get; set; }
        [DataMember]
        public string customerid { get; set; }
        [DataMember]
        public string quotationid { get; set; }
        [DataMember]
        public string supplierid { get; set; }     
        [DataMember]
        public List<UpdateDocStatusReq> UpdateDocStatusReqList { get; set; }    
     }
    [DataContract]
    public class UpdateDocStatusReq
    {
        [DataMember]
        public string BookingID { get; set; }
        [DataMember]
        public string DocPointsID { get; set; }
        [DataMember]
        public string StatusID { get; set; }
        [DataMember]
        public string UserID { get; set; }
        [DataMember]
        public string AppNo { get; set; }
        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string FileUrl { get; set; }

        [DataMember]
        public int DocumentType { get; set; }
        [DataMember]
        public int DocCategoryID { get; set; }
        [DataMember]
        public int DocumentID { get; set; }
        [DataMember]
        public string lastModified { get; set; }

        //DocCatType,string DocType
        [DataMember]
        public string DocCatTypeName { get; set; }
        [DataMember]
        public string DocTypeName { get; set; }
    }

    [DataContract]
    public class DocumentForRes
    {
        [DataMember]
        public string docCategoryId { get; set; }
        [DataMember]
        public string docCategoryName { get; set; }
        [DataMember]
        public string DocStatus { get; set; }
        [DataMember]
        public string docType { get; set; }
        [DataMember]
        public string DocPointsID { get; set; }
        [DataMember]
        public string docTypeId { get; set; }
        [DataMember]
        public string statusID { get; set; }
        [DataMember]
        public string statusName { get; set; }
        [DataMember]
        public string CreatedON { get; set; }
        [DataMember]
        public string LastModified { get; set; }
        [DataMember]
        public string MergedFile { get; set; }
        [DataMember]
        public string MergedFileName { get; set; }
        [DataMember]
        public List<filesInfo> files { get; set; }
        [DataMember]
        public bool isName { get; set; }
        [DataMember]
        public string IsMerged { get; set; }
        [DataMember]
        public string MergedMsg { get; set; }
    }

    [DataContract]
    public class KYCDocuments
    {
        [DataMember]
        public int StatusID { get; set; }

        [DataMember]
        public List<Availabledoc> availableDocs { get; set; }

        [DataMember(EmitDefaultValue=false)]
        public List<DocumentForRes> DocumentForResList { get; set; }
    }
    [DataContract]
    public class NonKYCDocuments
    {
        [DataMember]
        public int StatusID { get; set; }
        [DataMember]
        public List<Availabledoc> availableDocs { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<DocumentForRes> DocumentForResList { get; set; }
    }
    [DataContract]
    public class OtherDocuments
    {
        [DataMember]
        public int StatusID { get; set; }
        [DataMember]
        public List<Availabledoc> availableDocs { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<DocumentForRes> DocumentForResList { get; set; }
    }
    [DataContract]
    public class Availabledoc
    {
        [DataMember]
        public string docCategoryId { get; set; }
        [DataMember]
        public string docCategoryName { get; set; }
        [DataMember]
        public string DocStatus { get; set; }
        [DataMember]
        public Docs docs { get; set; }
        [DataMember]
        public string lastModified { get; set; }
    }
    [DataContract]
    public class Docs
    {
        [DataMember]
        public string docType { get; set; }
        [DataMember]
        public string DocPointsID { get; set; }
        [DataMember]
        public string docTypeId { get; set; }
        [DataMember]
        public string statusID { get; set; }
        [DataMember]
        public string statusName { get; set; }
        [DataMember]
        public string CreatedON { get; set; }
        [DataMember]
        public string MergedFile { get; set; }
        [DataMember]
        public string MergedFileName { get; set; }
        [DataMember]
        public List<filesInfo> files { get; set; }
    }
    [DataContract]
    public class filesInfo
    {
        [DataMember]
        public string FileDetails { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string extension { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
}
