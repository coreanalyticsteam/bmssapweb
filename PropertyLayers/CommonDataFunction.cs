﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PropertyLayers
{
      
    static class CommonDataFunction
    {
        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 5, 30, 0);

        public static DateTime ToDateTime(this long longDate)
        {
            return UnixEpoch.AddTicks(longDate * TimeSpan.TicksPerMillisecond);
        }

    }
}
