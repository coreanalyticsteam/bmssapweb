﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PropertyLayers
{
    class CallTransferData
    {
    }
    //[DataContract]
    //public class ProductDetails
    //{  
    //    [DataMember]
    //    public int ProductID
    //    { get; set; }

    //    [DataMember]
    //    public string ProductName
    //    { get; set; }
    //}
    [DataContract]
    public class GroupDetails
    {
        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public int GroupID
        { get; set; }

        [DataMember]
        public string GroupName
        { get; set; }
    }

    [DataContract]
    public class DialerSearchRequest
    {
        [DataMember]
        public long LeadID
        { get; set; }    

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public int AgentType
        { get; set; }

        [DataMember]
        public int GroupID
        { get; set; }

        [DataMember]
        public string AgentNameEmpID
        { get; set; }

        [DataMember]
        public int Type
        { get; set; }
    }
    [DataContract]
    public class LeadAgentList
    {
        [DataMember]
        public long LeadID
        { get; set; }       

        [DataMember]
        public int AgentID
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public string ProductName
        { get; set; }

        [DataMember]
        public string AgentEmpID
        { get; set; }

        [DataMember]
        public string AgentName
        { get; set; }

        [DataMember]
        public string AgentGroupName
        { get; set; }

        [DataMember]
        public string LeadProcessType
        { get; set; }

        [DataMember]
        public string LeadStatus
        { get; set; }

        [DataMember]
        public int LeadStatusID
        { get; set; }

        [DataMember]
        public int LeadSubStatusID
        { get; set; }

        [DataMember]
        [System.ComponentModel.DefaultValue("INACTIVE")]
        public string DialerStatus
        { get; set; }

        [DataMember]
        public string StateTime
        { get; set; }

       
    }

}
