﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropertyLayers
{
    [DataContract]
    public class Tickets
    {
         [DataMember]
        public long LeadID { get; set; }
         [DataMember]
         public string IsAutoResolved { get; set; }
         [DataMember]
        public int ProductID { get; set; }
         [DataMember]
        public string ProductName { get; set; }
         [DataMember]
        public int InsurerID { get; set; }
         [DataMember]
        public string InsurerName { get; set; }
         [DataMember]
        public long CustomerID { get; set; }
         [DataMember]
        public string Name { get; set; }
         [DataMember]
        public long MobileNo { get; set; }
         [DataMember]
        public string EmailID { get; set; }
         [DataMember]
        public string Subject { get; set; }
         [DataMember]
        public string Comments { get; set; }
         [DataMember]
        public string PolicyNumber { get; set; }
         [DataMember]
        public string ClaimNumber { get; set; }
         [DataMember]
        public string Source { get; set; }
         [DataMember]
        public int Issue { get; set; }
         [DataMember]
        public List<Attachments> Attachments { get; set; }
         [DataMember]
        public string FollowUpDate { get; set; }
         [DataMember]
        public int AutoClosure { get; set; }
         [DataMember]
        public string AddressDetails { get; set; }
         [DataMember]
        public long Pincode { get; set; }
    }
    [DataContract]
    public class Attachments
    {
         [DataMember]
        public string FileName { get; set; }
         [DataMember]
        public string AttachmentURL { get; set; }
    }
    [DataContract]
    public class TicketResponse
    {
         [DataMember]
        public string Data { get; set; }
         [DataMember]
        public string Error { get; set; }
         [DataMember]
        public int ErrorCode { get; set; }
    }
    [DataContract]
    public class IVRResponse
    {
         [DataMember]
        public long LeadId { get; set; }
         [DataMember]
        public string Response { get; set; }
         [DataMember]
        public string AppIDSource { get; set; }
    }
    [DataContract]
    public class BMSReport
    {
        [DataMember]
        public string FromDate { get; set; }
        [DataMember]
        public string ToDate { get; set; }
        [DataMember]
        public int ProductID { get; set; }
    }

    [DataContract]
    public class CommunicationModel
    {

        //[DataMember]
        //public ObjectId id
        //{
        //    get;
        //    set;
        //}
        [DataMember]
        public string ObjectID
        {
            get;
            set;
        }
        
        [DataMember]
        public long LeadID
        {
            get;
            set;
        }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int ProductID
        {
            get;
            set;
        }


        [DataMember]
        public int AgentID
        {
            get;
            set;
        }
        

        [DataMember]
        public bool Seen
        {
            get;
            set;
        }

        [DataMember]
        public long SeenOn
        {
            get;
            set;
        }

        [DataMember]
        public long CreatedOn
        {
            get;
            set;
        }
    }

}
