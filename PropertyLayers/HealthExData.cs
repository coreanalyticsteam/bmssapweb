﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace PropertyLayers
{
    class HealthExData
    {
    }

    [DataContract]
    public class ResultClass
    {
        [DataMember]
        public bool IsAuth { get; set; }

        [DataMember]
        public string Success { get; set; }

        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public bool IsSuccess { get; set; }

    }

    [DataContract]
    public class UserDetail
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string MobileNo { get; set; }

        [DataMember]
        public string EmailID { get; set; }

        [DataMember]
        public UserType UserType { get; set; }

        [DataMember]
        public UserRole UserRole { get; set; }

        [DataMember]
        public string LoginID { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public bool IsAcitve { get; set; }

        [DataMember]
        public int MappingId { get; set; }

        [DataMember]
        public int CreatedBy { get; set; }

        [DataMember]
        public Products Product { get; set; }

        [DataMember]
        public Insurer Insurer { get; set; }

        [DataMember]
        public int Default { get; set; } 

        [DataMember]
        public List<ApplicationRights> ApplicationRights { get; set; }
        [DataMember]
        public List<InsurerUserMapping> InsurerUserMapping { get; set; }
        [DataMember]
        public List<MenuRights> MenuRights { get; set; }

    }
    [DataContract]
    public class InsurerUserMapping
    {
        [DataMember]
        public int SNO { get; set; }

        [DataMember]
        public int MappingId { get; set; }
        [DataMember]
        public int InsurerID { get; set; }
        [DataMember]
        public int ProductId { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public string SupplierName { get; set; }

        [DataMember]
        public string ProductName { get; set; }
    }
    [DataContract]
    public class UserType {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
    [DataContract]
    public class UserRole
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
    [DataContract]
    public class ApplicationRights
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ApplicationId { get; set; }
        [DataMember]
        public string AppName { get; set; }

        [DataMember]
        public bool IsChecked { get; set; }
    }
    [DataContract]
    public class MenuRights
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int MenuID { get; set; }

        [DataMember]
        public int Default { get; set; }

        [DataMember]
        public string MenuName { get; set; }

        [DataMember]
        public bool IsChecked { get; set; }
    }
    [DataContract]
    public class Products
    {
        [DataMember]
        public int ProductID { get; set; }

        [DataMember]
        public string Product { get; set; }
    }
    [DataContract]
    public class Insurer
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string SupplierName { get; set; }
    }
    [DataContract]
    public class LeadDetails
    {
        [DataMember]
        public long ExID
        { get; set; }

        [DataMember]
        public long SelectionID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long OldLeadID
        { get; set; }

        [DataMember]
        public int ExLeadType
        { get; set; }

        [DataMember]
        public long NewLeadID
        { get; set; }           

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string OfferCreatedON
        { get; set; }

        [DataMember]
        public string HealthExMarkDate
        { get; set; }

        [DataMember]
        public string RejectedOn
        { get; set; }

        [DataMember]
        public string InsurerStatus
        { get; set; }

        [DataMember]
        public int InsurerActionDetails
        { get; set; }

        [DataMember]
        public int InsurerStatusID
        { get; set; }

        [DataMember]
        public string SupplierName
        { get; set; }

        [DataMember]
        public int SupplierId
        { get; set; }

        [DataMember]
        public string ProposalNo
        { get; set; }

        [DataMember]
        public string OldProposalNo
        { get; set; }

        [DataMember]
        public string RejectionReason
        { get; set; }

        [DataMember]
        public string PEDInfo
        { get; set; }

        [DataMember]
        public string Remarks
        { get; set; }

        [DataMember]
        public bool IsMark
        { get; set; }

        [DataMember]
        public int ExStatusID
        { get; set; }

        [DataMember]
        public string LeadStatus
        { get; set; }

        [DataMember]
        public int ExSubStatusID
        { get; set; }

        [DataMember]
        public string ExStatusName
        { get; set; }


        [DataMember]
        public string InsurerLeadStatus
        { get; set; }

        [DataMember]
        public string KeyMatrix
        { get; set; }

        [DataMember]
        public string PlanName
        { get; set; }


        //[DataMember]
        //public int DocumentID
        //{ get; set; }

        //[DataMember]
        //public int DocumentID
        //{ get; set; }

        [DataMember]
        public int ALL_PENDING
        { get; set; }
        [DataMember]
        public int ACCEPTED
        { get; set; }
        [DataMember]
        public int REJECTED
        { get; set; }
        [DataMember]
        public int ACCEPT_COND
        { get; set; }
        [DataMember]
        public int ADD_INFO
        { get; set; }
        [DataMember]
        public int Customer_Acc
        { get; set; }
        
        [DataMember]
        public int Customer_Rej
        { get; set; }
        
        [DataMember]
        public DateTime UploadDate
        { get; set; }

        [DataMember]
        public String InsurerName
        { get; set; }       

        [DataMember]
        public Int16 BasePremium
        { get; set; }
        
    }
    //@FilterParam varchar(150),@FilterType int,@FromDate Datetime,@ToDate Datetime,@InsurerID int

    //[DataContract]
    //public class LeadDetailsList
    //{
    //    [DataMember]
    //    public List<LeadDetails> LeadDetail
    //    { get; set; }
    //}

    [DataContract]
    public class MasterSupplier
    {
        [DataMember]
        public int InsurerID
        { get; set; }

        [DataMember]
        public string InsurerName
        { get; set; }
    }
    [DataContract]
    public class ReqMarkExSearch
    {
        //[DataMember]
        //public string FilterParam
        //{ get; set; }

        private string _FilterParam;
        [DataMember]
        public string FilterParam
        {
            get { return _FilterParam; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _FilterParam = string.Empty;
                else
                    _FilterParam = value;
            }
        }


        [DataMember]
        public int FilterType
        { get; set; }

        //[DataMember]
        //public string FromDate
        //{ get; set; }

        private string _FromDate;
        [DataMember]
        public string FromDate
        {
            get { return _FromDate; }
            set
            {
                long chk;
                if (long.TryParse(value, out chk))
                    _FromDate = CommonDataFunction.ToDateTime(Convert.ToInt64(value)).ToString();
                else
                    _FromDate = value;

            }
        }

        //[DataMember]
        //public string ToDate
        //{ get; set; }
        private string _ToDate;
        [DataMember]
        public string ToDate
        {
            get { return _ToDate; }
            set {
                long chk;
                if (long.TryParse(value, out chk))
                    _ToDate = CommonDataFunction.ToDateTime(Convert.ToInt64(value)).ToString();
                else
                    _ToDate = value;
               
            }
        }


        [DataMember]
        public int InsurerID
        { get; set; }

        [DataMember]
        public int Status
        { get; set; }

        [DataMember]
        public int KeyMatrix
        { get; set; } 
       
    }

    [DataContract]
    public class ReqMarkHealthEx
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public List<LeadDetails> LeadDetailsList
        { get; set; }
    }

    [DataContract]
    public class ReqMarkNewLead
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long EnquiryID
        { get; set; }

        [DataMember]
        public long ItemID
        { get; set; }
    }

    [DataContract]
    public class SelectedInsurerDetails
    {
        //ExID,InsurerID,InsurerName,PlanID,PlanName,SumInsured,Term,BasePremium
        [DataMember]
        public long ExID
        { get; set; }

        [DataMember]
        public long SelectionID
        { get; set; }


        [DataMember]
        public int InsurerID
        { get; set; }

        [DataMember]
        public string InsurerName
        { get; set; }

        [DataMember]
        public int PlanID
        { get; set; }

        [DataMember]
        public string PlanName
        { get; set; }

        [DataMember]
        public string SumInsured
        { get; set; }

        [DataMember]
        public string Term
        { get; set; }

        [DataMember]
        public string BasePremium
        { get; set; }
    }

    

    [DataContract]
    public class ResUploadCaseToInsurer
    {
        
        [DataMember]
        public CustomerLeadDetails CustomerLead
        { get; set; }

        [DataMember]
        public List<MasterDocument> MasterDocumentList
        { get; set; }

        [DataMember]
        public List<MemberDetails> MemberDetailsList
        { get; set; }

        [DataMember]
        public ProposerDetails ProposerDetails
        { get; set; }

        [DataMember]
        public List<SupplierPlanDetails> SupplierPlanDetailsList
        { get; set; }

        [DataMember]
        public List<SelectedInsurerDetails> SelectedInsurerDetailsList
        { get; set; }

        [DataMember]
        public List<MemberDocumentDetails> MemberDocumentDetailsList
        { get; set; }

        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public string StatusName
        { get; set; }

    }

    [DataContract]
    public class ResInsurerUnderwriting
    {
        [DataMember]
        public bool IsLoadPremium
        { get; set; }

        [DataMember]
        public int PremLoadingPerc
        { get; set; }

        [DataMember]
        public decimal PremLoadingAmount
        { get; set; }

        [DataMember]
        public decimal FinalPremium
        { get; set; }

        [DataMember]
        public bool IsDiseaseExclusion
        { get; set; }

        [DataMember]
        public bool IsDiseaseExclusionPerm
        { get; set; }

        [DataMember]
        public string DiseaseExclusionPerm
        { get; set; }

        [DataMember]
        public bool IsDiseaseExclusionTemp
        { get; set; }

        [DataMember]
        public string DiseaseExclusionTemp
        { get; set; }

        [DataMember]
        public bool IsCoPay
        { get; set; }

        [DataMember]
        public int CoPay
        { get; set; }

        [DataMember]
        public int CopayType
        { get; set; }

        [DataMember]
        public int LoadingType
        { get; set; }

        [DataMember]
        public int InsurerAction
        { get; set; }

        [DataMember]
        public string InsurerComment
        { get; set; }

        [DataMember]
        public string PremiumPaid
        { get; set; }

        [DataMember]
        public bool IsMemberExclusion
        { get; set; }

        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public string StatusName
        { get; set; }

        [DataMember]
        public int InsurerStatusID
        { get; set; }

        [DataMember]
        public int InsurerOldStatusID
        { get; set; }

        [DataMember]
        public string InsurerRemarks
        { get; set; }

        [DataMember]
        public CustomerLeadDetails CustomerLead
        { get; set; }

        [DataMember]
        public List<MemberDetails> MemberDetailsList
        { get; set; }

        [DataMember]
        public ProposerDetails ProposerDetails
        { get; set; }

        [DataMember]
        public SelectedInsurerDetails InsurerDetails
        { get; set; }

        [DataMember]
        public List<MemberDocumentDetails> MemberDocumentDetailsList
        { get; set; }

        [DataMember]
        public InsurerAction InsurerActionDetails
        { get; set; }

        [DataMember]
        public List<InsurerRemarksDetails> InsurerRemarksDetailsList
        { get; set; }   

        [DataMember]
        public List<InsurerAdditionalInfoDeatils> InsurerAdditionalInfoDeatilsList
        { get; set; }

        [DataMember]
        public List<CopayDetails> CopayDetailsList
        { get; set; }

        [DataMember]
        public List<LoadingDetails> LoadingDetailsList
        { get; set; }

    }

    [DataContract]
    public class CopayDetails
    {
        [DataMember]
        public long MemberID
        { get; set; }

        [DataMember]
        public string MemberName
        { get; set; }

        [DataMember]
        public string RelationShip
        { get; set; }

        [DataMember]
        public int Copay
        { get; set; }

    }

    [DataContract]
    public class LoadingDetails
    {
        [DataMember]
        public long MemberID
        { get; set; }

        [DataMember]
        public string MemberName
        { get; set; }

        [DataMember]
        public string RelationShip
        { get; set; }

        [DataMember]
        public int MemberLoading
        { get; set; }

    }
    [DataContract]
    public class InsurerAdditionalInfoDeatils
    {
        [DataMember]
        public int InfoType
        { get; set; }

        [DataMember]
        public string InfoName
        { get; set; }

        [DataMember]
        public long MemberID
        { get; set; }

        [DataMember]
        public string MemberName
        { get; set; }

        [DataMember]
        public string Relation { get; set; }

        [DataMember]
        public string Comments
        { get; set; }
    }

    [DataContract]
    public class ResPBInsurerUnderwriting
    {
        [DataMember]
        public CustomerLeadDetails CustomerLead
        { get; set; }

        [DataMember]
        public List<ResInsurerUnderwriting> InsurerUnderwritingDetails
        { get; set; }

        [DataMember]
        public List<MemberDocumentDetails> MemberDocumentDetailsList
        { get; set; }

        [DataMember]
        public List<MasterDocument> MasterDocumentList
        { get; set; }

        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public string StatusName
        { get; set; }

    }



    [DataContract]
    public class ResCustomerDocUpload
    {
        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string ProductName
        { get; set; }

        [DataMember]
        public string EmailID
        { get; set; }

        [DataMember]
        public string MobileNo
        { get; set; }

        [DataMember]
        public long ExID
        { get; set; }
        [DataMember]
        public long CustomerID
        { get; set; }
        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long EnquiryID
        { get; set; }


        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public string EncryptEnquiryID
        { get; set; }

        //[DataMember]
        //public List<ResInsurerUnderwriting> InsurerUnderwritingDetails
        //{ get; set; }

        [DataMember]
        public List<MemberDocumentDetails> MemberDocumentDetailsList
        { get; set; }

        [DataMember]
        public List<MasterDocument> MasterDocumentList
        { get; set; }

        [DataMember]
        public string EncryptStatusID
        { get; set; }

        [DataMember]
        public string StatusName
        { get; set; }

        //ProposerDetails
        [DataMember]
        public ProposerDetails ProposerDetails
        { get; set; }

    }


    [DataContract]
    public class MasterDocument
    {
        [DataMember]
        public int DocumentId
        { get; set; }

        [DataMember]
        public string Document
        { get; set; }
    }

    [DataContract]
    public class MemberDocumentDetails
    {
        //SNO,DocumentName,MemberID,MemberName,DocURL,Comments
        [DataMember]
        public long SNO
        { get; set; }

        [DataMember]
        public int InsurerID
        { get; set; }

        [DataMember]
        public int DocumentID
        { get; set; }

        [DataMember]
        public string DocumentName
        { get; set; }

        [DataMember]
        public string FileName
        { get; set; }

        [DataMember]
        public string MemberID
        { get; set; }

        [DataMember]
        public string MemberName
        { get; set; }

        [DataMember]
        public string DocURL
        { get; set; }

        [DataMember]
        public string FileData
        { get; set; }

        [DataMember]
        public string Comments
        { get; set; }
    }

    [DataContract]
    public class InspectionDocument
    {
        [DataMember]
        public string FileName
        { get; set; }

        [DataMember(IsRequired=false)]
        public string DocURL
        { get; set; }

        [DataMember]
        public string FileData
        { get; set; }

        [DataMember]
        public string CreatedBy
        { get; set; }

        [DataMember]
        public string CreatedOn
        { get; set; }
    }

    [DataContract]
    public class PostMemberDocumentDetails
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public int Type
        { get; set; }

        //[DataMember]
        //public int InspectionID
        //{ get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long ExID
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public string EncryptExID
        { get; set; }

        [DataMember]
        public List<MemberDocumentDetails> MemberDocumentDetailsList
        { get; set; }
    }

    [DataContract]
    public class InspectionDocumentDetails
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public string InspectionID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public List<InspectionDocument> InspectionDocumentList
        { get; set; }
    }

    [DataContract]
    public class MemberDetails
    {
        [DataMember]
        public long CustMemId
        { get; set; }

        [DataMember]
        public string InsuredName
        { get; set; }

        [DataMember]
        public string ExclusionReason
        { get; set; }

        [DataMember]
        public string DOB
        { get; set; }

        [DataMember]
        public string Gender
        { get; set; }

        [DataMember]
        public string RelationType
        { get; set; }

        [DataMember]
        public string IsActive
        { get; set; }

        [DataMember]
        public bool IsExcluded
        { get; set; }

        [DataMember]
        public string PED
        { get; set; }

    }


    [DataContract]
    public class PostPlanSelectionData
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long ExID
        { get; set; }
        

        [DataMember]
        public List<PostInsurerPlanData> PostInsurerPlanDataList
        { get; set; }

    }

    [DataContract]
    public class PostInsurerPlanData
    {
        [DataMember]
        public int InsurerID
        { get; set; }

        [DataMember]
        public long WebSelectionID
        { get; set; }

        [DataMember]
        public int PlanID
        { get; set; }

        [DataMember]
        public string InsurerName
        { get; set; }

        [DataMember]
        public string PlanName
        { get; set; }

        [DataMember]
        public int Term
        { get; set; }

        [DataMember]
        public decimal SumInsured
        { get; set; }

        [DataMember]
        public decimal BasePremium
        { get; set; }

    }
     
    [DataContract]
    public class InsurerAction
    {         
        [DataMember]
        public bool Accept
        { get; set; }

        [DataMember]
        public bool Reject
        { get; set; }

        [DataMember]
        public bool AcceptWithConditions
        { get; set; }

        [DataMember]
        public bool AdditionalInfoRequired
        { get; set; }

    }

    public class DiseaseExclusionDetails
    {
        public string DiseaseExclusion { get; set; }
        public Diseasetype DiseaseType { get; set; }
    }

    public class Diseasetype
    {
        public string IsDiseaseExclusionPerm { get; set; }
        public string DiseaseExclusionPerm { get; set; }
        public string IsDiseaseExclusionTemp { get; set; }
        public string DiseaseExclusionTemp { get; set; }
    }

    [DataContract]
    public class PostInsurerActionData
    {
        //IsLoadPremium,FinalPremium,IsDiseaseExlusion,DiseaseExclusionType,DiseaseExclusion,
        //IsCoPay,CoPay,IsMemberExclusion
        //StatusID,Remarks 
        [DataMember]
        public long ExID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long NewLeadID
        { get; set; }

        [DataMember]
        public string EmailID
        { get; set; }

        [DataMember]
        public long EnquiryID
        { get; set; }

        [DataMember]
        public int InsurerID
        { get; set; }

        [DataMember]
        public long SelectionID
        { get; set; }

        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public bool IsLoadPremium
        { get; set; }

        [DataMember]
        public decimal FinalPremium
        { get; set; }

        [DataMember]
        public bool IsDiseaseExclusion
        { get; set; }

        [DataMember]
        public bool IsDiseaseExclusionPerm
        { get; set; }

        [DataMember]
        public string DiseaseExclusionPerm
        { get; set; }

        [DataMember]
        public bool IsDiseaseExclusionTemp
        { get; set; }

        [DataMember]
        public string DiseaseExclusionTemp
        { get; set; }

        [DataMember]
        public bool IsCoPay
        { get; set; }

        [DataMember]
        public int CoPay
        { get; set; }

        [DataMember]
        public bool IsMemberExclusion
        { get; set; }

        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public string Remarks
        { get; set; }

        [DataMember]
        public int PBResponse
        { get; set; }  

        [DataMember]
        public List<MemberDetails> MemberDetailsList
        { get; set; }

        [DataMember]
        public int CopayType
        { get; set; }

        [DataMember]
        public int LoadingType
        { get; set; }

        [DataMember]
        public int PremLoadingPerc
        { get; set; }

        [DataMember]
        public decimal PremLoadingAmount
        { get; set; }

        [DataMember]
        public List<InsurerAdditionalInfoDeatils> InsurerAdditionalInfoDeatilsList
        { get; set; }

        [DataMember]
        public List<CopayDetails> CopayDetailsList
        { get; set; }

        [DataMember]
        public List<LoadingDetails> LoadingDetailsList
        { get; set; }
    }

    [DataContract]
    public class UserDetails
    {
        //Name,EmailID,MobileNo,LoginID,UR.UserType,UR.UserRole
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string EmailID
        { get; set; }

        [DataMember]
        public string MobileNo
        { get; set; }

        [DataMember]
        public string LoginID
        { get; set; }

        [DataMember]
        public int UserType
        { get; set; }

        [DataMember]
        public int UserRole
        { get; set; }

        [DataMember]
        public string Token
        { get; set; }

        [DataMember]
        public int LoginStatus
        { get; set; }

        [DataMember]
        public int InsurerID
        { get; set; }
             [DataMember]
        public string InsurerName
        { get; set; }
        

    }

    [DataContract]
    public class ReqRemoveFromHealthEx
    {
        //LeadID, string UserID
        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long ExID
        { get; set; }

        [DataMember]
        public int UserID
        { get; set; }

    }

    [DataContract]
    public class UserLoginRequest
    {

        [DataMember]
        public string LoginID
        { get; set; }

        [DataMember]
        public string Password
        { get; set; }

        [DataMember]
        public string Token
        { get; set; }

        [DataMember]
        public DateTime ExpireOn
        { get; set; }

        [DataMember]
        public int HitCount
        { get; set; }

        [DataMember]          
        public int LoginUserType
        { get; set; }

    }

    [DataContract]
    public class CustomerLeadDetails
    {

        [DataMember]
        public long OldLeadID
        { get; set; }

        [DataMember]
        public long NewLeadID
        { get; set; }

        [DataMember]
        public long EnquiryID
        { get; set; }

        [DataMember]
        public string EncryptEnquiryID
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string ProductName
        { get; set; }

        [DataMember]
        public string EmailID
        { get; set; }

        [DataMember]
        public string MobileNo
        { get; set; }

        //[DataMember]
        //public string ProductName
        //{ get; set; }

    }

    [DataContract]
    public class PaymentDetailsData
    {                 

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long EnquiryID
        { get; set; }

        [DataMember]
        public string PaymentMode
        { get; set; }

        [DataMember]
        public int PaymentType
        { get; set; }

        [DataMember]
        public decimal PaymentAmount
        { get; set; }

        [DataMember]
        public int PaymentStatus
        { get; set; }

    }

    [DataContract]
    public class UpdatePremiumReq
    {

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long SelectionID
        { get; set; }

        [DataMember]
        public decimal PremiumPaid
        { get; set; }

        [DataMember]
        public int UserID
        { get; set; }

    }

    [DataContract]
    public class PaymentDetailsForEx
    {
        [DataMember]
        public long EnquiryID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public string PaymentMode
        { get; set; }

        [DataMember]
        public string PaymentType
        { get; set; }

        [DataMember]
        public string PaymentAmount
        { get; set; }

        [DataMember]
        public string PaymentStatus
        { get; set; }

        [DataMember]
        public string CreatedOn
        { get; set; }

    }

    [DataContract]
    public class InsurerRemarksDetails
    {
        [DataMember]
        public int UserID
        { get; set; }

        [DataMember]
        public int InsurerID
        { get; set; }

        [DataMember]
        public long ExID
        { get; set; }

        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long SelectionID
        { get; set; }

        [DataMember]
        public string Remarks
        { get; set; }


        [DataMember]
        public string CreatedOn
        { get; set; }

    }

    [DataContract]
    public class AddNewUser
    {
        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string EmailID
        { get; set; }

        [DataMember]
        public string MobileNo
        { get; set; }

        [DataMember]
        public string LoginID
        { get; set; }

        [DataMember]
        public string Password
        { get; set; }

        [DataMember]
        public string CreatedBy
        { get; set; }

        [DataMember]
        public string UserType
        { get; set; }

        [DataMember]
        public string UserRole
        { get; set; }

        [DataMember]
        public string UserTypeName
        { get; set; }

        [DataMember]
        public string UserRoleName
        { get; set; }

        [DataMember]
        public string InsurerID
        { get; set; }

        [DataMember]
        public string InsurerName
        { get; set; }

        [DataMember]
        public string Type
        { get; set; }

        [DataMember]
        public string UserID
        { get; set; }

        [DataMember]
        public string ApplicationType
        { get; set; }


    }

    [DataContract]
    public class ChangePassword
    {
        [DataMember]
        public string OldPassword
        { get; set; }

        [DataMember]
        public string NewPassword
        { get; set; }

        [DataMember]
        public string UserId
        { get; set; }

        //[DataMember]
        //public string UserType
        //{ get; set; }
    }


    [DataContract]
    public class UpdateUserInfo
    {
        [DataMember]
        public string Name
        { get; set; }

        [DataMember]
        public string EmailID
        { get; set; }

        [DataMember]
        public string MobileNo
        { get; set; }

        [DataMember]
        public string LoginID
        { get; set; }

        [DataMember]
        public string Password
        { get; set; }

        [DataMember]
        public string CreatedBy
        { get; set; }

        [DataMember]
        public string UserType
        { get; set; }

        [DataMember]
        public string UserRole
        { get; set; }

        [DataMember]
        public string UserTypeName
        { get; set; }

        [DataMember]
        public string UserRoleName
        { get; set; }


        [DataMember]
        public string InsurerID
        { get; set; }

        [DataMember]
        public string InsurerName
        { get; set; }

        [DataMember]
        public string Type
        { get; set; }

        [DataMember]
        public string UserID
        { get; set; }

        [DataMember]
        public string ApplicationType
        { get; set; }


    }
}
