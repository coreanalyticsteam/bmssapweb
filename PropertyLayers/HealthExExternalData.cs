﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PropertyLayers
{
    class HealthExExternalData
    {
    }

    [DataContract]
    public class SupplierPlanDetails
    {
        [DataMember]
        public string DisplayOrder
        { get; set; }

        [DataMember]
        public string EndTime
        { get; set; }

        [DataMember]
        public string Features
        { get; set; }

        [DataMember]
        public string FinalCorporatePremium
        { get; set; }

        [DataMember]
        public string FinalPremium
        { get; set; }

        [DataMember]
        public string IsMobileReleased
        { get; set; }

        [DataMember]
        public string PlanId
        { get; set; }

        [DataMember]
        public string PlanName
        { get; set; }

        [DataMember]
        public string PlanPercentage
        { get; set; }


        [DataMember]
        public string PlanScore
        { get; set; }

        [DataMember]
        public string PlanTermID
        { get; set; }

        [DataMember]
        public string PreferedText
        { get; set; }

        [DataMember]
        public string SelectionRelevance
        { get; set; }

        [DataMember]
        public string StartTime
        { get; set; }

        [DataMember]
        public string StpType
        { get; set; }

        [DataMember]
        public string SumInsured
        { get; set; }

        [DataMember]
        public string SupplierId
        { get; set; }

        [DataMember]
        public string SupplierName
        { get; set; }

        [DataMember]
        public string TopFeatures
        { get; set; }

        [DataMember]
        public string TotalRecords
        { get; set; }
    }

    [DataContract]
    public class HealthID
    {
        [DataMember]
        public long EnquiryID { get; set; }
        [DataMember]
        public long LeadID { get; set; }
        [DataMember]
        public long CustomerID { get; set; }
        [DataMember]
        public long ProposerID { get; set; }
        [DataMember]
        public long MatrixLeadID { get; set; }
        [DataMember]
        public long CustomerMemeberID { get; set; }
        [DataMember]
        public long VisitLogID { get; set; }
        [DataMember]
        public long NeedID { get; set; }
    }


    [DataContract]
    public class CJPlanSelection
    {
        [DataMember]
        public long EnquiryId
        { get; set; }

        [DataMember]
        public string Premium
        { get; set; }

        [DataMember]
        public string PlanId
        { get; set; }

        [DataMember]
        public string Suminsured
        { get; set; }

        [DataMember]
        public string Term
        { get; set; }

    }


    [DataContract]
    public class HealthEXDetails
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public long LeadID { get; set; }
        [DataMember]
        public long NewLeadID { get; set; }
        [DataMember]
        public long EnquiryId { get; set; }
        [DataMember]
        public long NewEnquiryId { get; set; }
        [DataMember]
        public HealthID Identifiers { get; set; }

    }

    [DataContract]
    public class HealthExSelections
    {
        [DataMember]
        public long BookingId { get; set; }
        [DataMember]
        public long SelectionID { get; set; }
        [DataMember]
        public long EnquiryId { get; set; }
        [DataMember]
        public long ProfileId { get; set; }
        [DataMember]
        public int PlanId { get; set; }
        [DataMember]
        public int Suminsured { get; set; }
        [DataMember]
        public int Term { get; set; }
        [DataMember]
        public int Premium { get; set; }
        [DataMember]
        public int Hittype { get; set; }

        [DataMember]
        public int PolicyType { get; set; }
        [DataMember]
        public string PlanName { get; set; }
        [DataMember]
        public string SupplierName { get; set; }
        [DataMember]
        public int SupplierId { get; set; }
    }

    [DataContract]
    public class PostInsurerResSelection
    {
        [DataMember]
        public long EnquiryID { get; set; }

        [DataMember]
        public long SelectionID { get; set; }

        [DataMember]
        public string Copay { get; set; }

        [DataMember]
        public string CopayText { get; set; }

        [DataMember]
        public LoadingTextDetail Loading { get; set; }

        [DataMember]
        public decimal OverloadingPremium { get; set; }

        [DataMember]
        public int Submitted { get; set; }

        [DataMember]
        public string Exclusions { get; set; }


        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public int InsurerStatus { get; set; }

        [DataMember]
        public string Conditions { get; set; }
    }

    [DataContract]
    public class ConditionDetails
    {
        [DataMember]
        public string MemberName { get; set; }

        [DataMember]
        public string Relation { get; set; }

        [DataMember]
        public List<AdditionalDocDetails> RemarkList { get; set; }

    }
    [DataContract]
    public class AdditionalDocDetails
    {
        [DataMember]
        public string AdditionalInfoName { get; set; }

        [DataMember]
        public string Remark { get; set; }


    }

    [DataContract]
    public class ExternalCopayDetails
    {
        [DataMember]
        public string MemberName { get; set; }

        [DataMember]
        public string RelationShip { get; set; }

        [DataMember]
        public string Copay { get; set; }

    }

    [DataContract]
    public class ExternalLoadingDetails
    {
        [DataMember]
        public Int64 MemberID { get; set; }

        [DataMember]
        public decimal MemberLoading { get; set; }

    }
    [DataContract]
    public class LoadingTextDetail
    {
        [DataMember]
        public decimal BaseLoading { get; set; }

        [DataMember]
        public List<ExternalLoadingDetails> MemberLoading { get; set; }
    }


    [DataContract]
    public class MembersRejPost
    {
        [DataMember]
        public long MemberId { get; set; }
        [DataMember]
        public long SelectionID { get; set; }
        [DataMember]
        public long EnquiryID { get; set; }
    }


    [DataContract]
    public class Appointmentstatus
    {
        [DataMember]
        public string AppointmentStatus { get; set; }
        [DataMember]
        public string Area { get; set; }
        [DataMember]
        public string CallDate { get; set; }
        [DataMember]
        public string CallRemarks { get; set; }
        [DataMember]
        public string CaseID { get; set; }
        [DataMember]
        public string Categary { get; set; }
        [DataMember]
        public string CenterName { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public int CityID { get; set; }
        [DataMember]
        public string DateofAppointment { get; set; }
        [DataMember]
        public int LeadID { get; set; }
        [DataMember]
        public string MedicalTest { get; set; }
        [DataMember]
        public int MedicalTestID { get; set; }
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public string MemberName { get; set; }
        [DataMember]
        public int PlanID { get; set; }
        [DataMember]
        public string Policynumber { get; set; }
        [DataMember]
        public string TimeofAppointment { get; set; }
        [DataMember]
        public string VisitType { get; set; }
    }

    [DataContract]
    public class MedicalDetail
    {
        public MedicalDetails[] MedicalDetailList { get; set; }
    }
    [DataContract]
    public class MedicalDetails
    {
        [DataMember]
        public string AppointmentStatus { get; set; }
        [DataMember]
        public string Area { get; set; }
        [DataMember]
        public object CallDate { get; set; }
        [DataMember]
        public object CallRemarks { get; set; }
        [DataMember]
        public object CaseID { get; set; }
        [DataMember]
        public string Categary { get; set; }
        [DataMember]
        public string CenterName { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public int CityID { get; set; }
        [DataMember]
        public string DateofAppointment { get; set; }
        [DataMember]
        public int LeadID { get; set; }
        [DataMember]
        public string MedicalTest { get; set; }
        [DataMember]
        public int MedicalTestID { get; set; }
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public string MemberName { get; set; }
        [DataMember]
        public object PackageID { get; set; }
        [DataMember]
        public int PlanID { get; set; }
        [DataMember]
        public object Policynumber { get; set; }
        [DataMember]
        public string TimeofAppointment { get; set; }
        [DataMember]
        public string VisitType { get; set; }

    }
    [DataContract]
    public class LeadDocumentDetails
    {
        [DataMember]
        public string DecisionUrl { get; set; }

        [DataMember]
        public List<DocDetails> TestReports { get; set; }
    }
    [DataContract]
    public class DocDetails
    {
        [DataMember]
        public Int64 MemberId { get; set; }

        [DataMember]
        public string DocUrl { get; set; }
    }

}
