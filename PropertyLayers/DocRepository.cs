﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace PropertyLayers
{
    [DataContract]
    public class DocRepository
    {

    }

    [DataContract]
    public class ProductDetails
    {
        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public string ProductName
        { get; set; }
    }

    //@LeadID BIGINT=0,@FromAgentID VARCHAR(20)='',@ToAgentID VARCHAR(20)='',
    //@MobileNo VARCHAR(20)='',@EventType SMALLINT=0
    [DataContract]
    public class LogReqDetails
    {
        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public string FromAgentID
        { get; set; }

        [DataMember]
        public string ToAgentID
        { get; set; }

        [DataMember]
        public string MobileNo
        { get; set; }

        [DataMember]
        public int EventType
        { get; set; }

    }

   
    [DataContract]
    public class DropDownData
    {
        [DataMember]
        public int id
        { get; set; }

        [DataMember]
        public string label
        { get; set; }
    }

    [DataContract]
    public class DocumentDetails
    {
        [DataMember]
        public List<DropDownData> DocCategoryList
        { get; set; }

        [DataMember]
        public List<DropDownData> DocumentList
        { get; set; }
    }


    public class MappingSettingData
    {
        public string Portability
        { get; set; }

        public string MedInspection
        { get; set; }

        public string STP
        { get; set; }
    }
    [DataContract]
    public class MasterDocumentMappingRequest
    {
        [DataMember]
        public string ProductID
        { get; set; }

        [DataMember]
        public string GroupID
        { get; set; }

        [DataMember]
        public string SupplierIDs
        { get; set; }

        [DataMember]
        public string StatusID
        { get; set; }

        [DataMember]
        public string SubStatusID
        { get; set; }

        [DataMember]
        public string StpIDs
        { get; set; }

        [DataMember]
        public string PortabilityIDs
        { get; set; }

        [DataMember]
        public string MedInspectionIDs
        { get; set; }

        [DataMember]
        public string DocCategoryID
        { get; set; }

        [DataMember]
        public string DocumentIDs
        { get; set; }

        [DataMember]
        public string CreatedBy
        { get; set; }


    }

    [DataContract]
    public class DocumentMasterDetailsResp
    {
        [DataMember]
        public string GroupID
        { get; set; }

        [DataMember]
        public string StatusName
        { get; set; }

        [DataMember]
        public string Documentcategory
        { get; set; }

        [DataMember]
        public string Documents
        { get; set; }

        [DataMember]
        public string STP
        { get; set; }

        [DataMember]
        public string Portability
        { get; set; }

        [DataMember]
        public string MedicalOrInspection
        { get; set; }
    }


    [DataContract]
    public class Document
    {
        [DataMember]
        public int DocumentID
        { get; set; }
        [DataMember]
        public int DocumentCategoryID
        { get; set; }

        [DataMember]
        public string DocumentName
        { get; set; }

        [DataMember]
        public long CustDocumentID
        { get; set; }

        [DataMember]
        public string HostName
        { get; set; }

        [DataMember]
        public string DocumentUrl
        { get; set; }
    }

    [DataContract]
    public class CustomerDocDetails
    {
        [DataMember]
        public string ProductName
        { get; set; }

        [DataMember]
        public string SupplierName
        { get; set; }

        [DataMember]
        public string PlanName
        { get; set; }


        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public int SupplierId
        { get; set; }

        [DataMember]
        public int PlanId
        { get; set; }


        [DataMember]
        public List<DocumentCategory> DocumentCategoryList
        { get; set; }

    }

    [DataContract]
    public class UploadDocReq
    {
        [DataMember]
        public long LeadID
        { get; set; }

        [DataMember]
        public long CustomerID
        { get; set; }

        [DataMember]
        public int DocCategoryID
        { get; set; }

        [DataMember]
        public int DocumentID
        { get; set; }

        [DataMember]
        public long ParentCustDocumentID
        { get; set; }

        [DataMember]
        public string HostName
        { get; set; }

        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public string ProductName
        { get; set; }

        [DataMember]
        public int UserID
        { get; set; }    

        [DataMember]
        public string FileName
        { get; set; }

        [DataMember]
        public string FileStream
        { get; set; }

    }
    [DataContract]
    public class DocumentCategory
    {
        [DataMember]
        public int DocumentCategoryID
        { get; set; }

        [DataMember]
        public string DocumentCategoryName
        { get; set; }

        [DataMember]
        public List<Document> MasterDocument
        { get; set; }

        [DataMember]
        public List<Document> ExistingCustDocument
        { get; set; }

        [DataMember]
        public List<Document> ExistingLeadDocument
        { get; set; }

    }

    [DataContract]
    public class GetDocumentEditResp
    {
        [DataMember]
        public int ProductID
        { get; set; }

        [DataMember]
        public int StatusID
        { get; set; }

        [DataMember]
        public int SubStatusID
        { get; set; }

        [DataMember]
        public int DocCategoryID
        { get; set; }

        [DataMember]
        public List<int> DcoumentIDList
        { get; set; }

        [DataMember]
        public List<int> STPList
        { get; set; }

        [DataMember]
        public List<int> PortabilityList
        { get; set; }

        [DataMember]
        public List<int> MedInspectionList
        { get; set; }

        [DataMember]
        public List<SelectedSupplierIDResp> SelectedSupplierIDRespList
        { get; set; }
    }

    [DataContract]
    public class SelectedSupplierIDResp
    {
        [DataMember]
        public int id
        { get; set; }
    }


}
