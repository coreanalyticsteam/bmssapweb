﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace PropertyLayers
{
    [DataContract]
    public class MediaFileInfo
    {
        [DataMember]
        public string RegNo { get; set; } 
      
        [DataMember]
        public string InspectionId { get; set; }

        [DataMember]
        public string InspectionDate { get; set; }

        [DataMember]
        public string MobileNo { get; set; }

        [DataMember]
        public FileDetails Fileinfo { get; set; }

    }

    [DataContract]
    public class FileDetails
    {
        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public Stream MediaData { get; set; }

        [DataMember]
        public string FileUrl { get; set; }

        [DataMember]
        public string FileUrlInternal { get; set; }

        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public string MediaBase64 { get; set; } 
    }

    [DataContract]
    public class InspectionBookingDetails
    {
        [DataMember]
        public int IbdId { get; set; }

        [DataMember]
        public string InspectionId { get; set; }

        [DataMember]
        public int InsurerId { get; set; }

        [DataMember]
        public long CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public long LeadId { get; set; }

        [DataMember]
        public long BookingId { get; set; }

        [DataMember]
        public int InspectionStatusId { get; set; }

        [DataMember]
        public string VehicleNumber { get; set; }

        [DataMember]
        public byte[] MediaFileData { get; set; }

        [DataMember]
        public string UploadedDate { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public int UserID { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public string StatusName { get; set; }

        [DataMember]
        public string InsurerName { get; set; }

        [DataMember]
        public string LeadSource { get; set; }

        [DataMember]
        public int SubStatusId { get; set; }

        [DataMember]
        public string SubStatusName { get; set; }

        [DataMember]
        public string MobileNo { get; set; }

        //[DataMember]
        //public string ToDate { get; set; }

        private string _toDate;
        [DataMember]
        public string ToDate
        {
            get { return _toDate; }
            set
            {
                long chk;
                _toDate = long.TryParse(value, out chk) ? CommonDataFunction.ToDateTime(Convert.ToInt64(value)).ToString() : value;
            }
        }

        private string _fromDate;
        [DataMember]
        public string FromDate
        {
            get { return _fromDate; }
            set
            {
                long chk;
                _fromDate = long.TryParse(value, out chk) ? CommonDataFunction.ToDateTime(Convert.ToInt64(value)).ToString() : value;
            }
        }

        [DataMember]
        public int Insurer { get; set; }
  
    }

    [DataContract]
    public class InspectionRollbackDetails
    {
        [DataMember]
        public string InspectionId { get; set; }

        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public int SubStatusId { get; set; }

        [DataMember]
        public int UserID { get; set; }

        [DataMember]
        public int InsurerId { get; set; }
    }

    [DataContract]
    public class InspectionRemarksDetails
    {
        [DataMember]
        public int InspectionId { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }

    [DataContract]
    public class StatusCollection
    {
        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public string StatusName { get; set; }

        [DataMember]
        public int SubStatusId { get; set; }

        [DataMember]
        public string SubStatusName { get; set; }
    }

    [DataContract]
    public class InsurerCollection
    {
        [DataMember]
        public int InsurerId { get; set; }

        [DataMember]
        public string InsurerName { get; set; }
    }

    //[DataContract]
    //public class AddNewUser
    //{
    //    [DataMember]
    //    public string Name
    //    { get; set; }

    //    [DataMember]
    //    public string EmailID
    //    { get; set; }

    //    [DataMember]
    //    public string MobileNo
    //    { get; set; }

    //    [DataMember]
    //    public string LoginID
    //    { get; set; }

    //    [DataMember]
    //    public string Password
    //    { get; set; }

    //    [DataMember]
    //    public string CreatedBy
    //    { get; set; }

    //    [DataMember]
    //    public string UserType
    //    { get; set; }

    //    [DataMember]
    //    public string UserRole
    //    { get; set; }

    //    [DataMember]
    //    public string UserTypeName
    //    { get; set; }

    //    [DataMember]
    //    public string UserRoleName
    //    { get; set; }

    //    [DataMember]
    //    public string InsurerID
    //    { get; set; }

    //    [DataMember]
    //    public string InsurerName
    //    { get; set; }

    //    [DataMember]
    //    public string Type
    //    { get; set; }

    //    [DataMember]
    //    public string UserID
    //    { get; set; }

    //    [DataMember]
    //    public string ApplicationType
    //    { get; set; }


    //}

    //[DataContract]
    //public class ChangePassword
    //{
    //    [DataMember]
    //    public string OldPassword
    //    { get; set; }

    //    [DataMember]
    //    public string NewPassword
    //    { get; set; }

    //    [DataMember]
    //    public string UserId
    //    { get; set; }

    //    //[DataMember]
    //    //public string UserType
    //    //{ get; set; }
    //}


    //[DataContract]
    //public class UpdateUserInfo
    //{
    //    [DataMember]
    //    public string Name
    //    { get; set; }

    //    [DataMember]
    //    public string EmailID
    //    { get; set; }

    //    [DataMember]
    //    public string MobileNo
    //    { get; set; }

    //    [DataMember]
    //    public string LoginID
    //    { get; set; }

    //    [DataMember]
    //    public string Password
    //    { get; set; }

    //    [DataMember]
    //    public string CreatedBy
    //    { get; set; }

    //    [DataMember]
    //    public string UserType
    //    { get; set; }

    //    [DataMember]
    //    public string UserRole
    //    { get; set; }

    //    [DataMember]
    //    public string UserTypeName
    //    { get; set; }

    //    [DataMember]
    //    public string UserRoleName
    //    { get; set; }


    //    [DataMember]
    //    public string InsurerID
    //    { get; set; }

    //    [DataMember]
    //    public string InsurerName
    //    { get; set; }

    //    [DataMember]
    //    public string Type
    //    { get; set; }

    //    [DataMember]
    //    public string UserID
    //    { get; set; }

    //    [DataMember]
    //    public string ApplicationType
    //    { get; set; }


    //}
    [DataContract]
    public class ActionResult
    {
        [DataMember]
        public string FileGuid { get; set; }
        [DataMember]
        public string FileName { get; set; }
    }

    [DataContract]
    public class LabURLBody
    {
        [DataMember]
        public Int64 ID { get; set; }
        [DataMember]
        public string InspectionId { get; set; }
        [DataMember]
        public string MobileNo { get; set; }
        [DataMember]
        public string VehicleNo { get; set; }
        [DataMember]
        public DateTime InspectionDate { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string FileUrl { get; set; }
        [DataMember]
        public string ContentType { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }
        [DataMember]
        public string FileUrlInternal { get; set; }
    }
}
