﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PropertyLayers
{
    class Email
    {
    }

    [DataContract]
    public class SendResponse
    {
        [DataMember]
        public string Response
        {
            get;
            set;
        }
        [DataMember]
        public string Description
        {
            get;
            set;
        }
    }


    public enum CommunicationType
    {
        ALL = 0,
        Email = 1,
        SMS = 2,
        Call = 3,
        Chat = 4,
        OTP = 5,
        IBCall = 6
    }
}
