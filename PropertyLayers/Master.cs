﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropertyLayers
{
    //class Master
    //{
        
    //}
    //[DataContract]
    //public class ResultClass
    //{
    //    [DataMember]
    //    public string Success { get; set; }
    //    [DataMember]
    //    public string Error { get; set; }
    //    [DataMember]
    //    public bool IsAuth { get; set; }
    //    [DataMember]
    //    public bool IsSuccess { get; set; }
    //}
    [DataContract]
    public class TicketIssueName
    {
        [DataMember]
        public string IssueName { get; set; }

        [DataMember]
        public List<TicketIssueTypeMaster> TicketIssueTypeMaster { get; set; }
    }


    [DataContract]
    public class TicketIssueTypeMaster
    {
        [DataMember]
        public int IssueTypeId { get; set; }

        [DataMember]
        public int ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string IssueName { get; set; }

        [DataMember]
        public int ParentID { get; set; }

        [DataMember]
        public Boolean IsActive { get; set; }

        [DataMember]
        public Boolean IsMyAccount { get; set; }

        [DataMember]
        public int SequenceID { get; set; }

        [DataMember]
        public int TopIssue { get; set; }

        [DataMember]
        public int CategoryType { get; set; }

        [DataMember]
        public Boolean IsChat { get; set; }

        [DataMember]
        public Boolean IsAutoClosure { get; set; }
        [DataMember]
        public int TAT { get; set; }
    }
}
