﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace DataAccessLayer
{
    public class ConnectionClass
    {
        public static string ProductDBsqlConnection()
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            //return reader.GetValue("ConnectionString", typeof(string)).ToString();//"Data Source=10.0.10.43;Initial Catalog=productdb;User ID=PBLive;Password=PB123Live;";
            //return "Data Source=10.0.10.43;Initial Catalog=productdb;User ID=PBLive;Password=PB123Live;";
            return ConfigurationManager.AppSettings["ProductDBsqlConnection"];
            //return "Data Source=10.209.65.242;Initial Catalog=ProductDB; User ID=PBLive;Password=PB123Live;";
        }
        public static string PBCromasqlConnection()
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            //return reader.GetValue("ConnectionString", typeof(string)).ToString();//"Data Source=10.0.10.43;Initial Catalog=productdb;User ID=PBLive;Password=PB123Live;";
            //return "Data Source=10.0.10.43;Initial Catalog=PBCroma;User ID=PBLive;Password=PB123Live;";
            //return "Data Source=10.209.65.242;Initial Catalog=PBCroma; User ID=PBLive;Password=PB123Live;";
            return ConfigurationManager.AppSettings["PBCromasqlConnection"];
        }
        public static string ReplPBCromasqlConnection()
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            //return reader.GetValue("ConnectionString", typeof(string)).ToString();//"Data Source=10.0.10.43;Initial Catalog=productdb;User ID=PBLive;Password=PB123Live;";
            //return "Data Source=10.0.10.43;Initial Catalog=PBCroma;User ID=PBLive;Password=PB123Live;";
            //return "Data Source=10.209.65.242;Initial Catalog=PBCroma; User ID=PBLive;Password=PB123Live;";
            return ConfigurationManager.AppSettings["ConnectionStringBMSReports"];
        }
        public static string LoggingsqlConnection()
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            //return reader.GetValue("ConnectionString", typeof(string)).ToString();//"Data Source=10.0.10.43;Initial Catalog=productdb;User ID=PBLive;Password=PB123Live;";
            //return "Data Source=10.0.10.43;Initial Catalog=PBCroma;User ID=PBLive;Password=PB123Live;";
            //return "Data Source=10.72.179.209;Initial Catalog=Logging; User ID=PBLive;Password=PB123Live;";
            return ConfigurationManager.AppSettings["LoggingsqlConnection"];
            
        }

        public static string MasterSupplierPlanForEx()
        {

            return ConfigurationManager.AppSettings["MasterSupplierPlanForEx"];
           

        }

        public static string MarkHealthEx()
        {
            return ConfigurationManager.AppSettings["MarkHealthEx"];
           

        }

        public static string UpdateSelectedPlanOnCJ()
        {
            return ConfigurationManager.AppSettings["UpdateSelectedPlanOnCJ"];
            
        }
        
        public static string GetHealthProposerLink()
        {
            return ConfigurationManager.AppSettings["GetHealthProposer"];
           
        }

        public static string RejectMembersLink()
        {
            return ConfigurationManager.AppSettings["RejectMembers"];
           
        }

        public static string SelectionOverloadLink()
        {
            return ConfigurationManager.AppSettings["SelectionOverload"];

        }

        public static string MedicalDetails()
        {
            return ConfigurationManager.AppSettings["MedicalDetails"];

        }

        public static string InsurerDecisionURL()
        {
            return ConfigurationManager.AppSettings["InsurerDecisionURL"];
        }
    }
}
