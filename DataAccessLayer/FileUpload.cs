﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.IO;
using System.Xml.Linq;
using System.Net;
using Newtonsoft.Json;
using PropertyLayers;
namespace DataAccessLayer
{
    public class FileUpload
    {
        DataSet ds;
        DataTable dt;
        SqlDataAdapter adp;
        SqlConnection con;
        SqlCommand cmd;
        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
        string GetContentType(string Ext)
        {
            Ext = Ext.ToLower();
            string mimeType = "";
            if (Ext== "png")
            {
                mimeType = "image/png";
            }
            else if (Ext== "bmp")
            {
                mimeType = "image/bmp";
            }
            else if (Ext== "emf")
            {
                mimeType = "image/x-emf";
            }
            else if (Ext== "exif")
            {
                mimeType = "image/jpeg";
            }
            else if (Ext== "gif")
            {
                mimeType = "image/gif";
            }
            else if (Ext== "icon")
            {
                mimeType = "image/ico";
            }
            else if (Ext== "jpeg")
            {
                mimeType = "image/jpeg";
            }
            else if (Ext == "jpg")
            {
                mimeType = "image/jpeg";
            }
            else if (Ext== "memorybmp")
            {
                mimeType = "image/bmp";
            }
            else if (Ext== "tiff")
            {
                mimeType = "image/tiff";
            }
            else if (Ext== "wmf")
            {
                mimeType = "image/wmf";
            }
            else if (Ext == "doc")
            {
                mimeType = "application/msword";
            }
            else if (Ext == "docx")
            {
                mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            }
            else if (Ext == "xlsx")
            {
                mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }
            else if (Ext == "xls")
            {
                mimeType = "application/vnd.ms-excel";
            }          
            else if (Ext == "pdf")
            {
                mimeType = "application/pdf";
            }
            else { mimeType = "application/pdf"; }
            return mimeType;
        }
        public dynamic GetPostAPIResponse(string URL, Stream file, string FileName)
        {
            dynamic DataResult = null;
            string FileExt = "";
            FileExt = Path.GetExtension(FileName);
            
            if (FileExt.Contains('.'))
            {
                FileExt = FileExt.Remove(0, 1);
            
            }
            FileExt = GetContentType(FileExt); // GetContentType(FileExt);
           
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
            //dynamic PayloadData = "{\"file \" :" + data + ", \"remarks \" :" + FileName + " }"; ;
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Uri.EscapeUriString(URL));
            request.Method = "POST";
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.KeepAlive = true;
            var postData = request.GetRequestStream();
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, "file", FileName, FileExt);

            postData.Write(boundarybytes, 0, boundarybytes.Length);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            postData.Write(headerbytes, 0, headerbytes.Length);


            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = file.Read(buffer, 0, buffer.Length)) != 0)
            {
                postData.Write(buffer, 0, bytesRead);
            }

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            postData.Write(trailer, 0, trailer.Length);


            postData.Close();

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string strREsp = responseReader.ReadToEnd();
                            DataResult = JsonConvert.DeserializeObject<dynamic>(strREsp);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
            }


            return DataResult;
        }


        public EndorsementAddFieldResponse UpdateDocumentDetails(ReqDocUploadRequest objReqDocUploadRequest)
        {
            EndorsementAddFieldResponse objEndorsementAddFieldResponse = new EndorsementAddFieldResponse();
            EndorsementDocReq objEndorsementDocReq;
            List<EndorsementDocReq> EndorsementDocReqList = new List<EndorsementDocReq>();
            List<EndorsementDocReq> EndorsementExistngDocList = new List<EndorsementDocReq>();
            string Message = string.Empty;
            int Res = 0;
            if (objReqDocUploadRequest.ReqDocRequestList.Count>0)
            {
                foreach (ReqDocRequest objReqDocRequest in objReqDocUploadRequest.ReqDocRequestList)
                {
                    if (objReqDocRequest.ParentCustDocumentID != 0)
                    {
                        //db.AddInParameter(dbCommand, "@ParentCustDocumentID", DbType.Int32, CustomerBooking.ParentCustDocumentID);
                        objReqDocRequest.FileUrl = "";
                    }
                    else
                    {
                        objReqDocRequest.FileUrl = GetFileUrl(objReqDocUploadRequest.CustomerID, objReqDocUploadRequest.LeadID, objReqDocUploadRequest.ProductID, objReqDocRequest.FileName, objReqDocRequest.FileStream, "Mongo");
                    }
                }
            }
            try
            {
                var xEle = new XElement("ReqDocRequestList",
                                        from objReqDocRequestList in objReqDocUploadRequest.ReqDocRequestList
                                        select new XElement("ReqDocRequestList",
                                                       new XAttribute("DocCategoryID", objReqDocRequestList.DocCategoryID),
                                                       new XAttribute("DocumentID", objReqDocRequestList.DocumentID),
                                                       new XAttribute("DocumentUrl", objReqDocRequestList.FileUrl),
                                                       new XAttribute("ParentCustDocumentID", objReqDocRequestList.ParentCustDocumentID),
                                                        new XAttribute("HostName", objReqDocRequestList.HostName)
                                                   )).ToString();
                con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                con.Open();
                cmd = new SqlCommand("[Customer].[EndorsementDocReqUpload]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CustomerID", DbType.Int64).Value = objReqDocUploadRequest.CustomerID;
                cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objReqDocUploadRequest.LeadID;
                cmd.Parameters.Add("@DetailsID", DbType.Int64).Value = objReqDocUploadRequest.DetailsID;
                cmd.Parameters.Add("@FieldID", DbType.Int16).Value = objReqDocUploadRequest.FieldID;
                cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                cmd.Parameters.Add("@UserId", DbType.Int16).Value = objReqDocUploadRequest.UserID;
               // Res=cmd.ExecuteNonQuery();
                adp = new SqlDataAdapter(cmd);
                ds = new DataSet();
                adp.Fill(ds);


                string innerExpression = "", outerExpression = "", Rules = "*";
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objEndorsementDocReq = new EndorsementDocReq();
                        objEndorsementDocReq.RuleID = Convert.ToInt64(dr["RuleID"]);
                        objEndorsementDocReq.DocumentID = Convert.ToInt16(dr["DocumentID"]);
                        objEndorsementDocReq.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                        objEndorsementDocReq.Document = Convert.ToString(dr["Document"]);
                        objEndorsementDocReq.DocOperator = Convert.ToString(dr["DocOperator"]);
                        objEndorsementDocReq.RuleOperator = Convert.ToString(dr["RuleOperator"]);
                        objEndorsementDocReq.HostName = Convert.ToString(dr["HostName"]);
                        objEndorsementDocReq.DocumentUrl = Convert.ToString(dr["DocumentUrl"]);
                        EndorsementDocReqList.Add(objEndorsementDocReq);
                    }
                    if (ds != null && ds.Tables.Count == 4 && ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[3].Rows)
                        {
                            objEndorsementDocReq = new EndorsementDocReq();

                            objEndorsementDocReq.DocumentID = Convert.ToInt16(dr["DocumentID"]);
                            objEndorsementDocReq.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                            objEndorsementDocReq.Document = Convert.ToString(dr["Document"]);

                            objEndorsementDocReq.HostName = Convert.ToString(dr["HostName"]);
                            objEndorsementDocReq.DocumentUrl = Convert.ToString(dr["DocumentUrl"]);
                            objEndorsementDocReq.CustDocumentID = Convert.ToInt64(dr["CustDocumentID"]);
                            EndorsementExistngDocList.Add(objEndorsementDocReq);
                        }
                    }
                    string filter, docValue;

                    outerExpression = "(";
                    outerExpression = " ( ";
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        filter = "RuleID=" + ds.Tables[1].Rows[i]["RuleID"].ToString();
                        DataRow[] foundRows;
                        foundRows = ds.Tables[2].Select(filter);
                        innerExpression = "(";
                        foreach (DataRow dr in foundRows)
                        {
                            if (dr["DocumentUrl"].ToString() != "0")
                            {
                                innerExpression = innerExpression + " true ";
                            }
                            else
                            {
                                innerExpression = innerExpression + " false ";
                            }
                            innerExpression = innerExpression + dr["DocOperator"].ToString();
                        }
                        innerExpression = innerExpression + " ) ";
                        outerExpression = outerExpression + innerExpression + ds.Tables[1].Rows[i]["RuleOperator"].ToString();

                    }
                    outerExpression = outerExpression + ")";
                }

                System.Data.DataTable table = new System.Data.DataTable();
                table.Columns.Add("", typeof(Boolean));
                table.Columns[0].Expression = outerExpression;
                System.Data.DataRow r = table.NewRow();
                table.Rows.Add(r);
                bool blResult = (Boolean)r[0];
                objEndorsementAddFieldResponse.IsDocReceived = blResult;


                if (Res > 0) { Message = "File uploaded successfully."; }
                
            }
            catch (Exception ex) { return null; }
            if (EndorsementDocReqList.Count == 0)
            {
                objEndorsementAddFieldResponse.IsDocReceived = true;
            }
            objEndorsementAddFieldResponse.DetailsID = objReqDocUploadRequest.DetailsID;
            objEndorsementAddFieldResponse.EndorsementDocReqList = EndorsementDocReqList;
            objEndorsementAddFieldResponse.EndorsementExistngDocList = EndorsementExistngDocList;    
            return objEndorsementAddFieldResponse;           
        }

        public int UploadCustomerDocuments(UploadDocReq objUploadDocReq)
        {
            int Res = 0;
            string FileUrl = "";

            if (objUploadDocReq.ParentCustDocumentID != 0)
            {
                FileUrl = "";
            }
            else
            {
                FileUrl = GetFileUrl(objUploadDocReq.CustomerID, objUploadDocReq.LeadID, objUploadDocReq.ProductID, objUploadDocReq.FileName,
                        objUploadDocReq.FileStream, "Mongo");
            }

            try
            {

                con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                con.Open();
                cmd = new SqlCommand("[Customer].[UpdateCustomerDocuments]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CustomerID", DbType.Int64).Value = objUploadDocReq.CustomerID;
                cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objUploadDocReq.LeadID;
                cmd.Parameters.Add("@DocCategoryID", DbType.Int64).Value = objUploadDocReq.DocCategoryID;
                cmd.Parameters.Add("@DocumentID", DbType.Int16).Value = objUploadDocReq.DocumentID;
                cmd.Parameters.Add("@DocumentUrl", DbType.String).Value = FileUrl;
                cmd.Parameters.Add("@ParentCustDocumentID", DbType.Int64).Value = objUploadDocReq.ParentCustDocumentID;
                cmd.Parameters.Add("@HostName", DbType.Int16).Value = objUploadDocReq.HostName;
                cmd.Parameters.Add("@UserId", DbType.Int16).Value = objUploadDocReq.UserID;
                Res = cmd.ExecuteNonQuery();
            }
            catch { }
            return Res;
        }
        public string UpdateEndorsementCopyDetails(ReqDocUploadRequest objReqDocUploadRequest)
        {
            EndorsementAddFieldResponse objEndorsementAddFieldResponse = new EndorsementAddFieldResponse();
            EndorsementDocReq objEndorsementDocReq;
            List<EndorsementDocReq> EndorsementDocReqList = new List<EndorsementDocReq>();
            List<EndorsementDocReq> EndorsementExistngDocList = new List<EndorsementDocReq>();
            string Message = string.Empty;
            int Res = 0;
            if (objReqDocUploadRequest.ReqDocRequestList.Count > 0)
            {
                foreach (ReqDocRequest objReqDocRequest in objReqDocUploadRequest.ReqDocRequestList)
                {
                    
                        objReqDocRequest.FileUrl = GetFileUrl(objReqDocUploadRequest.CustomerID, objReqDocUploadRequest.LeadID, objReqDocUploadRequest.ProductID, objReqDocRequest.FileName, objReqDocRequest.FileStream, "Mongo");
                   
                }
            }
            try
            {
                var xEle = new XElement("ReqDocRequestList",
                                        from objReqDocRequestList in objReqDocUploadRequest.ReqDocRequestList
                                        select new XElement("ReqDocRequestList",
                                                       new XAttribute("DocCategoryID", objReqDocRequestList.DocCategoryID),
                                                       new XAttribute("DocumentID", objReqDocRequestList.DocumentID),
                                                       new XAttribute("DocumentUrl", objReqDocRequestList.FileUrl)
                                                   )).ToString();
                con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                con.Open();
                cmd = new SqlCommand("[Customer].[EndorsementCopyUpload]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CustomerID", DbType.Int64).Value = objReqDocUploadRequest.CustomerID;
                cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objReqDocUploadRequest.LeadID;
                cmd.Parameters.Add("@DetailsID", DbType.Int64).Value = objReqDocUploadRequest.DetailsID;
                cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                cmd.Parameters.Add("@UserId", DbType.Int16).Value = objReqDocUploadRequest.UserID;
                Res = cmd.ExecuteNonQuery();
                if (Res > 0)
                {
                    Message = "Document uploaded successfully.";
                }
                else
                {
                    Message = "Some Error occured ,Please try again.";

                }
                con.Close();
                return Message;

            }
            catch { return "Some Error occured ,Please try again."; }
        }


        public string GetFileUrl(long CustomerID, long LeadID, int ProductID, string FileName, string FileStream, string UploadMode)
        {
            string DocumentURL = "";
            byte[] data = System.Convert.FromBase64String(FileStream);
            MemoryStream ms = new MemoryStream(data);
            string extPath = Path.GetExtension(FileName).ToLower();
            if (UploadMode == "FTP")
            {
                //StringBuilder MasterFolderPath = new StringBuilder(System.Configuration.ConfigurationManager.AppSettings["CustomerSpecificDocPath"].ToString());
                //StringBuilder MasterFolderPath = new StringBuilder("Customer_Docs\PB.CustomerPolicyDocument_Test\CustId_@CustomerID\@ProductName\@Year\BookingId_@LeadId");
                //MasterFolderPath.Replace("@CustomerID", Convert.ToString(CustomerBooking.CustomerID));
                //MasterFolderPath.Replace("@LeadId", Convert.ToString(CustomerBooking.LeadID));
                //MasterFolderPath.Replace("@Year", Convert.ToString(DateTime.Now.Year));
                //MasterFolderPath.Replace("@ProductName", Convert.ToString(CustomerBooking.ProductName));
                //if (ServerFileUpload.UploadFileToFTP(ms, Convert.ToString(MasterFolderPath), CustomerBooking.FileName))
                //{
                //    CustomerBooking.DocumentURL = MasterFolderPath + "/" + CustomerBooking.FileName;
                //}
            }
            else if (UploadMode == "Mongo")
            {
                var CustID = Convert.ToString(CustomerID);
                var ProdID = Convert.ToString(ProductID);
                var BookID = Convert.ToString(LeadID);
                //string postURL = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SoftCopyUploadURL"]);
                string postURL = ConfigurationManager.AppSettings["UploadDocument"]; //"https://cs.policybazaar.com/cs/repo/uploadPolicyCopy?metaDataJson={";
                //string postURL = "https://apiqa.policybazaar.com/cs/repo/uploadPolicyCopy?metaDataJson={";
                postURL = postURL + "\"leadId\":" + BookID + " , \"customerId\" :" + CustID + " , \"productId\" :" + ProdID + " }";
                dynamic DataResult = GetPostAPIResponse(postURL, ms, FileName);

                if (!String.IsNullOrEmpty(Convert.ToString(DataResult.policyCopyDetails.policyDocUrl.Value)))
                {
                    DocumentURL = Convert.ToString(DataResult.policyCopyDetails.policyDocUrl.Value);
                }
            }
            return DocumentURL;
        }
    }

    public class ServerFileUpload
    {
        public static bool IsFTPWorking()
        {
            return true;
        }

        public static bool FtpDirectoryExists(string directoryPath, string ftpUser, string ftpPassword)
        {
            bool IsExists = true;
            try
            {
                if (!Directory.Exists(directoryPath))
                    IsExists = false;
            }
            catch (Exception ex)
            {

                IsExists = false;
            }
            return IsExists;
        }


        public static bool UploadFileToFTP(Stream file, string MasterServerPath, string FileName)
        {

            string[] SubDirectories;
            string NewMasterServerPath = string.Empty;
            string userName = string.Empty, password = string.Empty;
            
            //NewMasterServerPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FtpDocPath"]);
            NewMasterServerPath = "";
            SubDirectories = MasterServerPath.Replace(NewMasterServerPath, "").Split('/');
            
            for (int i = 1; i < SubDirectories.Length; i++)
            {
                NewMasterServerPath += "/" + SubDirectories[i] + "/";
                //NewMasterServerPath += "\\" + SubDirectories[i] + "\\";
                if (!FtpDirectoryExists(NewMasterServerPath, userName, password))
                    FtpMakeDirectory(NewMasterServerPath, userName, password);
            }

            if ((FtpDirectoryExists(NewMasterServerPath, userName, password)))
            {
                FileStream targetStream = null;
                try
                {
                    string filePath = NewMasterServerPath + "/" + FileName;

                    using (targetStream = new FileStream(filePath, FileMode.Create,
                                          FileAccess.Write, FileShare.None))
                    {
                        //read from the input stream in 65000 byte chunks

                        const int bufferLen = 65000;
                        byte[] buffer = new byte[bufferLen];
                        int count = 0;
                        while ((count = file.Read(buffer, 0, bufferLen)) > 0)
                        {
                            // save to output stream
                            targetStream.Write(buffer, 0, count);
                        }
                        targetStream.Close();
                        file.Close();
                    }
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
            return false;
        }

        public static bool FtpFileExists(string directoryPath, string ftpUser, string ftpPassword)
        {
            bool IsExists = true;
            try
            {
                if (!System.IO.File.Exists(directoryPath))
                    IsExists = false;
            }
            catch (Exception ex)
            {

                IsExists = false;
            }
            return IsExists;
        }

        public static bool FtpMakeDirectory(string MasterServerPath, string ftpUser, string ftpPassword)
        {
            bool IsExists = true;
            try
            {
                Directory.CreateDirectory(MasterServerPath);
            }
            catch (Exception ex)
            {
                IsExists = false;

            }
            return IsExists;
        }
      
        public static Stream GetStreamFromFtp(string MasterServerPath)
        {
            byte[] buff = null;
            try
            {
                FileStream fs = new FileStream(MasterServerPath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                long numBytes = new FileInfo(MasterServerPath).Length;
                buff = br.ReadBytes((int)numBytes);
                Stream sr = new MemoryStream(buff);
                return sr;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                buff = null;
            }
        }

        

    }

}
