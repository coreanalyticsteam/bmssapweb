﻿using Newtonsoft.Json;
using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

using System.Runtime.Serialization.Json;
namespace DataAccessLayer
{
    public class HealthExDL
    {
       
        DataSet ds;
        DataTable dt;
        SqlDataAdapter adp;
        //SqlConnection con;
        SqlCommand cmd;

        public List<LeadDetails> GetAllExLead(ReqMarkExSearch objReqMarkExSearch)
        {
            LeadDetails objLeadDetails;
            List<LeadDetails> objLeadDetailsList = new List<LeadDetails>();  
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetAllExLead]", con);
                    cmd.Parameters.Add("@FilterParam", SqlDbType.VarChar).Value = string.IsNullOrEmpty(objReqMarkExSearch.FilterParam) ? "" : objReqMarkExSearch.FilterParam;
                    cmd.Parameters.Add("@FilterType", SqlDbType.Int).Value = objReqMarkExSearch.FilterType;
                    cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = objReqMarkExSearch.FromDate;
                    cmd.Parameters.Add("@ToDate", SqlDbType.Date).Value = objReqMarkExSearch.ToDate;
                    cmd.Parameters.Add("@InsurerID", SqlDbType.Int).Value = objReqMarkExSearch.InsurerID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objLeadDetails = new LeadDetails();
                            objLeadDetails.OldLeadID = Convert.ToInt64(dr["LeadID"]);
                            objLeadDetails.CustomerID = Convert.ToInt64(dr["CustomerID"]);
                            objLeadDetails.Name = Convert.ToString(dr["Name"]);
                            objLeadDetails.OfferCreatedON = Convert.ToString(dr["OfferCreatedON"]);
                            objLeadDetails.SupplierName = Convert.ToString(dr["SupplierName"]);
                            //objLeadDetails.ProposalNo = Convert.ToString(dr["ProposalNo"]);
                            objLeadDetails.OldProposalNo = Convert.ToString(dr["ProposalNo"]);
                            objLeadDetails.RejectionReason = Convert.ToString(dr["RejectionReason"]);
                            objLeadDetails.PEDInfo = Convert.ToString(dr["PEDInfo"]);
                            objLeadDetails.IsMark = false;
                            objLeadDetailsList.Add(objLeadDetails);
                        }
                    }
                }
            }
            catch
            {

            }
            return objLeadDetailsList;
        }

        public LeadDetails ExLoginGetLeadData(long LeadID)
        {
            LeadDetails objLeadDetails = new LeadDetails();   
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[ExLoginGetLeadData]", con);
                    cmd.Parameters.Add("@LeadID", SqlDbType.BigInt).Value = LeadID;

                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        objLeadDetails = new LeadDetails();
                        objLeadDetails.OldLeadID = Convert.ToInt64(dr["LeadID"]);
                        objLeadDetails.NewLeadID = Convert.ToInt64(dr["NewLeadID"]);
                        objLeadDetails.CustomerID = Convert.ToInt64(dr["CustomerID"]);
                        objLeadDetails.Name = Convert.ToString(dr["Name"]);
                        objLeadDetails.ExLeadType = Convert.ToInt16(dr["ExLeadType"]);
                        objLeadDetails.ExStatusID = Convert.ToInt16(dr["StatusID"]);
                        objLeadDetails.ExID = Convert.ToInt64(dr["ExID"]);
                        //objLeadDetails.ProductID = Convert.ToString(dr["ProductID"]);

                    }
                }
            }
            catch
            {

            }
            return objLeadDetails;
        }


        public List<MasterSupplier> GetSupplierList()
        {
            MasterSupplier objMasterSupplier;
            List<MasterSupplier> objMasterSupplierList = new List<MasterSupplier>();
            //ds = new DataSet();
            //try
            //{
            //    con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
            //    con.Open();
            //    cmd = new SqlCommand("[BMS].[GetAllExLead]", con);                 
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    adp = new SqlDataAdapter(cmd);
            //    adp.Fill(ds);

            //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in ds.Tables[0].Rows)
            //        {
            //            objMasterSupplier = new MasterSupplier();
            //            objMasterSupplier.InsurerID = Convert.ToInt64(dr["LeadID"]);                        
            //            objMasterSupplier.InsurerName = Convert.ToString(dr["Name"]);
            //            objMasterSupplierList.Add(objMasterSupplier);
            //        }
            //    }
            //}
            //catch
            //{

            //}

            objMasterSupplier = new MasterSupplier();
            objMasterSupplier.InsurerID = 0;
            objMasterSupplier.InsurerName = "Select Insurer";
            objMasterSupplierList.Add(objMasterSupplier);
            objMasterSupplier = new MasterSupplier();
            objMasterSupplier.InsurerID = 1;
            objMasterSupplier.InsurerName = "Reliance General Insurance Co. Ltd.";
            objMasterSupplierList.Add(objMasterSupplier);

            objMasterSupplier = new MasterSupplier();
            objMasterSupplier.InsurerID = 2;
            objMasterSupplier.InsurerName = "HDFC ERGO General Insurance Co. Ltd.";
            objMasterSupplierList.Add(objMasterSupplier);
            objMasterSupplier = new MasterSupplier();
            objMasterSupplier.InsurerID = 3;
            objMasterSupplier.InsurerName = "ICICI Lombard General Insurance Company Ltd.";
            objMasterSupplierList.Add(objMasterSupplier);
            objMasterSupplier = new MasterSupplier();
            objMasterSupplier.InsurerID = 4;
            objMasterSupplier.InsurerName = "Cholamandalam MS General Insurance Co. Ltd.";
            objMasterSupplierList.Add(objMasterSupplier);

            return objMasterSupplierList;
        }


        public List<SupplierPlanDetails> GetSupplierPlanDetails(string LeadID)
        {          
                                        
            List<SupplierPlanDetails> objMasterSupplierList = new List<SupplierPlanDetails>();
            try
            {
                WebRequest request = WebRequest.Create(ConnectionClass.MasterSupplierPlanForEx() + LeadID);
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();
                //// Display the status.
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                //// Get the stream containing content returned by the server.
                //Stream dataStream = response.GetResponseStream();
                //// Open the stream using a StreamReader for easy access.
                //StreamReader reader = new StreamReader(dataStream);
                //// Read the content.
                //string responseFromServer = reader.ReadToEnd();
                //// Display the content.
                //Console.WriteLine(responseFromServer);
                //// Clean up the streams and the response.
                //reader.Close();
                //response.Close();


                // HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConnectionClass.MasterSupplierPlanForEx() + LeadID);
                // request.Method = "GET";
                // request.ContentType = "application/json";
                // request.KeepAlive = false;
                //// var buffer = Encoding.ASCII.GetBytes(LeadID);
                // //request.ContentLength = buffer.Length;
                // var postData = request.GetRequestStream();
                //// postData.Write(buffer, 0, buffer.Length);
                // postData.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream())
                    {
                        if (webStream != null)
                        {
                            using (StreamReader responseReader = new StreamReader(webStream))
                            {
                                string strREsp = responseReader.ReadToEnd();
                                objMasterSupplierList = JsonConvert.DeserializeObject<List<SupplierPlanDetails>>(strREsp);
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                }
            }
            catch { }

            return objMasterSupplierList;

        }

        public int MarkLeadForHealthEx(ReqMarkHealthEx objReqMarkHealthEx)
        {
            int res = 0;
            try
            {
                ds = new DataSet();
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[MarkLeadForHealthEx]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var xEle = new XElement("ReqMarkHealthEx",
                                            from objLeadDetailsList in objReqMarkHealthEx.LeadDetailsList
                                            select new XElement("ReqMarkHealthEx",
                                                           new XAttribute("OldLeadID", objLeadDetailsList.OldLeadID)
                                                       )).ToString();

                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = objReqMarkHealthEx.UserID;
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    //cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    //res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    con.Close();
                }
            }
            catch { }
            return res;
        }
        public bool MarkNewLeadForHealthEx(ReqMarkNewLead objReqMarkNewLead)
        {
            int res = 0;
            bool result = false;
            try
            {
                ds = new DataSet();
               // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[MarkNewLeadForHealthEx]", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = objReqMarkNewLead.UserID;
                    cmd.Parameters.Add("@EnquiryID", DbType.Int64).Value = objReqMarkNewLead.EnquiryID;
                    cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objReqMarkNewLead.LeadID;
                    cmd.Parameters.Add("@ItemID", DbType.Int64).Value = objReqMarkNewLead.ItemID;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    //con.Close();
                }
            }
            catch { }
            if (res > 0)
                result = true;
            return result;
        }

        public ResUploadCaseToInsurer GetExLeadDetailsForCaseSend(long ExID)
        {
            ResUploadCaseToInsurer objResUploadCaseToInsurer = new ResUploadCaseToInsurer();
            
            List<MasterDocument> MasterDocumentList = new List<MasterDocument>();
            MasterDocument objMasterDocument;
           
            List<MemberDetails> MemberDetailsList = new List<MemberDetails>();
            MemberDetails objMemberDetails;
            
            

            List<SelectedInsurerDetails> SelectedInsurerDetailsList = new List<SelectedInsurerDetails>();
            SelectedInsurerDetails objSelectedInsurerDetails;
           
            List<MemberDocumentDetails> MemberDocumentDetailsList = new List<MemberDocumentDetails>();
            MemberDocumentDetails objMemberDocumentDetails;

            CustomerLeadDetails objCustomerLeadDetails = new CustomerLeadDetails();

            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetExLeadDetailsForCaseSend]", con);
                    cmd.Parameters.Add("@ExID", SqlDbType.BigInt).Value = ExID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0)
                    {

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            //DocumentId,Document
                            objMasterDocument = new MasterDocument();
                            objMasterDocument.DocumentId = Convert.ToInt16(dr["DocumentId"].ToString());
                            objMasterDocument.Document = dr["Document"].ToString();
                            MasterDocumentList.Add(objMasterDocument);
                        }
                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            //ExID,InsurerID,InsurerName,PlanID,PlanName,SumInsured,Term,BasePremium
                            objSelectedInsurerDetails = new SelectedInsurerDetails();
                            objSelectedInsurerDetails.ExID = Convert.ToInt64(dr["ExID"].ToString());
                            objSelectedInsurerDetails.InsurerID = Convert.ToInt16(dr["InsurerID"].ToString());
                            objSelectedInsurerDetails.InsurerName = dr["InsurerName"].ToString();
                            objSelectedInsurerDetails.PlanID = Convert.ToInt16(dr["PlanID"].ToString());
                            objSelectedInsurerDetails.PlanName = dr["PlanName"].ToString();
                            objSelectedInsurerDetails.SumInsured = dr["SumInsured"].ToString();
                            objSelectedInsurerDetails.Term = dr["Term"].ToString();
                            objSelectedInsurerDetails.BasePremium = dr["BasePremium"].ToString();
                            SelectedInsurerDetailsList.Add(objSelectedInsurerDetails);
                        }
                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            //SNO,DocumentName,MemberID,MemberName,DocURL,Comments
                            objMemberDocumentDetails = new MemberDocumentDetails();
                            objMemberDocumentDetails.SNO = Convert.ToInt64(dr["SNO"].ToString());
                            objMemberDocumentDetails.DocumentName = dr["DocumentName"].ToString();
                            objMemberDocumentDetails.MemberID = dr["MemberID"].ToString();
                            objMemberDocumentDetails.MemberName = dr["MemberName"].ToString();
                            objMemberDocumentDetails.DocURL = dr["DocURL"].ToString();
                            objMemberDocumentDetails.Comments = dr["Comments"].ToString();
                            MemberDocumentDetailsList.Add(objMemberDocumentDetails);
                        }

                        foreach (DataRow dr in ds.Tables[3].Rows)
                        {

                            objResUploadCaseToInsurer.StatusID = Convert.ToInt16(dr["StatusID"].ToString());
                            objResUploadCaseToInsurer.StatusName = dr["Remarks"].ToString();

                        }

                        if (ds.Tables[4].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[4].Rows[0];
                            objCustomerLeadDetails.OldLeadID = Convert.ToInt64(dr["OldLeadID"].ToString());
                            objCustomerLeadDetails.NewLeadID = Convert.ToInt64(dr["NewLeadID"].ToString());
                            objCustomerLeadDetails.CustomerID = Convert.ToInt64(dr["CustomerID"].ToString());
                            objCustomerLeadDetails.ProductID = Convert.ToInt16(dr["ProductID"].ToString());
                            objCustomerLeadDetails.EnquiryID = Convert.ToInt64(dr["EnquiryID"].ToString());
                            objCustomerLeadDetails.EncryptEnquiryID = Convert.ToBase64String(Encoding.UTF8.GetBytes(dr["EnquiryID"].ToString())).ToString();
                            objCustomerLeadDetails.Name = dr["Name"].ToString();
                            objCustomerLeadDetails.MobileNo = dr["MobileNo"].ToString();
                            objCustomerLeadDetails.EmailID = dr["EmailID"].ToString();
                        }
                    }
                    //service for member asmit
                    objResUploadCaseToInsurer.ProposerDetails = GetMemberDetails(objCustomerLeadDetails.EnquiryID);
                    //foreach (DataRow dr in ds.Tables[0].Rows)
                    //{
                    //    //PolicyInsuredID,CustPolicyID,CustMemId,IsPrimary,IsActive,InsuredName,DOB,Gender,RelationWithProposerID,R.RelationType,'' PED
                    //    objMemberDetails = new MemberDetails();
                    //    objMemberDetails.CustMemId = Convert.ToInt64(dr["PolicyInsuredID"].ToString());
                    //    objMemberDetails.IsActive = dr["IsActive"].ToString();
                    //    objMemberDetails.InsuredName = dr["InsuredName"].ToString();
                    //    objMemberDetails.DOB = dr["DOB"].ToString();
                    //    objMemberDetails.Gender = dr["Gender"].ToString();
                    //    objMemberDetails.RelationType = dr["RelationType"].ToString();
                    //    objMemberDetails.PED = dr["PED"].ToString();
                    //    MemberDetailsList.Add(objMemberDetails);

                    //}
                }
               
            }
            catch { }

            List<SupplierPlanDetails> SupplierPlanDetailsList = new List<SupplierPlanDetails>();
            SupplierPlanDetailsList = GetSupplierPlanDetails(Convert.ToString(objCustomerLeadDetails.OldLeadID));

            objResUploadCaseToInsurer.CustomerLead = objCustomerLeadDetails;
            objResUploadCaseToInsurer.MasterDocumentList = MasterDocumentList;
            objResUploadCaseToInsurer.MemberDetailsList = MemberDetailsList;
            objResUploadCaseToInsurer.SupplierPlanDetailsList = SupplierPlanDetailsList;
            objResUploadCaseToInsurer.MemberDocumentDetailsList = MemberDocumentDetailsList;
            objResUploadCaseToInsurer.SelectedInsurerDetailsList = SelectedInsurerDetailsList;
            return objResUploadCaseToInsurer;
        }

        public int AddSupplierPlanForEx(PostPlanSelectionData objPostPlanSelectionData)
        {
            //UpdateNewLeadDetailsInHealthEx(objPostPlanSelectionData.LeadID, objPostPlanSelectionData.UserID);
            HealthEXDetails objHealthEXDetails = new HealthEXDetails();
            objHealthEXDetails = MarkHealthServiceWithNewLead(Convert.ToString(objPostPlanSelectionData.LeadID));

            List<HealthExSelections> HealthExSelectionsList= UpdateSelectedPlanOnCJ(objPostPlanSelectionData, objHealthEXDetails.NewEnquiryId);
            int res = 0;
            try
            {
                ds = new DataSet();
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AddSupplierPlanForEx]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var xEle = new XElement("PostInsurerPlanData",
                                            from objHealthExSelections in HealthExSelectionsList
                                            select new XElement("PostInsurerPlanData",
                                                           new XAttribute("InsurerID", objHealthExSelections.SupplierId),
                                                           new XAttribute("PlanID", objHealthExSelections.PlanId),
                                                           new XAttribute("InsurerName", objHealthExSelections.SupplierName),
                                                           new XAttribute("PlanName", objHealthExSelections.PlanName),
                                                           new XAttribute("Term", objHealthExSelections.Term),
                                                           new XAttribute("SumInsured", objHealthExSelections.Suminsured),
                                                           new XAttribute("BasePremium", objHealthExSelections.Premium),
                                                           new XAttribute("SelectionID", objHealthExSelections.SelectionID)
                                                       )).ToString();

                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = objPostPlanSelectionData.UserID;
                    cmd.Parameters.Add("@ExID", DbType.Int64).Value = objPostPlanSelectionData.ExID;
                    cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objPostPlanSelectionData.LeadID;
                    cmd.Parameters.Add("@NewLeadID", SqlDbType.BigInt).Value = objHealthEXDetails.NewLeadID;
                    cmd.Parameters.Add("@ItemID", SqlDbType.BigInt).Value = objHealthEXDetails.Identifiers.NeedID;
                    cmd.Parameters.Add("@EnquiryID", SqlDbType.BigInt).Value = objHealthEXDetails.NewEnquiryId;
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    //cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    //res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    //con.Close();
                }

            }
            catch { }
            return res;
        }

        public bool InsertSupplierPlanNewHEx(List<HealthExSelections> HealthExSelectionsList)
        {                       
            int res = 0;
            bool result = false;
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    ds = new DataSet();
                    // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[InsertSupplierPlanNewHEx]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var xEle = new XElement("PostInsurerPlanData",
                                            from objHealthExSelections in HealthExSelectionsList
                                            select new XElement("PostInsurerPlanData",
                                                           new XAttribute("InsurerID", objHealthExSelections.SupplierId),
                                                           new XAttribute("PlanID", objHealthExSelections.PlanId),
                                                           new XAttribute("InsurerName", objHealthExSelections.SupplierName),
                                                           new XAttribute("PlanName", objHealthExSelections.PlanName),
                                                           new XAttribute("Term", objHealthExSelections.Term),
                                                           new XAttribute("SumInsured", objHealthExSelections.Suminsured),
                                                           new XAttribute("BasePremium", objHealthExSelections.Premium),
                                                           new XAttribute("SelectionID", objHealthExSelections.SelectionID)
                                                       )).ToString();
                    cmd.Parameters.Add("@EnquiryId", DbType.Int64).Value = HealthExSelectionsList[0].EnquiryId;
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    res = cmd.ExecuteNonQuery();
                    // con.Close();
                }

            }
            catch { }
            if (res > 0)
                result = true;
            return result;
        }

        public int AddMemberDouments(PostMemberDocumentDetails objPostMemberDocumentDetails)
        {
            int res = 0;
            try
            {
                FileUpload objFileUpload = new FileUpload();
                foreach (MemberDocumentDetails objMemberDocumentDetails in objPostMemberDocumentDetails.MemberDocumentDetailsList)
                {
                    if (objMemberDocumentDetails.FileData != "")
                    {
                        objMemberDocumentDetails.DocURL = objFileUpload.GetFileUrl(objPostMemberDocumentDetails.CustomerID, objPostMemberDocumentDetails.LeadID, objPostMemberDocumentDetails.ProductID, objMemberDocumentDetails.FileName, objMemberDocumentDetails.FileData, "Mongo");
                    }
                    else
                    {
                        objMemberDocumentDetails.DocURL = "";
                    }
                }
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    ds = new DataSet();
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AddMemberDouments]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var xEle = new XElement("MemberDocumentDetailsList",
                                            from MemberDocumentDetailsList in objPostMemberDocumentDetails.MemberDocumentDetailsList
                                            select new XElement("MemberDocumentDetailsList",
                                                // new XAttribute("InsurerID", MemberDocumentDetailsList.InsurerID),
                                                           new XAttribute("DocumentID", MemberDocumentDetailsList.DocumentID),
                                                           new XAttribute("DocumentName", MemberDocumentDetailsList.DocumentName),
                                                           new XAttribute("MemberID", MemberDocumentDetailsList.MemberID),
                                                           new XAttribute("MemberName", MemberDocumentDetailsList.MemberName),
                                                           new XAttribute("DocURL", MemberDocumentDetailsList.DocURL),
                                                // new XAttribute("FileData", MemberDocumentDetailsList.FileData),
                                                           new XAttribute("Comments", MemberDocumentDetailsList.Comments)
                                                       )).ToString();

                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = objPostMemberDocumentDetails.UserID;
                    cmd.Parameters.Add("@Type", DbType.Int16).Value = objPostMemberDocumentDetails.Type;
                    //cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objPostMemberDocumentDetails.LeadID;
                    cmd.Parameters.Add("@ExID", DbType.Int64).Value = objPostMemberDocumentDetails.ExID;
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    //cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    //res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    //con.Close();
                }

            }
            catch (Exception ex)
            {
            }
            return res;
        }

        public List<LeadDetails> GetLeadsForPBDashboard(ReqMarkExSearch objReqMarkExSearch)
        {
            LeadDetails objLeadDetails;
            List<LeadDetails> objLeadDetailsList = new List<LeadDetails>();
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetLeadsForPBDashboard]", con);
                    cmd.Parameters.Add("@FilterParam", SqlDbType.VarChar).Value = string.IsNullOrEmpty(objReqMarkExSearch.FilterParam) ? "" : objReqMarkExSearch.FilterParam;
                    cmd.Parameters.Add("@FilterType", SqlDbType.Int).Value = objReqMarkExSearch.FilterType;
                    cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = objReqMarkExSearch.FromDate;
                    cmd.Parameters.Add("@ToDate", SqlDbType.Date).Value = objReqMarkExSearch.ToDate;
                    cmd.Parameters.Add("@Status", SqlDbType.Int).Value = objReqMarkExSearch.Status;
                    cmd.Parameters.Add("@KeyMatrix", SqlDbType.Int).Value = objReqMarkExSearch.KeyMatrix;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objLeadDetails = new LeadDetails();
                            objLeadDetails.ExID = Convert.ToInt64(dr["ExID"]);
                            objLeadDetails.Name = Convert.ToString(dr["Name"]);
                            objLeadDetails.ExLeadType = Convert.ToInt16(dr["ExLeadType"]);
                            objLeadDetails.OldLeadID = Convert.ToInt64(dr["OldLeadID"]);
                            objLeadDetails.NewLeadID = Convert.ToInt64(dr["NewLeadID"]);
                            objLeadDetails.CustomerID = Convert.ToInt64(dr["CustomerID"]);
                            objLeadDetails.ProposalNo = Convert.ToString(dr["ProposalNo"]);
                            objLeadDetails.ExStatusID = Convert.ToInt16(dr["StatusID"]);
                            objLeadDetails.Remarks = Convert.ToString(dr["Remarks"]);
                            objLeadDetails.HealthExMarkDate = Convert.ToString(dr["HealthExMarkDate"]);
                            objLeadDetails.ALL_PENDING = Convert.ToInt16(dr["ALL_PENDING"]);
                            objLeadDetails.ACCEPTED = Convert.ToInt16(dr["ACCEPTED"]);
                            objLeadDetails.REJECTED = Convert.ToInt16(dr["REJECTED"]);
                            objLeadDetails.ACCEPT_COND = Convert.ToInt16(dr["ACCEPT_COND"]);
                            objLeadDetails.ADD_INFO = Convert.ToInt16(dr["ADD_INFO"]);
                            objLeadDetails.Customer_Acc = Convert.ToInt16(dr["Customer_Acc"]);
                            objLeadDetails.Customer_Rej = Convert.ToInt16(dr["Customer_Rej"]);
                            //objLeadDetails.UploadDate = Convert.ToDateTime(dr["UploadDate"]);
                            //objLeadDetails.PlanName = Convert.ToString(dr["PlanName"]);
                            //objLeadDetails.InsurerName = Convert.ToString(dr["InsurerName"]);
                            //objLeadDetails.BasePremium = Convert.ToInt16(dr["BasePremium"]);

                            objLeadDetails.IsMark = false;
                            objLeadDetailsList.Add(objLeadDetails);
                        }
                    }
                }
            }
            catch
            {

            }
            return objLeadDetailsList;
        }

        public ResInsurerUnderwriting GetExLeadDetailsForInsurer(long ExID, long SelectionID, int InsurerID, int UserID)
        {              

            ResInsurerUnderwriting objResInsurerUnderwriting = new ResInsurerUnderwriting();            

            List<MemberDetails> MemberDetailsList = new List<MemberDetails>();
            MemberDetails objMemberDetails;
            InsurerAdditionalInfoDeatils objInsurerAdditionalInfoDeatils;
            CopayDetails objCopayDetails;
            LoadingDetails objLoadingDetails;
            List<InsurerAdditionalInfoDeatils> objInsurerAdditionalInfoDeatilsList = new List<InsurerAdditionalInfoDeatils>();
            List<CopayDetails> objCopayDetailsList = new List<CopayDetails>();
            List<LoadingDetails> objLoadingDetailsList = new List<LoadingDetails>();
            SelectedInsurerDetails objSelectedInsurerDetails;
            objSelectedInsurerDetails = new SelectedInsurerDetails();

            List<MemberDocumentDetails> MemberDocumentDetailsList = new List<MemberDocumentDetails>();
            MemberDocumentDetails objMemberDocumentDetails;
            InsurerAction objInsurerAction = new InsurerAction();
            CustomerLeadDetails objCustomerLeadDetails = new CustomerLeadDetails();
            ds = new DataSet();
            try
            {
                
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();

                    cmd = new SqlCommand("[BMS].[GetExLeadDetailsForInsurer]", con);
                    cmd.Parameters.Add("@ExID", SqlDbType.BigInt).Value = ExID;
                    cmd.Parameters.Add("@SelectionID", SqlDbType.BigInt).Value = SelectionID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0)
                    {

                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            objSelectedInsurerDetails.ExID = Convert.ToInt64(dr["ExID"].ToString());
                            objSelectedInsurerDetails.InsurerID = Convert.ToInt16(dr["InsurerID"].ToString());
                            objSelectedInsurerDetails.InsurerName = dr["InsurerName"].ToString();
                            objSelectedInsurerDetails.PlanID = Convert.ToInt16(dr["PlanID"].ToString());
                            objSelectedInsurerDetails.PlanName = dr["PlanName"].ToString();
                            objSelectedInsurerDetails.SumInsured = dr["SumInsured"].ToString();
                            objSelectedInsurerDetails.Term = dr["Term"].ToString();
                            objSelectedInsurerDetails.BasePremium = dr["BasePremium"].ToString();

                            objResInsurerUnderwriting.IsLoadPremium = Convert.ToBoolean(dr["IsLoadPremium"].ToString());
                            objResInsurerUnderwriting.FinalPremium = Convert.ToDecimal(dr["FinalPremium"].ToString());
                            objResInsurerUnderwriting.IsDiseaseExclusion = Convert.ToBoolean(dr["IsDiseaseExlusion"].ToString());

                            objResInsurerUnderwriting.IsDiseaseExclusionTemp = Convert.ToBoolean(dr["IsDiseaseExlusionTemp"].ToString());
                            objResInsurerUnderwriting.IsDiseaseExclusionPerm = Convert.ToBoolean(dr["IsDiseaseExlusionPerm"].ToString());
                            objResInsurerUnderwriting.DiseaseExclusionPerm = dr["DiseaseExclusionPerm"].ToString();
                            objResInsurerUnderwriting.DiseaseExclusionTemp = dr["DiseaseExclusionTemp"].ToString();

                            objResInsurerUnderwriting.IsCoPay = Convert.ToBoolean(dr["IsCoPay"].ToString());
                            objResInsurerUnderwriting.CoPay = Convert.ToInt16(dr["CoPay"].ToString());
                            objResInsurerUnderwriting.InsurerAction = Convert.ToInt16(dr["InsurerAction"].ToString());
                            objResInsurerUnderwriting.IsMemberExclusion = Convert.ToBoolean(dr["IsMemberExclusion"].ToString());
                            objResInsurerUnderwriting.InsurerComment = dr["InsurerComment"].ToString();

                            objResInsurerUnderwriting.PremiumPaid = Convert.ToString(dr["PremiumPaid"]);

                            objResInsurerUnderwriting.PremLoadingPerc = Convert.ToInt16(dr["PremLoadingPerc"].ToString());
                            objResInsurerUnderwriting.PremLoadingAmount =Convert.ToDecimal( dr["PremLoadingAmount"].ToString());
                            objResInsurerUnderwriting.CopayType = Convert.ToInt16(dr["CopayType"].ToString());
                            objResInsurerUnderwriting.LoadingType = Convert.ToInt16(dr["LoadingType"].ToString()); 
                        }

                        foreach (DataRow dr in ds.Tables[2].Rows)
                        {
                            //SNO,DocumentName,MemberID,MemberName,DocURL,Comments
                            objMemberDocumentDetails = new MemberDocumentDetails();
                            objMemberDocumentDetails.SNO = Convert.ToInt64(dr["SNO"].ToString());
                            objMemberDocumentDetails.DocumentName = dr["DocumentName"].ToString();
                            objMemberDocumentDetails.MemberID = dr["MemberID"].ToString();
                            objMemberDocumentDetails.MemberName = dr["MemberName"].ToString();
                            objMemberDocumentDetails.DocURL = dr["DocURL"].ToString();
                            objMemberDocumentDetails.Comments = dr["Comments"].ToString();
                            MemberDocumentDetailsList.Add(objMemberDocumentDetails);
                        }

                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[3].Rows[0];
                            objResInsurerUnderwriting.StatusID = Convert.ToInt16(dr["StatusID"].ToString());
                            objResInsurerUnderwriting.StatusName = dr["Remarks"].ToString();
                        }
                        if (ds.Tables[4].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[4].Rows[0];
                            objResInsurerUnderwriting.InsurerStatusID = Convert.ToInt16(dr["StatusID"].ToString());
                            objResInsurerUnderwriting.InsurerOldStatusID = Convert.ToInt16(dr["StatusID"].ToString());
                            objResInsurerUnderwriting.InsurerRemarks = dr["Remarks"].ToString();
                        }

                        if (ds.Tables[5].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[5].Rows[0];
                            objCustomerLeadDetails.OldLeadID = Convert.ToInt64(dr["OldLeadID"].ToString());
                            objCustomerLeadDetails.NewLeadID = Convert.ToInt64(dr["NewLeadID"].ToString());
                            objCustomerLeadDetails.CustomerID = Convert.ToInt64(dr["CustomerID"].ToString());
                            objCustomerLeadDetails.ProductID = Convert.ToInt16(dr["ProductID"].ToString());
                            objCustomerLeadDetails.EnquiryID = Convert.ToInt64(dr["EnquiryID"].ToString());
                            objCustomerLeadDetails.EncryptEnquiryID = Convert.ToBase64String(Encoding.UTF8.GetBytes(dr["EnquiryID"].ToString())).ToString();
                            objCustomerLeadDetails.Name = dr["Name"].ToString();
                            objCustomerLeadDetails.MobileNo = dr["MobileNo"].ToString();
                            objCustomerLeadDetails.EmailID = dr["EmailID"].ToString();
                        }
                        if (ds.Tables.Count >= 7)
                        {
                            if (ds.Tables[6].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[6].Rows)
                                {
                                    objCopayDetails = new CopayDetails();
                                    objCopayDetails.Copay = Convert.ToInt16(dr["Copay"]);
                                    objCopayDetails.MemberID = Convert.ToInt64(dr["MemberID"]);
                                    objCopayDetails.MemberName = Convert.ToString(dr["MemberName"]);
                                    objCopayDetailsList.Add(objCopayDetails);
                                }
                                objResInsurerUnderwriting.CopayDetailsList = objCopayDetailsList;
                                //objResInsurerUnderwriting.InsurerAdditionalInfoDeatilsList
                                //objResInsurerUnderwriting.CopayDetailsList
                            }
                        }
                        if (ds.Tables.Count >= 8)
                        {
                            if (ds.Tables[7].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[7].Rows)
                                {
                                    objInsurerAdditionalInfoDeatils = new InsurerAdditionalInfoDeatils();
                                    objInsurerAdditionalInfoDeatils.MemberID = Convert.ToInt64(dr["MemberID"]);
                                    objInsurerAdditionalInfoDeatils.MemberName = Convert.ToString(dr["MemberName"]);
                                    objInsurerAdditionalInfoDeatils.Comments = Convert.ToString(dr["Comments"]);
                                    objInsurerAdditionalInfoDeatils.InfoType = Convert.ToInt16(dr["InfoType"]);
                                    objInsurerAdditionalInfoDeatils.InfoName = Convert.ToString(dr["InfoName"]);
                                    objInsurerAdditionalInfoDeatilsList.Add(objInsurerAdditionalInfoDeatils);
                                }
                                objResInsurerUnderwriting.InsurerAdditionalInfoDeatilsList = objInsurerAdditionalInfoDeatilsList;
                            }
                        }
                        if (ds.Tables.Count >= 9)
                        {
                            if (ds.Tables[8].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[8].Rows)
                                {
                                    objLoadingDetails = new LoadingDetails();
                                    objLoadingDetails.MemberLoading = Convert.ToInt16(dr["Loading"]);
                                    objLoadingDetails.MemberID = Convert.ToInt64(dr["MemberID"]);
                                    objLoadingDetails.MemberName = Convert.ToString(dr["MemberName"]);
                                    objLoadingDetailsList.Add(objLoadingDetails);
                                }
                                objResInsurerUnderwriting.LoadingDetailsList = objLoadingDetailsList;
                            }
                        }
                        objResInsurerUnderwriting.ProposerDetails = GetMemberDetails(objCustomerLeadDetails.EnquiryID);

                        foreach (Coveredmember objCoveredMembers in objResInsurerUnderwriting.ProposerDetails.CoveredMembers)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                if (objCoveredMembers.MemberId == Convert.ToInt64(dr["MemberID"].ToString()))
                                {
                                    objCoveredMembers.IsExcluded = true;
                                    objCoveredMembers.ExclusionReason = dr["ExclusionReason"].ToString();
                                }
                                //objMemberDetails.CustMemId = Convert.ToInt64(dr["PolicyInsuredID"].ToString());
                                //objMemberDetails.IsActive = dr["IsActive"].ToString();
                                //objMemberDetails.InsuredName = dr["InsuredName"].ToString();
                                //objMemberDetails.DOB = dr["DOB"].ToString();
                                //objMemberDetails.Gender = dr["Gender"].ToString();
                                //objMemberDetails.RelationType = dr["RelationType"].ToString();
                                //objMemberDetails.PED = dr["PED"].ToString();
                                //objMemberDetails.IsExcluded = Convert.ToBoolean(Convert.ToInt32(dr["IsExcluded"].ToString()));
                                //objMemberDetails.ExclusionReason = dr["ExclusionReason"].ToString();
                                // MemberDetailsList.Add(objMemberDetails);
                            }
                        }
                    }

                }
            }
            catch { }
            objResInsurerUnderwriting.InsurerRemarksDetailsList = GetInsurerRemarks(UserID.ToString(), InsurerID.ToString(), ExID.ToString(), "0", SelectionID.ToString());
            if (objResInsurerUnderwriting.InsurerAction == 2)
            {
                objInsurerAction.Accept = true;
            }
            else if (objResInsurerUnderwriting.InsurerAction == 3)
            {
                objInsurerAction.Reject = true;
            }
            else if (objResInsurerUnderwriting.InsurerAction == 4)
            {
                objInsurerAction.AcceptWithConditions = true;
            }
            else if (objResInsurerUnderwriting.InsurerAction == 5 || objResInsurerUnderwriting.InsurerAction == 6)
            {
                objInsurerAction.AdditionalInfoRequired = true;
            }
            //objResInsurerUnderwriting.CoveredMembersList = GetMemberDetails(objCustomerLeadDetails.EnquiryID);
           
            objResInsurerUnderwriting.CustomerLead = objCustomerLeadDetails;
            objResInsurerUnderwriting.InsurerActionDetails = objInsurerAction;
           
           

            objResInsurerUnderwriting.MemberDetailsList = MemberDetailsList;
            objResInsurerUnderwriting.InsurerDetails = objSelectedInsurerDetails;
            objResInsurerUnderwriting.MemberDocumentDetailsList = MemberDocumentDetailsList;    
            return objResInsurerUnderwriting;
        }

        public int UpdateInsurerAction(PostInsurerActionData objPostInsurerActionData)
        {
            Guid id = Guid.NewGuid();

            bool res = false;
            if (objPostInsurerActionData.IsMemberExclusion)
            {
                List<MembersRejPost> objMembersRejPostList = new List<MembersRejPost>();
                MembersRejPost objMembersRejPost;
                if (objPostInsurerActionData.MemberDetailsList.Count > 0)
                {
                    foreach (MemberDetails objMemberDetails in objPostInsurerActionData.MemberDetailsList)
                    {
                        objMembersRejPost = new MembersRejPost();
                        objMembersRejPost.EnquiryID = objPostInsurerActionData.EnquiryID;
                        objMembersRejPost.SelectionID = objPostInsurerActionData.SelectionID;
                        objMembersRejPost.MemberId = objMemberDetails.CustMemId;
                        objMembersRejPostList.Add(objMembersRejPost);
                    }
                    res = JsonConvert.DeserializeObject<bool>(MakeRequest(ConnectionClass.RejectMembersLink(), objMembersRejPostList, "POST", "application/json"));
                } 
            }

            if ((objPostInsurerActionData.IsMemberExclusion && res) || (!objPostInsurerActionData.IsMemberExclusion && !res))
            {
                res = false;
                PostInsurerResSelection objPostInsurerResSelection = new PostInsurerResSelection();
                objPostInsurerResSelection.Remarks = objPostInsurerActionData.Remarks;
                List<ConditionDetails> ConditionDetailsList = new List<ConditionDetails>();
                ConditionDetails objConditionDetails;
                
                AdditionalDocDetails objAdditionalDocDetails;
                List<ExternalCopayDetails> ExternalCopayDetailsList = new List<ExternalCopayDetails>();
                List<ExternalLoadingDetails> ExternalLoadingDetailsList = new List<ExternalLoadingDetails>();
                ExternalCopayDetails objExternalCopayDetails;
                ExternalLoadingDetails objExternalLoadingDetails;
                LoadingTextDetail LoadingText = new LoadingTextDetail();
                LoadingText.BaseLoading = Convert.ToDecimal(0);
                LoadingText.MemberLoading = new List<ExternalLoadingDetails>();

                if (objPostInsurerActionData.InsurerAdditionalInfoDeatilsList != null && objPostInsurerActionData.InsurerAdditionalInfoDeatilsList.Count > 0)
                {  
                    var DistinctItems = objPostInsurerActionData.InsurerAdditionalInfoDeatilsList.GroupBy(x => x.MemberName).Select(y => y.First());
                    foreach (InsurerAdditionalInfoDeatils _InsurerAdditionalInfoDeatils in DistinctItems)
                    {
                        objConditionDetails = new ConditionDetails();
                        objConditionDetails.MemberName = _InsurerAdditionalInfoDeatils.MemberName;
                        objConditionDetails.Relation = _InsurerAdditionalInfoDeatils.Relation;
                        List<AdditionalDocDetails> RemarkList = new List<AdditionalDocDetails>();
                        foreach (InsurerAdditionalInfoDeatils _InsurerAdditionalInfoDeatilsRemarks in objPostInsurerActionData.InsurerAdditionalInfoDeatilsList)
                        {
                            if (objConditionDetails.MemberName == _InsurerAdditionalInfoDeatilsRemarks.MemberName)
                            {
                                objAdditionalDocDetails = new AdditionalDocDetails();
                                objAdditionalDocDetails.AdditionalInfoName = _InsurerAdditionalInfoDeatilsRemarks.InfoName;
                                objAdditionalDocDetails.Remark = _InsurerAdditionalInfoDeatilsRemarks.Comments;
                                RemarkList.Add(objAdditionalDocDetails);
                            }
                        }
                        objConditionDetails.RemarkList = RemarkList;
                        ConditionDetailsList.Add(objConditionDetails);
                    }
                    objPostInsurerResSelection.Conditions = JsonConvert.SerializeObject(ConditionDetailsList);
                }
                if (objPostInsurerActionData.CopayDetailsList != null && objPostInsurerActionData.CopayDetailsList.Count > 0)
                {
                    foreach (CopayDetails _CopayDetails in objPostInsurerActionData.CopayDetailsList)
                    {
                        objExternalCopayDetails = new ExternalCopayDetails();
                        objExternalCopayDetails.Copay = Convert.ToString(_CopayDetails.Copay);
                        objExternalCopayDetails.MemberName = _CopayDetails.MemberName;
                        objExternalCopayDetails.RelationShip = _CopayDetails.RelationShip;
                        ExternalCopayDetailsList.Add(objExternalCopayDetails);
                    }
                    objPostInsurerResSelection.CopayText = JsonConvert.SerializeObject(ExternalCopayDetailsList);
                }

                if (objPostInsurerActionData.LoadingDetailsList != null && objPostInsurerActionData.LoadingDetailsList.Count > 0 && objPostInsurerActionData.LoadingType == 2)
                {
                    foreach (LoadingDetails _LoadingDetails in objPostInsurerActionData.LoadingDetailsList)
                    {
                        objExternalLoadingDetails = new ExternalLoadingDetails();
                        objExternalLoadingDetails.MemberLoading = Convert.ToInt32(_LoadingDetails.MemberLoading);
                        objExternalLoadingDetails.MemberID = Convert.ToInt64(_LoadingDetails.MemberID);
                        ExternalLoadingDetailsList.Add(objExternalLoadingDetails);
                    }
                    LoadingText.MemberLoading = ExternalLoadingDetailsList;
                }
                if (objPostInsurerActionData.IsLoadPremium && objPostInsurerActionData.LoadingType == 1)
                {
                    objPostInsurerResSelection.OverloadingPremium = objPostInsurerActionData.FinalPremium;
                    LoadingText.BaseLoading = objPostInsurerActionData.PremLoadingPerc;
                }
                objPostInsurerResSelection.Loading = LoadingText;
               
                objPostInsurerResSelection.InsurerStatus = objPostInsurerActionData.StatusID;
                objPostInsurerResSelection.SelectionID = objPostInsurerActionData.SelectionID;
                objPostInsurerResSelection.EnquiryID = objPostInsurerActionData.EnquiryID;

                if (objPostInsurerActionData.CoPay==0)
                {
                    objPostInsurerResSelection.Copay = null;
                    objPostInsurerActionData.CoPay = 0;
                }
                else
                {
                    objPostInsurerResSelection.Copay =Convert.ToString( objPostInsurerActionData.CoPay);
                }

                objPostInsurerResSelection.Exclusions = "";
                DiseaseExclusionDetails obDiseaseExclusion = new DiseaseExclusionDetails();
                if (objPostInsurerActionData.IsDiseaseExclusion)
                {
                    obDiseaseExclusion.DiseaseExclusion = Convert.ToString(objPostInsurerActionData.IsDiseaseExclusion);
                    obDiseaseExclusion.DiseaseType = new Diseasetype()
                    {
                        IsDiseaseExclusionPerm = Convert.ToString(objPostInsurerActionData.IsDiseaseExclusionPerm),
                        DiseaseExclusionPerm = Convert.ToString(objPostInsurerActionData.DiseaseExclusionPerm),
                        IsDiseaseExclusionTemp = Convert.ToString(objPostInsurerActionData.IsDiseaseExclusionTemp),
                        DiseaseExclusionTemp = Convert.ToString(objPostInsurerActionData.DiseaseExclusionTemp)

                    };
                    objPostInsurerResSelection.Exclusions = JsonConvert.SerializeObject(obDiseaseExclusion);
                }
                string st = JsonConvert.SerializeObject(objPostInsurerResSelection);

                res = JsonConvert.DeserializeObject<bool>(MakeRequest(ConnectionClass.SelectionOverloadLink(), objPostInsurerResSelection, "POST", "application/json"));
               
            }
            int returnValue = 0;
            if (res)
            {
                Communication.SendCommunicationSync(objPostInsurerActionData.NewLeadID, "InsurerSubmitted", "", objPostInsurerActionData.EmailID, "1", 2, Convert.ToString(objPostInsurerActionData.SelectionID));
                try
                {
                    //MemberID,MemberName,Remarks
                    ds = new DataSet();
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                    {
                        con.Open();
                        cmd = new SqlCommand("[BMS].[UpdateInsurerAction]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        var xEle = new XElement("MemberDetailsList",
                                                from MemberDocumentDetailsList in objPostInsurerActionData.MemberDetailsList
                                                select new XElement("MemberDetailsList",
                                                    // new XAttribute("InsurerID", MemberDocumentDetailsList.InsurerID),
                                                               new XAttribute("MemberID", MemberDocumentDetailsList.CustMemId),
                                                               new XAttribute("MemberName", MemberDocumentDetailsList.InsuredName),
                                                               new XAttribute("Remarks", MemberDocumentDetailsList.ExclusionReason)
                                                           )).ToString();
                        var XMLAddInfo = "";
                        if (objPostInsurerActionData.InsurerAdditionalInfoDeatilsList != null && objPostInsurerActionData.InsurerAdditionalInfoDeatilsList.Count > 0)
                        {
                            XMLAddInfo = new XElement("AddInfo",
                                                   from OBJInsurerAdditionalInfoDeatilsList in objPostInsurerActionData.InsurerAdditionalInfoDeatilsList
                                                   select new XElement("AddInfo",
                                                       // new XAttribute("InsurerID", MemberDocumentDetailsList.InsurerID),
                                                                  new XAttribute("InfoType", OBJInsurerAdditionalInfoDeatilsList.InfoType),
                                                                  new XAttribute("InfoName", OBJInsurerAdditionalInfoDeatilsList.InfoName),
                                                                  new XAttribute("MemberID", OBJInsurerAdditionalInfoDeatilsList.MemberID),
                                                                  new XAttribute("MemberName", OBJInsurerAdditionalInfoDeatilsList.MemberName),
                                                                  new XAttribute("Comments", OBJInsurerAdditionalInfoDeatilsList.Comments)
                                                              )).ToString();
                        }
                        var XMLCopay = "";
                        if (objPostInsurerActionData.CopayDetailsList != null && objPostInsurerActionData.CopayDetailsList.Count > 0)
                        {
                            XMLCopay = new XElement("Copay",
                                                 from objCopayDetailsList in objPostInsurerActionData.CopayDetailsList
                                                 select new XElement("Copay",
                                                     // new XAttribute("InsurerID", MemberDocumentDetailsList.InsurerID),
                                                                new XAttribute("MemberID", objCopayDetailsList.MemberID),
                                                                new XAttribute("MemberName", objCopayDetailsList.MemberName),
                                                                new XAttribute("Copay", objCopayDetailsList.Copay)
                                                            )).ToString();

                        }
                        var XMLLoading = "";
                        if (objPostInsurerActionData.LoadingDetailsList != null && objPostInsurerActionData.LoadingDetailsList.Count > 0)
                        {
                            XMLLoading = new XElement("Loading",
                                                 from objLoadingDetailsList in objPostInsurerActionData.LoadingDetailsList
                                                 select new XElement("Loading",
                                                            new XAttribute("MemberID", objLoadingDetailsList.MemberID),
                                                            new XAttribute("MemberName", objLoadingDetailsList.MemberName),
                                                            new XAttribute("Loading", objLoadingDetailsList.MemberLoading)
                                                        )).ToString();
                        }

                        cmd.Parameters.Add("@UserID", DbType.Int16).Value = objPostInsurerActionData.UserID;
                        cmd.Parameters.Add("@InsurerID", DbType.Int16).Value = objPostInsurerActionData.InsurerID;
                        cmd.Parameters.Add("@ExID", DbType.Int64).Value = objPostInsurerActionData.ExID;
                        cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objPostInsurerActionData.LeadID;
                        cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                        cmd.Parameters.Add("@XMLAddInfo", DbType.Xml).Value = XMLAddInfo;
                        cmd.Parameters.Add("@XMLCopay", DbType.Xml).Value = XMLCopay;
                        cmd.Parameters.Add("@XMLLoading", DbType.Xml).Value = XMLLoading;
                        //@IsLoadPremium ,@FinalPremium ,@IsDiseaseExlusion ,@DiseaseExclusionType ,@DiseaseExclusion ,
                        //@IsCoPay ,@CoPay ,@IsMemberExclusion ,@StatusID ,@Remarks
                        cmd.Parameters.Add("@IsLoadPremium", DbType.Byte).Value = objPostInsurerActionData.IsLoadPremium;
                        cmd.Parameters.Add("@SelectionID", DbType.Int64).Value = objPostInsurerActionData.SelectionID;
                        cmd.Parameters.Add("@FinalPremium", DbType.Decimal).Value = objPostInsurerActionData.FinalPremium;
                        cmd.Parameters.Add("@IsDiseaseExlusion", DbType.Byte).Value = objPostInsurerActionData.IsDiseaseExclusion;
                        cmd.Parameters.Add("@IsDiseaseExlusionPerm", DbType.Byte).Value = objPostInsurerActionData.IsDiseaseExclusionPerm;
                        cmd.Parameters.Add("@IsDiseaseExlusionTemp", DbType.Byte).Value = objPostInsurerActionData.IsDiseaseExclusionTemp;
                        cmd.Parameters.Add("@DiseaseExclusionTemp", DbType.String).Value = objPostInsurerActionData.DiseaseExclusionTemp;
                        cmd.Parameters.Add("@DiseaseExclusionPerm", DbType.String).Value = objPostInsurerActionData.DiseaseExclusionPerm;
                        cmd.Parameters.Add("@IsCoPay", DbType.Byte).Value = objPostInsurerActionData.IsCoPay;
                        cmd.Parameters.Add("@CoPay", DbType.Int16).Value = objPostInsurerActionData.CoPay;
                        cmd.Parameters.Add("@IsMemberExclusion", DbType.Byte).Value = objPostInsurerActionData.IsMemberExclusion;
                        cmd.Parameters.Add("@StatusID", DbType.Int16).Value = objPostInsurerActionData.StatusID;
                        cmd.Parameters.Add("@Remarks", DbType.String).Value = objPostInsurerActionData.Remarks;

                        cmd.Parameters.Add("@CopayType", DbType.Int16).Value = objPostInsurerActionData.CopayType;
                        cmd.Parameters.Add("@LoadingType", DbType.Int16).Value = objPostInsurerActionData.LoadingType;
                        cmd.Parameters.Add("@PremLoadingAmount", DbType.Decimal).Value = objPostInsurerActionData.PremLoadingAmount;
                        cmd.Parameters.Add("@PremLoadingPerc", DbType.Int16).Value = objPostInsurerActionData.PremLoadingPerc;

                        returnValue = cmd.ExecuteNonQuery();
                        //   con.Close();
                    }
                }
                catch { }
            }
            return returnValue;
        }

        public ResCustomerDocUpload GetCustomerDocUploadDetails(string ExID)
        {
            ResCustomerDocUpload objResCustomerDocUpload = new ResCustomerDocUpload();
            ExID = Crypto.DecryptCipherTextToPlainText(ExID);
            MemberDocumentDetails objMemberDocumentDetails;
            MasterDocument objMasterDocument;
            List<MemberDocumentDetails> MemberDocumentDetailsList = new List<MemberDocumentDetails>();
            List<MasterDocument> objMasterDocumentList = new List<MasterDocument>();
            ds = new DataSet();
            try
            {
               
                // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetCustomerDocUploadDetails]", con);
                    cmd.Parameters.Add("@ExID", SqlDbType.BigInt).Value = ExID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);                 
                    if (ds != null && ds.Tables.Count > 0)
                    {

                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[2].Rows[0];
                            objResCustomerDocUpload.Name = Convert.ToString(dr["Name"].ToString());
                            objResCustomerDocUpload.ProductName = "Health";
                            objResCustomerDocUpload.EmailID = Convert.ToString(dr["EmailID"].ToString());
                            objResCustomerDocUpload.MobileNo = Convert.ToString(dr["MobileNo"].ToString());
                            objResCustomerDocUpload.CustomerID = Convert.ToInt64(dr["CustomerID"].ToString());
                            objResCustomerDocUpload.ProductID = Convert.ToInt16(dr["ProductID"].ToString());
                            objResCustomerDocUpload.LeadID = Convert.ToInt64(dr["NewLeadID"].ToString());
                            objResCustomerDocUpload.EnquiryID = Convert.ToInt64(dr["EnquiryID"].ToString());
                            objResCustomerDocUpload.EncryptEnquiryID = Crypto.EncryptPlainTextToCipherText(ExID);
                            objResCustomerDocUpload.ExID = Convert.ToInt64( ExID);
                      
                        }

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            //SNO,DocumentName,MemberID,MemberName,DocURL,Comments
                            objMemberDocumentDetails = new MemberDocumentDetails();
                            objMemberDocumentDetails.SNO = Convert.ToInt64(dr["SNO"].ToString());
                            objMemberDocumentDetails.DocumentName = dr["DocumentName"].ToString();
                            objMemberDocumentDetails.MemberID = dr["MemberID"].ToString();
                            objMemberDocumentDetails.MemberName = dr["MemberName"].ToString();
                            objMemberDocumentDetails.DocURL = dr["DocURL"].ToString();
                            objMemberDocumentDetails.Comments = dr["Comments"].ToString();
                            MemberDocumentDetailsList.Add(objMemberDocumentDetails);
                        }

                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[1].Rows[0];
                            objResCustomerDocUpload.EncryptStatusID = Crypto.EncryptPlainTextToCipherText(dr["StatusID"].ToString());
                            objResCustomerDocUpload.StatusName = dr["Remarks"].ToString();
                        }

                        foreach (DataRow dr in ds.Tables[3].Rows)
                        {
                            //DocumentId,Document
                            objMasterDocument = new MasterDocument();
                            objMasterDocument.DocumentId = Convert.ToInt16(dr["DocumentId"].ToString());
                            objMasterDocument.Document = dr["Document"].ToString();
                            objMasterDocumentList.Add(objMasterDocument);
                        }
                    }
                }
                objResCustomerDocUpload.ProposerDetails = GetMemberDetails(objResCustomerDocUpload.EnquiryID);                  
            }
            catch(Exception ex) { }
            objResCustomerDocUpload.MasterDocumentList = objMasterDocumentList;
            objResCustomerDocUpload.MemberDocumentDetailsList = MemberDocumentDetailsList;
            return objResCustomerDocUpload;
        }
        public int AddCustomerDouments(PostMemberDocumentDetails objPostMemberDocumentDetails)
        {
           
            int res = 0;
            try
            {
                objPostMemberDocumentDetails.ExID = Convert.ToInt64(Crypto.DecryptCipherTextToPlainText(objPostMemberDocumentDetails.EncryptExID));
                FileUpload objFileUpload = new FileUpload();
                foreach (MemberDocumentDetails objMemberDocumentDetails in objPostMemberDocumentDetails.MemberDocumentDetailsList)
                {
                    if (objMemberDocumentDetails.FileData != "")
                    {
                        objMemberDocumentDetails.DocURL = objFileUpload.GetFileUrl(objPostMemberDocumentDetails.CustomerID, objPostMemberDocumentDetails.LeadID, objPostMemberDocumentDetails.ProductID, objMemberDocumentDetails.FileName, objMemberDocumentDetails.FileData, "Mongo");
                    }
                    else
                    {
                        objMemberDocumentDetails.DocURL = "";
                    }
                }
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    ds = new DataSet();
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AddCustomerDouments]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var xEle = new XElement("MemberDocumentDetailsList",
                                            from MemberDocumentDetailsList in objPostMemberDocumentDetails.MemberDocumentDetailsList
                                            select new XElement("MemberDocumentDetailsList",
                                                           new XAttribute("DocumentID", MemberDocumentDetailsList.DocumentID),
                                                           new XAttribute("DocumentName", MemberDocumentDetailsList.DocumentName),
                                                           new XAttribute("MemberID", MemberDocumentDetailsList.MemberID),
                                                           new XAttribute("MemberName", MemberDocumentDetailsList.MemberName),
                                                           new XAttribute("DocURL", MemberDocumentDetailsList.DocURL),
                                                           new XAttribute("Comments", MemberDocumentDetailsList.Comments)
                                                       )).ToString();                  
                    cmd.Parameters.Add("@ExID", DbType.Int64).Value = objPostMemberDocumentDetails.ExID;
                    cmd.Parameters.Add("@UserID", DbType.Int64).Value = objPostMemberDocumentDetails.UserID;
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    cmd.ExecuteNonQuery();
                }

            }
            catch { }
            return res;
        }

        public int AddInspectionDocuments(InspectionDocumentDetails objInspectionDocumentDetails)
        {

            int res = 0;
            try
            {
                //objPostMemberDocumentDetails.ExID = Convert.ToInt64(Crypto.DecryptCipherTextToPlainText(objPostMemberDocumentDetails.EncryptExID));
                FileUpload objFileUpload = new FileUpload();
                //InspectionDocument objInspectionDocument1 = new InspectionDocument();
                foreach (InspectionDocument objInspectionDocument in objInspectionDocumentDetails.InspectionDocumentList)
                {
                    if (objInspectionDocument.FileData != "")
                    {
                        objInspectionDocument.DocURL = objFileUpload.GetFileUrl(objInspectionDocumentDetails.CustomerID, objInspectionDocumentDetails.LeadID, objInspectionDocumentDetails.ProductID, objInspectionDocument.FileName, objInspectionDocument.FileData, "Mongo");
                        using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                        {
                            ds = new DataSet();

                            con.Open();
                            cmd = new SqlCommand("[BMS].[AddInspectionDocuments]", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objInspectionDocumentDetails.LeadID;
                            cmd.Parameters.Add("@InspectionID", DbType.String).Value = objInspectionDocumentDetails.InspectionID;
                            cmd.Parameters.Add("@UserID", DbType.Int32).Value = objInspectionDocumentDetails.UserID;
                            cmd.Parameters.Add("@FileName", DbType.Int32).Value = objInspectionDocument.FileName;
                            cmd.Parameters.Add("@DocURL", DbType.String).Value = objInspectionDocument.DocURL;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        objInspectionDocument.DocURL = "";
                    }

                   
                }               

            }
            catch { }
            return res;
        }

        public ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(long ExID,int Type=0)
        {
            ResPBInsurerUnderwriting objResPBInsurerUnderwriting = new ResPBInsurerUnderwriting();

            List<ResInsurerUnderwriting> objResInsurerUnderwritingList = new List<ResInsurerUnderwriting>();

            ResInsurerUnderwriting objResInsurerUnderwriting ;//= new ResInsurerUnderwriting(); 

            InsurerAdditionalInfoDeatils objInsurerAdditionalInfoDeatils;
            CopayDetails objCopayDetails;
            List<InsurerAdditionalInfoDeatils> objInsurerAdditionalInfoDeatilsList; ;
            List<CopayDetails> objCopayDetailsList;
            List<LoadingDetails> objLoadingDetailsList;
            LoadingDetails objLoadingDetails;

            SelectedInsurerDetails objSelectedInsurerDetails;
            

            List<MemberDocumentDetails> MemberDocumentDetailsList = new List<MemberDocumentDetails>();
            MemberDocumentDetails objMemberDocumentDetails;
            InsurerAction objInsurerAction = new InsurerAction();

            MasterDocument objMasterDocument;
            CustomerLeadDetails objCustomerLeadDetails = new CustomerLeadDetails();
            List<MasterDocument> objMasterDocumentList = new List<MasterDocument>();
            ds = new DataSet();
            try
            {
               // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetPBInsurerUnderwritingDetails]", con);
                    cmd.Parameters.Add("@ExID", SqlDbType.BigInt).Value = ExID;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                    //cmd.Parameters.Add("@InsurerID", SqlDbType.Int).Value = InsurerID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    string filter = "";
                    if (ds != null && ds.Tables.Count > 0)
                    {

                        if (ds.Tables[6].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[6].Rows[0];
                            objCustomerLeadDetails.OldLeadID = Convert.ToInt64(dr["OldLeadID"].ToString());
                            objCustomerLeadDetails.NewLeadID = Convert.ToInt64(dr["NewLeadID"].ToString());
                            objCustomerLeadDetails.CustomerID = Convert.ToInt64(dr["CustomerID"].ToString());
                            objCustomerLeadDetails.ProductID = Convert.ToInt16(dr["ProductID"].ToString());
                            objCustomerLeadDetails.EnquiryID = Convert.ToInt64(dr["EnquiryID"].ToString());
                            objCustomerLeadDetails.EncryptEnquiryID = Convert.ToBase64String(Encoding.UTF8.GetBytes(dr["EnquiryID"].ToString())).ToString();
                            objCustomerLeadDetails.Name = dr["Name"].ToString();
                            objCustomerLeadDetails.MobileNo = dr["MobileNo"].ToString();
                            objCustomerLeadDetails.EmailID = dr["EmailID"].ToString();
                        }

                        ProposerDetails objProposerDetails = new ProposerDetails();
                        objProposerDetails = GetMemberDetails(objCustomerLeadDetails.EnquiryID);

                        foreach (DataRow drInsurer in ds.Tables[0].Rows)
                        {
                            objSelectedInsurerDetails = new SelectedInsurerDetails();

                            objResInsurerUnderwriting = new ResInsurerUnderwriting();

                            objSelectedInsurerDetails.ExID = Convert.ToInt64(drInsurer["ExID"].ToString());
                            objSelectedInsurerDetails.SelectionID = Convert.ToInt64(drInsurer["SelectionID"].ToString());
                            objSelectedInsurerDetails.InsurerID = Convert.ToInt16(drInsurer["InsurerID"].ToString());
                            objSelectedInsurerDetails.InsurerName = drInsurer["InsurerName"].ToString();
                            objSelectedInsurerDetails.PlanID = Convert.ToInt16(drInsurer["PlanID"].ToString());
                            objSelectedInsurerDetails.PlanName = drInsurer["PlanName"].ToString();
                            objSelectedInsurerDetails.SumInsured = drInsurer["SumInsured"].ToString();
                            objSelectedInsurerDetails.Term = drInsurer["Term"].ToString();
                            objSelectedInsurerDetails.BasePremium = drInsurer["BasePremium"].ToString();
                            objResInsurerUnderwriting.IsLoadPremium = Convert.ToBoolean(drInsurer["IsLoadPremium"].ToString());
                            objResInsurerUnderwriting.FinalPremium = Convert.ToDecimal(drInsurer["FinalPremium"].ToString());
                            objResInsurerUnderwriting.IsDiseaseExclusion = Convert.ToBoolean(drInsurer["IsDiseaseExlusion"].ToString());
                            objResInsurerUnderwriting.IsDiseaseExclusionTemp = Convert.ToBoolean(drInsurer["IsDiseaseExlusionTemp"].ToString());
                            objResInsurerUnderwriting.IsDiseaseExclusionPerm = Convert.ToBoolean(drInsurer["IsDiseaseExlusionPerm"].ToString());
                            objResInsurerUnderwriting.DiseaseExclusionPerm = drInsurer["DiseaseExclusionPerm"].ToString();
                            objResInsurerUnderwriting.DiseaseExclusionTemp = drInsurer["DiseaseExclusionTemp"].ToString();
                            objResInsurerUnderwriting.IsCoPay = Convert.ToBoolean(drInsurer["IsCoPay"].ToString());
                            objResInsurerUnderwriting.CoPay = Convert.ToInt16(drInsurer["CoPay"].ToString());
                            objResInsurerUnderwriting.InsurerAction = Convert.ToInt16(drInsurer["InsurerAction"].ToString());
                            objResInsurerUnderwriting.InsurerComment = Convert.ToString(drInsurer["InsurerComment"].ToString());
                            objResInsurerUnderwriting.IsMemberExclusion = Convert.ToBoolean(drInsurer["IsMemberExclusion"].ToString());
                            objResInsurerUnderwriting.PremiumPaid = Convert.ToString(drInsurer["PremiumPaid"]);
                            objResInsurerUnderwriting.PremLoadingPerc = Convert.ToInt16(drInsurer["PremLoadingPerc"].ToString());
                            objResInsurerUnderwriting.PremLoadingAmount = Convert.ToDecimal(drInsurer["PremLoadingAmount"].ToString());
                            objResInsurerUnderwriting.CopayType = Convert.ToInt16(drInsurer["CopayType"].ToString());
                            objResInsurerUnderwriting.LoadingType = Convert.ToInt16(drInsurer["LoadingType"].ToString());

                            DataRow drStatus = ds.Tables[3].Rows[0];
                            filter = " SelectionID=" + drInsurer["SelectionID"].ToString();
                            DataRow[] foundRowsStatus;
                            foundRowsStatus = ds.Tables[3].Select(filter);
                            if (foundRowsStatus.Length > 0)
                            {
                                objResInsurerUnderwriting.InsurerStatusID = Convert.ToInt16(foundRowsStatus[0]["StatusID"].ToString());
                                objResInsurerUnderwriting.InsurerRemarks = foundRowsStatus[0]["Remarks"].ToString();
                            }


                            objInsurerAction = new InsurerAction();
                            if (objResInsurerUnderwriting.InsurerAction == 2)
                            {
                                objInsurerAction.Accept = true;
                            }
                            else if (objResInsurerUnderwriting.InsurerAction == 3)
                            {
                                objInsurerAction.Reject = true;
                            }
                            else if (objResInsurerUnderwriting.InsurerAction == 4)
                            {
                                objInsurerAction.AcceptWithConditions = true;
                            }
                            else if (objResInsurerUnderwriting.InsurerAction == 5 || objResInsurerUnderwriting.InsurerAction == 6)
                            {
                                objInsurerAction.AdditionalInfoRequired = true;
                            }


                            //1 member details
                            objResInsurerUnderwriting.ProposerDetails = objProposerDetails;
                            foreach (Coveredmember objCoveredMembers in objResInsurerUnderwriting.ProposerDetails.CoveredMembers)
                            {
                                filter = "InsurerID=" + Convert.ToInt16(drInsurer["InsurerID"]).ToString() + " AND MemberID=" + Convert.ToString(objCoveredMembers.MemberId) + " AND SelectionID=" + drInsurer["SelectionID"].ToString();
                                DataRow[] foundRows;
                                foundRows = ds.Tables[4].Select(filter);

                                foreach (DataRow drIns in foundRows)
                                {
                                    if (drIns["MemberID"].ToString() == Convert.ToString(objCoveredMembers.MemberId))
                                    {
                                        objCoveredMembers.IsExcluded = true;
                                        objCoveredMembers.ExclusionReason = drIns["ExclusionReason"].ToString();
                                    }
                                }
                            }
                            //copay and additional info details

                            filter = "InsurerID=" + Convert.ToInt16(drInsurer["InsurerID"]).ToString()
                                    + " AND SelectionID=" + drInsurer["SelectionID"].ToString();
                            DataRow[] CopayRows;
                            CopayRows = ds.Tables[7].Select(filter);
                            DataRow[] AddInfoRows;
                            AddInfoRows = ds.Tables[8].Select(filter);
                            DataRow[] LoadingRows;
                            LoadingRows = ds.Tables[9].Select(filter);

                            objInsurerAdditionalInfoDeatilsList = new List<InsurerAdditionalInfoDeatils>();
                            objCopayDetailsList = new List<CopayDetails>();
                            objLoadingDetailsList = new List<LoadingDetails>();
                            foreach (DataRow drCopay in CopayRows)
                            {
                                objCopayDetails = new CopayDetails();
                                objCopayDetails.Copay = Convert.ToInt16(drCopay["Copay"]);
                                objCopayDetails.MemberID = Convert.ToInt64(drCopay["MemberID"]);
                                objCopayDetails.MemberName = Convert.ToString(drCopay["MemberName"]);
                                objCopayDetailsList.Add(objCopayDetails);


                            }
                            objResInsurerUnderwriting.CopayDetailsList = objCopayDetailsList;

                            foreach (DataRow drAddi in AddInfoRows)
                            {
                                objInsurerAdditionalInfoDeatils = new InsurerAdditionalInfoDeatils();
                                objInsurerAdditionalInfoDeatils.MemberID = Convert.ToInt64(drAddi["MemberID"]);
                                objInsurerAdditionalInfoDeatils.MemberName = Convert.ToString(drAddi["MemberName"]);
                                objInsurerAdditionalInfoDeatils.Comments = Convert.ToString(drAddi["Comments"]);
                                objInsurerAdditionalInfoDeatils.InfoType = Convert.ToInt16(drAddi["InfoType"]);
                                objInsurerAdditionalInfoDeatils.InfoName = Convert.ToString(drAddi["InfoName"]);
                                objInsurerAdditionalInfoDeatilsList.Add(objInsurerAdditionalInfoDeatils);   

                            }
                            objResInsurerUnderwriting.InsurerAdditionalInfoDeatilsList = objInsurerAdditionalInfoDeatilsList;

                            foreach (DataRow drLoading in LoadingRows)
                            {
                                objLoadingDetails = new LoadingDetails();
                                objLoadingDetails.MemberLoading = Convert.ToInt16(drLoading["Loading"]);
                                objLoadingDetails.MemberID = Convert.ToInt64(drLoading["MemberID"]);
                                objLoadingDetails.MemberName = Convert.ToString(drLoading["MemberName"]);
                                objLoadingDetailsList.Add(objLoadingDetails);


                            }
                            objResInsurerUnderwriting.LoadingDetailsList = objLoadingDetailsList;
                            //loading,copay and additional info details End
                            objResInsurerUnderwriting.InsurerActionDetails = objInsurerAction;
                            //objResInsurerUnderwriting.MemberDetailsList = MemberDetailsList;
                            objResInsurerUnderwriting.InsurerDetails = objSelectedInsurerDetails;

                            objResInsurerUnderwritingList.Add(objResInsurerUnderwriting);
                        }

                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            //SNO,DocumentName,MemberID,MemberName,DocURL,Comments
                            objMemberDocumentDetails = new MemberDocumentDetails();
                            objMemberDocumentDetails.SNO = Convert.ToInt64(dr["SNO"].ToString());
                            objMemberDocumentDetails.DocumentName = dr["DocumentName"].ToString();
                            objMemberDocumentDetails.MemberID = dr["MemberID"].ToString();
                            objMemberDocumentDetails.MemberName = dr["MemberName"].ToString();
                            objMemberDocumentDetails.DocURL = dr["DocURL"].ToString();
                            objMemberDocumentDetails.Comments = dr["Comments"].ToString();
                            MemberDocumentDetailsList.Add(objMemberDocumentDetails);
                        }
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[2].Rows[0];
                            objResPBInsurerUnderwriting.StatusID = Convert.ToInt16(dr["StatusID"].ToString());
                            objResPBInsurerUnderwriting.StatusName = dr["Remarks"].ToString();
                        }

                        foreach (DataRow dr in ds.Tables[5].Rows)
                        {
                            //DocumentId,Document
                            objMasterDocument = new MasterDocument();
                            objMasterDocument.DocumentId = Convert.ToInt16(dr["DocumentId"].ToString());
                            objMasterDocument.Document = dr["Document"].ToString();
                            objMasterDocumentList.Add(objMasterDocument);
                        }



                        objResPBInsurerUnderwriting.CustomerLead = objCustomerLeadDetails;
                        objResPBInsurerUnderwriting.MemberDocumentDetailsList = MemberDocumentDetailsList;
                        objResPBInsurerUnderwriting.InsurerUnderwritingDetails = objResInsurerUnderwritingList;
                        objResPBInsurerUnderwriting.MasterDocumentList = objMasterDocumentList;
                    }
                }
            }
            catch { }

            
            return objResPBInsurerUnderwriting;
        }


        public UserDetails Login(UserLoginRequest objUserLoginRequest)
        {
            UserDetails objUserDetails = new UserDetails();            
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AuthenticateUser]", con);
                    cmd.Parameters.Add("@LoginID", SqlDbType.VarChar).Value = objUserLoginRequest.LoginID;//objReqMarkExSearch.FilterParam;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = objUserLoginRequest.Password;//objReqMarkExSearch.FilterType;
                    cmd.Parameters.Add("@Token", SqlDbType.VarChar).Value = objUserLoginRequest.Token;//objReqMarkExSearch.FromDate;
                    cmd.Parameters.Add("@ExpireOn", SqlDbType.Date).Value = objUserLoginRequest.ExpireOn;//objReqMarkExSearch.ToDate;
                    cmd.Parameters.Add("@LoginUserType", SqlDbType.TinyInt).Value = objUserLoginRequest.LoginUserType;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;

                    adp.Fill(ds);
                    objUserDetails.LoginStatus = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr;
                        dr = ds.Tables[0].Rows[0];
                        objUserDetails.UserID = Convert.ToInt16(dr["UserID"].ToString());
                        objUserDetails.UserType = Convert.ToInt16(dr["UserType"].ToString());
                        objUserDetails.UserRole = Convert.ToInt16(dr["UserRole"].ToString());
                        objUserDetails.Name = dr["Name"].ToString();
                        objUserDetails.EmailID = dr["EmailID"].ToString();
                        objUserDetails.MobileNo = dr["MobileNo"].ToString();
                        objUserDetails.InsurerName = Convert.ToString(dr["InsurerName"]);
                        objUserDetails.LoginID = dr["LoginID"].ToString();
                        objUserDetails.InsurerID = Convert.ToInt16(dr["InsurerID"].ToString());
                        objUserDetails.Token = objUserLoginRequest.Token;
                    }
                }
            }
            catch { }

            return objUserDetails;
        }
        public bool RequestAuthenticate(string Token,string UserID)
        {
            UserDetails objUserDetails = new UserDetails();            
            ds = new DataSet();
            bool res = false;
            try
            {
               // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[RequestAuthenticate]", con);
                    cmd.Parameters.Add("@Token", SqlDbType.VarChar).Value = Token;
                    cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = UserID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString()) == 1 ? true : false;
                }
            }
            catch { }

            return res;
        }


        public int RemoveFromHealthEx(ReqRemoveFromHealthEx objReqRemoveFromHealthEx)
        {
            UserDetails objUserDetails = new UserDetails();
            ds = new DataSet();
            int res = 0;
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[RemoveFromHealthEx]", con);
                    cmd.Parameters.Add("@ExID", SqlDbType.VarChar).Value = objReqRemoveFromHealthEx.ExID;
                    cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = objReqRemoveFromHealthEx.UserID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    res = cmd.ExecuteNonQuery();
                }
            }
            catch { }

            return res;
        }

        public int InsertInsurerRemarks(InsurerRemarksDetails objInsurerRemarksDetails)
        {
            UserDetails objUserDetails = new UserDetails();
            ds = new DataSet();
            int res = 0;
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[EXInsertInsurerRemarks]", con);
                    cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = objInsurerRemarksDetails.UserID;
                    cmd.Parameters.Add("@InsurerID", SqlDbType.VarChar).Value = objInsurerRemarksDetails.InsurerID;
                    cmd.Parameters.Add("@ExID", SqlDbType.VarChar).Value = objInsurerRemarksDetails.ExID;
                    cmd.Parameters.Add("@LeadID", SqlDbType.VarChar).Value = objInsurerRemarksDetails.LeadID;
                    cmd.Parameters.Add("@SelectionID", SqlDbType.VarChar).Value = objInsurerRemarksDetails.SelectionID;
                    cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = objInsurerRemarksDetails.Remarks;
                    cmd.CommandType = CommandType.StoredProcedure;
                    res = cmd.ExecuteNonQuery();
                }
            }
            catch { }

            return res;
        }

        public List<InsurerRemarksDetails> GetInsurerRemarks(string UserID, string InsurerID, string ExID, string LeadID, string SelectionID)
        {
            List<InsurerRemarksDetails> objInsurerRemarksDetailsList = new List<InsurerRemarksDetails>();
            InsurerRemarksDetails objInsurerRemarksDetails;               
            ds = new DataSet();
            int res = 0;
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[EXGetInsurerRemarks]", con);
                    cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = UserID;
                    cmd.Parameters.Add("@InsurerID", SqlDbType.VarChar).Value = InsurerID;
                    cmd.Parameters.Add("@ExID", SqlDbType.VarChar).Value = ExID;
                    cmd.Parameters.Add("@LeadID", SqlDbType.VarChar).Value = LeadID;
                    cmd.Parameters.Add("@SelectionID", SqlDbType.VarChar).Value = SelectionID;    
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objInsurerRemarksDetails = new InsurerRemarksDetails();
                            //objInsurerRemarksDetails.RemarkID = Convert.ToInt64(dr["RemarkID"]);
                            objInsurerRemarksDetails.UserID = Convert.ToInt16(dr["UserID"]);
                            objInsurerRemarksDetails.InsurerID = Convert.ToInt16(dr["InsurerID"]);
                            objInsurerRemarksDetails.ExID = Convert.ToInt64(dr["ExID"]);
                            objInsurerRemarksDetails.LeadID = Convert.ToInt64(dr["LeadID"]);
                            objInsurerRemarksDetails.Remarks = Convert.ToString(dr["Remarks"]);
                            objInsurerRemarksDetails.CreatedOn = Convert.ToString(dr["CreatedOn"]);
                            objInsurerRemarksDetailsList.Add(objInsurerRemarksDetails);
                        }
                    }
                }
            }
            catch { }

            return objInsurerRemarksDetailsList;
        }
        public List<LeadDetails> GetLeadsForInsurerDashboard(ReqMarkExSearch objReqMarkExSearch)
         {
            LeadDetails objLeadDetails;
            List<LeadDetails> objLeadDetailsList = new List<LeadDetails>();
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetLeadsForInsurerDashboard]", con);
                    cmd.Parameters.Add("@FilterParam", SqlDbType.VarChar).Value = string.IsNullOrEmpty(objReqMarkExSearch.FilterParam) ? "" : objReqMarkExSearch.FilterParam;
                    cmd.Parameters.Add("@FilterType", SqlDbType.Int).Value = objReqMarkExSearch.FilterType;
                    cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = objReqMarkExSearch.FromDate;
                    cmd.Parameters.Add("@ToDate", SqlDbType.Date).Value = objReqMarkExSearch.ToDate;
                    cmd.Parameters.Add("@Status", SqlDbType.Int).Value = objReqMarkExSearch.Status;
                    cmd.Parameters.Add("@KeyMatrix", SqlDbType.Int).Value = objReqMarkExSearch.KeyMatrix;
                    cmd.Parameters.Add("@InsurerID", SqlDbType.Int).Value = objReqMarkExSearch.InsurerID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objLeadDetails = new LeadDetails();
                            objLeadDetails.ExID = Convert.ToInt64(dr["ExID"]);
                            objLeadDetails.Name = Convert.ToString(dr["Name"]);
                            objLeadDetails.ExLeadType = Convert.ToInt16(dr["ExLeadType"]);
                            objLeadDetails.OldLeadID = Convert.ToInt64(dr["OldLeadID"]);
                            objLeadDetails.NewLeadID = Convert.ToInt64(dr["NewLeadID"]);
                            objLeadDetails.CustomerID = Convert.ToInt64(dr["CustomerID"]);
                            objLeadDetails.ProposalNo = Convert.ToString(dr["ProposalNo"]);
                            objLeadDetails.ExStatusID = Convert.ToInt16(dr["StatusID"]);
                            objLeadDetails.Remarks = Convert.ToString(dr["Remarks"]);
                            objLeadDetails.HealthExMarkDate = Convert.ToString(dr["HealthExMarkDate"]);
                            objLeadDetails.InsurerActionDetails = Convert.ToInt16(dr["InsurerActionDetails"]);
                            objLeadDetails.SelectionID = Convert.ToInt64(dr["SelectionID"]);
                            objLeadDetails.InsurerStatusID = Convert.ToInt16(dr["InsurerStatusID"]);
                            objLeadDetails.InsurerStatus = Convert.ToString(dr["InsurerRemarks"]);
                            objLeadDetails.PlanName = Convert.ToString(dr["PlanName"]);
                            objLeadDetails.KeyMatrix = Convert.ToString(dr["KeyMatrix"]);
                            objLeadDetailsList.Add(objLeadDetails);
                        }
                    }
                }
            }
            catch
            {

            }
            return objLeadDetailsList;
        }

        public HealthEXDetails MarkHealthServiceWithNewLead(string LeadID)
        {

            HealthEXDetails objHealthEXDetails = new HealthEXDetails();
            try
            {
                WebRequest request = WebRequest.Create(ConnectionClass.MarkHealthEx() + LeadID);
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();
                //// Display the status.
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                //// Get the stream containing content returned by the server.
                //Stream dataStream = response.GetResponseStream();
                //// Open the stream using a StreamReader for easy access.
                //StreamReader reader = new StreamReader(dataStream);
                //// Read the content.
                //string responseFromServer = reader.ReadToEnd();
                //// Display the content.
                //Console.WriteLine(responseFromServer);
                //// Clean up the streams and the response.
                //reader.Close();
                //response.Close();


                // HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConnectionClass.MasterSupplierPlanForEx() + LeadID);
                // request.Method = "GET";
                // request.ContentType = "application/json";
                // request.KeepAlive = false;
                //// var buffer = Encoding.ASCII.GetBytes(LeadID);
                // //request.ContentLength = buffer.Length;
                // var postData = request.GetRequestStream();
                //// postData.Write(buffer, 0, buffer.Length);
                // postData.Close();

                try
                {
                    WebResponse webResponse = request.GetResponse();
                    using (Stream webStream = webResponse.GetResponseStream())
                    {
                        if (webStream != null)
                        {
                            using (StreamReader responseReader = new StreamReader(webStream))
                            {
                                string strREsp = responseReader.ReadToEnd();
                                objHealthEXDetails = JsonConvert.DeserializeObject<HealthEXDetails>(strREsp);
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                }
            }
            catch { }

            return objHealthEXDetails;

        }

        public int LogOut(int UserID)
        {                                 
            ds = new DataSet();
            int res = 0;
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[LogoutUser]", con);
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    res = cmd.ExecuteNonQuery();
                }
            }
            catch { } 
            return res;
        }
        //public int UpdateNewLeadDetailsInHealthEx(PostPlanSelectionData objPostPlanSelectionData)
        //{   
        //    HealthEXDetails objHealthEXDetails = new HealthEXDetails();
        //    objHealthEXDetails = MarkHealthServiceWithNewLead(Convert.ToString(objPostPlanSelectionData.LeadID));
        //    ds = new DataSet();
        //    int res = 0;
        //    try
        //    {
        //        con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
        //        con.Open();
        //        //NewLeadID BIGINT,@ItemID BIGINT,@EnquiryID BIGINT,@OldLeadID BIGINT,@UserID int
        //        cmd = new SqlCommand("[BMS].[UpdateNewLeadDetailsInHealthEx]", con);
        //        cmd.Parameters.Add("@NewLeadID", SqlDbType.BigInt).Value = objHealthEXDetails.Identifiers.MatrixLeadID;
        //        cmd.Parameters.Add("@ItemID", SqlDbType.BigInt).Value = objHealthEXDetails.Identifiers.NeedID;
        //        cmd.Parameters.Add("@EnquiryID", SqlDbType.BigInt).Value = objHealthEXDetails.EnquiryId;
        //        cmd.Parameters.Add("@OldLeadID", SqlDbType.BigInt).Value = Convert.ToInt64(objPostPlanSelectionData.LeadID);
        //        cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = objPostPlanSelectionData.UserID;

        //        cmd.CommandType = CommandType.StoredProcedure;
        //        res = cmd.ExecuteNonQuery();


        //    }
        //    catch { }

        //    return res;
        //}

        public List<HealthExSelections> UpdateSelectedPlanOnCJ(PostPlanSelectionData objPostPlanSelectionData, long EnquiryId)
        {
            List<HealthExSelections> HealthExSelectionsList = new List<HealthExSelections>();
            bool res = false;
            CJPlanSelection objCJPlanSelection;
            List<CJPlanSelection> objCJPlanSelectionList = new List<CJPlanSelection>();
            foreach (PostInsurerPlanData objPostInsurerPlanData in objPostPlanSelectionData.PostInsurerPlanDataList)
            {
                objCJPlanSelection = new CJPlanSelection();
                objCJPlanSelection.EnquiryId = EnquiryId;
                objCJPlanSelection.Premium = Convert.ToString(objPostInsurerPlanData.BasePremium);
                objCJPlanSelection.Suminsured = Convert.ToString(objPostInsurerPlanData.SumInsured);
                objCJPlanSelection.Term = Convert.ToString(objPostInsurerPlanData.Term);
                objCJPlanSelection.PlanId = Convert.ToString(objPostInsurerPlanData.PlanID);
                objCJPlanSelectionList.Add(objCJPlanSelection);
            }
            try
            {

                HealthExSelectionsList = JsonConvert.DeserializeObject<List<HealthExSelections>>(MakeRequest(ConnectionClass.UpdateSelectedPlanOnCJ(), objCJPlanSelectionList, "POST", "application/json").ToString());
            }
            catch { }

            return HealthExSelectionsList;

        }
        public ProposerDetails GetMemberDetails(long EnquiryId)
        {
            ProposerDetails objProposerDetails = new ProposerDetails();
            try
            {
                objProposerDetails = Deserialise<ProposerDetails>(MakeRequest(ConnectionClass.GetHealthProposerLink() + EnquiryId, EnquiryId, "GET", "application/json"));
                //objProposerDetails =JsonConvert.DeserializeObject<ProposerDetails>(MakeRequest(ConnectionClass.GetHealthProposerLink() + EnquiryId, EnquiryId, "GET", "application/json"));
            }
            catch { }

            return objProposerDetails;

        }
      
        public bool AcceptOrRejectInsurer(PostInsurerActionData objPostInsurerActionData)
        {
            bool res = false;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "AcceptOrRejectInsurer" +
                   "\r\n JSONRequest=" + JsonConvert.SerializeObject(objPostInsurerActionData) );
            try
            {
                if (objPostInsurerActionData.PBResponse == 1)    //accept
                {
                    PostInsurerResSelection objPostInsurerResSelection = new PostInsurerResSelection();
                    if (objPostInsurerActionData.IsMemberExclusion)
                    {
                        List<MembersRejPost> objMembersRejPostList = new List<MembersRejPost>();
                        MembersRejPost objMembersRejPost;
                        if (objPostInsurerActionData.MemberDetailsList.Count > 0)
                        {
                            foreach (MemberDetails objMemberDetails in objPostInsurerActionData.MemberDetailsList)
                            {
                                objMembersRejPost = new MembersRejPost();
                                objMembersRejPost.EnquiryID = objPostInsurerActionData.EnquiryID;
                                objMembersRejPost.SelectionID = objPostInsurerActionData.SelectionID;
                                objMembersRejPost.MemberId = objMemberDetails.CustMemId;
                                objMembersRejPostList.Add(objMembersRejPost);
                            }

                            res = JsonConvert.DeserializeObject<bool>(MakeRequest(ConnectionClass.RejectMembersLink(), objMembersRejPostList, "POST", "application/json"));
                        }



                    }
                    objPostInsurerResSelection.Exclusions = "";
                    DiseaseExclusionDetails obDiseaseExclusion = new DiseaseExclusionDetails();
                    if (objPostInsurerActionData.IsDiseaseExclusion)
                    {

                        obDiseaseExclusion.DiseaseExclusion = Convert.ToString(objPostInsurerActionData.IsDiseaseExclusion);
                        obDiseaseExclusion.DiseaseType = new Diseasetype()
                        {
                            IsDiseaseExclusionPerm = Convert.ToString(objPostInsurerActionData.IsDiseaseExclusionPerm),
                            DiseaseExclusionPerm = Convert.ToString(objPostInsurerActionData.DiseaseExclusionPerm),
                            IsDiseaseExclusionTemp = Convert.ToString(objPostInsurerActionData.IsDiseaseExclusionTemp),
                            DiseaseExclusionTemp = Convert.ToString(objPostInsurerActionData.DiseaseExclusionTemp)

                        };
                        objPostInsurerResSelection.Exclusions = JsonConvert.SerializeObject(obDiseaseExclusion);
                        //objPostInsurerResSelection.Exclusions = "DiseaseExclusion:" + Convert.ToString(objPostInsurerActionData.IsDiseaseExclusion) +
                        //                                        ",DiseaseType:{ IsDiseaseExclusionPerm:" + Convert.ToString(objPostInsurerActionData.IsDiseaseExclusionPerm) +
                        //                                        ",DiseaseExclusionPerm:" + Convert.ToString(objPostInsurerActionData.DiseaseExclusionPerm) +
                        //                                        ",IsDiseaseExclusionTemp:" + Convert.ToString(objPostInsurerActionData.IsDiseaseExclusionTemp) +
                        //                                        ",DiseaseExclusionTemp:" + Convert.ToString(objPostInsurerActionData.DiseaseExclusionTemp) + "}";
                       
                    }
                    
                    objPostInsurerResSelection.SelectionID = objPostInsurerActionData.SelectionID;
                    objPostInsurerResSelection.EnquiryID = objPostInsurerActionData.EnquiryID;

                    if (objPostInsurerActionData.IsCoPay)
                    {
                        objPostInsurerResSelection.Copay = Convert.ToString(objPostInsurerActionData.CoPay);
                    }
                    if (objPostInsurerActionData.IsLoadPremium)
                    {
                        objPostInsurerResSelection.OverloadingPremium = objPostInsurerActionData.FinalPremium;
                    }
                    objPostInsurerResSelection.Submitted = 1;
                    res = JsonConvert.DeserializeObject<bool>(MakeRequest(ConnectionClass.SelectionOverloadLink(), objPostInsurerResSelection, "POST", "application/json"));

                    UpdateHealthExStatus(objPostInsurerActionData.NewLeadID, objPostInsurerActionData.LeadID, objPostInsurerActionData.SelectionID, 3, objPostInsurerActionData.UserID);
                    Communication.SendCommunicationSync(objPostInsurerActionData.NewLeadID, "PaymentLinkHEX", "", objPostInsurerActionData.EmailID, "1", 2, Convert.ToString(objPostInsurerActionData.SelectionID));
                }
                else if (objPostInsurerActionData.PBResponse == 2)  //reject
                {                      
                    UpdateHealthExStatus(objPostInsurerActionData.NewLeadID, objPostInsurerActionData.LeadID, objPostInsurerActionData.SelectionID, 4, objPostInsurerActionData.UserID);
                }
            }
            catch (Exception ex) { oStringBuilder.Append("\r\n Error=" + ex.Message); }
            finally
            {
                common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "AcceptOrRejectInsurer.txt");

            }
            return res;
        }

        public bool ResendAccptenceEmail(PostInsurerActionData objPostInsurerActionData)
        {
            bool res = false;
            //StringBuilder oStringBuilder = new StringBuilder();
            //oStringBuilder.Append("\r\n Time=" + DateTime.Now +
            //       "\r\n Service=" + "AcceptOrRejectInsurer" +
            //       "\r\n JSONRequest=" + JsonConvert.SerializeObject(objPostInsurerActionData));
            //try
            //{
                Communication.SendCommunicationSync(objPostInsurerActionData.NewLeadID, "PaymentLinkHEX", "", objPostInsurerActionData.EmailID, "1", 2, Convert.ToString(objPostInsurerActionData.SelectionID));
            //}
            //catch (Exception ex) { oStringBuilder.Append("\r\n Error=" + ex.Message); }
            //finally
            //{
            //    common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "AcceptOrRejectInsurer.txt");

            //}
            return res;
        }

        public List<MedicalDetails> CustomerMedicalDetails(string base64EncodedLeadID)
        {
            List<MedicalDetails> objMedicalDetails = new List<MedicalDetails>(); ;
            base64EncodedLeadID = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(base64EncodedLeadID));
            
            try
            {

                objMedicalDetails = JsonConvert.DeserializeObject<List<MedicalDetails>>(MakeRequest(ConnectionClass.MedicalDetails() + base64EncodedLeadID, base64EncodedLeadID, "GET", "application/json"));
            }
            catch { }
            return objMedicalDetails;
        }
        public bool UpdateHealthExStatus(long NewLeadID, long OldLeadID, long SelectionID, int Type, int UserID)
        {
            bool Output = false;
            int res = 0;
            try
            {
                ds = new DataSet();
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[UpdateHealthExStatusOnCJ]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@NewLeadID", DbType.Int64).Value = NewLeadID;
                    cmd.Parameters.Add("@OldLeadID", DbType.Int64).Value = OldLeadID;
                    cmd.Parameters.Add("@SelectionID", DbType.Int64).Value = SelectionID;
                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = UserID;
                    cmd.Parameters.Add("@Type", DbType.Int16).Value = Type;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    //con.Close();   
                }
            }
            catch (Exception ex){ }
            if (res > 0)
                Output = true;

            if (Type == 1 && Output)//send email for doc upload
            {
                try
                {
                    ds = new DataSet();
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                    {
                        con.Open();
                        cmd = new SqlCommand("[BMS].[UpdateHealthExStatusOnCJ]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@NewLeadID", DbType.Int64).Value = NewLeadID;
                        cmd.Parameters.Add("@OldLeadID", DbType.Int64).Value = OldLeadID;
                        cmd.Parameters.Add("@SelectionID", DbType.Int64).Value = SelectionID;
                        cmd.Parameters.Add("@UserID", DbType.Int16).Value = UserID;
                        cmd.Parameters.Add("@Type", DbType.Int16).Value = Type;

                        cmd.CommandType = CommandType.StoredProcedure;
                        adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            Communication.SendCommunicationSync(Convert.ToInt64(ds.Tables[0].Rows[0]["LeadID"].ToString()), "UploadDocument", "", ds.Tables[0].Rows[0]["EmailID"].ToString(), "1", 2, "0");
                        }
                        //con.Close();   
                    }
                }
                catch (Exception ex) { }
                
            }
            return Output;
        }

        public static T Deserialise<T>(string json)
        {
            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                T result = (T)deserializer.ReadObject(stream);
                return result;
            }
        }

        //JsonConvert.DeserializeObject<List<SupplierPlanDetails>>
        public static string MakeRequest(string requestUrl, object JSONRequest, string JSONmethod, string JSONContentType)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {
               
                oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                    "\r\n requestUrl=" + requestUrl.ToString() +
                    "\r\n JSONRequest=" + JsonConvert.SerializeObject(JSONRequest) +
                    "\r\n JSONmethod=" + JSONmethod.ToString() +
                    "\r\n JSONContentType=" + JSONContentType );
             
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                request.Timeout = Convert.ToInt32(100000);
                request.Method = JSONmethod;
                request.ContentType = JSONContentType;// "application/json";

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                if (JSONmethod == "POST")
                {
                    byte[] bytes = encoding.GetBytes(JsonConvert.SerializeObject(JSONRequest));

                    request.ContentLength = bytes.Length;

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        // Send the data.
                        requestStream.Write(bytes, 0, bytes.Length);
                    }
                }
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string objRespons = responseReader.ReadToEnd();
                            oStringBuilder.Append("\r\n objRespons=" + objRespons.ToString());
                            return objRespons; 
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                oStringBuilder.Append("\r\n Error=" + ex.Message);
                return null;
                //"\r\n \r\n \r\n "
            }
            finally
            {
                common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "Service.txt");

            }
        }         

        public bool UpdatePaymentDetails(PaymentDetailsData objPaymentDetailsData)
        {
            bool result = false;
            int res = 0;          
            try
            {
                ds = new DataSet();
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[MTX].[UpdatePaymentDetailsForEx]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@EnquiryID", DbType.Int64).Value = objPaymentDetailsData.EnquiryID;
                    cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objPaymentDetailsData.LeadID;
                    cmd.Parameters.Add("@PaymentMode", SqlDbType.VarChar).Value = objPaymentDetailsData.PaymentMode;
                    cmd.Parameters.Add("@PaymentType", SqlDbType.Int).Value = objPaymentDetailsData.PaymentType;
                    cmd.Parameters.Add("@PaymentAmount", SqlDbType.Int).Value = objPaymentDetailsData.PaymentAmount;
                    cmd.Parameters.Add("@PaymentStatus", SqlDbType.Int).Value = objPaymentDetailsData.PaymentStatus;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    res = cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    //con.Close();
                }

            }
            catch { }
            if (res > 0)
                result = true;
            return result;
        }

        public List<PaymentDetailsForEx> GETPaymentDetailsForEx(string base64EncodedLeadID)
        {
            List<PaymentDetailsForEx> objPaymentDetailsForExList = new List<PaymentDetailsForEx>();
            PaymentDetailsForEx objPaymentDetailsForEx;
            long LeadID = 0;
            base64EncodedLeadID = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(base64EncodedLeadID));
            if (!long.TryParse(base64EncodedLeadID, out LeadID))
            {
                return null;
            }
         
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[MTX].[GETPaymentDetailsForEx]", con);
                    cmd.Parameters.Add("@LeadID", SqlDbType.VarChar).Value = LeadID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objPaymentDetailsForEx = new PaymentDetailsForEx();
                            objPaymentDetailsForEx.EnquiryID = Convert.ToInt64(dr["EnquiryID"]);
                            objPaymentDetailsForEx.LeadID = Convert.ToInt64(dr["LeadID"]);
                            objPaymentDetailsForEx.PaymentMode = Convert.ToString(dr["PaymentMode"]);
                            objPaymentDetailsForEx.PaymentType = Convert.ToString(dr["PaymentType"]);
                            objPaymentDetailsForEx.PaymentAmount = Convert.ToString(dr["PaymentAmount"]);
                            objPaymentDetailsForEx.PaymentStatus = Convert.ToString(dr["PaymentStatus"]);
                            objPaymentDetailsForEx.CreatedOn = Convert.ToString(dr["CreatedOn"]);
                            objPaymentDetailsForExList.Add(objPaymentDetailsForEx);
                        }
                    }
                }
            }
            catch
            {
                return null;
            }
            return objPaymentDetailsForExList;
        }


        public bool UpdatePremiumForSelection(UpdatePremiumReq objUpdatePremiumReq)
        {
            bool result = false;
            int res = 0;
            try
            {
                ds = new DataSet();
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[ExInsurerSelectionPremiumUpdate]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SelectionID", DbType.Int64).Value = objUpdatePremiumReq.SelectionID;
                    cmd.Parameters.Add("@LeadID", DbType.Int64).Value = objUpdatePremiumReq.LeadID;
                    cmd.Parameters.Add("@PremiumPaid", SqlDbType.VarChar).Value = objUpdatePremiumReq.PremiumPaid;
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = objUpdatePremiumReq.UserID;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    res = cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    //con.Close();
                }

            }
            catch { }
            if (res > 0)
                result = true;
            return result;
        }

        public LeadDocumentDetails GetHExLeadMemberDocDetails(Int64 LeadID, long SelectionID)
        {
            LeadDocumentDetails objDocDetails = new LeadDocumentDetails();
            List<DocDetails> objMemDocDetailsList = new List<DocDetails>();
            DocDetails objMemDocDetails;

            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetHExLeadMemberDocDetails]", con);
                    cmd.Parameters.Add("@LeadID", SqlDbType.VarChar).Value = LeadID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objMemDocDetails = new DocDetails();
                            objMemDocDetails.MemberId = Convert.ToInt64(dr["MemberID"]);
                            objMemDocDetails.DocUrl = Convert.ToString(dr["DocURL"]);
                            objMemDocDetailsList.Add(objMemDocDetails);
                        }
                        objDocDetails.TestReports = objMemDocDetailsList;
                    }
                }
            }
            catch
            {
                return null;
            }
            objDocDetails.DecisionUrl = ConnectionClass.InsurerDecisionURL() + Convert.ToString(LeadID) + "/" + Convert.ToString(SelectionID);
            return objDocDetails;
        }
        public int AddNewUsers(AddNewUser objAddNewUser)
        {
            int res = 0;

            try
            {
                ds = new DataSet();

                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AddNewUser]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Name", DbType.String).Value = objAddNewUser.Name;
                    cmd.Parameters.Add("@EmailID", DbType.String).Value = objAddNewUser.EmailID;
                    cmd.Parameters.Add("@MobileNo", DbType.Int64).Value = objAddNewUser.MobileNo;
                    cmd.Parameters.Add("@LoginID", DbType.String).Value = objAddNewUser.LoginID;
                    cmd.Parameters.Add("@Password", DbType.String).Value = objAddNewUser.Password;
                    cmd.Parameters.Add("@CreatedBy", DbType.Int32).Value = objAddNewUser.CreatedBy;
                    cmd.Parameters.Add("@UserType", DbType.Int32).Value = objAddNewUser.UserTypeName;
                    cmd.Parameters.Add("@UserRole", DbType.Int32).Value = objAddNewUser.UserRoleName;
                    cmd.Parameters.Add("@InsurerID", DbType.Int32).Value = objAddNewUser.InsurerID;
                    //cmd.Parameters.Add("@ApplicationID", DbType.Int32).Value = objAddNewUser.ApplicationType;
                    cmd.Parameters.Add("@InsurerName", DbType.String).Value = objAddNewUser.InsurerName;
                    cmd.Parameters.Add("@Type", DbType.String).Value = objAddNewUser.Type;
                    cmd.Parameters.Add("@UserID", DbType.String).Value = objAddNewUser.UserID;

                    cmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    // cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    //


                    res = cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@Error"].Value.ToString());
                    // con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return res;


        }

        public int ChangePassword(ChangePassword objChangePassword)
        {
            int res = 0;

            try
            {
                ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[ExChangePassword]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@OldPassword", DbType.String).Value = objChangePassword.OldPassword;
                    cmd.Parameters.Add("@NewPassword", DbType.String).Value = objChangePassword.NewPassword;
                    cmd.Parameters.Add("@UserId", DbType.String).Value = objChangePassword.UserId;
                    // cmd.Parameters.Add("@UserType", DbType.Int32).Value = objChangePassword.UserType;
                    cmd.Parameters.Add("@error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    // cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    //


                    res = cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@error"].Value.ToString());
                    // con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return res;


        }

        public List<MasterSupplier> GetSupplierListByProduct(string ProductID)
        {
            MasterSupplier objmastersupplier;
            List<MasterSupplier> objmastersupplierList = new List<MasterSupplier>();
            try
            {
                ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetSuppliers]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ProductID", DbType.Int32).Value = ProductID;
                    cmd.ExecuteNonQuery();
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objmastersupplier = new MasterSupplier();
                            objmastersupplier.InsurerName = Convert.ToString(dr["SupplierName"]);
                            objmastersupplier.InsurerID = Convert.ToInt32(dr["SupplierProduct_ID"]);
                            objmastersupplierList.Add(objmastersupplier);
                        }
                    }
                    //con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return objmastersupplierList;
        }

        public List<AddNewUser> GetHealthExUser()
        {
            AddNewUser objAddNewUser;
            List<AddNewUser> objGetHealthExUser = new List<AddNewUser>();
            try
            {
                ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetHealthExUser]", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.Add("@ProductID", DbType.Int32).Value = ProductID;
                    cmd.ExecuteNonQuery();
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objAddNewUser = new AddNewUser();
                            objAddNewUser.Name = Convert.ToString(dr["Name"]);
                            objAddNewUser.EmailID = Convert.ToString(dr["EmailID"]);
                            objAddNewUser.MobileNo = Convert.ToString(dr["MobileNo"]);
                            objAddNewUser.LoginID = Convert.ToString(dr["LoginID"]);
                            objAddNewUser.Password = Convert.ToString(dr["Password"]);
                            objAddNewUser.UserType = Convert.ToString(dr["UserTypeName"]);
                            objAddNewUser.UserRole = Convert.ToString(dr["UserRoleName"]);
                            objAddNewUser.UserTypeName = Convert.ToString(dr["UserType"]);
                            objAddNewUser.UserRoleName = Convert.ToString(dr["UserRole"]);
                            objAddNewUser.InsurerName = Convert.ToString(dr["InsurerName"]);
                            objAddNewUser.InsurerID = Convert.ToString(dr["InsurerID"]);
                            objAddNewUser.UserID = Convert.ToString(dr["UserID"]);
                            //objAddNewUser.ApplicationType = Convert.ToString(dr["ApplicationTypeID"]);

                            objGetHealthExUser.Add(objAddNewUser);
                        }
                    }
                    //con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return objGetHealthExUser;
        }

        public int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo)
        {
            int res = 0;

            try
            {
                ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AddNewUser]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Name", DbType.String).Value = objUpdateUserInfo.Name;
                    cmd.Parameters.Add("@EmailID", DbType.String).Value = objUpdateUserInfo.EmailID;
                    cmd.Parameters.Add("@MobileNo", DbType.Int64).Value = objUpdateUserInfo.MobileNo;
                    cmd.Parameters.Add("@LoginID", DbType.String).Value = objUpdateUserInfo.LoginID;
                    cmd.Parameters.Add("@Password", DbType.String).Value = objUpdateUserInfo.Password;
                    cmd.Parameters.Add("@CreatedBy", DbType.Int32).Value = objUpdateUserInfo.CreatedBy;
                    cmd.Parameters.Add("@UserType", DbType.Int32).Value = objUpdateUserInfo.UserTypeName;
                    cmd.Parameters.Add("@UserRole", DbType.Int32).Value = objUpdateUserInfo.UserRoleName;
                    cmd.Parameters.Add("@InsurerID", DbType.Int32).Value = objUpdateUserInfo.InsurerID;
                   // cmd.Parameters.Add("@ApplicationID", DbType.Int32).Value = 2;
                    cmd.Parameters.Add("@InsurerName", DbType.String).Value = objUpdateUserInfo.InsurerName;
                    cmd.Parameters.Add("@Type", DbType.String).Value = objUpdateUserInfo.Type;
                    cmd.Parameters.Add("@UserID", DbType.String).Value = objUpdateUserInfo.UserID;
                    cmd.ExecuteNonQuery();

                    //con.Close();
                }

            }
            catch (Exception ex)
            {

            }
            return res;
        }

    }
}
