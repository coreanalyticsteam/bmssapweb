﻿using System.IO;
using System.Net;
//using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Office.Interop;
using ClosedXML.Excel;
//using ClosedXML;
using DocumentFormat.OpenXml;
using System.Web;
using System.ServiceModel.Web;
using System.Xml.Linq;
using System.Net.Http;
//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Spreadsheet;

namespace DataAccessLayer
{
    public class InspectionDal
    {
        DataSet _ds;
        DataTable _dt;
        SqlDataAdapter _adp;
        SqlConnection con;
        SqlCommand cmd;

        /// <summary>
        /// Upload MediaFile(videos/images) Details
        /// </summary>
        /// <param name="objMediaFileInfo"></param>
        /// <returns></returns>
        public bool UpdateMediaFileDetails(MediaFileInfo objMediaFileInfo)
        {
            try
            {
                string documentUrl = string.Empty;
                var fd = new FileUpload();
                //string postUrl = ConfigurationManager.AppSettings["UploadDocument"];
                MediaFileInfo objInfo = new MediaFileInfo();
                objInfo.InspectionId = objMediaFileInfo.InspectionId; 
                objInfo.Fileinfo = new FileDetails();
                LabURLBody objlabbody = new LabURLBody();
                objInfo.Fileinfo.FileUrl = objMediaFileInfo.Fileinfo.FileUrl;

                //string postUrl = ConfigurationManager.AppSettings["UploadFile"];
                //var result = MakeRequest(postUrl, objInfo, "POST", "application/json");
                //var result = MakeRequest(postUrl, objInfo, "POST", "application/json");
                //documentUrl = postUrl + result.Substring(1, result.Length - 2);
               //postUrl = postUrl + "\"leadId\":" + objMediaFileInfo.MobileNo + " , \"customerId\" :" + objMediaFileInfo.MobileNo + " , \"productId\" :" + "117" + " }";
                //dynamic dataResult = fd.GetPostAPIResponse(postUrl, objMediaFileInfo.Fileinfo.MediaData, objMediaFileInfo.Fileinfo.FileName);
                //if (!String.IsNullOrEmpty(Convert.ToString(dataResult.policyCopyDetails.policyDocUrl.Value)))
                //    documentUrl = Convert.ToString(dataResult.policyCopyDetails.policyDocUrl.Value);

                int res = 0;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Insert_MediaFileDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = objMediaFileInfo.InspectionId;
                    sqlCommand.Parameters.AddWithValue("@InspectionDate", DbType.DateTime).Value = Convert.ToDateTime(objMediaFileInfo.InspectionDate);
                    sqlCommand.Parameters.AddWithValue("@FileName", DbType.String).Value = objMediaFileInfo.Fileinfo.FileName;
                   // sqlCommand.Parameters.AddWithValue("@FileUrl", DbType.String).Value = documentUrl;
                    sqlCommand.Parameters.AddWithValue("@FileUrl", DbType.String).Value = objMediaFileInfo.Fileinfo.FileUrl;
                    sqlCommand.Parameters.AddWithValue("@ContentType", DbType.String).Value = objMediaFileInfo.Fileinfo.ContentType;
                    sqlCommand.Parameters.AddWithValue("@MobileNo", DbType.String).Value = objMediaFileInfo.MobileNo;
                    sqlCommand.Parameters.AddWithValue("@VehicleNo", DbType.String).Value = objMediaFileInfo.RegNo;
                    sqlCommand.Parameters.AddWithValue("@FileUrlInternal", DbType.String).Value = objMediaFileInfo.Fileinfo.FileUrlInternal;
                    con.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _ds = new DataSet();
                    _adp.Fill(_ds);
                    if (_ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in _ds.Tables[0].Rows)
                        {
                            objlabbody.ID = Convert.ToInt64(dr["ID"]);
                            objlabbody.CreatedOn = Convert.ToDateTime(dr["CurrentDate"]);
                        }
                    }
                }
                if (Convert.ToString(ConfigurationManager.AppSettings["AutoInspectionFlag"]) == "1")
                {
                    try
                    {
                        objlabbody.InspectionId = objMediaFileInfo.InspectionId;
                        objlabbody.MobileNo = objMediaFileInfo.MobileNo;
                        objlabbody.VehicleNo = objMediaFileInfo.RegNo;
                        objlabbody.InspectionDate = Convert.ToDateTime(objMediaFileInfo.InspectionDate);
                        objlabbody.FileName = objMediaFileInfo.Fileinfo.FileName;
                        objlabbody.FileUrl = objMediaFileInfo.Fileinfo.FileUrl;
                        objlabbody.ContentType = objMediaFileInfo.Fileinfo.ContentType;
                        objlabbody.IsActive = true;
                        objlabbody.FileUrlInternal = objMediaFileInfo.Fileinfo.FileUrlInternal; ;

                        dynamic Data = Newtonsoft.Json.JsonConvert.SerializeObject(objlabbody);
                        Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(Data);
                        common.MakeRequest(ConfigurationManager.AppSettings["AutoInspectionAPI"], json, "POST", "application/json", 20000);
                    }
                    catch (Exception ex)
                    {

                    }
                }
                return res != -1;

            }
            catch (Exception ex)
            {
                common.ErrorNInfoLogMethod(objMediaFileInfo.InspectionId + " " + ex.Message, "InspectionService.txt");
            }
            return false;
        }

        //public async Task GetAsync(string URL)
        //    {
              

        //        using (var client = new HttpClient())
        //        {
        //            try
        //            {
        //                var response = await client.GetAsync(URL);
        //                response.EnsureSuccessStatusCode(); // Throw exception if call is not successful

        //                await response.Content.ReadAsStringAsync();
        //            }
        //            catch (HttpRequestException)
        //            {

        //            }
        //        }
        //    }

        public static string MakeRequest(string requestUrl, object jsonRequest, string jsoNmethod, string jsonContentType)
        {
            try
            {
               

                var request = (HttpWebRequest)WebRequest.Create(requestUrl);
                request.Timeout = Convert.ToInt32(100000);
                request.Method = jsoNmethod;
                request.ContentType = jsonContentType;

                var encoding = new System.Text.UTF8Encoding();
                if (jsoNmethod != null && jsoNmethod == "POST")
                {
                    byte[] bytes = encoding.GetBytes(JsonConvert.SerializeObject(jsonRequest));

                    request.ContentLength = bytes.Length;

                    using (Stream requestStream = request.GetRequestStream())
                        requestStream.Write(bytes, 0, bytes.Length);
                }
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (var responseReader = new StreamReader(webStream))
                        {
                            return responseReader.ReadToEnd();
                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {

                return null;//throw ex;
            }
        }

        /// <summary>
        /// Get Videos Inspection Booking Details
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <returns></returns>
        public List<InspectionBookingDetails> GetInspectionBookingDetails(string inspectionId)
        {
            List<InspectionBookingDetails> objList = new List<InspectionBookingDetails>();

            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_InspectionBookingDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = inspectionId;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Int16).Value = 1;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                if (_dt.Rows.Count > 0)
                {
                    objList.AddRange(from DataRow item in _dt.Rows
                                     select new InspectionBookingDetails
                                     {
                                         BookingId = item["BookingId"] != DBNull.Value ? Convert.ToInt64(item["BookingId"]) : 0,
                                         CustomerId = item["CustomerId"] != DBNull.Value ? Convert.ToInt64(item["CustomerId"]) : 0,
                                         CustomerName = item["CustomerName"] != DBNull.Value ? Convert.ToString(item["CustomerName"]) : string.Empty,
                                         //IbdId = item["IBDId"] != DBNull.Value ? Convert.ToInt32(item["IBDId"]) : 0,
                                         StatusName = item["StatusName"] != DBNull.Value ? Convert.ToString(item["StatusName"]) : string.Empty,
                                         StatusId = item["InspectionStatusID"] != DBNull.Value ? Convert.ToInt32(item["InspectionStatusID"]) : 0,
                                         SubStatusId = item["InspectionSubStatusID"] != DBNull.Value ? Convert.ToInt32(item["InspectionSubStatusID"]) : 0,
                                         SubStatusName = item["SubStatusName"] != DBNull.Value ? Convert.ToString(item["SubStatusName"]) : string.Empty,
                                         InsurerId = item["InsurerId"] != DBNull.Value ? Convert.ToInt32(item["InsurerId"]) : 0,
                                         InsurerName = item["InsurerName"] != DBNull.Value ? Convert.ToString(item["InsurerName"]) : string.Empty,
                                         LeadSource = item["LeadSource"] != DBNull.Value ? Convert.ToString(item["LeadSource"]) : string.Empty,
                                         LeadId = item["LeadId"] != DBNull.Value ? Convert.ToInt64(item["LeadId"]) : 0,
                                         Remarks = item["Remarks"] != DBNull.Value ? Convert.ToString(item["Remarks"]) : string.Empty,
                                         VehicleNumber = item["VehicleNumber"] != DBNull.Value ? Convert.ToString(item["VehicleNumber"]) : string.Empty,
                                         InspectionId = item["InspectionId"] != DBNull.Value ? Convert.ToString(item["InspectionId"]) : string.Empty,
                                         UploadedDate = item["CreatedOn"] != DBNull.Value ? Convert.ToString(item["CreatedOn"]) : string.Empty
                                     });
                }
                return objList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDetails"></param>
        /// <returns></returns>
        public List<InspectionBookingDetails> InspectionDetailsList(InspectionBookingDetails objDetails)
        {
            List<InspectionBookingDetails> objList = new List<InspectionBookingDetails>();
            try
            {
                string fromDateTime = string.Empty;
                string toDateTime = string.Empty;

                if (!string.IsNullOrEmpty(objDetails.FromDate) && !string.IsNullOrEmpty(objDetails.ToDate))
                {
                    fromDateTime = Convert.ToDateTime(objDetails.FromDate).ToString("yyyy-MM-dd");
                    toDateTime = Convert.ToDateTime(objDetails.ToDate).ToString("yyyy-MM-dd"); 
                }

                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_InspectionBookingDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = objDetails.InspectionId;
                    sqlCommand.Parameters.AddWithValue("@CustomerId", DbType.String).Value = objDetails.CustomerId;
                    sqlCommand.Parameters.AddWithValue("@VehicleNumber", DbType.String).Value = objDetails.VehicleNumber;
                    if (objDetails.StatusName != "-Select-")
                        sqlCommand.Parameters.AddWithValue("@StatusName", DbType.String).Value = objDetails.StatusName;
                    sqlCommand.Parameters.AddWithValue("@MobileNo", DbType.String).Value = objDetails.MobileNo;
                    sqlCommand.Parameters.AddWithValue("@FromDate", DbType.Date).Value = fromDateTime;
                    sqlCommand.Parameters.AddWithValue("@ToDate", DbType.Date).Value = toDateTime;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Int16).Value = 2;
                    sqlCommand.Parameters.AddWithValue("@InsurerId", DbType.Int16).Value = objDetails.Insurer;
                    sqlCommand.Parameters.AddWithValue("@BookingId", DbType.Int64).Value = objDetails.BookingId;
                    sqlCommand.Parameters.AddWithValue("@LeadId", DbType.Int64).Value = objDetails.LeadId;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                if (_dt.Rows.Count > 0)
                {
                    foreach (DataRow item in _dt.Rows)
                    {
                        InspectionBookingDetails objBookingDetails = new InspectionBookingDetails();
                        if (!string.IsNullOrEmpty(Convert.ToString(item["BookingId"])))
                            objBookingDetails.BookingId = Convert.ToInt64(item["BookingId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CustomerId"])))
                            objBookingDetails.CustomerId = Convert.ToInt64(item["CustomerId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CustomerName"])))
                            objBookingDetails.CustomerName = Convert.ToString(item["CustomerName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["IBDId"])))
                            objBookingDetails.IbdId = Convert.ToInt32(item["IBDId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InsurerId"])))
                            objBookingDetails.InsurerId = Convert.ToInt32(item["InsurerId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InsurerName"])))
                            objBookingDetails.InsurerName = Convert.ToString(item["InsurerName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["StatusName"])))
                            objBookingDetails.StatusName = Convert.ToString(item["StatusName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["LeadSource"])))
                            objBookingDetails.LeadSource = Convert.ToString(item["LeadSource"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InspectionStatusID"])))
                            objBookingDetails.StatusId = Convert.ToInt32(item["InspectionStatusID"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["SubStatusName"])))
                            objBookingDetails.SubStatusName = Convert.ToString(item["SubStatusName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InspectionSubStatusID"])))
                            objBookingDetails.SubStatusId = Convert.ToInt32(item["InspectionSubStatusID"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["LeadId"])))
                            objBookingDetails.LeadId = Convert.ToInt64(item["LeadId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["VehicleNumber"])))
                            objBookingDetails.VehicleNumber = Convert.ToString(item["VehicleNumber"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CreatedOn"])))
                            objBookingDetails.UploadedDate = Convert.ToString(item["CreatedOn"]);
                        objBookingDetails.InspectionId = Convert.ToString(item["InspectionId"]);
                        objList.Add(objBookingDetails);
                    }
                }
                return objList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDetails"></param>
        /// <returns></returns>
        /// 

        public Byte[] PDFInspectionDetailsList(InspectionBookingDetails objDetails)
        {

            List<InspectionBookingDetails> objList = new List<InspectionBookingDetails>();
            try
            {
                string fromDateTime = string.Empty;
                string toDateTime = string.Empty;

                if (!string.IsNullOrEmpty(objDetails.FromDate) && !string.IsNullOrEmpty(objDetails.ToDate))
                {
                    fromDateTime = Convert.ToDateTime(objDetails.FromDate).ToString("yyyy-MM-dd");
                    toDateTime = Convert.ToDateTime(objDetails.ToDate).ToString("yyyy-MM-dd");
                }

                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_InspectionBookingDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = objDetails.InspectionId;
                    sqlCommand.Parameters.AddWithValue("@CustomerId", DbType.String).Value = objDetails.CustomerId;
                    sqlCommand.Parameters.AddWithValue("@VehicleNumber", DbType.String).Value = objDetails.VehicleNumber;
                    if (objDetails.StatusName != "-Select-")
                        sqlCommand.Parameters.AddWithValue("@StatusName", DbType.String).Value = objDetails.StatusName;
                    sqlCommand.Parameters.AddWithValue("@MobileNo", DbType.String).Value = objDetails.MobileNo;
                    sqlCommand.Parameters.AddWithValue("@FromDate", DbType.Date).Value = fromDateTime;
                    sqlCommand.Parameters.AddWithValue("@ToDate", DbType.Date).Value = toDateTime;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Int16).Value = 2;
                    sqlCommand.Parameters.AddWithValue("@InsurerId", DbType.Int16).Value = objDetails.Insurer;
                    sqlCommand.Parameters.AddWithValue("@BookingId", DbType.Int64).Value = objDetails.BookingId;
                    sqlCommand.Parameters.AddWithValue("@LeadId", DbType.Int64).Value = objDetails.LeadId;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }

                //ClosedXML.Excel.XLWorkbook wbook = new ClosedXML.Excel.XLWorkbook();
                //wbook.Worksheets.Add(_dt, "tab1");
                MemoryStream fs = new MemoryStream();
                if (_dt.Rows.Count > 0)
                {
                    XLWorkbook wb = new XLWorkbook();

                    wb.Worksheets.Add(_dt, "WorksheetName");


                    wb.SaveAs(fs);
                    fs.Position = 0;


                }
                return fs.ToArray();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //private static Byte[] ExportDataSetToExcel(DataSet ds)
        //{
        //    try
        //    {
        //        //Creae an Excel application instance
        //        Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
        //        MemoryStream fs = new MemoryStream();
                
        //        //Create an Excel workbook instance and open it from the predefined location
        //        //Microsoft.Office.Interop.Excel.Workbook excelWorkBook = new Microsoft.Office.Interop.Excel.Workbook();// excelApp.Workbooks.Open(@"E:\Org.xlsx");
        //        Microsoft.Office.Interop.Excel.Workbook excelWorkBook = excelApp.Workbooks.Add(fs);

        //        foreach (DataTable table in ds.Tables)
        //        {
        //            //Add a new worksheet to workbook with the Datatable name
        //            Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet = excelWorkBook.Sheets.Add();
        //            excelWorkSheet.Name = table.TableName;

        //            for (int i = 1; i < table.Columns.Count + 1; i++)
        //            {
        //                excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
        //            }

        //            for (int j = 0; j < table.Rows.Count; j++)
        //            {
        //                for (int k = 0; k < table.Columns.Count; k++)
        //                {
        //                    excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
        //                }
        //            }
        //        }

        //        excelWorkBook.Save();
        //        excelWorkBook.Close();
        //        excelApp.Quit();
        //        return fs.ToArray();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        

        /// <summary>
        /// Get Video Inspection Status
        /// </summary>
        /// <returns></returns>
        public List<StatusCollection> GetStatusCollection(string userType)
        {
            try
            {
                List<StatusCollection> objCollections = new List<StatusCollection>();
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_VideoInspectionStatus]"
                    };
                    sqlCommand.Parameters.AddWithValue("@UserType", DbType.Byte).Value = userType;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _ds = new DataSet();
                    _adp.Fill(_ds);
                }
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in _ds.Tables[0].Rows)
                    {
                        StatusCollection collection = new StatusCollection();
                        collection.StatusId = Convert.ToInt32(dr["StatusId"]);
                        collection.StatusName = Convert.ToString(dr["StatusName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["SubStatusId"])))
                            collection.SubStatusId = Convert.ToInt32(dr["SubStatusId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["SubStatusName"])))
                            collection.SubStatusName = Convert.ToString(dr["SubStatusName"]);
                        objCollections.Add(collection);
                    }
                }
                return objCollections;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InspectionBookingDetails> GetInsurerInspectionDetails(InspectionBookingDetails objBookingDetails)
        {
            List<InspectionBookingDetails> objList = new List<InspectionBookingDetails>();
            try
            {
                string fromDateTime = string.Empty;
                string toDateTime = string.Empty;

                if (!string.IsNullOrEmpty(objBookingDetails.FromDate) && !string.IsNullOrEmpty(objBookingDetails.ToDate))
                {
                    fromDateTime = Convert.ToDateTime(objBookingDetails.FromDate).ToString("yyyy-MM-dd");
                    toDateTime = Convert.ToDateTime(objBookingDetails.ToDate).ToString("yyyy-MM-dd");
                }
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_InspectionBookingDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = objBookingDetails.InspectionId;
                    sqlCommand.Parameters.AddWithValue("@VehicleNumber", DbType.String).Value = objBookingDetails.VehicleNumber;
                    sqlCommand.Parameters.AddWithValue("@InsurerId", DbType.String).Value = objBookingDetails.InsurerId;
                    sqlCommand.Parameters.AddWithValue("@FromDate", DbType.String).Value = fromDateTime;
                    sqlCommand.Parameters.AddWithValue("@ToDate", DbType.Date).Value = toDateTime;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Date).Value = 3;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                if (_dt.Rows.Count > 0)
                {
                    foreach (DataRow item in _dt.Rows)
                    {
                        InspectionBookingDetails objDetails = new InspectionBookingDetails();
                        if (!string.IsNullOrEmpty(Convert.ToString(item["BookingId"])))
                            objDetails.BookingId = Convert.ToInt64(item["BookingId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CustomerId"])))
                            objDetails.CustomerId = Convert.ToInt64(item["CustomerId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CustomerName"])))
                            objDetails.CustomerName = Convert.ToString(item["CustomerName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["StatusName"])))
                            objDetails.StatusName = Convert.ToString(item["StatusName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InspectionStatusID"])))
                            objDetails.StatusId = Convert.ToInt32(item["InspectionStatusID"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["SubStatusName"])))
                            objDetails.SubStatusName = Convert.ToString(item["SubStatusName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InspectionSubStatusID"])))
                            objDetails.SubStatusId = Convert.ToInt32(item["InspectionSubStatusID"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["VehicleNumber"])))
                            objDetails.VehicleNumber = Convert.ToString(item["VehicleNumber"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InspectionId"])))
                            objDetails.InspectionId = Convert.ToString(item["InspectionId"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["InsurerName"])))
                            objDetails.InsurerName = Convert.ToString(item["InsurerName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CreatedOn"])))
                            objDetails.UploadedDate = Convert.ToString(item["CreatedOn"]);
                        objList.Add(objDetails);
                    }
                }
                return objList;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<MediaFileInfo> GetMediaFileUrl(string inspectionId)
        {
            try
            {
                List<MediaFileInfo> mediaFiles = new List<MediaFileInfo>();
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_MediaFileInfo]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = inspectionId;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                if (_dt.Rows.Count > 0)
                {
                    foreach (DataRow row in _dt.Rows)
                    {
                        MediaFileInfo objFileInfo = new MediaFileInfo();
                        objFileInfo.Fileinfo = new FileDetails();
                        objFileInfo.Fileinfo.FileName = Convert.ToString(row["FileName"]);
                        objFileInfo.Fileinfo.FileUrl = Convert.ToString(row["FileUrl"]);
                        objFileInfo.Fileinfo.FileUrlInternal = Convert.ToString(row["FileUrlInternal"]);
                        mediaFiles.Add(objFileInfo);
                    }
                }
                return mediaFiles;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InspectionDocument> GetInspectionsDocument(string inspectionId)
        {
            try
            {
                List<InspectionDocument> inspectiondocumentfiles = new List<InspectionDocument>();
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[GetInspectionDocuments]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = inspectionId;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                if (_dt.Rows.Count > 0)
                {
                    foreach (DataRow row in _dt.Rows)
                    {
                        InspectionDocument objInspectionDocument = new InspectionDocument();
                        objInspectionDocument.FileName = Convert.ToString(row["FileName"]);
                        objInspectionDocument.DocURL = Convert.ToString(row["DocURL"]);
                        objInspectionDocument.CreatedBy = Convert.ToString(row["Name"]);
                        objInspectionDocument.CreatedOn = Convert.ToString(row["CreatedOn"]);
                        inspectiondocumentfiles.Add(objInspectionDocument);
                    }
                }
                return inspectiondocumentfiles;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateStatus(InspectionBookingDetails objDetails)
        {
            try
            {
                int res = 0;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Insert_BookingStatusDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = objDetails.InspectionId;
                    sqlCommand.Parameters.AddWithValue("@StatusId", DbType.Int32).Value = objDetails.StatusId;
                    sqlCommand.Parameters.AddWithValue("@SubStatusId", DbType.String).Value = objDetails.SubStatusId;
                    sqlCommand.Parameters.AddWithValue("@Remarks", DbType.String).Value = objDetails.Remarks;
                    sqlCommand.Parameters.AddWithValue("@CreatedBy", DbType.Int32).Value = objDetails.UserID;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Int32).Value = 1;
                    con.Open();
                     //sqlCommand.ExecuteNonQuery();
                     _adp = new SqlDataAdapter(sqlCommand);
                     _dt = new DataTable();
                     _adp.Fill(_dt);
                     if (objDetails.StatusId == 3)
                     {
                         if (_dt.Rows.Count > 0)
                         {
                             DataRow dr = _dt.Rows[0];

                             string[] SMSwords = ConfigurationManager.AppSettings["UWApprovedSMS"].Split(',');
                             
                             foreach (string word in SMSwords)
                             {
                                 string SMS = "";
                                 Console.WriteLine(word);
                                 if (SMS == "")
                                     SMS = "\"" + word + "\"";
                                 string SMSBody = "Id:##InspectionId##%0aRegNo:##Vehicle##%0a##InsurerName## UW Approved";
                                 SMSBody = SMSBody.Replace("##InspectionId##", dr["InspectionId"].ToString());
                                 SMSBody = SMSBody.Replace("##Vehicle##", dr["VehicleNumber"].ToString());
                                 SMSBody = SMSBody.Replace("##InsurerName##", dr["ShortName"].ToString());
                                 // string jsonSMS = "{\"CommunicationDetails\":{\"LeadID\":\"\",\"ProductID\":\"\",\"Conversations\":[{\"ToReceipent\":[\"" + dr["MobileNo"] + "\"],\"From\":\"\",\"CreatedBy\":\"InspectionQCApproved\",\"UserID\":124,\"LeadStatusID\":\" \",\"TriggerName\":\"Renewal_Expiry_Reminder\"}],\"CommunicationType\":2}}";
                                 string jsonSMS = "{\"CommunicationDetails\": {\"LeadID\": 0,\"ProductID\": 117,\"Conversations\": [{\"ToReceipent\": [##MobileNo##],\"Body\": \"##SMSBody##\",\"Subject\": \"SMS Title\",\"CreatedBy\": \"Channel\",\"AutoTemplate\": true}],\"CommunicationType\": 2}}";
                                 //jsonSMS = jsonSMS.Replace("##MobileNo##", dr["MobileNo"].ToString());
                                 jsonSMS = jsonSMS.Replace("##MobileNo##", SMS);
                                 jsonSMS = jsonSMS.Replace("##SMSBody##", SMSBody);
                                 Communication.SendEmailSms_Comm(jsonSMS);

                             }
                            

                             string[] Towords = ConfigurationManager.AppSettings["UWApprovedToEmail"].Split(',');
                             string ToEmail = "";
                             foreach (string word in Towords)
                             {
                                 Console.WriteLine(word);
                                 if (ToEmail == "")
                                     ToEmail = "\"" + word + "\"";
                                 else
                                     ToEmail = ToEmail + ",\"" + word + "\"";

                             }
                             string[] words = ConfigurationManager.AppSettings["UWApprovedCCEmail"].Split(',');
                             string CCEmail = "";
                             foreach (string word in words)
                             {
                                 Console.WriteLine(word);
                                 if (CCEmail == "")
                                     CCEmail = "\"" + word + "\"";
                                 else
                                     CCEmail = CCEmail + ",\"" + word + "\"";

                             }
                             Communication.SendEmailQCApproved(dr["VehicleNumber"].ToString(), dr["InspectionId"].ToString(), ToEmail, CCEmail, "UW Approved", dr["InsurerName"].ToString());
                         }

                     }
                }
                return res != -1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InspectionBookingDetails> InsurerInspectionHistory(string inspectionId, int type)
        {
            List<InspectionBookingDetails> objList = new List<InspectionBookingDetails>();
            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_InspectionHistory]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = inspectionId;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Int32).Value = type;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                if (_dt.Rows.Count > 0)
                {
                    foreach (DataRow item in _dt.Rows)
                    {
                        InspectionBookingDetails objDetails = new InspectionBookingDetails();
                        if (!string.IsNullOrEmpty(Convert.ToString(item["StatusName"])))
                            objDetails.StatusName = Convert.ToString(item["StatusName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["SubStatusName"])))
                            objDetails.SubStatusName = Convert.ToString(item["SubStatusName"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["Remarks"])))
                            objDetails.Remarks = Convert.ToString(item["Remarks"]);
                      
                        if (!string.IsNullOrEmpty(Convert.ToString(item["Name"])))
                            objDetails.UserName = Convert.ToString(item["Name"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CreatedOn"])))
                            objDetails.UploadedDate = Convert.ToString(item["CreatedOn"]);
                        objList.Add(objDetails);
                    }
                }
                return objList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdatePbInspectionStatus(InspectionBookingDetails objDetails)
        {
            try
            {
                int res = 0;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Insert_BookingStatusDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = objDetails.InspectionId;
                    sqlCommand.Parameters.AddWithValue("@StatusId", DbType.Int32).Value = objDetails.StatusId;
                    sqlCommand.Parameters.AddWithValue("@SubStatusId", DbType.String).Value = objDetails.SubStatusId;
                    sqlCommand.Parameters.AddWithValue("@Remarks", DbType.String).Value = objDetails.Remarks;
                    sqlCommand.Parameters.AddWithValue("@CreatedBy", DbType.Int32).Value = objDetails.UserID;
                    sqlCommand.Parameters.AddWithValue("@InsurerId", DbType.Int32).Value = objDetails.InsurerId;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Int32).Value = 2;
                    con.Open();
                    //sqlCommand.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                    if (objDetails.StatusId == 1)
                    {
                        if (_dt.Rows.Count > 0)
                        {
                            DataRow dr = _dt.Rows[0];
                            string SMSBody = "Id:##InspectionId##%0aRegNo:##Vehicle##%0a##InsurerName## QC Approved";
                            SMSBody = SMSBody.Replace("##InspectionId##", dr["InspectionId"].ToString());
                            SMSBody = SMSBody.Replace("##Vehicle##", dr["VehicleNumber"].ToString());
                            SMSBody = SMSBody.Replace("##InsurerName##", dr["ShortName"].ToString());
                            // string jsonSMS = "{\"CommunicationDetails\":{\"LeadID\":\"\",\"ProductID\":\"\",\"Conversations\":[{\"ToReceipent\":[\"" + dr["MobileNo"] + "\"],\"From\":\"\",\"CreatedBy\":\"InspectionQCApproved\",\"UserID\":124,\"LeadStatusID\":\" \",\"TriggerName\":\"Renewal_Expiry_Reminder\"}],\"CommunicationType\":2}}";
                            string jsonSMS = "{\"CommunicationDetails\": {\"LeadID\": 0,\"ProductID\": 117,\"Conversations\": [{\"ToReceipent\": [\"##MobileNo##\"],\"Body\": \"##SMSBody##\",\"Subject\": \"SMS Title\",\"CreatedBy\": \"Channel\",\"AutoTemplate\": true}],\"CommunicationType\": 2}}";
                            jsonSMS = jsonSMS.Replace("##MobileNo##", dr["MobileNo"].ToString());
                          //  jsonSMS = jsonSMS.Replace("##MobileNo##", "9953141600");
                            jsonSMS = jsonSMS.Replace("##SMSBody##", SMSBody);
                            Communication.SendEmailSms_Comm(jsonSMS);
                            string[] Towords = ConfigurationManager.AppSettings["UWApprovedToEmail"].Split(',');
                            string ToEmail = "";
                            foreach (string word in Towords)
                            {
                                Console.WriteLine(word);
                                if (ToEmail == "")
                                    ToEmail = "\"" + word + "\"";
                                else
                                    ToEmail = ToEmail + ",\"" + word + "\"";

                            }
                            string[] words = ConfigurationManager.AppSettings["QCApprovedCCEmail"].Split(',');
                            string CCEmail = "";
                            foreach (string word in words)
                            {
                                Console.WriteLine(word);
                                if (CCEmail == "")
                                    CCEmail = "\"" + word + "\"";
                                else
                                    CCEmail = CCEmail +",\"" + word + "\"";

                            }
                            //Communication.SendEmailQCApproved(dr["VehicleNumber"].ToString(), dr["InspectionId"].ToString(),"\""+ dr["EmailId"].ToString()+"\"", CCEmail, "QC Approved");
                            Communication.SendEmailQCApproved(dr["VehicleNumber"].ToString(), dr["InspectionId"].ToString(), ToEmail, CCEmail, "QC Approved", dr["InsurerName"].ToString());
                        }

                    }
                }
                return res != -1;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool InspectionRollback(InspectionRollbackDetails objRollbackDetails)
        {
            try
            {
                int res = 0;
                using (SqlConnection con = new SqlConnection())
                {
                    con.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = con,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[InspectionRollback]"
                        //CommandText = "[BMS].[Insert_BookingStatusDetails]"
                    };
                    sqlCommand.Parameters.AddWithValue("@InspectionId", DbType.String).Value = objRollbackDetails.InspectionId;
                    sqlCommand.Parameters.AddWithValue("@StatusId", DbType.Int32).Value = objRollbackDetails.StatusId;
                    sqlCommand.Parameters.AddWithValue("@SubStatusId", DbType.Int32).Value = objRollbackDetails.SubStatusId;
                   // sqlCommand.Parameters.AddWithValue("@Remarks", DbType.String).Value = objDetails.Remarks;
                    sqlCommand.Parameters.AddWithValue("@CreatedBy", DbType.Int32).Value = objRollbackDetails.UserID;
                    sqlCommand.Parameters.AddWithValue("@InsurerId", DbType.Int32).Value = objRollbackDetails.InsurerId;
                    sqlCommand.Parameters.AddWithValue("@Type", DbType.Int32).Value = 2;
                    con.Open();
                    //sqlCommand.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                return res != -1;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InsurerCollection> GetInsurerCollection()
        {
            try
            {
                List<InsurerCollection> objInsurerCollections = new List<InsurerCollection>();
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.PBCromasqlConnection();
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "[BMS].[Get_InspectionInsurers]"
                    };
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }
                if (_dt.Rows.Count > 0)
                {
                    objInsurerCollections.AddRange(from DataRow item in _dt.Rows
                        select new InsurerCollection
                        {
                            InsurerId = Convert.ToInt32(item["InsurerId"]), InsurerName = Convert.ToString(item["InsurerName"])
                        });
                }
                return objInsurerCollections;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int AddNewUsers(AddNewUser objAddNewUser)
        {
            int res = 0;

            try
            {
                _ds = new DataSet();

                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AddNewUser]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Name", DbType.String).Value = objAddNewUser.Name;
                    cmd.Parameters.Add("@EmailID", DbType.String).Value = objAddNewUser.EmailID;
                    cmd.Parameters.Add("@MobileNo", DbType.Int64).Value = objAddNewUser.MobileNo;
                    cmd.Parameters.Add("@LoginID", DbType.String).Value = objAddNewUser.LoginID;
                    cmd.Parameters.Add("@Password", DbType.String).Value = objAddNewUser.Password;
                    cmd.Parameters.Add("@CreatedBy", DbType.Int32).Value = objAddNewUser.CreatedBy;
                    cmd.Parameters.Add("@UserType", DbType.Int32).Value = objAddNewUser.UserTypeName;
                    cmd.Parameters.Add("@UserRole", DbType.Int32).Value = objAddNewUser.UserRoleName;
                    cmd.Parameters.Add("@InsurerID", DbType.Int32).Value = objAddNewUser.InsurerID;
                    //cmd.Parameters.Add("@ApplicationID", DbType.Int32).Value = objAddNewUser.ApplicationType;
                    cmd.Parameters.Add("@InsurerName", DbType.String).Value = objAddNewUser.InsurerName;
                    cmd.Parameters.Add("@Type", DbType.String).Value = objAddNewUser.Type;
                    cmd.Parameters.Add("@UserID", DbType.String).Value = objAddNewUser.UserID;

                    cmd.Parameters.Add("@Error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    // cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    //


                    res = cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@Error"].Value.ToString());
                    // con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return res;


        }

        public int ChangePassword(ChangePassword objChangePassword)
        {
            int res = 0;

            try
            {
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[ExChangePassword]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@OldPassword", DbType.String).Value = objChangePassword.OldPassword;
                    cmd.Parameters.Add("@NewPassword", DbType.String).Value = objChangePassword.NewPassword;
                    cmd.Parameters.Add("@UserId", DbType.String).Value = objChangePassword.UserId;
                    // cmd.Parameters.Add("@UserType", DbType.Int32).Value = objChangePassword.UserType;
                    cmd.Parameters.Add("@error", SqlDbType.Int).Direction = ParameterDirection.Output;
                    // cmd.ExecuteNonQuery();
                    //adp = new SqlDataAdapter(cmd);
                    //adp.Fill(ds);
                    //


                    res = cmd.ExecuteNonQuery();
                    res = Convert.ToInt16(cmd.Parameters["@error"].Value.ToString());
                    // con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return res;


        }

        public List<MasterSupplier> GetSupplierListByProduct(string ProductID)
        {
            MasterSupplier objmastersupplier;
            List<MasterSupplier> objmastersupplierList = new List<MasterSupplier>();
            try
            {
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetSuppliers]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ProductID", DbType.Int32).Value = ProductID;
                    cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);

                    if (_ds != null && _ds.Tables.Count > 0 && _ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in _ds.Tables[0].Rows)
                        {
                            objmastersupplier = new MasterSupplier();
                            objmastersupplier.InsurerName = Convert.ToString(dr["SupplierName"]);
                            objmastersupplier.InsurerID = Convert.ToInt32(dr["SupplierProduct_ID"]);
                            objmastersupplierList.Add(objmastersupplier);
                        }
                    }
                    //con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return objmastersupplierList;
        }

        public List<AddNewUser> GetHealthExUser()
        {
            AddNewUser objAddNewUser;
            List<AddNewUser> objGetHealthExUser = new List<AddNewUser>();
            try
            {
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    // con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetHealthExUser]", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.Add("@ProductID", DbType.Int32).Value = ProductID;
                    cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);

                    if (_ds != null && _ds.Tables.Count > 0 && _ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in _ds.Tables[0].Rows)
                        {
                            objAddNewUser = new AddNewUser();
                            objAddNewUser.Name = Convert.ToString(dr["Name"]);
                            objAddNewUser.EmailID = Convert.ToString(dr["EmailID"]);
                            objAddNewUser.MobileNo = Convert.ToString(dr["MobileNo"]);
                            objAddNewUser.LoginID = Convert.ToString(dr["LoginID"]);
                            objAddNewUser.Password = Convert.ToString(dr["Password"]);
                            objAddNewUser.UserType = Convert.ToString(dr["UserTypeName"]);
                            objAddNewUser.UserRole = Convert.ToString(dr["UserRoleName"]);
                            objAddNewUser.UserTypeName = Convert.ToString(dr["UserType"]);
                            objAddNewUser.UserRoleName = Convert.ToString(dr["UserRole"]);
                            objAddNewUser.InsurerName = Convert.ToString(dr["InsurerName"]);
                            objAddNewUser.InsurerID = Convert.ToString(dr["InsurerID"]);
                            objAddNewUser.UserID = Convert.ToString(dr["UserID"]);
                            //objAddNewUser.ApplicationType = Convert.ToString(dr["ApplicationTypeID"]);

                            objGetHealthExUser.Add(objAddNewUser);
                        }
                    }
                    //con.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return objGetHealthExUser;
        }

        public int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo)
        {
            int res = 0;

            try
            {
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[AddNewUser]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Name", DbType.String).Value = objUpdateUserInfo.Name;
                    cmd.Parameters.Add("@EmailID", DbType.String).Value = objUpdateUserInfo.EmailID;
                    cmd.Parameters.Add("@MobileNo", DbType.Int64).Value = objUpdateUserInfo.MobileNo;
                    cmd.Parameters.Add("@LoginID", DbType.String).Value = objUpdateUserInfo.LoginID;
                    cmd.Parameters.Add("@Password", DbType.String).Value = objUpdateUserInfo.Password;
                    cmd.Parameters.Add("@CreatedBy", DbType.Int32).Value = objUpdateUserInfo.CreatedBy;
                    cmd.Parameters.Add("@UserType", DbType.Int32).Value = objUpdateUserInfo.UserTypeName;
                    cmd.Parameters.Add("@UserRole", DbType.Int32).Value = objUpdateUserInfo.UserRoleName;
                    cmd.Parameters.Add("@InsurerID", DbType.Int32).Value = objUpdateUserInfo.InsurerID;
                    //cmd.Parameters.Add("@ApplicationID", DbType.Int32).Value = 1;
                    cmd.Parameters.Add("@InsurerName", DbType.String).Value = objUpdateUserInfo.InsurerName;
                    cmd.Parameters.Add("@Type", DbType.String).Value = objUpdateUserInfo.Type;
                    cmd.Parameters.Add("@UserID", DbType.String).Value = objUpdateUserInfo.UserID;
                    cmd.ExecuteNonQuery();

                    //con.Close();
                }

            }
            catch (Exception ex)
            {

            }
            return res;


        }

        public ResultClass GetApplicationAccess(int UserID)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetExApplicationRights]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@UserID", DbType.String).Value =  UserID;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess =false;
            }
            return result;
        }

        public ResultClass GetUserDetails(int UserID)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetUserDetails]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@UserID", DbType.String).Value = UserID;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                   
                    UserDetail oUD = new UserDetail();
                    if (UserID != 0)
                    {
                        DataRow oDr = _ds.Tables[0].Rows[0];
                        oUD.UserId = Convert.ToInt32(oDr["UserID"]);
                        oUD.Name = Convert.ToString(oDr["Name"]);
                        oUD.EmailID = Convert.ToString(oDr["EmailID"]);
                        //oUD.MobileNo = Convert.ToInt32(oDr["MobileNo"]);
                        oUD.LoginID = Convert.ToString(oDr["LoginID"]);
                        oUD.Password = Convert.ToString(oDr["Password"]);
                        UserType oUT = new UserType();
                        oUT.ID = Convert.ToInt32(oDr["UserType"]);
                        oUT.Name = Convert.ToString(oDr["UserTypeName"]);
                        oUD.UserType = oUT;
                        if (oUT.ID == 1)
                        {
                            if (Convert.ToString(oDr["UserRoleName"]).Length > 0)
                            {
                                UserRole oUR = new UserRole();
                                oUR.ID = Convert.ToInt32(oDr["UserRole"]);
                                oUR.Name = Convert.ToString(oDr["UserRoleName"]);
                                oUD.UserRole = oUR;
                            }
                        }
                        oUD.IsAcitve = Convert.ToBoolean(oDr["IsAcitve"]);
                        if (oUT.ID == 2)
                        {
                            if (oDr["ProductId"] != null)
                            {
                                if (oDr["ProductId"].ToString().Length > 0)
                                {
                                    Products oPr = new Products();
                                    oPr.ProductID = Convert.ToInt32(oDr["ProductId"]);
                                    oPr.Product = Convert.ToString(oDr["ProductName"]);
                                    oUD.Product = oPr;
                                }
                            }

                            if (oDr["InsurerID"] != null)
                            {
                                if (oDr["InsurerID"].ToString().Length > 0)
                                {
                                    Insurer oIn = new Insurer();
                                    oIn.ID = Convert.ToInt32(oDr["InsurerID"]);
                                    oIn.SupplierName = Convert.ToString(oDr["SupplierName"]);
                                    oUD.Insurer = oIn;
                                }
                            }
                        }
                    }
                    List<ApplicationRights> ListAR = new List<ApplicationRights>();
                    foreach (DataRow oDr1 in _ds.Tables[1].Rows)
                    {
                        ApplicationRights oAR = new ApplicationRights();
                        oAR.Id = Convert.ToInt32(oDr1["Id"]);
                        oAR.ApplicationId = Convert.ToInt32(oDr1["ApplicationId"]);
                        oAR.AppName = Convert.ToString(oDr1["AppName"]);
                        if(oAR.Id > 0)
                        oAR.IsChecked = true;
                        ListAR.Add(oAR);
                    }
                    oUD.ApplicationRights = ListAR;

                    List<MenuRights> ListMR = new List<MenuRights>();
                    foreach (DataRow oDr2 in _ds.Tables[2].Rows)
                    {
                        MenuRights oMR = new MenuRights();
                        oMR.ID = Convert.ToInt32(oDr2["ID"]);
                        oMR.MenuID = Convert.ToInt32(oDr2["MenuID"]);
                        oMR.Default = Convert.ToInt16(oDr2["Default"]);
                        oMR.MenuName = Convert.ToString(oDr2["MenuName"]);
                        //if (oMR.ID > 0)
                        oMR.IsChecked = Convert.ToBoolean(oDr2["IsActive"]);
                        if (oMR.Default == 1)
                            oUD.Default = Convert.ToInt32(oDr2["MenuID"]);
                        ListMR.Add(oMR);
                    }
                    oUD.MenuRights = ListMR;


                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(oUD);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }
       public ResultClass SaveUserDetails(UserDetail Json)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[SaveUserDetails]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@UserID", DbType.Int32).Value = Json.UserId;
                    cmd.Parameters.Add("@Name", DbType.String).Value = Json.Name;
                    cmd.Parameters.Add("@EmailID", DbType.String).Value = Json.EmailID;
                    cmd.Parameters.Add("@MobileNo", DbType.String).Value = Json.MobileNo;
                    cmd.Parameters.Add("@LoginID", DbType.String).Value = Json.LoginID;
                    cmd.Parameters.Add("@Password", DbType.String).Value = Json.Password;
                    cmd.Parameters.Add("@IsAcitve", DbType.Boolean).Value = Json.IsAcitve;
                    //cmd.Parameters.Add("@CreatedBy", DbType.String).Value = ;
                    cmd.Parameters.Add("@UserType", DbType.Int16).Value = Json.UserType.ID;
                    if (Json.UserType.ID == 1)
                    {
                        cmd.Parameters.Add("@UserRole", DbType.Int16).Value = Json.UserRole.ID;
                    }
                    if (Json.UserType.ID == 2)
                    {
                        cmd.Parameters.Add("@InsurerID", DbType.Int32).Value = Json.Insurer.ID;
                        cmd.Parameters.Add("@ProductId", DbType.Int32).Value = Json.Product.ProductID;
                    }
                   // cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    con.Close();
                    result.Error = _ds.Tables[0].Rows[0][0].ToString();
                    if (result.Error == "")
                    {
                        Json.UserId = Convert.ToInt32(_ds.Tables[1].Rows[0][0]);
                        foreach (ApplicationRights oAR in Json.ApplicationRights)
                        {
                            con.Open();
                            cmd = new SqlCommand("[BMS].[InsertExUserApplicationLoginRight]", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@Id", DbType.Int32).Value = oAR.Id;
                            cmd.Parameters.Add("@UserID", DbType.Int32).Value = Json.UserId;
                            cmd.Parameters.Add("@ApplicationId", DbType.Int16).Value = oAR.ApplicationId;
                            cmd.Parameters.Add("@GiveRights", DbType.Boolean).Value = oAR.IsChecked;
                            cmd.ExecuteNonQuery();
                            con.Close();
                            if (oAR.ApplicationId == 3)
                            {
                                Json.UserId = Convert.ToInt32(_ds.Tables[1].Rows[0][0]);
                                foreach (MenuRights oMR in Json.MenuRights)
                                {             
                                     con.Open();
                                    cmd = new SqlCommand("[BMS].[InsertBookingScoreMenuMapping]", con);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add("@Id", DbType.Int32).Value = oMR.ID;
                                    cmd.Parameters.Add("@UserID", DbType.Int32).Value = Json.UserId;
                                    
                                        cmd.Parameters.Add("@MenuID", DbType.Int16).Value = oMR.MenuID;
                                        cmd.Parameters.Add("@IsChecked", DbType.Boolean).Value = oMR.IsChecked;
                                
                                    if (Json.Default == oMR.MenuID)
                                    {
                                        cmd.Parameters.Add("@Default", DbType.Int16).Value = Json.Default;
                                    }
                                    if (oMR.ID > 0 || (oMR.ID == 0 && oMR.IsChecked))
                                        cmd.ExecuteNonQuery(); 
                                     con.Close();
                                }
                                                               
                            }
                          
                            //_adp = new SqlDataAdapter(cmd);
                            //_adp.Fill(_ds);
                        }                      
                        result = GetUserDetails(Json.UserId);
                        result.Error = "";
                    }
                    else
                    {
                        result.IsSuccess = false;
                    }
                    con.Close();
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

       public ResultClass GetUserDetailsTicket(int UserID)
       {
           ResultClass result = new ResultClass();

           try
           {
               result.IsAuth = true;
               _ds = new DataSet();
               using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
               {
                   //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                   con.Open();
                   cmd = new SqlCommand("[BMS].[GetUserDetailsTicket]", con);
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.Parameters.Add("@UserID", DbType.String).Value = UserID;
                   //cmd.ExecuteNonQuery();
                   _adp = new SqlDataAdapter(cmd);
                   _adp.Fill(_ds);
                   //con.Close();

                   UserDetail oUD = new UserDetail();
                   if (UserID != 0)
                   {
                       DataRow oDr = _ds.Tables[0].Rows[0];
                       oUD.UserId = Convert.ToInt32(oDr["UserID"]);
                       oUD.Name = Convert.ToString(oDr["Name"]);
                       oUD.EmailID = Convert.ToString(oDr["EmailID"]);
                       //oUD.MobileNo = Convert.ToInt32(oDr["MobileNo"]);
                       oUD.LoginID = Convert.ToString(oDr["LoginID"]);
                       oUD.Password = Convert.ToString(oDr["Password"]);
                       UserType oUT = new UserType();
                       oUT.ID = Convert.ToInt32(oDr["UserType"]);
                       oUT.Name = Convert.ToString(oDr["UserTypeName"]);
                       oUD.UserType = oUT;
                       if (oUT.ID == 1)
                       {
                           if (Convert.ToString(oDr["UserRoleName"]).Length > 0)
                           {
                               UserRole oUR = new UserRole();
                               oUR.ID = Convert.ToInt32(oDr["UserRole"]);
                               oUR.Name = Convert.ToString(oDr["UserRoleName"]);
                               oUD.UserRole = oUR;
                           }
                       }
                       oUD.IsAcitve = Convert.ToBoolean(oDr["IsAcitve"]);
                       oUD.MappingId = Convert.ToInt32(oDr["InsurerID"]); 
                       //if (oUT.ID == 2)
                       //{
                       //    if (oDr["ProductId"] != null)
                       //    {
                       //        if (oDr["ProductId"].ToString().Length > 0)
                       //        {
                       //            Products oPr = new Products();
                       //            oPr.ProductID = Convert.ToInt32(oDr["ProductId"]);
                       //            oPr.Product = Convert.ToString(oDr["ProductName"]);
                       //            oUD.Product = oPr;
                       //        }
                       //    }

                       //    if (oDr["InsurerID"] != null)
                       //    {
                       //        if (oDr["InsurerID"].ToString().Length > 0)
                       //        {
                       //            Insurer oIn = new Insurer();
                       //            oIn.ID = Convert.ToInt32(oDr["InsurerID"]);
                       //            oIn.SupplierName = Convert.ToString(oDr["SupplierName"]);
                       //            oUD.Insurer = oIn;
                       //        }
                       //    }
                       //}
                   }
                   List<InsurerUserMapping> ListAR = new List<InsurerUserMapping>();
                   foreach (DataRow oDr1 in _ds.Tables[1].Rows)
                   {
                       InsurerUserMapping oAR = new InsurerUserMapping();
                       oAR.SNO = Convert.ToInt32(oDr1["SNO"]);
                       oAR.MappingId = Convert.ToInt32(oDr1["MappingId"]);
                       oAR.InsurerID = Convert.ToInt32(oDr1["InsurerID"]);
                       oAR.ProductId = Convert.ToInt32(oDr1["ProductId"]);
                       oAR.IsActive = Convert.ToBoolean(oDr1["IsActive"]);
                       oAR.SupplierName = Convert.ToString(oDr1["SupplierName"]);
                       oAR.ProductName = Convert.ToString(oDr1["ProductName"]);
                      
                       ListAR.Add(oAR);
                   }
                   oUD.InsurerUserMapping = ListAR;


                   result.IsSuccess = true;
                   result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(oUD);
                   return result;
               }

           }
           catch (Exception ex)
           {
               result.IsSuccess = false;
           }
           return result;
       }

       public ResultClass SaveUserDetailsTicket(UserDetail Json)
       {
           ResultClass result = new ResultClass();

           try
           {
               result.IsAuth = true;
               _ds = new DataSet();
               using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
               {
                   //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                   con.Open();
                   cmd = new SqlCommand("[BMS].[SaveUserDetailsTicket]", con);
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.Parameters.Add("@UserID", DbType.Int32).Value = Json.UserId;
                   cmd.Parameters.Add("@Name", DbType.String).Value = Json.Name;
                   cmd.Parameters.Add("@EmailID", DbType.String).Value = Json.EmailID;
                   cmd.Parameters.Add("@MobileNo", DbType.String).Value = Json.MobileNo;
                   cmd.Parameters.Add("@LoginID", DbType.String).Value = Json.LoginID;
                   cmd.Parameters.Add("@Password", DbType.String).Value = Json.Password;
                   cmd.Parameters.Add("@IsAcitve", DbType.Boolean).Value = Json.IsAcitve;
                   cmd.Parameters.Add("@CreatedBy", DbType.String).Value = Json.CreatedBy;
                   cmd.Parameters.Add("@UserType", DbType.Int16).Value = 2;
                   cmd.Parameters.Add("@InsurerID", DbType.Int32).Value = Json.MappingId;
                
                   var xEle = new XElement("UserMapping",
                                   from objUserMapping in Json.InsurerUserMapping
                                   select new XElement("UserMapping",
                                                  new XAttribute("SNO", Convert.ToString(objUserMapping.SNO)),
                                                   new XAttribute("MappingId", Convert.ToString(objUserMapping.MappingId)),
                                                    new XAttribute("ProductId", Convert.ToString(objUserMapping.ProductId)),
                                                     new XAttribute("InsurerID", Convert.ToString(objUserMapping.InsurerID)),
                                                     new XAttribute("SupplierName", Convert.ToString(objUserMapping.SupplierName)),
                                                     new XAttribute("ProductName", Convert.ToString(objUserMapping.ProductName)),
                                                     new XAttribute("IsActive", Convert.ToString(objUserMapping.IsActive))
                                              )).ToString();
                   cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;    
                   _adp = new SqlDataAdapter(cmd);
                   _adp.Fill(_ds);
                   con.Close();
                   result.Error = _ds.Tables[0].Rows[0][0].ToString();
                   if (result.Error == "")
                   {
                       Json.UserId = Convert.ToInt32(_ds.Tables[1].Rows[0][0]);
                  
                       result = GetUserDetailsTicket(Json.UserId);
                       result.Error = "";
                   }
                   else
                   {
                       result.IsSuccess = false;
                   }
                   con.Close();
                   return result;
               }

           }
           catch (Exception ex)
           {
               result.IsSuccess = false;
           }
           return result;
       }
        public ResultClass GetProducts()
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetProducts]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }
        public ResultClass GetSuppliers(int ProductId)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetSuppliers]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ProductId", DbType.String).Value = ProductId;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }
        public ResultClass GetExUser()
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetExUser]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("@ApplicationRight", DbType.String).Value = 1;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }
        public ResultClass GetExUserTicket()
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetExUserTicket]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("@ApplicationRight", DbType.String).Value = 1;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }
        public ResultClass ReplicateInspection(string InspectionID)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[ReplicateInspection]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@InspectionId", DbType.String).Value = InspectionID;
                    cmd.Parameters.Add("@CreatedBy", DbType.String).Value = WebOperationContext.Current.IncomingRequest.Headers["UserID"];
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = _ds.Tables[0].Rows[0][0].ToString();
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

        public ResultClass UpdateVehicleNo(string VehicleNumber, string InspectionID)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[UpdateVehicleNumber]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@VehicleNumber", DbType.String).Value = VehicleNumber;
                    cmd.Parameters.Add("@InspectionId", DbType.String).Value = InspectionID;
                    cmd.Parameters.Add("@Updatedby", DbType.String).Value = WebOperationContext.Current.IncomingRequest.Headers["UserID"];
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    result.Error = _ds.Tables[0].Rows[0][0].ToString();
                    //con.Close();
                    if (result.Error == "1")
                    {
                        result.IsSuccess = true;
                    }
                    else
                    {
                        result.IsSuccess = false;
                    }
                    //result.Success = _ds.Tables[0].Rows[0][0].ToString();
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

        public ResultClass DeleteInspectionDocument(string DocURL, string InspectionId)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[DeleteInspectionDocument]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@DocURL", DbType.String).Value = DocURL;
                    cmd.Parameters.Add("@InspectionId", DbType.String).Value = InspectionId;
                    cmd.Parameters.Add("@Updatedby", DbType.String).Value = WebOperationContext.Current.IncomingRequest.Headers["UserID"];
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    result.Error = _ds.Tables[0].Rows[0][0].ToString();
                    //con.Close();
                    if (result.Error == "1")
                    {
                        result.IsSuccess = true;
                    }
                    else
                    {
                        result.IsSuccess = false;
                    }
                    //result.Success = _ds.Tables[0].Rows[0][0].ToString();
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

    }
}
