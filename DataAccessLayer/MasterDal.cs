﻿using PropertyLayers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class MasterDal
    {
        Hashtable htData = null;
        DataSet _ds;
        DataTable _dt;
        SqlDataAdapter _adp;
        SqlConnection con;
        SqlCommand cmd;
        public ResultClass GetTicketIssueMaster()
        {
            ResultClass result = new ResultClass();
            DataSet _ds = new DataSet();
            try
            {
                htData = new Hashtable();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetTicketIssueMaster]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("@UserID", DbType.String).Value = UserID;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);

                    DataView dv = new DataView(_ds.Tables[1]);
                    List<TicketIssueName> IssueList = new List<TicketIssueName>();
                    foreach (DataRow oDR in _ds.Tables[0].Rows)
                    {
                        TicketIssueName oTicketIssueName = new TicketIssueName();
                        oTicketIssueName.IssueName = Convert.ToString(oDR["IssueName"]);
                        dv.RowFilter = "IssueName = '" + oTicketIssueName.IssueName + "'";
                        List<TicketIssueTypeMaster> TicketIssueTypeMasterList = new List<TicketIssueTypeMaster>();
                        foreach (DataRowView oDRs in dv)
                        {
                            TicketIssueTypeMaster oTicketIssueTypeMaster = new TicketIssueTypeMaster();
                            oTicketIssueTypeMaster.IssueTypeId = Convert.ToInt32(oDRs["IssueTypeId"]);
                            oTicketIssueTypeMaster.ProductID = Convert.ToInt32(oDRs["ProductID"]);
                            oTicketIssueTypeMaster.ProductName = Convert.ToString(oDRs["ProductName"]);
                            oTicketIssueTypeMaster.IssueName = Convert.ToString(oDRs["IssueName"]);
                            oTicketIssueTypeMaster.ParentID = Convert.ToInt32(oDRs["ParentID"]);
                            oTicketIssueTypeMaster.IsActive = Convert.ToBoolean(oDRs["IsActive"]);
                            oTicketIssueTypeMaster.IsMyAccount = Convert.ToBoolean(oDRs["IsMyAccount"]);
                            oTicketIssueTypeMaster.SequenceID = Convert.ToInt32(oDRs["SequenceID"]);
                            oTicketIssueTypeMaster.TopIssue = Convert.ToInt32(oDRs["TopIssue"]);
                            oTicketIssueTypeMaster.CategoryType = Convert.ToInt32(oDRs["CategoryType"]);
                            oTicketIssueTypeMaster.IsChat = Convert.ToBoolean(oDRs["IsChat"]);
                            oTicketIssueTypeMaster.IsAutoClosure = Convert.ToBoolean(oDRs["IsAutoClosure"]);
                            oTicketIssueTypeMaster.TAT = Convert.ToInt32(oDRs["TAT"]);
                            TicketIssueTypeMasterList.Add(oTicketIssueTypeMaster);
                        }
                        oTicketIssueName.TicketIssueTypeMaster = TicketIssueTypeMasterList;

                        IssueList.Add(oTicketIssueName);
                    }

                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(IssueList);
                    result.IsAuth = true;
                }
            }
            catch (Exception ex)
            {
                result.IsAuth = false;
            }
        
            return result;
        }
        public ResultClass GetProducts()
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetProducts]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

        public ResultClass SaveTicketIssueTypeMaster(TicketIssueTypeMaster Json)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[InsertTicketIssueTypeMaster]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IssueTypeId", DbType.Int32 ).Value = Json.IssueTypeId;
                    cmd.Parameters.Add("@ProductID", DbType.Int32).Value = Json.ProductID;
                    cmd.Parameters.Add("@IssueName", DbType.String).Value = Json.IssueName;
                    cmd.Parameters.Add("@ParentID", DbType.Int32).Value = Json.ParentID;
                    cmd.Parameters.Add("@IsActive", DbType.Boolean).Value = Json.IsActive;
                    cmd.Parameters.Add("@IsMyAccount", DbType.Boolean).Value = Json.IsMyAccount;
                    cmd.Parameters.Add("@SequenceID", DbType.Int32).Value = Json.SequenceID;
                    cmd.Parameters.Add("@TopIssue", DbType.Int32).Value = Json.TopIssue;
                    cmd.Parameters.Add("@CategoryType", DbType.Int32).Value = Json.CategoryType;
                    cmd.Parameters.Add("@IsChat", DbType.Boolean).Value = Json.IsChat;
                    cmd.Parameters.Add("@IsAutoClosure", DbType.Boolean).Value = Json.IsAutoClosure;
                    cmd.Parameters.Add("@TAT", DbType.Int32).Value = Json.TAT;
                   // cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    con.Close();
                    result.IsSuccess = true;
                    result.Success = _ds.Tables[0].Rows[0][0].ToString();
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

        public ResultClass GetAllIssues()
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetAllIssue]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.ExecuteNonQuery();
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    //con.Close();
                    result.IsSuccess = true;
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }
        public ResultClass GetTicketSubIssueMaster(String Json)
        {
            ResultClass result = new ResultClass();
            DataSet _ds = new DataSet();
            try
            {
                htData = new Hashtable();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetSubTicketIssueMaster]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@IssueName", DbType.Int32).Value = Json;
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds);
                    result.IsAuth = true;
                }
            }
            catch (Exception ex)
            {
                result.IsAuth = false;
            }

            return result;
        }
    }
}
