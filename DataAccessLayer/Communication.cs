﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using PropertyLayers;
using System.Xml.Linq;
using System.Data.Common;
using System.IO;
using Newtonsoft.Json;
using System.Net;

namespace DataAccessLayer
{
    public class Communication
    {

        public static void SendEmailSms_Comm(string JsonCommModel)
        {
            System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
            string CommServiceURLQA = System.Configuration.ConfigurationManager.AppSettings["CommLink"] + "send";
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "SendEmailSms_Comm" +
                   "\r\n JSONRequest=" + JsonCommModel);
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(CommServiceURLQA);
                request.Timeout = 120000;
                request.Method = "POST";
                request.ContentType = "application/json";

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                byte[] bytes = encoding.GetBytes(JsonCommModel);

                request.ContentLength = bytes.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    // Send the data.
                    requestStream.Write(bytes, 0, bytes.Length);
                }

                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            var r = JsonConvert.DeserializeObject(responseReader.ReadToEnd());
                            oStringBuilder.Append("\r\n Response=" + JsonConvert.SerializeObject(r));
                        }
                    }
                }
            }
            catch (Exception ex) { oStringBuilder.Append("\r\n Error=" + ex.Message); }
            finally
            {
                common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "SendEmailSms_Comm.txt");

            }

        }

        delegate void SendMailViaCommService(string JsonCommModel);
        private static void SendMailViaCommServiceComplete(IAsyncResult ar)
        {
            SendMailViaCommService d = (SendMailViaCommService)ar.AsyncState;
            d.EndInvoke(ar);
        }

        public static void SendCommunicationSync(long leadId, string TriggerName, string MobileNo, string EmailID, string BookingType, int ProductID, string SelectionID)
        {

            string JsonCommModelSMS = "", JsonCommModelMail = "";

            //JsonCommModelSMS = "{\"CommunicationDetails\":{\"LeadID\":" + leadId + ",\"IsBooking\":true,\"ProductID\":" + ProductID.ToString() + ", \"Conversations\":[{\"ToReceipent\":[\"" + MobileNo + "\"],\"TriggerName\":\"" + TriggerName + "\",\"CreatedBy\":\"BMS\",\"UserID\":9657,\"AutoTemplate\":true,\"SupplierId\":[" + SupplierId + "],\"LeadStatusID\":\"0\",\"SubStatusID\":0}], \"BookingDetail\":{\"BookingType\":\"" + BookingType + "\"} ,\"CommunicationType\":2}}";
            //SendEmailSms_Comm(JsonCommModelSMS);

            JsonCommModelMail = "{\"CommunicationDetails\":{\"LeadID\":" + leadId + ",\"SelectionID\":" + SelectionID + ",\"IsBooking\":true,\"ProductID\":"
                + ProductID.ToString() + ", \"Conversations\":[{\"From\":\"info@policybazaar.com\",\"ToReceipent\":[\"" + EmailID + "\"],\"BccEmail\":[\"asmit@policybazaar.com\",\"abrar@policybazaar.com\",\"lalan@policybazaar.com\",\"healthxchange@policybazaar.com\"],\"TriggerName\":\""
                + TriggerName + "\",\"CreatedBy\":\"HealthEX\",\"UserID\":124,\"AutoTemplate\":true,\"LeadStatusID\":\"0\",\"SubStatusID\":0}], \"BookingDetail\":{\"BookingType\":\"" + BookingType + "\"} ,\"CommunicationType\":1}}";
            SendEmailSms_Comm(JsonCommModelMail);
        }

        public static void SendEmailQCApproved(string Vehicle, string InspectionId, string EmailID, string CCEmail,string Status,string InsurerName)
        {
            string jsonEmail = "{\"CommunicationDetails\":{\"LeadID\":0,\"CustID\":0,\"ProductID\":117,\"Conversations\":[{\"From\":\"info@policybazaar.com\",\"CCEmail\":[" + CCEmail + "],\"ToReceipent\":["
                + EmailID + "],\"BccEmail\":[],\"Body\":\"Hi Team,<br><br>InspectionId : " + InspectionId + ",<br>Registration Number : " + Vehicle + ",<br>Status : " + InsurerName+" "+Status + "<br><br><br>\",\"Subject\":\"Inspection " + Status + " Notification\",\"CreatedBy\":\"Inspection\",\"MailAttachments\":[],\"TriggerName\":\"\",\"AutoTemplate\":true,\"DisplaySenderName\":\"PolicyBazaar.com\",}],\"CommunicationType\":1}}";
            SendEmailSms_Comm(jsonEmail);
        }

    }
}
