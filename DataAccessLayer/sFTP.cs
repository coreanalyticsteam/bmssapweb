﻿using Renci.SshNet;
using Renci.SshNet.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Configuration;

namespace DataAccessLayer
{
    public class sFTP
    {
        [DllImport("urlmon.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern Int32 URLDownloadToFile(
            [MarshalAs(UnmanagedType.IUnknown)] object callerPointer,
            [MarshalAs(UnmanagedType.LPWStr)] string url,
            [MarshalAs(UnmanagedType.LPWStr)] string filePathWithName,
            Int32 reserved,
            IntPtr callBack);

        private static bool IsDirectoryExists(string path, SftpClient sftp)
        {
            bool isDirectoryExist = false;

            try
            {
                sftp.ChangeDirectory(path);
                isDirectoryExist = true;
            }
            catch (SftpPathNotFoundException)
            {
                return false;
            }
            return isDirectoryExist;
        }
        private static bool CreateAndChkDirectoryExists(string path, SftpClient sftp)
        {
            try
            {
                if (!IsDirectoryExists(path, sftp))
                {
                    sftp.CreateDirectory(path);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex) { common.ErrorNInfoLogMethod(ex.Message.ToString(), "SFTP.txt"); return false; }
        }
        private static SftpClient CreateClient(string host, string username, string password)
        {   
            try
            {
                using (var sftp = new SftpClient(host, username, password))
                {
                    sftp.Connect();
                    return sftp;
                }
            }
            catch (Exception ex)
            {
                common.ErrorNInfoLogMethod(ex.Message.ToString(), "SFTP.txt");
                return null; 
            }
        }
        public static Stream DownloadFile(string url)
        {
            try
            {
                //string FilePath = ConfigurationManager.AppSettings["LocalPathForMerge"];
                //if (!Directory.Exists(FilePath))
                //    Directory.CreateDirectory(FilePath);
                //FilePath = FilePath + @"\" + url.Split('/')[url.Split('/').Length - 1];

                //URLDownloadToFile(null, url, FilePath, 0, IntPtr.Zero);
                //return FilePath;//new FileInfo(destinationFullPathWithName);

                WebRequest MyRequest = HttpWebRequest.Create(url);
                WebResponse MyResponse = MyRequest.GetResponse();

                // we get newly create link into string RealURL
                string RealURL = MyResponse.ResponseUri.ToString();

                // Creates an HttpWebRequest with the specified URL.
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(RealURL);

                // Sends the HttpWebRequest and waits for the response.
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                Stream receiveStream = myHttpWebResponse.GetResponseStream();
                return receiveStream;
            }
            catch (Exception ex)
            {
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " DownloadFile- url:" + url + "  " + ex.Message, "DownloadFileLocal.txt");
                return null;
            }
        }

        public static string UploadFileToSFTP(string sftpHost, string sftpUsername, string sftpPassword, string sftpFolderPath, string FilePath, string FileName, string FilePathType = "REMOTE")
        {
            try
            {
                using (var sftp = new SftpClient(sftpHost, sftpUsername, sftpPassword))
                {
                    sftp.Connect();
                    if (CreateAndChkDirectoryExists(sftpFolderPath, sftp))
                    {
                        var webClient = new WebClient();
                        if (FilePathType == "REMOTE")
                        {
                            int _chk = 0;
                            try
                            {
                                MemoryStream ms = new MemoryStream(webClient.DownloadData(FilePath));
                                sftp.UploadFile(ms, sftpFolderPath + FileName, null);
                                sftp.Disconnect();
                                sftp.Dispose();
                                ms.Flush();
                                _chk = 0;
                                return "SUCCESS";
                            }
                            catch (Exception ex)
                            {
                                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + ": UploadFileToSFTP:" + ": FilePath: " + FilePath + " Error in WebClient:" + ex.Message + "   " + ex.Message.ToString(), "SFTP.txt");
                                //return "ERROR";
                            }
                            if (_chk == 0)
                            {
                                
                                try
                                {
                                    WebRequest MyRequest = HttpWebRequest.Create(FilePath);
                                    WebResponse MyResponse = MyRequest.GetResponse();

                                    // we get newly create link into string RealURL
                                    string RealURL = MyResponse.ResponseUri.ToString();

                                    // Creates an HttpWebRequest with the specified URL.
                                    HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(RealURL);

                                    // Sends the HttpWebRequest and waits for the response.
                                    HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                                    Stream receiveStream = myHttpWebResponse.GetResponseStream();
                                    sftp.UploadFile(receiveStream, sftpFolderPath + FileName, null);
                                    sftp.Disconnect();
                                    sftp.Dispose();                                   
                                    _chk = 1;
                                    return "SUCCESS";
                                }
                                catch (Exception ex)
                                {
                                    common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + ": UploadFileToSFTP:" + ": FilePath: " + FilePath + " Error in DownloadFile:" + ex.Message + "   " + ex.Message.ToString(), "SFTP.txt");
                                    return "ERROR";
                                }
                            }
                            else
                            {
                                return "SUCCESS";
                            }
                        }
                        else if (FilePathType == "LOCAL")
                        {
                            using (var fileStream = new FileStream(FilePath, FileMode.Open))
                            {
                                sftp.UploadFile(fileStream, sftpFolderPath + FileName, null);                                   
                            }
                            sftp.Disconnect();
                            sftp.Dispose();
                            return "SUCCESS";
                        }
                        else
                        {
                            return "ERROR";
                        }                         
                    }
                    else
                    {

                        return "ERROR";
                    }
                }
            }
            catch (Exception ex) 
            {
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + ": UploadFileToSFTP:" + ": FilePath: " + FilePath + ex.Message.ToString(), "SFTP.txt");
                return "ERROR";; 
            }
        }
        //public static void DownloadAll()
        //{
        //    string host = "202.191.172.29";
        //    string username = "pbazaar";
        //    string password = "T@lic@123";
        //    string localDirectory = @"D:\policycopy\aa.pdf";
        //    FileInfo f = new FileInfo(localDirectory);
        //    string uploadfile = f.FullName;
        //    Console.WriteLine(f.Name);
        //    Console.WriteLine("uploadfile" + uploadfile);

        //    string remoteDirectory = "/export/home/policybazaar/02032017/";


        //    using (var sftp = new SftpClient(host, username, password))
        //    {
        //        sftp.Connect();
        //        bool fileExists = IsDirectoryExists("/export/home/policybazaar/02032017777/", sftp);
        //        if (!fileExists)
        //        {
        //            sftp.CreateDirectory("/export/home/policybazaar/02032017777/");
        //        }
        //        if (sftp.IsConnected)
        //        {
        //            WebRequest req = HttpWebRequest.Create("http://www.pdf995.com/samples/pdf.pdf");
        //            using (Stream stream = req.GetResponse().GetResponseStream())
        //            {
        //                FileStream fsrteam = stream as FileStream;
        //                //var fsrteam = (FileStream)stream;
        //                if (fsrteam != null)
        //                {
        //                    //If you have a folder located at sftp://ftp.example.com/share
        //                    //then you can add this like:
        //                    sftp.UploadFile(fsrteam, remoteDirectory + f.Name, null);
        //                    sftp.Disconnect();
        //                    sftp.Dispose();
                            
        //                }
        //            }

        //        }
        //    }
        //    //sftp.UploadFile(@"D:\policycopy\", remoteDirectory);

        //    //var files = sftp.ListDirectory(remoteDirectory);

        //    //foreach (var file in files)
        //    //{
        //    //    string remoteFileName = file.Name;
        //    //    if ((!file.Name.StartsWith(".")) && ((file.LastWriteTime.Date == DateTime.Today)))

        //    //        using (Stream file1 = File.OpenWrite(localDirectory + remoteFileName))
        //    //        {
        //    //            sftp.DownloadFile(remoteDirectory + remoteFileName, file1);
        //    //        }
        //    //}


        //}
    }
}
