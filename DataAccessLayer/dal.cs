﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using PropertyLayers;
using System.Xml.Linq;
using System.Data.Common;
using System.IO;
namespace DataAccessLayer
{
    public class dal
    {
        DataSet ds;
        DataTable  dt;
        SqlDataAdapter adp;
        //SqlConnection con;
        SqlCommand cmd;
     
        public EndorsementListDetails GetEndorsementListDetails(long LeadID)
        {
            EndorsementListDetails objEndorsementListDetails = new EndorsementListDetails();
            BookingEndorsementList objBookingEndorsement;
            List<BookingEndorsementList> objBookingEndorsementList = new List<BookingEndorsementList>();

            ds = new DataSet();
            try
            {
                //cmd = new SqlCommand();
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[customer].[GetAllCustomerEndorsement]", con);
                    cmd.Parameters.Add("@BookingID", SqlDbType.Int).Value = LeadID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {

                            objEndorsementListDetails.LeadID = Convert.ToInt64(dr["LeadID"]);
                            objEndorsementListDetails.CustName = Convert.ToString(dr["Name"]);
                            objEndorsementListDetails.CustomerID = Convert.ToInt64(dr["CustomerID"]);
                            objEndorsementListDetails.SupplierID = Convert.ToInt16(dr["SupplierID"]);
                            objEndorsementListDetails.PlanId = Convert.ToInt16(dr["PlanId"]);
                            objEndorsementListDetails.PlanName = Convert.ToString(dr["PlanName"]);
                            objEndorsementListDetails.SupplierName = Convert.ToString(dr["SupplierName"]);
                            objEndorsementListDetails.ProductName = Convert.ToString(dr["ProductName"]);

                        }
                        if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[1].Rows)
                            {
                                objBookingEndorsement = new BookingEndorsementList();
                                objBookingEndorsement.DetailsID = Convert.ToInt64(dr["DetailsID"]);
                                objBookingEndorsement.CreatedDate = Convert.ToString(dr["CreatedDate"]);
                                objBookingEndorsement.StatusMasterName = Convert.ToString(dr["StatusMasterName"]);
                                objBookingEndorsement.ReasonName = Convert.ToString(dr["ReasonName"]);
                                objBookingEndorsement.NextStatus = Convert.ToString(dr["NextStatus"]);
                                objBookingEndorsementList.Add(objBookingEndorsement);
                            }
                        }

                        if (ds != null && ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                        {
                            objEndorsementListDetails.IsEligible = 1;
                        }
                        else
                        {
                            objEndorsementListDetails.IsEligible = 0;
                        }




                    }
                }

            }
            catch
            {

            }
            objEndorsementListDetails.EndorsementList = objBookingEndorsementList;
           
            return objEndorsementListDetails;
        }

        public List<EndorsementStatusdetailsResponse> GetEndorsementStatusdetails(long DetailsID)
        {            
            EndorsementStatusdetailsResponse objEndorsementStatusdetailsResponse;
            List<EndorsementStatusdetailsResponse> objEndorsementStatusdetailsResponseList = new List<EndorsementStatusdetailsResponse>();

            ds = new DataSet();
            try
            {
                //cmd = new SqlCommand();
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[customer].[GetEndorsementStatusdetails]", con);
                    cmd.Parameters.Add("@DetailsID", SqlDbType.BigInt).Value = DetailsID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objEndorsementStatusdetailsResponse = new EndorsementStatusdetailsResponse();
                            objEndorsementStatusdetailsResponse.StatusMasterName = Convert.ToString(dr["StatusMasterName"]);
                            objEndorsementStatusdetailsResponse.CreatedOn = Convert.ToString(dr["CreatedOn"]);
                            objEndorsementStatusdetailsResponse.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                            objEndorsementStatusdetailsResponse.StatusMasterID = Convert.ToInt16(dr["StatusMasterID"]);
                            objEndorsementStatusdetailsResponse.IsRollBack = Convert.ToInt16(dr["IsRollBack"]);
                            objEndorsementStatusdetailsResponse.IsActive = Convert.ToInt16(dr["IsActive"]);
                            objEndorsementStatusdetailsResponseList.Add(objEndorsementStatusdetailsResponse);
                        }
                    }

                }
            }
            catch
            {

            }
            

            return objEndorsementStatusdetailsResponseList;
        }

        public List<EndorsementStatusdetailsResponse> RollbackEndorsement(EndorsementRollBackRequest objEndorsementRollBackRequest)
        {
            EndorsementStatusdetailsResponse objEndorsementStatusdetailsResponse;
            List<EndorsementStatusdetailsResponse> objEndorsementStatusdetailsResponseList = new List<EndorsementStatusdetailsResponse>();

            ds = new DataSet();
            try
            {
                //cmd = new SqlCommand();
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());    
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[customer].[RollbackEndorsement]", con);
                    cmd.Parameters.Add("@DetailsID", SqlDbType.BigInt).Value = objEndorsementRollBackRequest.DetailsID;
                    cmd.Parameters.Add("@BookingID", SqlDbType.BigInt).Value = objEndorsementRollBackRequest.BookingID;
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = objEndorsementRollBackRequest.UserID;
                    cmd.Parameters.Add("@StatusMasterID", SqlDbType.Int).Value = objEndorsementRollBackRequest.StatusMasterID;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    int res = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objEndorsementStatusdetailsResponse = new EndorsementStatusdetailsResponse();
                            objEndorsementStatusdetailsResponse.StatusMasterName = Convert.ToString(dr["StatusMasterName"]);
                            objEndorsementStatusdetailsResponse.CreatedOn = Convert.ToString(dr["CreatedOn"]);
                            objEndorsementStatusdetailsResponse.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                            objEndorsementStatusdetailsResponse.StatusMasterID = Convert.ToInt16(dr["StatusMasterID"]);
                            objEndorsementStatusdetailsResponse.IsRollBack = Convert.ToInt16(dr["IsRollBack"]);
                            objEndorsementStatusdetailsResponse.IsActive = Convert.ToInt16(dr["IsActive"]);
                            objEndorsementStatusdetailsResponseList.Add(objEndorsementStatusdetailsResponse);
                        }
                    }

                }
            }
            catch
            {

            }


            return objEndorsementStatusdetailsResponseList;
        }

        public EndorsementDetail GetEndorsementDetails(long LeadID, long DetailsID, int Type)
        {
            EndorsementFieldMaster objEndorsementFieldMaster;
            EndorsementStatusDetails objEndorsementStatusDetails = new EndorsementStatusDetails();
            List<EndorsementFieldMaster> objEndorsementFieldList = new List<EndorsementFieldMaster>();
            List<EndorsementFieldMaster> objEndorsementFieldDetailList = new List<EndorsementFieldMaster>();
            List<EndorsementStatusDetails> RejectionReasonList = new List<EndorsementStatusDetails>();
            EndorsementAddtionalInfo objEndorsementAddtionalInfo = new EndorsementAddtionalInfo();
            EndorsementStatusDetails objRejEndorsementStatusDetails;
            EndorsementDocumentCopy objEndorsementDocumentCopy;
            List<EndorsementDocumentCopy> objEndorsementDocumentCopyList = new List<EndorsementDocumentCopy>();
            EndorsementDetail objEndorsementDetail = new EndorsementDetail();
            int chkDoc = 0;
            ds  = new DataSet();
            try
            {
                //cmd = new SqlCommand();
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[GetCustomerEndorsement]", con);
                    cmd.Parameters.Add("@BookingID", SqlDbType.Int).Value = LeadID;
                    cmd.Parameters.Add("@DetailsID", SqlDbType.Int).Value = DetailsID;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    int Result = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {

                            objEndorsementDetail.LeadID = Convert.ToInt64(dr["LeadID"]);
                            objEndorsementDetail.ProductID = Convert.ToInt16(dr["ProductID"]);
                            objEndorsementDetail.CustName = Convert.ToString(dr["Name"]);
                            objEndorsementDetail.CustomerID = Convert.ToInt64(dr["CustomerID"]);
                            objEndorsementDetail.SupplierID = Convert.ToInt16(dr["SupplierID"]);
                            objEndorsementDetail.PlanId = Convert.ToInt16(dr["PlanId"]);
                            objEndorsementDetail.PlanName = Convert.ToString(dr["PlanName"]);
                            objEndorsementDetail.SupplierName = Convert.ToString(dr["SupplierName"]);
                            objEndorsementDetail.ProductName = Convert.ToString(dr["ProductName"]);

                        }
                        if (Result == 3)
                        {
                            objEndorsementStatusDetails.StatusMasterID = 99;
                        }
                        else if (Result == 1)
                        {
                            objEndorsementStatusDetails.StatusMasterID = -1;
                            objEndorsementStatusDetails.ReasonId = -1;
                            objEndorsementStatusDetails.ReasonName = "";
                            objEndorsementStatusDetails.StatusMasterName = "";


                            //foreach (DataRow dr in ds.Tables[0].Rows)
                            //{

                            //    objEndorsementDetail.LeadID = Convert.ToInt64(dr["LeadID"]);
                            //    objEndorsementDetail.CustName = Convert.ToString(dr["Name"]);
                            //    objEndorsementDetail.CustomerID = Convert.ToInt64(dr["CustomerID"]);
                            //    objEndorsementDetail.SupplierID = Convert.ToInt16(dr["SupplierID"]);
                            //    objEndorsementDetail.PlanId = Convert.ToInt16(dr["PlanId"]);
                            //    objEndorsementDetail.PlanName = Convert.ToString(dr["PlanName"]);
                            //    objEndorsementDetail.SupplierName = Convert.ToString(dr["SupplierName"]);
                            //    objEndorsementDetail.ProductName = Convert.ToString(dr["ProductName"]);

                            //}
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[1].Rows)
                                {
                                    objEndorsementFieldMaster = new EndorsementFieldMaster();
                                    objEndorsementFieldMaster.FieldID = Convert.ToInt16(dr["FieldID"]);
                                    objEndorsementFieldMaster.MasterType = Convert.ToInt16(dr["MasterType"]);
                                    objEndorsementFieldMaster.FieldName = Convert.ToString(dr["FieldName"]);
                                    objEndorsementFieldMaster.OldValue = Convert.ToString(dr["OldValue"]);
                                    objEndorsementFieldMaster.DocReq = Convert.ToString(dr["DocReq"]);
                                    objEndorsementFieldMaster.FieldType = Convert.ToString(dr["FieldType"]);

                                    objEndorsementFieldList.Add(objEndorsementFieldMaster);



                                }
                            }
                        }
                        else if (Result == 2 && ds.Tables.Count >= 6)
                        {


                            foreach (DataRow dr in ds.Tables[1].Rows)
                            {
                                objEndorsementFieldMaster = new EndorsementFieldMaster();
                                objEndorsementFieldMaster.SNo = Convert.ToInt16(dr["SNo"]);
                                objEndorsementFieldMaster.FieldID = Convert.ToInt16(dr["FieldID"]);
                                objEndorsementFieldMaster.MasterType = Convert.ToInt16(dr["MasterType"]);
                                objEndorsementFieldMaster.FieldName = Convert.ToString(dr["FieldName"]);
                                objEndorsementFieldMaster.OldValue = Convert.ToString(dr["OldValue"]);
                                objEndorsementFieldMaster.NewValue = Convert.ToString(dr["NewValue"]);
                                objEndorsementFieldMaster.DocReq = Convert.ToString(dr["DocReq"]);
                                objEndorsementFieldMaster.FieldType = Convert.ToString(dr["FieldType"]);
                                objEndorsementFieldMaster.IsDocReceived = Convert.ToInt16(dr["IsDocReceived"]);
                                objEndorsementFieldDetailList.Add(objEndorsementFieldMaster);
                                if (objEndorsementFieldMaster.IsDocReceived == 0)
                                {
                                    chkDoc = chkDoc + 1;
                                }
                            }
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                objEndorsementStatusDetails.StatusMasterID = Convert.ToString(ds.Tables[2].Rows[0]["StatusMasterID"]) == "0" ? 1001 : Convert.ToInt16(ds.Tables[2].Rows[0]["StatusMasterID"]);
                                //Convert.ToInt16(ds.Tables[2].Rows[0]["StatusMasterID"]);
                                objEndorsementStatusDetails.StatusMasterName = Convert.ToString(ds.Tables[2].Rows[0]["StatusMasterName"]);
                                objEndorsementStatusDetails.ReasonId = String.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[0]["ReasonId"])) ? 0 : Convert.ToInt16(ds.Tables[2].Rows[0]["ReasonId"]);
                                objEndorsementStatusDetails.ReasonName = Convert.ToString(ds.Tables[2].Rows[0]["ReasonName"]);
                            }
                            else
                            {
                                objEndorsementStatusDetails.StatusMasterID = 1001;
                            }
                            if (ds.Tables[3].Rows.Count > 0)
                            {
                                objEndorsementAddtionalInfo.DetailId = String.IsNullOrEmpty(Convert.ToString(ds.Tables[3].Rows[0]["DetailID"])) ? -1 : Convert.ToInt64(ds.Tables[3].Rows[0]["DetailId"]);
                                objEndorsementAddtionalInfo.IsPremiumChange = String.IsNullOrEmpty(Convert.ToString(ds.Tables[3].Rows[0]["IsPremiumChange"])) ? -1 : Convert.ToInt16(ds.Tables[3].Rows[0]["IsPremiumChange"]);
                                objEndorsementAddtionalInfo.PremiumChange = String.IsNullOrEmpty(Convert.ToString(ds.Tables[3].Rows[0]["PremiumChange"])) ? Convert.ToDecimal(00.00) : Convert.ToInt16(ds.Tables[3].Rows[0]["PremiumChange"]);
                                objEndorsementAddtionalInfo.PremiumCollectedRefunded = String.IsNullOrEmpty(Convert.ToString(ds.Tables[3].Rows[0]["PremiumCollectedRefunded"])) ? -1 : Convert.ToInt16(ds.Tables[3].Rows[0]["PremiumCollectedRefunded"]);
                                objEndorsementAddtionalInfo.SCVerified = String.IsNullOrEmpty(Convert.ToString(ds.Tables[3].Rows[0]["SCVerified"])) ? -1 : Convert.ToInt16(ds.Tables[3].Rows[0]["SCVerified"]);
                                objEndorsementAddtionalInfo.SCReceived = String.IsNullOrEmpty(Convert.ToString(ds.Tables[3].Rows[0]["SCReceived"])) ? -1 : Convert.ToInt16(ds.Tables[3].Rows[0]["SCReceived"]);

                            }
                            if (ds.Tables[4].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[4].Rows)
                                {
                                    objRejEndorsementStatusDetails = new EndorsementStatusDetails();
                                    objRejEndorsementStatusDetails.StatusMasterID = Convert.ToInt16(dr["StatusMasterID"]);
                                    objRejEndorsementStatusDetails.StatusMasterName = Convert.ToString(dr["StatusMasterName"]);
                                    RejectionReasonList.Add(objRejEndorsementStatusDetails);
                                }
                            }
                            if (ds.Tables[5].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[5].Rows)
                                {
                                    objEndorsementDocumentCopy = new EndorsementDocumentCopy();
                                    objEndorsementDocumentCopy.Document = Convert.ToString(dr["Document"]);
                                    objEndorsementDocumentCopy.DocumentUrl = Convert.ToString(dr["DocumentUrl"]);
                                    objEndorsementDocumentCopyList.Add(objEndorsementDocumentCopy);
                                }
                            }

                            if (ds.Tables[6].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[6].Rows)
                                {
                                    objEndorsementFieldMaster = new EndorsementFieldMaster();
                                    objEndorsementFieldMaster.FieldID = Convert.ToInt16(dr["FieldID"]);
                                    objEndorsementFieldMaster.MasterType = Convert.ToInt16(dr["MasterType"]);
                                    objEndorsementFieldMaster.FieldName = Convert.ToString(dr["FieldName"]);
                                    objEndorsementFieldMaster.OldValue = Convert.ToString(dr["OldValue"]);
                                    objEndorsementFieldMaster.DocReq = Convert.ToString(dr["DocReq"]);
                                    objEndorsementFieldMaster.FieldType = Convert.ToString(dr["FieldType"]);

                                    objEndorsementFieldList.Add(objEndorsementFieldMaster);
                                }
                            }
                        }
                    }
                }
            }
            catch {
            
            }

            if (chkDoc > 0)
            {
                objEndorsementDetail.IsDocReceived = 0;
            }
            else {
                objEndorsementDetail.IsDocReceived = 1;
            }
            objEndorsementDetail.EndorsementDocumentCopyList = objEndorsementDocumentCopyList;
            objEndorsementDetail.RejectionReasonList = RejectionReasonList;
            objEndorsementDetail.EndorsementStatus = objEndorsementStatusDetails;
            objEndorsementDetail.EndorsementFieldMasterList = objEndorsementFieldList;
            objEndorsementDetail.EndorsementFieldListDetails = objEndorsementFieldDetailList;
            objEndorsementDetail.EndorsementAddtionalInformation = objEndorsementAddtionalInfo;
            return objEndorsementDetail;
        }
       
        public string UpdateEndorsementDetails(EndorsementDetail objEndorsementDetail)
        {
            int res = 0;
            string result="";
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[UpdateCustomerEndorsement]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objEndorsementDetail.LeadID;
                    cmd.Parameters.Add("@DetailId", DbType.Int64).Value = objEndorsementDetail.EndorsementAddtionalInformation.DetailId;
                    cmd.Parameters.Add("@NewStatusMasterID", DbType.Int16).Value = objEndorsementDetail.EndorsementStatus.StatusMasterID;
                    cmd.Parameters.Add("@NewReasonId", DbType.Int16).Value = objEndorsementDetail.EndorsementStatus.ReasonId;
                    cmd.Parameters.Add("@IsPremiumChange", DbType.Int16).Value = objEndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange;
                    cmd.Parameters.Add("@PremiumChange", DbType.Int16).Value = objEndorsementDetail.EndorsementAddtionalInformation.PremiumChange;
                    cmd.Parameters.Add("@PremiumCollectedRefunded", DbType.Int16).Value = objEndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded;
                    cmd.Parameters.Add("@SCVerified", DbType.Int16).Value = objEndorsementDetail.EndorsementAddtionalInformation.SCVerified;
                    cmd.Parameters.Add("@SCReceived", DbType.Int16).Value = objEndorsementDetail.EndorsementAddtionalInformation.SCReceived;
                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = objEndorsementDetail.UserID;
                    cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    res = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                }
            }
            catch { }
            
            result = "No changes found."; 
            if (res == 1) {
                result = "Endorsement created successfully.";
            }
            else if (res == 2)
            {
                result = "Please upload all required documents.";
            }
            else if (res == 3)
            {
                result = "Endorsement resubmitted successfully.";
            }
            else if (res == 4)
            {
                result = "Please upload endorsement copy.";
            }
            else if (res == 5)
            {
                result = "Endorsement rejected successfully.";
            }
            else if (res == 10)
            {
                result = "Status updated successfully.";
            }            
            return result;
        }

        public EndorsementAddFieldResponse AddNewEndorsementField(EndorsementFieldAddRequest NewRequestEndorsement)
        {
            EndorsementAddFieldResponse objEndorsementAddFieldResponse = new EndorsementAddFieldResponse();
            EndorsementDocReq objEndorsementDocReq;
            List<EndorsementDocReq> EndorsementExistngDocList = new List<EndorsementDocReq>();
            List<EndorsementDocReq> EndorsementDocReqList = new List<EndorsementDocReq>();
            long res = 0;
            string result;
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();

                    cmd = new SqlCommand("[Customer].[InsertNewEndorsementField]", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@BookingID", DbType.Int64).Value = NewRequestEndorsement.LeadID;
                    cmd.Parameters.Add("@FieldID", DbType.Int16).Value = NewRequestEndorsement.FieldID;
                    cmd.Parameters.Add("@OldValue", DbType.String).Value = NewRequestEndorsement.OldValue;
                    cmd.Parameters.Add("@NewValue", DbType.String).Value = NewRequestEndorsement.NewValue;
                    cmd.Parameters.Add("@DetailId", DbType.Int64).Value = NewRequestEndorsement.DetailsID;
                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = NewRequestEndorsement.UserID;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    //cmd.ExecuteNonQuery();
                    adp = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    adp.Fill(ds);
                    res = Convert.ToInt64(cmd.Parameters["@Result"].Value.ToString());
                    string innerExpression = "", outerExpression = "", Rules = "*";
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                objEndorsementDocReq = new EndorsementDocReq();
                                objEndorsementDocReq.RuleID = Convert.ToInt64(dr["RuleID"]);
                                objEndorsementDocReq.DocumentID = Convert.ToInt16(dr["DocumentID"]);
                                objEndorsementDocReq.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                                objEndorsementDocReq.Document = Convert.ToString(dr["Document"]);
                                objEndorsementDocReq.DocOperator = Convert.ToString(dr["DocOperator"]);
                                objEndorsementDocReq.RuleOperator = Convert.ToString(dr["RuleOperator"]);
                                objEndorsementDocReq.HostName = Convert.ToString(dr["HostName"]);
                                objEndorsementDocReq.DocumentUrl = Convert.ToString(dr["DocumentUrl"]);
                                EndorsementDocReqList.Add(objEndorsementDocReq);

                            }
                        }
                        if (ds != null && ds.Tables.Count == 4 && ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[3].Rows)
                            {
                                objEndorsementDocReq = new EndorsementDocReq();

                                objEndorsementDocReq.DocumentID = Convert.ToInt16(dr["DocumentID"]);
                                objEndorsementDocReq.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                                objEndorsementDocReq.Document = Convert.ToString(dr["Document"]);
                                objEndorsementDocReq.HostName = Convert.ToString(dr["HostName"]);
                                objEndorsementDocReq.DocumentUrl = Convert.ToString(dr["DocumentUrl"]);
                                objEndorsementDocReq.CustDocumentID = Convert.ToInt64(dr["CustDocumentID"]);
                                EndorsementExistngDocList.Add(objEndorsementDocReq);
                            }
                        }
                        string filter;
                        outerExpression = " ( ";
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            filter = "RuleID=" + ds.Tables[1].Rows[i]["RuleID"].ToString();
                            DataRow[] foundRows;
                            foundRows = ds.Tables[2].Select(filter);
                            innerExpression = "(";
                            foreach (DataRow dr in foundRows)
                            {
                                if (dr["DocumentUrl"].ToString() != "0")
                                {
                                    innerExpression = innerExpression + " true ";
                                }
                                else
                                {
                                    innerExpression = innerExpression + " false ";
                                }
                                innerExpression = innerExpression + dr["DocOperator"].ToString();
                            }
                            innerExpression = innerExpression + " ) ";
                            outerExpression = outerExpression + innerExpression + ds.Tables[1].Rows[i]["RuleOperator"].ToString();

                        }
                        outerExpression = outerExpression + " ) ";
                        if (outerExpression.Length > 6)
                        {
                            System.Data.DataTable table = new System.Data.DataTable();
                            table.Columns.Add("", typeof(Boolean));
                            table.Columns[0].Expression = outerExpression;
                            System.Data.DataRow r = table.NewRow();
                            table.Rows.Add(r);
                            bool blResult = (Boolean)r[0];
                            objEndorsementAddFieldResponse.IsDocReceived = blResult;
                        }
                        else
                        {
                            objEndorsementAddFieldResponse.IsDocReceived = true;
                        }

                    }
                }
            }
            catch { }
            
            if (res > 0) { result = Convert.ToString(res); }
           // else { result = "error"; }

            if (EndorsementDocReqList.Count == 0)
            {
                objEndorsementAddFieldResponse.IsDocReceived = true;
            }
            
            objEndorsementAddFieldResponse.DetailsID = res;
            objEndorsementAddFieldResponse.EndorsementDocReqList = EndorsementDocReqList;
            objEndorsementAddFieldResponse.EndorsementExistngDocList = EndorsementExistngDocList;

            if (objEndorsementAddFieldResponse.IsDocReceived)
            {
                EndorsementFieldUpdateReq EndorsementFieldUpdate = new EndorsementFieldUpdateReq();
                EndorsementFieldUpdate.BookingID = NewRequestEndorsement.LeadID;
                EndorsementFieldUpdate.DetailsID = objEndorsementAddFieldResponse.DetailsID;
                EndorsementFieldUpdate.FieldID = NewRequestEndorsement.FieldID;
                EndorsementFieldUpdate.UserID = NewRequestEndorsement.UserID;
                EndorsementFieldUpdate.TYPE = 2;
                UpdateEndorsementFieldDetails(EndorsementFieldUpdate);
               
            }

            return objEndorsementAddFieldResponse;
        }

        public EndorsementAddFieldResponse GetNewFieldDocs(EndorsementFieldAddRequest NewRequestEndorsement)
        {
            EndorsementAddFieldResponse objEndorsementAddFieldResponse = new EndorsementAddFieldResponse();
            EndorsementDocReq objEndorsementDocReq;
            List<EndorsementDocReq> EndorsementDocReqList = new List<EndorsementDocReq>();
            List<EndorsementDocReq> EndorsementExistngDocList = new List<EndorsementDocReq>();
            long res = 0;
            string result;
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[customer].[GetDocumentForField]", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@BookingID", DbType.Int64).Value = NewRequestEndorsement.LeadID;
                    cmd.Parameters.Add("@FieldID", DbType.Int16).Value = NewRequestEndorsement.FieldID;
                    cmd.Parameters.Add("@DetailId", DbType.Int64).Value = NewRequestEndorsement.DetailsID;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    //cmd.ExecuteNonQuery();
                    adp = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    adp.Fill(ds);
                    res = Convert.ToInt64(cmd.Parameters["@Result"].Value.ToString());
                    string innerExpression = "", outerExpression = "", Rules = "*";
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objEndorsementDocReq = new EndorsementDocReq();
                            objEndorsementDocReq.RuleID = Convert.ToInt64(dr["RuleID"]);
                            objEndorsementDocReq.DocumentID = Convert.ToInt16(dr["DocumentID"]);
                            objEndorsementDocReq.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                            objEndorsementDocReq.Document = Convert.ToString(dr["Document"]);
                            objEndorsementDocReq.DocOperator = Convert.ToString(dr["DocOperator"]);
                            objEndorsementDocReq.RuleOperator = Convert.ToString(dr["RuleOperator"]);
                            objEndorsementDocReq.HostName = Convert.ToString(dr["HostName"]);
                            objEndorsementDocReq.DocumentUrl = Convert.ToString(dr["DocumentUrl"]);
                            EndorsementDocReqList.Add(objEndorsementDocReq);
                        }
                        if (ds != null && ds.Tables.Count == 4 && ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[3].Rows)
                            {
                                objEndorsementDocReq = new EndorsementDocReq();
                                objEndorsementDocReq.DocumentID = Convert.ToInt16(dr["DocumentID"]);
                                objEndorsementDocReq.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                                objEndorsementDocReq.Document = Convert.ToString(dr["Document"]);
                                objEndorsementDocReq.HostName = Convert.ToString(dr["HostName"]);
                                objEndorsementDocReq.DocumentUrl = Convert.ToString(dr["DocumentUrl"]);
                                objEndorsementDocReq.CustDocumentID = Convert.ToInt64(dr["CustDocumentID"]);
                                EndorsementExistngDocList.Add(objEndorsementDocReq);
                            }
                        }
                        string filter, docValue;

                        outerExpression = " ( ";
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            filter = "RuleID=" + ds.Tables[1].Rows[i]["RuleID"].ToString();
                            DataRow[] foundRows;
                            foundRows = ds.Tables[2].Select(filter);
                            innerExpression = "(";
                            foreach (DataRow dr in foundRows)
                            {
                                if (dr["DocumentUrl"].ToString() != "0")
                                {
                                    innerExpression = innerExpression + " true ";
                                }
                                else
                                {
                                    innerExpression = innerExpression + " false ";
                                }
                                innerExpression = innerExpression + dr["DocOperator"].ToString();
                            }
                            innerExpression = innerExpression + " ) ";
                            outerExpression = outerExpression + innerExpression + ds.Tables[1].Rows[i]["RuleOperator"].ToString();

                        }
                        outerExpression = outerExpression + ")";
                    }

                    System.Data.DataTable table = new System.Data.DataTable();
                    table.Columns.Add("", typeof(Boolean));
                    table.Columns[0].Expression = outerExpression;
                    System.Data.DataRow r = table.NewRow();
                    table.Rows.Add(r);
                    bool blResult = (Boolean)r[0];
                    objEndorsementAddFieldResponse.IsDocReceived = blResult;
                }
            }
            catch { }
            //finally { con.Close(); }
            if (res > 0) { result = Convert.ToString(res); }
            if (EndorsementDocReqList.Count == 0)
            {
                objEndorsementAddFieldResponse.IsDocReceived = true;
            }
            objEndorsementAddFieldResponse.DetailsID = res;
            objEndorsementAddFieldResponse.EndorsementDocReqList = EndorsementDocReqList;
            objEndorsementAddFieldResponse.EndorsementExistngDocList = EndorsementExistngDocList;            
            return objEndorsementAddFieldResponse;
        }

        public string UpdateEndorsementFieldDetails(EndorsementFieldUpdateReq EndorsementFieldUpdate)
         {
            int  res = 0;
            string result;
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[UpdateEndorsementFieldDetails]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BookingID", DbType.Int64).Value = EndorsementFieldUpdate.BookingID;
                    cmd.Parameters.Add("@DetailsId", DbType.Int64).Value = EndorsementFieldUpdate.DetailsID;
                    cmd.Parameters.Add("@FieldID", DbType.Int16).Value = EndorsementFieldUpdate.FieldID;
                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = EndorsementFieldUpdate.UserID;
                    cmd.Parameters.Add("@TYPE", DbType.Int16).Value = EndorsementFieldUpdate.TYPE;
                    res = cmd.ExecuteNonQuery();
                }
            }
            catch { return "error"; }
            //finally { con.Close(); }
            if (res > 0) { result = Convert.ToString(res); }
            else { result = "error"; }
            return result;
        }
        public string InsertCustomerEndorsement(EndorsementDetail RequestEndorsementList)
        {
            long res = 0;
            string result;
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[InsertCustomerEndorsement]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var xEle = new XElement("EndorsementDetail",
                                            from Endorsement in RequestEndorsementList.EndorsementFieldMasterList
                                            select new XElement("EndorsementDetail",
                                                           new XAttribute("FieldID", Endorsement.FieldID),
                                                            new XAttribute("OldValue", Convert.ToString(Endorsement.OldValue)),
                                                           new XAttribute("NewValue", Endorsement.NewValue)
                                                       )).ToString();

                    cmd.Parameters.Add("@BookingID", DbType.Int64).Value = RequestEndorsementList.LeadID;
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = RequestEndorsementList.UserID;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    res = Convert.ToInt64(cmd.Parameters["@Result"].Value.ToString());
                }
            }
            catch { }
            //finally { con.Close(); }
            if (res > 0) { result = Convert.ToString(res); }
            else { result = "error"; }
            return result;
        }
        public int InsertEndorsementDocumentStatus(RequestEndorsementDocList objRequestEndorsementDocList)
        {
            int res = 0;
            try
            {
                ds = new DataSet();
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[InsertEndorsementDocumentStatus]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    var xEle = new XElement("EndorsementDocument",
                                            from objEndorsementDocStatus in objRequestEndorsementDocList.RequestEndorsementDocStatusList
                                            select new XElement("EndorsementDocument",
                                                           new XAttribute("CustDocumentID", objEndorsementDocStatus.CustDocumentId),
                                                            new XAttribute("VerificationStatusId", Convert.ToString(objEndorsementDocStatus.VerificationStatusId))
                                                       )).ToString();

                    cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objRequestEndorsementDocList.LeadID;
                    cmd.Parameters.Add("@UserID", DbType.Int16).Value = objRequestEndorsementDocList.UserID;
                    cmd.Parameters.Add("@DetailId", DbType.Int64).Value = objRequestEndorsementDocList.DetailId;
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    //cmd.ExecuteNonQuery();
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    con.Close();

                    string topFilter, filter, outerExpression, innerExpression;


                    List<FieldVerification> objFieldVerificationList = new List<FieldVerification>();
                    FieldVerification objFieldVerification;
                    for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                    {
                        objFieldVerification = new FieldVerification();
                        objFieldVerification.FieldID = Convert.ToInt16(ds.Tables[0].Rows[k]["FieldID"].ToString());
                        topFilter = "FieldID=" + ds.Tables[0].Rows[k]["FieldID"].ToString();
                        outerExpression = " ( "; innerExpression = "";
                        DataRow[] firstFilter = ds.Tables[1].Select(topFilter);
                        for (int i = 0; i < firstFilter.Length; i++)
                        {
                            filter = "RuleID=" + firstFilter[i]["RuleID"].ToString() + " AND FieldID=" + firstFilter[i]["FieldID"].ToString();
                            DataRow[] foundRows;
                            foundRows = ds.Tables[2].Select(filter);
                            innerExpression = "(";
                            foreach (DataRow dr in foundRows)
                            {
                                if (dr["VerificationID"].ToString() == "1")
                                {
                                    innerExpression = innerExpression + " true ";
                                }
                                else
                                {
                                    innerExpression = innerExpression + " false ";
                                }
                                innerExpression = innerExpression + dr["DocOperator"].ToString();
                            }
                            innerExpression = innerExpression + " ) ";
                            outerExpression = outerExpression + innerExpression + firstFilter[i]["RuleOperator"].ToString();

                        }
                        outerExpression = outerExpression + ")";
                        System.Data.DataTable table = new System.Data.DataTable();
                        table.Columns.Add("", typeof(Boolean));
                        table.Columns[0].Expression = outerExpression;
                        System.Data.DataRow r = table.NewRow();
                        table.Rows.Add(r);
                        bool blResult = (Boolean)r[0];
                        if (blResult)
                        {
                            objFieldVerification.IsDocRec = 1;
                        }
                        else
                        {
                            objFieldVerification.IsDocRec = 0;
                        }
                        objFieldVerificationList.Add(objFieldVerification);

                    }

                    var xmlFieldVerification = new XElement("FieldVerification",
                                            from fieldVerification in objFieldVerificationList
                                            select new XElement("FieldVerification",
                                                           new XAttribute("IsDocRec", fieldVerification.IsDocRec),
                                                            new XAttribute("FieldID", Convert.ToString(fieldVerification.FieldID))
                                                       )).ToString();

                    ds = new DataSet();
                    //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                    using (SqlConnection con1 = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                    {
                        con1.Open();
                        cmd = new SqlCommand("[customer].[UpdateEndorsementDocumentStatus]", con1);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objRequestEndorsementDocList.LeadID;
                        cmd.Parameters.Add("@UserID", DbType.Int16).Value = objRequestEndorsementDocList.UserID;
                        cmd.Parameters.Add("@DetailId", DbType.Int64).Value = objRequestEndorsementDocList.DetailId;
                        cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xmlFieldVerification;
                        cmd.Parameters.Add("@Result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                        //cmd.ExecuteNonQuery();
                        adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);
                        res = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());
                    }

                }

               
            }
            catch { }
            //finally { con.Close(); }
            return res;
        }

        public EndorsementDocList GetEndorsementDocList(EndorsementFieldUpdateReq EndorsementFieldUpdate)
        {
            EndorsementDocList objEndorsementDocList = new EndorsementDocList();
            List<DocCategory> objDocCategoryList=new List<DocCategory>();
            List<DocVerificationStatus> objDocVerificationStatusList=new List<DocVerificationStatus>();
            DocCategory objDocCategory;
            DocVerificationStatus objDocVerificationStatus;
            Documents objDocuments;
            List<Documents> objDocumentList;
           
            ds = new DataSet();
            try
            {
                
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[GetCustomerEndorsementDocumentDetails]", con);
                    cmd.Parameters.Add("@BookingID", SqlDbType.BigInt).Value = EndorsementFieldUpdate.BookingID;
                    cmd.Parameters.Add("@DetailsID", SqlDbType.BigInt).Value = EndorsementFieldUpdate.DetailsID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objDocCategory = new DocCategory();
                            objDocCategory.CategoryId = Convert.ToInt16(dr["DocCategoryId"]);
                            objDocCategory.CategoryName = Convert.ToString(dr["Documentcategory"]);
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                objDocumentList = new List<Documents>();
                                foreach (DataRow dr1 in ds.Tables[1].Rows)
                                {
                                    if (Convert.ToInt16(dr1["DocCategoryId"]) == Convert.ToInt16(dr["DocCategoryId"]))
                                    {

                                        objDocuments = new Documents();
                                        objDocuments.DocumentId = Convert.ToInt16(dr1["DocumentId"]);
                                        objDocuments.DocumentName = Convert.ToString(dr1["Document"]);
                                        objDocuments.DocumentUrl = Convert.ToString(dr1["DocumentUrl"]);
                                        objDocuments.CustDocumentId = Convert.ToInt64(dr1["CustDocumentId"]);
                                        objDocuments.SelectedStatus = new DocVerificationStatus
                                        {
                                            VerificationStatusId = String.IsNullOrEmpty(Convert.ToString(dr1["VerificationStatusId"])) ? 0 : Convert.ToInt16(dr1["VerificationStatusId"]),
                                            VerificationStatusName = Convert.ToString(dr1["VerificationStatusName"])
                                        };
                                        objDocumentList.Add(objDocuments);
                                    }
                                }
                                if (objDocumentList != null)
                                    objDocCategory.DocumentsList = objDocumentList;
                            }
                            objDocCategoryList.Add(objDocCategory);
                        }
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[2].Rows)
                            {
                                objDocVerificationStatus = new DocVerificationStatus();
                                objDocVerificationStatus.VerificationStatusId = Convert.ToInt16(dr["VerificationStatusId"]);
                                objDocVerificationStatus.VerificationStatusName = Convert.ToString(dr["VerificationStatusName"]);
                                objDocVerificationStatusList.Add(objDocVerificationStatus);
                            }
                        }

                    }
                }
            }
            catch { }
            objEndorsementDocList.DocCategoryList = objDocCategoryList;
            objEndorsementDocList.DocVerificationStatusList = objDocVerificationStatusList;
            return objEndorsementDocList;
        }

        public List<FieldMasterData> GetFieldMasterDataList(int FieldID)
        {
            List<FieldMasterData> objFieldMasterDataList = new List<FieldMasterData>();
            FieldMasterData objFieldMasterData;
            ds = new DataSet();
            try
            {

                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[CUSTOMER].[EndorsementFieldData]", con);
                    cmd.Parameters.Add("@FieldID", SqlDbType.Int).Value = FieldID;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objFieldMasterData = new FieldMasterData();
                            objFieldMasterData.Value = Convert.ToInt16(dr["Value"]);
                            objFieldMasterData.Text = Convert.ToString(dr["Text"]);
                            objFieldMasterDataList.Add(objFieldMasterData);
                        }


                    }
                }
            }
            catch { }

            return objFieldMasterDataList;
        }

        public string UploadFileDetails()
        {
            FileUpload objFileUpload = new FileUpload();
            return "";
        }


        public List<DropDownData> GetMasterDropDownData(int FilterID, int ProductId, int Type)
        {
            List<DropDownData> objDropDownDataList = new List<DropDownData>();
            DropDownData objDropDownData;          
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[GetDocumentRepMaster]", con);
                    cmd.Parameters.Add("@FilterID", SqlDbType.Int).Value = FilterID;
                    cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = ProductId;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objDropDownData = new DropDownData();
                            objDropDownData.id = Convert.ToInt16(dr["ID"]);
                            objDropDownData.label = Convert.ToString(dr["Name"]);
                            objDropDownDataList.Add(objDropDownData);
                        }
                    }

                }
            }
            catch
            {

            }           
            return objDropDownDataList;
        }

        public DocumentDetails GetMasterDocumentDetails()
        {
            DocumentDetails objDocumentDetails = new DocumentDetails();
            List<DropDownData> objDoc = new List<DropDownData>();
            List<DropDownData> objDocCat = new List<DropDownData>();
            DropDownData objDropDownData;

            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[GetMasterDocumentDetails]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count == 2)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objDropDownData = new DropDownData();
                            objDropDownData.id = Convert.ToInt16(dr["DocCategoryId"]);
                            objDropDownData.label = Convert.ToString(dr["DocumentCategory"]);
                            objDocCat.Add(objDropDownData);
                        }
                        foreach (DataRow dr in ds.Tables[1].Rows)
                        {
                            objDropDownData = new DropDownData();
                            objDropDownData.id = Convert.ToInt16(dr["DocumentId"]);
                            objDropDownData.label = Convert.ToString(dr["Document"]);
                            objDoc.Add(objDropDownData);
                        }
                    }
                }

            }
            catch
            {

            }
            objDocumentDetails.DocCategoryList = objDocCat;
            objDocumentDetails.DocumentList = objDoc;
            return objDocumentDetails;
        }

        public CustomerDocDetails GetCustomerDocumentDetails(string LeadID)
        {
            CustomerDocDetails objCustomerDocDetails = new CustomerDocDetails();
            List<DocumentCategory> objDocumentCategoryList = new List<DocumentCategory>();
            DocumentCategory objDocumentCategory;
            List<Document> MasterDocument;
            Document objDocument;
            List<Document> ExistingCustDocument;
            List<Document> ExistingLeadDocument;

            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[GetCustomerDocumentDetails]", con);
                    cmd.Parameters.Add("@LeadID", SqlDbType.BigInt).Value = Convert.ToInt64(LeadID);
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count == 5)
                    {
                        string filter = "";
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objDocumentCategory = new DocumentCategory();
                            objDocumentCategory.DocumentCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                            objDocumentCategory.DocumentCategoryName = Convert.ToString(dr["Documentcategory"]);

                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                MasterDocument = new List<Document>();
                                filter = "DocCategoryID=" + Convert.ToInt16(dr["DocCategoryID"]).ToString();
                                DataRow[] foundRows;
                                foundRows = ds.Tables[1].Select(filter);
                                foreach (DataRow dr1 in foundRows)
                                {
                                    objDocument = new Document();
                                    objDocument.DocumentID = Convert.ToInt16(dr1["DocumentId"]);
                                    objDocument.DocumentName = Convert.ToString(dr1["Document"]);
                                    objDocument.DocumentCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                                    MasterDocument.Add(objDocument);
                                }
                                objDocumentCategory.MasterDocument = MasterDocument;
                            }
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                MasterDocument = new List<Document>();
                                filter = "DocCategoryID=" + Convert.ToInt16(dr["DocCategoryID"]).ToString();
                                DataRow[] foundRows;
                                foundRows = ds.Tables[2].Select(filter);
                                foreach (DataRow dr1 in foundRows)
                                {
                                    objDocument = new Document();
                                    objDocument.DocumentID = Convert.ToInt16(dr1["DocumentId"]);
                                    objDocument.DocumentName = Convert.ToString(dr1["Document"]);
                                    objDocument.CustDocumentID = Convert.ToInt64(dr1["CustDocumentID"]);
                                    objDocument.HostName = Convert.ToString(dr1["HostName"]);
                                    objDocument.DocumentUrl = Convert.ToString(dr1["DocumentUrl"]);
                                    MasterDocument.Add(objDocument);
                                }
                                objDocumentCategory.ExistingLeadDocument = MasterDocument;
                            }
                            if (ds.Tables[3].Rows.Count > 0)
                            {
                                MasterDocument = new List<Document>();
                                filter = "DocCategoryID=" + Convert.ToInt16(dr["DocCategoryID"]).ToString();
                                DataRow[] foundRows;
                                foundRows = ds.Tables[3].Select(filter);
                                foreach (DataRow dr1 in foundRows)
                                {
                                    objDocument = new Document();
                                    objDocument.DocumentID = Convert.ToInt16(dr1["DocumentId"]);
                                    objDocument.DocumentName = Convert.ToString(dr1["Document"]);
                                    objDocument.CustDocumentID = Convert.ToInt64(dr1["CustDocumentID"]);
                                    objDocument.HostName = Convert.ToString(dr1["HostName"]);
                                    objDocument.DocumentUrl = Convert.ToString(dr1["DocumentUrl"]);
                                    MasterDocument.Add(objDocument);
                                }
                                objDocumentCategory.ExistingCustDocument = MasterDocument;
                            }
                            objDocumentCategoryList.Add(objDocumentCategory);

                        }
                        if (ds.Tables[4].Rows.Count > 0)
                        {
                            objCustomerDocDetails.ProductName = Convert.ToString(ds.Tables[4].Rows[0]["ProductName"]);
                            objCustomerDocDetails.SupplierName = Convert.ToString(ds.Tables[4].Rows[0]["SupplierName"]);
                            objCustomerDocDetails.PlanName = Convert.ToString(ds.Tables[4].Rows[0]["PlanName"]);
                            objCustomerDocDetails.CustomerID = Convert.ToInt64(ds.Tables[4].Rows[0]["CustomerID"]);
                            objCustomerDocDetails.ProductID = Convert.ToInt16(ds.Tables[4].Rows[0]["ProductID"]);
                            objCustomerDocDetails.SupplierId = Convert.ToInt16(ds.Tables[4].Rows[0]["SupplierId"]);
                            objCustomerDocDetails.PlanId = Convert.ToInt16(ds.Tables[4].Rows[0]["PlanId"]);
                        }

                    }

                }
            }
            catch
            {

            }
            objCustomerDocDetails.DocumentCategoryList = objDocumentCategoryList;
            return objCustomerDocDetails;
        }

        public int InsertDocumentMapping(MasterDocumentMappingRequest objMasterDocumentMappingRequest)
        {
            List<MappingSettingData> objMappingSettingData = new List<MappingSettingData>();
            char[] split = ",".ToCharArray();
            int CountRes = 0, Res = 0;

            objMappingSettingData =
                  (from Portability in objMasterDocumentMappingRequest.PortabilityIDs.Trim().Split(split, StringSplitOptions.RemoveEmptyEntries)
                   from STP in objMasterDocumentMappingRequest.StpIDs.Trim().Split(split, StringSplitOptions.RemoveEmptyEntries)
                   from MedInspection in objMasterDocumentMappingRequest.MedInspectionIDs.Trim().Split(split, StringSplitOptions.RemoveEmptyEntries)
                   select new MappingSettingData()
                   {
                       Portability = Convert.ToString(Portability),
                       STP = Convert.ToString(STP),
                       MedInspection = Convert.ToString(MedInspection)
                   }).ToList();
            CountRes = objMappingSettingData.Count * objMasterDocumentMappingRequest.SupplierIDs.Split(',').Length * objMasterDocumentMappingRequest.DocumentIDs.Split(',').Length;
            var xEle = new XElement("MappingSettingData",
                                    from SettingData in objMappingSettingData
                                    select new XElement("MappingSettingData",
                                                 new XAttribute("Portability", SettingData.Portability),
                                                   new XAttribute("STP", SettingData.STP),
                                                   new XAttribute("MedInspection", SettingData.MedInspection)
                                               )).ToString();

            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[InsertDocumentMapping]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ExGroupID", DbType.String).Value = objMasterDocumentMappingRequest.GroupID;
                cmd.Parameters.Add("@ProductID", DbType.Int16).Value = objMasterDocumentMappingRequest.ProductID;
                cmd.Parameters.Add("@SupplierIDs", DbType.String).Value = objMasterDocumentMappingRequest.SupplierIDs;
                //cmd.Parameters.Add("@StatusID", DbType.Int16).Value = objMasterDocumentMappingRequest.StatusID;
                //cmd.Parameters.Add("@SubStatusID", DbType.Int16).Value = objMasterDocumentMappingRequest.SubStatusID;
                cmd.Parameters.Add("@StatusID", DbType.Int16).Value = 0;
                cmd.Parameters.Add("@SubStatusID", DbType.Int16).Value = 0;
                cmd.Parameters.Add("@DocCategoryID", DbType.Int16).Value = objMasterDocumentMappingRequest.DocCategoryID;
                cmd.Parameters.Add("@DocumentIDs", DbType.String).Value = objMasterDocumentMappingRequest.DocumentIDs;
                cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                cmd.Parameters.Add("@CreatedBy", DbType.Int16).Value = objMasterDocumentMappingRequest.CreatedBy;
                cmd.Parameters.Add("@ResultOut", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                Res = Convert.ToInt32(cmd.Parameters["@ResultOut"].Value);
               
            }
            if (Res == CountRes)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<DocumentMasterDetailsResp> GetDocumentMasterDetails(int ProductID, int SupplierID)
        {
            List<DocumentMasterDetailsResp> objDocumentMasterDetailsRespList = new List<DocumentMasterDetailsResp>();
            DocumentMasterDetailsResp objDocumentMasterDetailsResp;
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[GetDocumentMasterDetails]", con);
                    cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = ProductID;
                    cmd.Parameters.Add("@SupplierID", SqlDbType.Int).Value = SupplierID;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            objDocumentMasterDetailsResp = new DocumentMasterDetailsResp();
                            objDocumentMasterDetailsResp.GroupID = Convert.ToString(dr["GroupID"]);
                            //objDocumentMasterDetailsResp.StatusName = Convert.ToString(dr["StatusName"]);
                            objDocumentMasterDetailsResp.Documentcategory = Convert.ToString(dr["Documentcategory"]);
                            objDocumentMasterDetailsResp.Documents = Convert.ToString(dr["Documents"]);
                            objDocumentMasterDetailsResp.STP = Convert.ToString(dr["STP"]);
                            objDocumentMasterDetailsResp.Portability = Convert.ToString(dr["Portability"]);
                            objDocumentMasterDetailsResp.MedicalOrInspection = Convert.ToString(dr["MedicalOrInspection"]);
                            objDocumentMasterDetailsRespList.Add(objDocumentMasterDetailsResp);
                        }
                    }
                }
            }
            catch
            {

            }
            return objDocumentMasterDetailsRespList;
        }

        public GetDocumentEditResp GetDocumentMasterDetailsEdit(string GroupID)
        {
            GetDocumentEditResp objGetDocumentEditResp = new GetDocumentEditResp();
            List<SelectedSupplierIDResp> SelectedSupplierIDRespList = new List<SelectedSupplierIDResp>();
            SelectedSupplierIDResp objSelectedSupplierIDResp;
            List<int> DcoumentIDList = new List<int>();
            List<int> STPList =new List<int>();
            List<int> PortabilityList=new List<int>();
            List<int> MedInspectionList=new List<int>();
            
            ds = new DataSet();
            try
            {
                //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[GetDocumentMasterDetails]", con);
                    cmd.Parameters.Add("@GroupID", SqlDbType.VarChar).Value = GroupID;
                    cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 2;
                    cmd.CommandType = CommandType.StoredProcedure;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dtSup = ds.Tables[0].DefaultView.ToTable(true, "SupplierId");
                        DataTable dtDoc = ds.Tables[0].DefaultView.ToTable(true, "DocumentId");
                        DataTable dtSTP = ds.Tables[0].DefaultView.ToTable(true, "STP");
                        DataTable dtPort = ds.Tables[0].DefaultView.ToTable(true, "Portability");
                        DataTable dtMedInsp = ds.Tables[0].DefaultView.ToTable(true, "MedicalOrInspection");

                        objGetDocumentEditResp.ProductID = Convert.ToInt16(ds.Tables[0].Rows[0]["ProductID"]);
                        //objGetDocumentEditResp.StatusID = Convert.ToInt16(ds.Tables[0].Rows[0]["StatusId"]);
                        //objGetDocumentEditResp.SubStatusID = Convert.ToInt16(ds.Tables[0].Rows[0]["SubStatusId"]);
                        objGetDocumentEditResp.DocCategoryID = Convert.ToInt16(ds.Tables[0].Rows[0]["DocCategoryID"]);
                        foreach (DataRow dr in dtSup.Rows)
                        {
                            objSelectedSupplierIDResp = new SelectedSupplierIDResp();
                            objSelectedSupplierIDResp.id = Convert.ToInt16(dr[0]);
                            SelectedSupplierIDRespList.Add(objSelectedSupplierIDResp);
                        }
                        foreach (DataRow dr in dtDoc.Rows)
                        {
                            DcoumentIDList.Add(Convert.ToInt16(dr[0]));
                        }
                        foreach (DataRow dr in dtSTP.Rows)
                        {
                            STPList.Add(Convert.ToInt16(dr[0]));
                        }
                        foreach (DataRow dr in dtPort.Rows)
                        {
                            PortabilityList.Add(Convert.ToInt16(dr[0]));
                        }
                        foreach (DataRow dr in dtMedInsp.Rows)
                        {
                            MedInspectionList.Add(Convert.ToInt16(dr[0]));

                        }
                        objGetDocumentEditResp.DcoumentIDList = DcoumentIDList;
                        objGetDocumentEditResp.STPList = STPList;
                        objGetDocumentEditResp.PortabilityList = PortabilityList;
                        objGetDocumentEditResp.MedInspectionList = MedInspectionList;
                        objGetDocumentEditResp.SelectedSupplierIDRespList = SelectedSupplierIDRespList;

                    }
                }
            }
            catch
            {

            }
            return objGetDocumentEditResp;
        }


        public DocumentDetailsResponse InsertUpdateDocumentsForBooking(DocumentDetailsRequest objDocumentDetailsRequest)
        {
            int res = 0;
            FileUpload objFileUpload = new FileUpload();
            DocumentDetailsResponse objDocumentDetailsResponse = new DocumentDetailsResponse();           
            foreach (DocumentDetailIDs objDocumentDetailIDs in objDocumentDetailsRequest.DocumentDetailIDsList)
            {
                if (objDocumentDetailIDs.FileString != "")
                {
                    objDocumentDetailIDs.FileUrl = objFileUpload.GetFileUrl(objDocumentDetailsRequest.CustomerID, objDocumentDetailsRequest.BookingID, objDocumentDetailsRequest.ProductID, objDocumentDetailIDs.File, objDocumentDetailIDs.FileString, "Mongo");
                }
                else
                {
                    objDocumentDetailIDs.FileUrl = "";
                }
            }
            var xEle = new XElement("DocumentDetailIDsList",
                                    from objDocumentDetailIDs in objDocumentDetailsRequest.DocumentDetailIDsList
                                    select new XElement("DocumentDetailIDsList",
                                                 new XAttribute("DocCategoryID", Convert.ToString(objDocumentDetailIDs.DocCategoryID)),
                                                   new XAttribute("DocumentID", Convert.ToString(objDocumentDetailIDs.DocumentID)),
                                                   new XAttribute("StatusID", Convert.ToString(objDocumentDetailIDs.StatusID)),
                                                   new XAttribute("DocName", Convert.ToString(objDocumentDetailIDs.DocName)),
                                                   new XAttribute("FileName", Convert.ToString(objDocumentDetailIDs.File)),
                                                   new XAttribute("FileUrl", Convert.ToString(objDocumentDetailIDs.FileUrl)),
                                                   new XAttribute("DocEmailRemarks", string.IsNullOrEmpty(objDocumentDetailIDs.DocEmailRemarks) ? "" : Convert.ToString(objDocumentDetailIDs.DocEmailRemarks))
                                               )).ToString();

            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[InsertUpdateDocumentsForBooking]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objDocumentDetailsRequest.BookingID;
                cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                cmd.Parameters.Add("@UserID", DbType.Int16).Value = objDocumentDetailsRequest.UserID;
                cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.Output;
                ds = new DataSet();
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                res = Convert.ToInt32(cmd.Parameters["@Return"].Value);
                if (res == 0)
                {
                    objDocumentDetailsResponse = BindDataToDocumentDetailsResponse(ds);
                    //if (objDocumentDetailsResponse.ProductID == 7 || objDocumentDetailsResponse.ProductID == 115)
                    //{
                    //    if (objDocumentDetailsResponse.ProductID == 7 && (objDocumentDetailsResponse.StatusID == 0))
                    //    {

                    //    }
                    //}
                }
                else
                {
                    objDocumentDetailsResponse.Return = 1;
                }
            }            
           
            return objDocumentDetailsResponse;
        }

        public DocumentDetailsResponse UpdateDocStatus(DocumentDetailsRequest objDocumentDetailsRequest)
        {
            int res = 0;
            FileUpload objFileUpload = new FileUpload();
            DocumentDetailsResponse objDocumentDetailsResponse = new DocumentDetailsResponse();
            //foreach (DocumentDetailIDs objDocumentDetailIDs in objDocumentDetailsRequest.DocumentDetailIDsList)
            //{
            //    if (objDocumentDetailIDs.FileString != "")
            //    {
            //        objDocumentDetailIDs.FileUrl = objFileUpload.GetFileUrl(objDocumentDetailsRequest.CustomerID, objDocumentDetailsRequest.BookingID, objDocumentDetailsRequest.ProductID, objDocumentDetailIDs.File, objDocumentDetailIDs.FileString, "Mongo");
            //    }
            //    else
            //    {
            //        objDocumentDetailIDs.FileUrl = "";
            //    }
            //}
            var xEle = new XElement("DocumentDetailIDsList",
                                    from objDocumentDetailIDs in objDocumentDetailsRequest.DocumentDetailIDsList
                                    select new XElement("DocumentDetailIDsList",
                                                 new XAttribute("DocCategoryID", Convert.ToString(objDocumentDetailIDs.DocCategoryID)),
                                                   new XAttribute("DocumentID", Convert.ToString(objDocumentDetailIDs.DocumentID)),
                                                   new XAttribute("StatusID", Convert.ToString(objDocumentDetailIDs.StatusID)),
                                                   new XAttribute("DocPointsID", Convert.ToString(objDocumentDetailIDs.DocPointsID)),
                                                   new XAttribute("DocEmailRemarks", string.IsNullOrEmpty(objDocumentDetailIDs.DocEmailRemarks) ? "" : Convert.ToString(objDocumentDetailIDs.DocEmailRemarks))
                                               )).ToString();

            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[UpdateDocStatus]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objDocumentDetailsRequest.BookingID;
                cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                cmd.Parameters.Add("@UserID", DbType.Int16).Value = objDocumentDetailsRequest.UserID;
                cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.Output;
                ds = new DataSet();
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                res = Convert.ToInt32(cmd.Parameters["@Return"].Value);
                if (res == 0)
                {
                    objDocumentDetailsResponse = BindDataToDocumentDetailsResponse(ds);
                }
                else
                {
                    objDocumentDetailsResponse.Return = 1;
                }
            }
            return objDocumentDetailsResponse;
        }


        public DocumentDetailsResponse UpdateDocumentsForBooking(ExistingDocumentDetailsRequest objExistingDocumentDetailsRequest)
        {
            int res = 0;
            DocumentDetailsResponse objDocumentDetailsResponse = new DocumentDetailsResponse();
            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[UpdateDocumentsForBooking]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objExistingDocumentDetailsRequest.BookingID;
                cmd.Parameters.Add("@UserID", DbType.Int16).Value = objExistingDocumentDetailsRequest.UserID;
                cmd.Parameters.Add("@ParentID", DbType.Int64).Value = objExistingDocumentDetailsRequest.ParentID;
                cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.Output;
                ds = new DataSet();
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                res = Convert.ToInt32(cmd.Parameters["@Return"].Value);
                if (res == 0)
                {
                    objDocumentDetailsResponse = BindDataToDocumentDetailsResponse(ds);
                }
                else
                {
                    objDocumentDetailsResponse.Return = 1;
                }
            }   
            return objDocumentDetailsResponse;
        }


        public DocumentDetailsResponse GETRequiredDocumentsForBooking(long BookingID)
        {                        
            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[GETRequiredDocumentsForBooking]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@BookingID", DbType.Int64).Value = BookingID;
                ds = new DataSet();
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
            }
            return BindDataToDocumentDetailsResponse(ds);
        }

        public DocumentDetailsResponse BindDataToDocumentDetailsResponse(DataSet ds)
        {
            DocumentDetailsResponse objDocumentDetailsResponse = new DocumentDetailsResponse();
            List<CustDocumentDetails> RecDocumentsForBooking = new List<CustDocumentDetails>();
            List<CustDocumentDetails> RecDocumentsForCustomer = new List<CustDocumentDetails>();
            List<CustDocumentDetails> WaitDocumentsForBooking = new List<CustDocumentDetails>();
            CustDocumentDetails objCustDocumentDetails;
            DocCategoryDetails objDocCategoryDetails;
            List<DocCategoryDetails> ReqDocCategoryDetails = new List<DocCategoryDetails>();
            string filter = string.Empty;
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objDocumentDetailsResponse.BookingID = Convert.ToInt64(ds.Tables[0].Rows[0]["BookingID"]);
                    objDocumentDetailsResponse.CustomerName = ds.Tables[0].Rows[0]["CustName"].ToString();
                    objDocumentDetailsResponse.ProductName = ds.Tables[0].Rows[0]["ProductName"].ToString();
                    objDocumentDetailsResponse.ProductID = Convert.ToInt16(ds.Tables[0].Rows[0]["ProductID"]);
                    objDocumentDetailsResponse.SupplierName = ds.Tables[0].Rows[0]["SupplierName"].ToString();
                    objDocumentDetailsResponse.PlanName = ds.Tables[0].Rows[0]["PlanName"].ToString();
                    objDocumentDetailsResponse.CustomerEmailID = ds.Tables[0].Rows[0]["CustomerEmailID"].ToString();
                    objDocumentDetailsResponse.CustomerMobile = ds.Tables[0].Rows[0]["CustomerMobile"].ToString();
                    objDocumentDetailsResponse.SupplierID = Convert.ToInt16(ds.Tables[0].Rows[0]["SupplierID"]);  
                    objDocumentDetailsResponse.BlockAutoCommunication = Convert.ToInt16(ds.Tables[0].Rows[0]["BlockAutoCommunication"]);
                    //objDocumentDetailsResponse.IsMedInspReq = Convert.ToInt16(ds.Tables[0].Rows[0]["IsMedInspReq"]);
                }

                if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {    CurStatusDetails objCurStatusDetails;
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {      objCurStatusDetails=new CurStatusDetails();
                        objDocCategoryDetails = new DocCategoryDetails();
                        objDocCategoryDetails.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                        objDocCategoryDetails.DocCategoryName = Convert.ToString(dr["Documentcategory"]);
                        objDocCategoryDetails.CustDocWait = Convert.ToInt16(dr["CustDocWait"])>0 ? true : false;
                        
                        filter = "DocCategoryID=" + Convert.ToInt16(dr["DocCategoryID"]).ToString();
                        DataRow[] foundRows;
                        foundRows = ds.Tables[2].Select(filter);
                        objDocCategoryDetails.DocumentDetailsList = new List<CustDocumentDetails>();
                        objDocCategoryDetails.DocumentDetailsList.Add(new CustDocumentDetails() { DocumentID = 0, DocumentName = "Select" });
                        foreach (DataRow dr1 in foundRows)
                        {
                            objCustDocumentDetails = new CustDocumentDetails();
                            objCustDocumentDetails.DocumentID = string.IsNullOrEmpty(Convert.ToString(dr1["DocumentId"])) ? 0 : Convert.ToInt16(dr1["DocumentId"]);
                            objCustDocumentDetails.DocumentName = string.IsNullOrEmpty(Convert.ToString(dr1["Document"])) ? "" : Convert.ToString(dr1["Document"]);
                            
                            if (!string.IsNullOrEmpty(Convert.ToString(dr1["RecDocID"])))
                            {
                                objDocCategoryDetails.SelectedDoc = new CustDocumentDetails() 
                                    { 
                                        DocumentID = Convert.ToInt16(dr1["RecDocID"]), 
                                        DocumentName = Convert.ToString(dr1["Document"]) ,
                                        DocName = Convert.ToString(dr1["DocRecName"]),
                                        DocUrl = Convert.ToString(dr1["RecDocUrl"])   ,
                                        DocEmailRemarks = Convert.ToString(dr1["DocEmailRemarks"])
                                        
                                    };
                               
                                    objCurStatusDetails.DocumentID = Convert.ToInt16(dr1["RecDocID"]);
                                    objCurStatusDetails.DocumentName = Convert.ToString(dr1["DocRecName"]);
                                    objCurStatusDetails.DocURL = Convert.ToString(dr1["RecDocUrl"]);
                                    if (string.IsNullOrEmpty(dr1["RecDocCatID"].ToString()))
                                    {

                                        objCurStatusDetails.DocumentID = -1;
                                    }
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(dr1["RecStatusID"])))
                            {
                                objDocCategoryDetails.SelectedStatus = new StatusDetails() { StatusID = Convert.ToInt16(dr1["RecStatusID"]) };
                                objCurStatusDetails.StatusID = Convert.ToInt16(dr1["RecStatusID"]);
                            }

                            if (string.IsNullOrEmpty(dr1["RecDocCatID"].ToString()))
                            {
                                objCurStatusDetails.StatusID = -1;
                            }
                           
                            objDocCategoryDetails.DocumentDetailsList.Add(objCustDocumentDetails);
                        }
                        //if (objCurStatusDetails.StatusID == 0) {objCurStatusDetails }
                        objDocCategoryDetails.CurStatusDetails = objCurStatusDetails;
                        ReqDocCategoryDetails.Add(objDocCategoryDetails);
                    }
                }
                if (ds.Tables.Count > 3 && ds.Tables[3].Rows.Count > 0)
                {
                    filter = "DocOwner=1";
                    DataRow[] foundRows1, foundRows2;
                    foundRows1 = ds.Tables[3].Select(filter);
                    foreach (DataRow dr in foundRows1)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["DocUrl"])))
                        {
                            objCustDocumentDetails = new CustDocumentDetails();
                            objCustDocumentDetails.DocumentID = Convert.ToInt16(dr["DocumentId"]);
                            objCustDocumentDetails.DocumentName = Convert.ToString(dr["Document"]);
                            objCustDocumentDetails.DocName = Convert.ToString(dr["DocName"]);
                            objCustDocumentDetails.DocUrl = Convert.ToString(dr["DocUrl"]);
                            objCustDocumentDetails.DocPointsID = Convert.ToInt64(dr["DocPointsID"]);
                            RecDocumentsForBooking.Add(objCustDocumentDetails);
                        }
                    }

                    filter = "DocOwner=0";
                    foundRows2 = ds.Tables[3].Select(filter);
                    foreach (DataRow dr in foundRows2)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["DocUrl"])))
                        {
                            objCustDocumentDetails = new CustDocumentDetails();
                            objCustDocumentDetails.DocumentID = Convert.ToInt16(dr["DocumentId"]);
                            objCustDocumentDetails.DocumentName = Convert.ToString(dr["Document"]);
                            objCustDocumentDetails.DocName = Convert.ToString(dr["DocName"]);
                            objCustDocumentDetails.DocUrl = Convert.ToString(dr["DocUrl"]);
                            objCustDocumentDetails.DocPointsID = Convert.ToInt64(dr["DocPointsID"]);
                            objCustDocumentDetails.ParentID = Convert.ToInt64(dr["ParentID"]);
                            RecDocumentsForCustomer.Add(objCustDocumentDetails);
                        }
                    }
                }
                if (ds.Tables.Count > 4 && ds.Tables[4].Rows.Count > 0)
                {
                    objDocumentDetailsResponse.StatusID = Convert.ToInt16(ds.Tables[4].Rows[0]["StatusID"].ToString());
                    objDocumentDetailsResponse.SubStatusID = Convert.ToInt16(ds.Tables[4].Rows[0]["SubStatusID"].ToString());
                }

                if (ds.Tables.Count > 5 && ds.Tables[5].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[5].Rows)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dr["DocUrl"])))
                        {
                            objCustDocumentDetails = new CustDocumentDetails();
                            objCustDocumentDetails.DocCategoryID = Convert.ToInt16(dr["DocCategoryID"]);
                            objCustDocumentDetails.SelectedStatus = new StatusDetails()
                            {
                                StatusID = Convert.ToInt16(dr["StatusID"]),
                                StatusName = Convert.ToString(dr["StatusName"])
                            };
                            objCustDocumentDetails.DocCategoryName = Convert.ToString(dr["DocumentCategory"]);

                            objCustDocumentDetails.DocumentID = Convert.ToInt16(dr["DocumentId"]);
                            objCustDocumentDetails.DocumentName = Convert.ToString(dr["Document"]);
                            objCustDocumentDetails.DocName = Convert.ToString(dr["DocName"]);
                            objCustDocumentDetails.DocUrl = Convert.ToString(dr["DocUrl"]);
                            objCustDocumentDetails.DocPointsID = Convert.ToInt64(dr["DocPointsID"]);
                            WaitDocumentsForBooking.Add(objCustDocumentDetails);
                        }
                    }
                }
            }
            objDocumentDetailsResponse.WaitDocumentsForBooking = WaitDocumentsForBooking;
            objDocumentDetailsResponse.RecDocumentsForBooking = RecDocumentsForBooking;
            objDocumentDetailsResponse.RecDocumentsForCustomer = RecDocumentsForCustomer;
            objDocumentDetailsResponse.ReqDocCategoryDetails = ReqDocCategoryDetails;
            return objDocumentDetailsResponse;
        }

        public List<DocumentAuditTrailResponse> GetDocumentAuditTrail(long BookingID)
        {
            List<DocumentAuditTrailResponse> objDocumentAuditTrailResponseList = new List<DocumentAuditTrailResponse>();
            DocumentAuditTrailResponse objDocumentAuditTrailResponse;
            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[GetDocumentAuditTrail]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@BookingID", DbType.Int64).Value = BookingID;
                ds = new DataSet();
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objDocumentAuditTrailResponse = new DocumentAuditTrailResponse();
                        objDocumentAuditTrailResponse.DocCategoryId = Convert.ToInt16(dr["DocCategoryId"]);
                        objDocumentAuditTrailResponse.Documentcategory = Convert.ToString(dr["Documentcategory"]);
                        objDocumentAuditTrailResponse.DocumentId = Convert.ToString(dr["DocumentId"]) != "" ? Convert.ToInt16(dr["DocumentId"]) : 0;
                        objDocumentAuditTrailResponse.Document = Convert.ToString(dr["Document"]);
                        objDocumentAuditTrailResponse.CreatedOn = Convert.ToString(dr["CreatedOn"]);
                        objDocumentAuditTrailResponse.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                        objDocumentAuditTrailResponse.DocStatus = Convert.ToString(dr["DocStatus"]);
                        objDocumentAuditTrailResponse.DocEmailRemarks = Convert.ToString(dr["DocEmailRemarks"]);
                        objDocumentAuditTrailResponseList.Add(objDocumentAuditTrailResponse);
                    }
                }
            }

            return objDocumentAuditTrailResponseList;
        }
    }
}
