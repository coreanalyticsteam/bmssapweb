﻿using iTextSharp.text.pdf;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DataAccessLayer
{
    public class common : WebBrowser
    {
        [DllImport("urlmon.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern Int32 URLDownloadToFile(
            [MarshalAs(UnmanagedType.IUnknown)] object callerPointer,
            [MarshalAs(UnmanagedType.LPWStr)] string url,
            [MarshalAs(UnmanagedType.LPWStr)] string filePathWithName,
            Int32 reserved,
            IntPtr callBack);
        public static  string DownloadFile(string url)
        {
            try
            {
                string FilePath = ConfigurationManager.AppSettings["LocalPathForMerge"];
                if (!Directory.Exists(FilePath))
                    Directory.CreateDirectory(FilePath);
                FilePath = FilePath + @"\" + url.Split('/')[url.Split('/').Length - 1];

                URLDownloadToFile(null, url, FilePath, 0, IntPtr.Zero);
                return FilePath;//new FileInfo(destinationFullPathWithName);
            }
            catch (Exception ex)
            {
                ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " DownloadFile- url:" + url + "  " + ex.Message, "DownloadFileLocal.txt");
                return null;
            }
        }

        public static void ErrorNInfoLogMethod(String sMsg, String FileName)
        {
            try
            {
                if (ConfigurationSettings.AppSettings["SaveLogs"] != null && Convert.ToString(ConfigurationSettings.AppSettings["SaveLogs"]) == "true")
                {
                    String FilePath = Convert.ToString(ConfigurationSettings.AppSettings["ErrorNInfoLogFilePath"]) + DateTime.Now.ToString("ddMMMyyyy") + @"\";
                    if (!String.IsNullOrEmpty(FilePath.Trim()))
                    {
                        if (!Directory.Exists(FilePath))
                            Directory.CreateDirectory(FilePath);
                        System.IO.File.AppendAllText(FilePath + FileName, sMsg + Environment.NewLine);
                    }
                }
            }
            catch { }
        }
        
        public static void ErrorNInfoLogMethod(String sMsg, String FileName, String FolderName)
        {
            try
            {
                if (ConfigurationSettings.AppSettings["SaveLogs"] != null && Convert.ToString(ConfigurationSettings.AppSettings["SaveLogs"]) == "true")
                {
                    String FilePath = Convert.ToString(ConfigurationSettings.AppSettings["ErrorNInfoLogFilePath"]) + DateTime.Now.ToString("ddMMMyyyy") + FolderName + @"\";
                    if (!String.IsNullOrEmpty(FilePath.Trim()))
                    {
                        if (!Directory.Exists(FilePath))
                            Directory.CreateDirectory(FilePath);
                        System.IO.File.AppendAllText(FilePath + FileName, sMsg + Environment.NewLine);
                    }
                }
            }
            catch { }
        }
        public static string MakeRequest(string requestUrl, object JSONRequest, string JSONmethod, string JSONContentType,Int32 TimeOut = 100000)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {


                oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                    "\r\n requestUrl=" + requestUrl.ToString() +
                    "\r\n JSONRequest=" + JsonConvert.SerializeObject(JSONRequest) +
                    "\r\n JSONmethod=" + JSONmethod.ToString() +
                    "\r\n JSONContentType=" + JSONContentType);

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                request.Timeout = Convert.ToInt32(TimeOut);
                request.Method = JSONmethod;
                request.ContentType = JSONContentType;// "application/json";

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                if (JSONmethod == "POST")
                {
                    byte[] bytes = encoding.GetBytes(JsonConvert.SerializeObject(JSONRequest));

                    request.ContentLength = bytes.Length;

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        // Send the data.
                        requestStream.Write(bytes, 0, bytes.Length);
                    }
                }
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string objRespons = responseReader.ReadToEnd();
                            oStringBuilder.Append("\r\n objRespons=" + objRespons.ToString());
                            return objRespons;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                oStringBuilder.Append("\r\n Error=" + ex.Message);
                return null;
                //"\r\n \r\n \r\n "
            }
            finally
            {
                common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "Service.txt");

            }
        }
        public static string PostXmlAndGetResponse(string xmlfile, string url)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            WebRequest webRequest = null;
            WebResponse webResponse = null;
            string returnVal = string.Empty;
            
            try
            {
                webRequest = WebRequest.Create(url);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                string fileContents = string.Empty;

                fileContents = xmlfile;

                using (StreamWriter sw = new StreamWriter(webRequest.GetRequestStream()))
                {
                    sw.WriteLine(fileContents);
                }
                webResponse = webRequest.GetResponse();
                using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
                {
                    returnVal = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                oStringBuilder.Append(ex.ToString());
                returnVal = string.Format("Error: {0}", ex.ToString());
                xmlfile = System.DateTime.Now.ToString() + "  :  " + xmlfile;
                common.ErrorNInfoLogMethod(xmlfile,  "PostXmlAndGetResponse.txt");
            }
            finally
            {
                if (webRequest != null)
                    webRequest.GetRequestStream().Close();
                if (webResponse != null)
                    webResponse.GetResponseStream().Close();

                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + "  Error in UploadToInsurerBAXA : " + oStringBuilder, "error.txt");
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + "  Error in UploadToInsurerBAXA : " + oStringBuilder, "error.txt");
            }
            return (returnVal);
        }

        static string[] ImageEXT = { ".JPEG", ".JFIF", ".EXIF", ".TIFF", ".TIF", ".GIF", ".BMP", ".PNG", ".JPG", "" };
        public static int GetFunType(string ext)
        {
            int res = 0;
            //if(Array.Exists(ImageEXT, x =>x == ext.ToUpper()))
            //    res = 1;
            if ((Array.IndexOf(ImageEXT, ext.ToUpper()) > 0) || Array.Exists(ImageEXT, x => x == ext.ToUpper()))
            {
                res = 1;
            }
            else if (ext.ToUpper() == ".PDF")
            {
                res = 2;
            }
            else if (ext.ToUpper() == ".DOCX")
            {
                res = 3;
            }
            else if (ext.ToUpper() == ".XLSX")
            {
                res = 4;
            }
            else
            {

            }
            return res;
        }
        private static readonly Object Lock = new Object();
        public static string MergeFile(List<string> strFile, string FileName, out string mergingMsg)
        {
            ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " Start MergeFile- strFile:" + JsonConvert.SerializeObject(strFile) , "StartMerger.txt");
            mergingMsg = string.Empty;
            try
            {
                List<string> pdfFile = new List<string>();
                string _strLocalPath = string.Empty;
                foreach (string str in strFile)
                {
                    _strLocalPath = string.Empty;
                    string ext = Path.GetExtension(str);
                    _strLocalPath = GetLocalPath(str);
                    if (string.IsNullOrEmpty(_strLocalPath))
                        _strLocalPath = DownloadFile(str);

                    if (GetFunType(ext) == 1)
                    {
                        pdfFile.Add(ConvertImageToPdf(_strLocalPath));
                    }
                    else if (GetFunType(ext) == 2)
                    {
                        pdfFile.Add(DownloadFile(str));
                    }
                    else if (GetFunType(ext) == 3)
                    {
                        pdfFile.Add(ConvertDocsToPdf(DownloadFile(str)));
                    }
                    else if (GetFunType(ext) == 4)
                    {
                        pdfFile.Add(ExportWorkbookToPdf(DownloadFile(str)));
                    }

                }
                string ResFile = FileName;
                FileName = string.IsNullOrWhiteSpace(FileName) ? null : FileName;
                lock (Lock)
                {
                    FileName = CombineMultiplePDFs(pdfFile, out mergingMsg, FileName);
                }
                if (!string.IsNullOrEmpty(FileName))
                {
                    //FileName = FileName.Replace(@"\", "/");
                    // FileName = FileName.Replace("/10.209.65.247/PB.CustomerPolicyDocuments/", "http://115.112.250.37:8070/");
                    //DateTime CurDate = System.DateTime.Now;
                    string FolderName = GetDate() + "/MergeDoc/";
                    //ConfigurationManager.AppSettings["pbdocumentPath"]
                    ResFile = ConfigurationManager.AppSettings["pbdocumentPath"] + FolderName + ResFile;
                    ErrorNInfoLogMethod(ResFile, "ResFile.txt");

                }
                else
                {
                    return null;
                }
                return ResFile;
                //return CombineMultiplePDFs(pdfFile, FileName);
            }
            catch (Exception ex)
            {
                ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " MergeFile- strFile:" + JsonConvert.SerializeObject(strFile) + "  " + ex.Message, "Merger.txt");
                return "";
            }
        }
        private static string GetLocalPath(string RemotePath)
        {
            try
            {
                string FilePath = ConfigurationManager.AppSettings["LocalPathForMerge"];
                if (!Directory.Exists(FilePath))
                    Directory.CreateDirectory(FilePath);
                FilePath = FilePath + @"\" + RemotePath.Split('/')[RemotePath.Split('/').Length - 1];

                using (WebClient myWebClient = new WebClient())
                {
                    myWebClient.DownloadFile(RemotePath, FilePath);
                }
                return FilePath;
            }
            catch (Exception ex)
            {
                ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " GetLocalPath- RemotePath:" + RemotePath + ex.Message, "Merger.txt");
                return "";
            }

        }

        public static string CombineMultiplePDFs(List<string> fileNames, out string mergingMsg, string outFile = null)
        {
            mergingMsg = String.Empty;
            if (fileNames.Count > 1)
            {
                iTextSharp.text.Document document = null;
                PdfCopy writer = null;
                PdfReader reader = null;
                try
                {
                    if (outFile == null)
                        outFile = Guid.NewGuid().ToString() + ".pdf";

                    //NetworkCredential theNetworkCredential = new NetworkCredential(ConfigurationManager.AppSettings["PBFTPUserID"], ConfigurationManager.AppSettings["PBFTPPassword"]);
                    //CredentialCache theNetCache = new CredentialCache();
                    //theNetCache.Add(new Uri(ConfigurationManager.AppSettings["PBFTPServer"]), "Basic", theNetworkCredential);

                    //DateTime CurDate = System.DateTime.Now;
                    string FolderName = GetDate() + @"\MergeDoc\";
                    String FilePath = Convert.ToString(ConfigurationManager.AppSettings["PBFTPServer"] + FolderName);

                    if (!String.IsNullOrEmpty(FilePath.Trim()))
                    {
                        if (!Directory.Exists(FilePath))
                            Directory.CreateDirectory(FilePath);
                        outFile = FilePath + @"\" + outFile;
                    }



                    try
                    {
                        // step 1: creation of a document-object
                        document = new iTextSharp.text.Document();
                        //if (System.IO.File.Exists(outFile))
                        //{
                        //    File.Create(outFile).Close();
                        //    System.IO.File.Delete(outFile);
                        //}
                        // step 2: we create a writer that listens to the document
                        using (FileStream fs = new FileStream(outFile, FileMode.Create))
                        {
                            writer = new PdfSmartCopy(document, fs);
                            //writer = new PdfCopy(document, fs);
                            // step 3: we open the document
                            document.Open();

                            foreach (string fileName in fileNames)
                            {

                                // we create a reader for a certain document
                                reader = new PdfReader(fileName);
                                PdfReader.unethicalreading = true;
                                reader.ConsolidateNamedDestinations();

                                // step 4: we add content
                                for (int i = 1; i <= reader.NumberOfPages; i++)
                                {
                                    ((PdfSmartCopy)writer).AddPage(writer.GetImportedPage(reader, i));
                                    //PdfImportedPage page = writer.GetImportedPage(reader, i);
                                    //writer.AddPage(page);
                                }
                                writer.FreeReader(reader);

                                //PRAcroForm form = reader.AcroForm;
                                //if (form != null)
                                //{
                                //    writer.CopyDocumentFields(reader);
                                //}

                                reader.Close();
                            }

                            // step 5: we close the document and writer
                            writer.Close();
                            document.Close();
                            return outFile.Replace(@"\\", @"\");
                        }

                    }
                    catch (Exception ex)
                    {
                        mergingMsg = ex.Message;
                        ErrorNInfoLogMethod(
                            System.DateTime.Now.ToString() + " CombineMultiplePDFs- fileNames:" +
                            JsonConvert.SerializeObject(fileNames) + " outFile:" + outFile + " error:" + ex.Message,
                            "Merger.txt");
                        return null;
                    }
                    finally
                    {
                        if (writer != null) writer.Close();
                        if (document != null) document.Close();
                        if (reader != null) reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    mergingMsg = mergingMsg + " ; " + ex.Message;
                    ErrorNInfoLogMethod(
                        System.DateTime.Now.ToString() + " CombineMultiplePDFs- fileNames:" +
                        JsonConvert.SerializeObject(fileNames) + " outFile:" + outFile + " error:" + ex.Message,
                        "Merger.txt");
                    return null;
                }
            }
            else
            {
                try
                {
                    if (outFile == null)
                        outFile = Guid.NewGuid().ToString() + ".pdf";

                    string FolderName = GetDate() + @"\MergeDoc\";
                    String FilePath = Convert.ToString(ConfigurationManager.AppSettings["PBFTPServer"] + FolderName);

                    if (!String.IsNullOrEmpty(FilePath.Trim()))
                    {
                        if (!Directory.Exists(FilePath))
                            Directory.CreateDirectory(FilePath);
                        outFile = FilePath + @"\" + outFile;
                    }

                    File.Copy(fileNames[0], outFile, true);

                    //using (WebClient myWebClient = new WebClient())
                    //{
                    //    myWebClient.DownloadFile(fileNames[0], outFile);
                    //}
                    return FilePath;
                }
                catch (Exception ex)
                {
                    mergingMsg = mergingMsg + " ; " + ex.Message;
                    ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " CombineMultiplePDFs- fileNames:" + JsonConvert.SerializeObject(fileNames) + " outFile:" + outFile + " error:" + ex.Message, "Merger.txt");
                    return "";
                }
            }
        }
        public static Microsoft.Office.Interop.Word.Document wordDocument { get; set; }
        public static string ConvertDocsToPdf(string srcFilename)
        {
            try
            {
                string dstFilename = Guid.NewGuid().ToString() + ".pdf";

                //NetworkCredential theNetworkCredential = new NetworkCredential(ConfigurationManager.AppSettings["PBFTPUserID"], ConfigurationManager.AppSettings["PBFTPPassword"]);
                //CredentialCache theNetCache = new CredentialCache();
                //theNetCache.Add(new Uri(ConfigurationManager.AppSettings["PBFTPServer"]), "Basic", theNetworkCredential);

                //DateTime CurDate = System.DateTime.Now;
                string FolderName = GetDate();
                String FilePath = Convert.ToString(ConfigurationManager.AppSettings["PBFTPServer"] + FolderName);
                if (!String.IsNullOrEmpty(FilePath.Trim()))
                {
                    if (!Directory.Exists(FilePath))
                        Directory.CreateDirectory(FilePath);
                    dstFilename = FilePath + @"\" + dstFilename;
                }



                Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
                wordDocument = appWord.Documents.Open(srcFilename);
                wordDocument.ExportAsFixedFormat(dstFilename, WdExportFormat.wdExportFormatPDF);
                appWord.Documents.Close();
                return dstFilename;
            }
            catch (Exception ex)
            {
                ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " ConvertDocsToPdf- srcFilename:" + srcFilename + "  " + ex.Message, "Merger.txt");
                return "0";
            }
        }
        public static string GetDate()
        {
            string date = "";
            DateTime dt = new System.DateTime();
            dt = System.DateTime.Now;
            if (dt.Day.ToString().Length > 1)
            {
                date = dt.Day.ToString();
            }
            else
            {
                date = "0" + dt.Day.ToString();

            }
            if (dt.Month.ToString().Length > 1)
            {
                date = date + dt.Month.ToString();
            }
            else
            {
                date = date + "0" + dt.Month.ToString();

            }
            date = date + dt.Year.ToString();
            return date;
        }
        public static string ConvertImageToPdf(string srcFilename)
        {
            try
            {
                ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " Start ConvertImageToPdf- srcFilename:" + srcFilename , "ImageToPdf.txt");
                string dstFilename = Guid.NewGuid().ToString() + ".pdf";

                //NetworkCredential theNetworkCredential = new NetworkCredential(ConfigurationManager.AppSettings["PBFTPUserID"], ConfigurationManager.AppSettings["PBFTPPassword"]);
                //CredentialCache theNetCache = new CredentialCache();
                //theNetCache.Add(new Uri(ConfigurationManager.AppSettings["PBFTPServer"]), "Basic", theNetworkCredential);

                //DateTime CurDate = System.DateTime.Now;
                string FolderName = GetDate();
                String FilePath = Convert.ToString(ConfigurationManager.AppSettings["PBFTPServer"] + FolderName);
                if (!String.IsNullOrEmpty(FilePath.Trim()))
                {
                    if (!Directory.Exists(FilePath))
                        Directory.CreateDirectory(FilePath);
                    dstFilename = FilePath + @"\" + dstFilename;
                }

                //string dstFilename = ConfigurationManager.AppSettings["FTPServer"] + "TATA_IMG_" + System.DateTime.Now.ToString() + ".pdf";

                iTextSharp.text.Rectangle pageSize = null;

                using (var srcImage = new Bitmap(srcFilename))
                {
                    pageSize = new iTextSharp.text.Rectangle(0, 0, srcImage.Width, srcImage.Height);
                }
                using (var ms = new MemoryStream())
                {
                    var document = new iTextSharp.text.Document(pageSize, 0, 0, 0, 0);
                    iTextSharp.text.pdf.PdfWriter.GetInstance(document, ms).SetFullCompression();
                    document.Open();
                    var image = iTextSharp.text.Image.GetInstance(srcFilename);
                    document.Add(image);
                    document.Close();

                    File.WriteAllBytes(dstFilename, ms.ToArray());
                }
                return dstFilename;
            }
            catch (Exception ex)
            {
                ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " ConvertImageToPdf- srcFilename:" + srcFilename + "  " + ex.Message, "ImageToPdf.txt");
                return "0";
            }
        }
        public static string ExportWorkbookToPdf(string workbookPath)
        {

            string outputPath = Guid.NewGuid().ToString() + ".pdf";

            //NetworkCredential theNetworkCredential = new NetworkCredential(ConfigurationManager.AppSettings["PBFTPUserID"], ConfigurationManager.AppSettings["PBFTPPassword"]);
            //CredentialCache theNetCache = new CredentialCache();
            //theNetCache.Add(new Uri(ConfigurationManager.AppSettings["PBFTPServer"]), "Basic", theNetworkCredential);

            //DateTime CurDate = System.DateTime.Now;
            string FolderName = GetDate();
            String FilePath = Convert.ToString(ConfigurationManager.AppSettings["PBFTPServer"] + FolderName);
            if (!String.IsNullOrEmpty(FilePath.Trim()))
            {
                if (!Directory.Exists(FilePath))
                    Directory.CreateDirectory(FilePath);
                outputPath = FilePath + @"\" + outputPath;
            }

            // If either required string is null or empty, stop and bail out
            if (string.IsNullOrEmpty(workbookPath) || string.IsNullOrEmpty(outputPath))
            {
                return "0";
            }

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

            // Create new instance of Excel
            excelApplication = new Microsoft.Office.Interop.Excel.Application();

            // Make the process invisible to the user
            excelApplication.ScreenUpdating = false;

            // Make the process silent
            excelApplication.DisplayAlerts = false;

            // Open the workbook that you wish to export to PDF
            excelWorkbook = excelApplication.Workbooks.Open(workbookPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (excelWorkbook == null)
            {
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;

                return "0";
            }

            var exportSuccessful = true;
            try
            {
                // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputPath);
            }
            catch (System.Exception ex)
            {
                ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " ExportWorkbookToPdf- srcFilename:" + workbookPath + "  " + ex.Message, "Merger.txt");
                // Mark the export as failed for the return value...
                exportSuccessful = false;

                // Do something with any exceptions here, if you wish...
                // MessageBox.Show...        
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                excelWorkbook.Close();
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;
            }

            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (System.IO.File.Exists(outputPath))
            {
                System.Diagnostics.Process.Start(outputPath);
            }
            if (!exportSuccessful)
                outputPath = "0";
            return outputPath;
        }
        //public void  DownloadAsZip(string file[]){

        //   HttpFileCollection filecollect = Request.Files;

        //    for (int i = 0; i < filecollect.Count; i++)

        //    {

        //        HttpPostedFile hpf = filecollect[i];

        //        if (hpf.ContentLength > 0)

        //        {

        //            hpf.SaveAs(Server.MapPath("ZipFolder") + "\\" +

        //              System.IO.Path.GetFileName(hpf.FileName));

        //            Label2.Text += " <br/> <b>File: </b>" + hpf.FileName +

        //              " <b>Size:</b> " + hpf.ContentLength + " <b>Type:</b> " +

        //              hpf.ContentType + "File Uploaded!";

        //        }

        //    }

        //    string zipfilepath = Server.MapPath("~/ZipFolder/");

        //    string[] x = Directory.GetFiles(zipfilepath);

        //    using (Ionic.Zip.ZipFile compress = new Ionic.Zip.ZipFile())

        //    {

        //        string dateofcreation = DateTime.Now.ToString("y");

        //        dateofcreation = dateofcreation.Replace("/", "");

        //        compress.AddFiles(x, dateofcreation);

        //        compress.Save(Server.MapPath(dateofcreation + ".zip"));

        //        Label1.Text = "ZIP Created Successfully";

        //    }

        //    if (Label1.Text == "ZIP Created Successfully")

        //    {

        //        Array.ForEach(Directory.GetFiles(zipfilepath),

        //          delegate(string deleteFile) { File.Delete(deleteFile); });

        //    }
        //}
    }
}
