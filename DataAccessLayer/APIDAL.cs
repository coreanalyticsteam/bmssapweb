﻿using Newtonsoft.Json;
using PropertyLayers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace DataAccessLayer
{
    public class APIDAL
    {
        Hashtable htData = null;
        DataSet _ds;
        DataTable _dt;
        SqlDataAdapter _adp;
        SqlConnection con;
        SqlCommand cmd;
        public ResultClass ICICIPru(string Json)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetICICIPruStatus]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                     List<ICICIPRUMobRes> tmp = JsonConvert.DeserializeObject<List<ICICIPRUMobRes>>(Json);
                   // XmlDocument xdoc = JsonConvert.DeserializeXmlNode("{\"root\":" + Json + "}", "root");
                     var xEle = new XElement("root",
                                  from objUserMapping in tmp
                                  select new XElement("root",
                                                 new XAttribute("POLICYKEY", Convert.ToString(objUserMapping.POLICYKEY)),
                                                  new XAttribute("APPLICATIONKEY", Convert.ToString(objUserMapping.APPLICATIONKEY)),
                                                   new XAttribute("FILLER_OWNERNAME", Convert.ToString(objUserMapping.FILLER_OWNERNAME)),
                                                    new XAttribute("SUBMISSIONDATE", Convert.ToString(objUserMapping.SUBMISSIONDATE)),
                                                    new XAttribute("FILLER_PRODUCTCODE", Convert.ToString(objUserMapping.FILLER_PRODUCTCODE)),
                                                    new XAttribute("PLAN_NAME", Convert.ToString(objUserMapping.PLAN_NAME)),
                                                    new XAttribute("FILLER_SUMINSURED", Convert.ToString(objUserMapping.FILLER_SUMINSURED)),
                                                    new XAttribute("POLICYSTATUS", Convert.ToString(objUserMapping.POLICYSTATUS)),
                                                    new XAttribute("EFFDATE", Convert.ToString(objUserMapping.EFFDATE)),
                                                    new XAttribute("FOLLOWUPCODE", Convert.ToString(objUserMapping.FOLLOWUPCODE)),
                                                    new XAttribute("RECEIVEDDATE", Convert.ToString(objUserMapping.RECEIVEDDATE)),
                                                    new XAttribute("REQUESTDATE", Convert.ToString(objUserMapping.REQUESTDATE)),
                                                    new XAttribute("PREMIUM_TERM", Convert.ToString(objUserMapping.PREMIUM_TERM)),
                                                    new XAttribute("RISK_TERM", Convert.ToString(objUserMapping.RISK_TERM)),
                                                    new XAttribute("MED_FOLLOWUPCODE", Convert.ToString(objUserMapping.MED_FOLLOWUPCODE)),
                                                    new XAttribute("MED_REQUESTDATE", Convert.ToString(objUserMapping.MED_REQUESTDATE)),
                                                    new XAttribute("BRANCHRECDDATE", Convert.ToString(objUserMapping.BRANCHRECDDATE)),
                                                    new XAttribute("COPSRECDDATE", Convert.ToString(objUserMapping.COPSRECDDATE)),
                                                    new XAttribute("EXAMDATE", Convert.ToString(objUserMapping.EXAMDATE)),
                                                    new XAttribute("FLAG", Convert.ToString(objUserMapping.FLAG)),
                                                    new XAttribute("DUMMY_2", Convert.ToString(objUserMapping.DUMMY_2))
                                             )).ToString();
                    cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    con.Close();
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }
    }
}
