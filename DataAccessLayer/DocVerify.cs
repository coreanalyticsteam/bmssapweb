﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using PropertyLayers;
using System.Xml.Linq;
using System.Data.Common;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using System.Xml.Serialization;

namespace DataAccessLayer
{

    public class DocVerify
    {
        DataSet ds;
        DataTable dt;
        SqlDataAdapter adp;
        //SqlConnection con;           
        SqlCommand cmd;

        //JsonConvert.DeserializeObject<bool>(MakeRequest(ConnectionClass.RejectMembersLink(), objMembersRejPostList, "POST", "application/json"));

        public bool checkChangesInDoc(UpdateDocStatusReq objUpdateDocStatusReq)
        {
            bool res = false;
            DocResponse objDocResponse = JsonConvert.DeserializeObject<DocResponse>(
               common.MakeRequest(ConfigurationManager.AppSettings["LifeURL"] + objUpdateDocStatusReq.AppNo + "&leadid=" + objUpdateDocStatusReq.BookingID,
                "", "GET", "application/json"));
            List<DocReqResp> objDocReqRespList = new List<DocReqResp>();
            DocReqResp objDocReqResp;

            KYCDocuments KYC_Documents = new KYCDocuments();
            NonKYCDocuments Non_KYC_Documents = new NonKYCDocuments();
            OtherDocuments Other_Documents = new OtherDocuments();
            List<DocumentForRes> DocumentForResListKYC = new List<DocumentForRes>();
            List<DocumentForRes> DocumentForResListNON = new List<DocumentForRes>();
            List<DocumentForRes> DocumentForResListOth = new List<DocumentForRes>();

            DocumentForRes objDocumentForRes;
            int docTypeId = 0;
            if (objDocResponse != null)
            {


                if (objUpdateDocStatusReq.DocumentType == 2 && objDocResponse.KYC_Documents != null)
                {
                    foreach (Availabledoc AvailabledocData in objDocResponse.KYC_Documents.availableDocs)
                    {

                        if (AvailabledocData.docs != null)
                        {


                            if (Convert.ToString(objUpdateDocStatusReq.DocCategoryID) == AvailabledocData.docCategoryId
                                         && AvailabledocData.docs.docTypeId == Convert.ToString(objUpdateDocStatusReq.DocumentID)
                                         && Convert.ToDateTime(AvailabledocData.lastModified) == Convert.ToDateTime(objUpdateDocStatusReq.lastModified))
                            {
                                res = true;
                                return res;
                            }


                        }
                    }
                }
                if (objUpdateDocStatusReq.DocumentType == 3 && objDocResponse.Non_KYC_Documents != null)
                {
                    foreach (Availabledoc AvailabledocData in objDocResponse.Non_KYC_Documents.availableDocs)
                    {

                        if (AvailabledocData.docs != null)
                        {

                            if (Convert.ToString(objUpdateDocStatusReq.DocCategoryID) == AvailabledocData.docCategoryId
                                          && AvailabledocData.docs.docTypeId == Convert.ToString(objUpdateDocStatusReq.DocumentID)
                                          && Convert.ToDateTime(AvailabledocData.lastModified) == Convert.ToDateTime(objUpdateDocStatusReq.lastModified))
                            {
                                res = true;
                                return res;
                            }


                        }
                    }
                }
                if (objUpdateDocStatusReq.DocumentType == 4 && objDocResponse.Other_Documents != null)
                {
                    foreach (Availabledoc AvailabledocData in objDocResponse.Other_Documents.availableDocs)
                    {

                        if (AvailabledocData.docs != null)
                        {

                            if (Convert.ToString(objUpdateDocStatusReq.DocCategoryID) == AvailabledocData.docCategoryId
                                         && AvailabledocData.docs.docTypeId == Convert.ToString(objUpdateDocStatusReq.DocumentID)
                                         && Convert.ToDateTime(AvailabledocData.lastModified) == Convert.ToDateTime(objUpdateDocStatusReq.lastModified))
                            {
                                res = true;
                                return res;
                            }


                        }
                    }


                }
            }

            return res;
        }
        public DocResponse UpdateSingleDocStatus(UpdateDocStatusReq objUpdateDocStatusReq)
        {
            DocResponse objDocResponse = new DocResponse();
            try
            {
                if (checkChangesInDoc(objUpdateDocStatusReq))
                {
                    int res = 0;

                    //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                    using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                    {
                        con.Open();
                        cmd = new SqlCommand("[Customer].[UpdateSingleDocStatus]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objUpdateDocStatusReq.BookingID;
                        cmd.Parameters.Add("@DocPointsID", DbType.Int16).Value = objUpdateDocStatusReq.DocPointsID;
                        cmd.Parameters.Add("@StatusID", DbType.Int64).Value = objUpdateDocStatusReq.StatusID;
                        cmd.Parameters.Add("@UserID", DbType.Int16).Value = objUpdateDocStatusReq.UserID;
                        cmd.Parameters.Add("@DocumentType", DbType.Int16).Value = objUpdateDocStatusReq.DocType;
                        cmd.Parameters.Add("@Type", DbType.Int16).Value = objUpdateDocStatusReq.Type;
                        cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();
                        res = Convert.ToInt32(cmd.Parameters["@Return"].Value);
                    }
                    if (res > 0)
                    {
                        if (objUpdateDocStatusReq.Type == "2")
                        {
                            UploadToInsurerTata(objUpdateDocStatusReq.FileUrl);
                        }
                        objDocResponse = GetPendingDocuments(objUpdateDocStatusReq.BookingID, objUpdateDocStatusReq.AppNo);

                    }
                    else
                    {
                        objDocResponse.IsErrorInDocSubmit = true;
                        objDocResponse.ErrorMsgInDocSubmit = "Some error occured, please try again.";
                    }
                }
                else
                {
                    objDocResponse.IsErrorInDocSubmit = true;
                    objDocResponse.ErrorMsgInDocSubmit = "There is change in doc, please and try again.";
                }
            }
            catch (Exception ex)
            {
                objDocResponse.IsErrorInDocSubmit = true;
                objDocResponse.ErrorMsgInDocSubmit = "Some error occured '" + ex .Message + "' , please try again.";
            }
            return objDocResponse;

        }

        public bool checkChangesForSubmitDoc(List<UpdateDocStatusReq> objListUpdateDocStatusReq)
        {
            bool res = false;
            if (objListUpdateDocStatusReq.Count > 0)
            {
                DocResponse objDocResponse = JsonConvert.DeserializeObject<DocResponse>(
                   common.MakeRequest(ConfigurationManager.AppSettings["LifeURL"] + objListUpdateDocStatusReq[0].AppNo + "&leadid=" + objListUpdateDocStatusReq[0].BookingID,
                    "", "GET", "application/json"));
                if (objDocResponse != null)
                {
                    if (objListUpdateDocStatusReq[0].DocumentType == 2)
                    {
                        int chkCount = 0, chkKYCCount = 0;
                        foreach (UpdateDocStatusReq objUpdateDocStatusReq in objListUpdateDocStatusReq)
                        {
                            if (objDocResponse.KYC_Documents != null)
                            {
                                foreach (Availabledoc AvailabledocData in objDocResponse.KYC_Documents.availableDocs)
                                {

                                    if (AvailabledocData.docs != null)
                                    {
                                        if (objUpdateDocStatusReq.DocCategoryID == Convert.ToInt16(AvailabledocData.docCategoryId)
                                                 && Convert.ToInt16(AvailabledocData.docs.docTypeId) == objUpdateDocStatusReq.DocumentID
                                                )
                                        {
                                            if (Convert.ToDateTime(AvailabledocData.lastModified) == Convert.ToDateTime(objUpdateDocStatusReq.lastModified))
                                            {
                                                chkCount = chkCount + 1;
                                                chkKYCCount = chkKYCCount + 1;
                                            }
                                            else
                                            {
                                                chkKYCCount = chkKYCCount + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (chkCount == chkKYCCount)
                        {
                            res = true;
                            return res;
                        }
                    }
                    else
                    {
                        if (objListUpdateDocStatusReq[0].DocumentType == 3 && objDocResponse.Non_KYC_Documents != null)
                        {
                            foreach (Availabledoc AvailabledocData in objDocResponse.Non_KYC_Documents.availableDocs)
                            {

                                if (AvailabledocData.docs != null)
                                {

                                    if (objListUpdateDocStatusReq[0].DocCategoryID == Convert.ToInt16(AvailabledocData.docCategoryId)
                                             && Convert.ToInt16(AvailabledocData.docs.docTypeId) == objListUpdateDocStatusReq[0].DocumentID
                                             && Convert.ToDateTime(AvailabledocData.lastModified) == Convert.ToDateTime(objListUpdateDocStatusReq[0].lastModified))
                                    {
                                        res = true;
                                        return res;
                                    }


                                }
                            }
                        }
                        if (objListUpdateDocStatusReq[0].DocumentType == 4 && objDocResponse.Other_Documents != null)
                        {
                            foreach (Availabledoc AvailabledocData in objDocResponse.Other_Documents.availableDocs)
                            {

                                if (AvailabledocData.docs != null)
                                {

                                    if (objListUpdateDocStatusReq[0].DocCategoryID == Convert.ToInt16(AvailabledocData.docCategoryId)
                                             && Convert.ToInt16(AvailabledocData.docs.docTypeId) == objListUpdateDocStatusReq[0].DocumentID
                                             && Convert.ToDateTime(AvailabledocData.lastModified) == Convert.ToDateTime(objListUpdateDocStatusReq[0].lastModified))
                                    {
                                        res = true;
                                        return res;
                                    }


                                }
                            }


                        }
                    }
                }

            }
            return res;
        }

        public void SaveSubmitLog(long BookingID, int StatusID, int DocGroupId, string APIRequest, string APIResponse, int CreatedBy)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                {
                    con.Open();
                    cmd = new SqlCommand("[Customer].[UpdateSingleDocStatus]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BookingID", DbType.Int64).Value = BookingID;
                    cmd.Parameters.Add("@DocGroupId", DbType.Int16).Value = DocGroupId;
                    cmd.Parameters.Add("@StatusID", DbType.Int16).Value = StatusID;
                    cmd.Parameters.Add("@CreatedBy", DbType.Int16).Value = CreatedBy;
                    cmd.Parameters.Add("@APIResponse", DbType.Int16).Value = APIResponse;
                    cmd.Parameters.Add("@APIRequest", DbType.Int16).Value = APIRequest;
                    cmd.ExecuteNonQuery();
                }
            }
            catch { }
        }
        public DocResponse SubmitDoc(UpdateDocReq objUpdateDocReq)
        {
            DocResponse objDocResponse = new DocResponse();
            if (checkChangesForSubmitDoc(objUpdateDocReq.UpdateDocStatusReqList))
            {
                int res = 0;
                int count = 0;
                if (objUpdateDocReq.supplierid == "2")      //TATA
                {
                    foreach (UpdateDocStatusReq objUpdateDocStatusReq in objUpdateDocReq.UpdateDocStatusReqList)
                    {
                        if (UploadToInsurerTata(objUpdateDocStatusReq.FileUrl))
                        {
                            count = count + 1;
                        }
                    }
                }
                else if (objUpdateDocReq.supplierid == "20")   //BAXA
                {

                    //objDocResponse;
                    foreach (UpdateDocStatusReq objUpdateDocStatusReq in objUpdateDocReq.UpdateDocStatusReqList)
                    {
                        if (!string.IsNullOrEmpty(objUpdateDocStatusReq.FileUrl)
                            && !string.IsNullOrEmpty(objUpdateDocReq.quotationid)
                            && !string.IsNullOrEmpty(objUpdateDocReq.proposalid)
                            && !string.IsNullOrEmpty(objUpdateDocReq.customerid)
                            && objUpdateDocReq.customerid != "Array"
                             && objUpdateDocReq.proposalid != "Array"
                             && objUpdateDocReq.quotationid != "Array"
                            )
                        {
                            if (UploadToInsurerBAXA(objUpdateDocStatusReq.FileUrl, objUpdateDocReq.quotationid,
                                objUpdateDocReq.proposalid, objUpdateDocReq.customerid,
                                "Customer.pdf", objUpdateDocStatusReq.DocCatTypeName, objUpdateDocStatusReq.DocTypeName, objUpdateDocReq.UpdateDocStatusReqList[0].BookingID))
                            {
                                count = count + 1;
                            }
                        }
                        else
                        {
                            objDocResponse.IsErrorInDocSubmit = true;
                            objDocResponse.ErrorMsgInDocSubmit = "quotationid/proposalid/customerid is missing.";
                        }
                    }

                }
                else if (objUpdateDocReq.supplierid == "9")     //KOTAK
                {
                    count = UploadToInsurerKotak(objUpdateDocReq.UpdateDocStatusReqList,
                             objUpdateDocReq.proposalid);
                }
                else
                {
                    objDocResponse.IsErrorInDocSubmit = true;
                    objDocResponse.ErrorMsgInDocSubmit = "Supplier ID is missing.";
                }
                if (count == objUpdateDocReq.UpdateDocStatusReqList.Count)
                {
                    //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                    using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                    {
                        con.Open();
                        cmd = new SqlCommand("[Customer].[UpdateSingleDocStatus]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BookingID", DbType.Int64).Value = objUpdateDocReq.UpdateDocStatusReqList[0].BookingID;
                        cmd.Parameters.Add("@DocPointsID", DbType.Int16).Value = objUpdateDocReq.UpdateDocStatusReqList[0].DocPointsID;
                        cmd.Parameters.Add("@StatusID", DbType.Int64).Value = objUpdateDocReq.UpdateDocStatusReqList[0].StatusID;
                        cmd.Parameters.Add("@UserID", DbType.Int16).Value = objUpdateDocReq.UpdateDocStatusReqList[0].UserID;
                        cmd.Parameters.Add("@DocumentType", DbType.Int16).Value = objUpdateDocReq.UpdateDocStatusReqList[0].DocumentType;
                        cmd.Parameters.Add("@Type", DbType.Int16).Value = 2;
                        cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.Output;
                        //adp = new SqlDataAdapter(cmd);
                        //ds = new DataSet();
                        //adp.Fill(ds);
                        cmd.ExecuteNonQuery();
                        res = Convert.ToInt32(cmd.Parameters["@Return"].Value);
                    }
                    if (res > 0)
                    {
                        objDocResponse = GetPendingDocuments(objUpdateDocReq.leadId, objUpdateDocReq.applicationNumber);
                        objDocResponse.IsErrorInDocSubmit = false;
                        objDocResponse.ErrorMsgInDocSubmit = "";

                    }

                }
                else if (!objDocResponse.IsErrorInDocSubmit)
                {
                    objDocResponse.IsErrorInDocSubmit = true;
                    objDocResponse.ErrorMsgInDocSubmit = "There is issue in insurer service.";
                }

            }
            else {
                objDocResponse.IsErrorInDocSubmit = true;
                objDocResponse.ErrorMsgInDocSubmit = "There is change in document , please refresh and try again.";
            } 
            try
            {
                SaveSubmitLog(Convert.ToInt64(objUpdateDocReq.BookingID), Convert.ToInt16(objUpdateDocReq.UpdateDocStatusReqList[0].StatusID), objUpdateDocReq.UpdateDocStatusReqList[0].DocCategoryID, "", objDocResponse.ErrorMsgInDocSubmit, Convert.ToInt16(objUpdateDocReq.UpdateDocStatusReqList[0].UserID));
            }
            catch { }
            return objDocResponse;
        }

        private bool UploadToInsurerTata(string FileUrl)
        {
            bool res = false;
            string FolderName = common.GetDate();

            if (sFTP.UploadFileToSFTP(ConfigurationManager.AppSettings["TATAFTPServer"],
               ConfigurationManager.AppSettings["TATAFTPUserID"],
               ConfigurationManager.AppSettings["TATAFTPPassword"],
               ConfigurationManager.AppSettings["sftpFolderPath"] + FolderName + "/",
              FileUrl, Path.GetFileName(FileUrl), "REMOTE") == "SUCCESS")
            {
                res = true;
            }
            return res;
        }
        private bool UploadToInsurerBAXA(string FileUrl, string QuotationId, string ProposalId, string CustomerId,
            string FileName, string DocCatType, string DocType, string BookingID)
        {
            bool res = false;
            try
            {
                byte[] imageData = null;
                if (!string.IsNullOrEmpty(FileUrl) && !string.IsNullOrEmpty(QuotationId) && !string.IsNullOrEmpty(ProposalId) && !string.IsNullOrEmpty(CustomerId))
                    using (var wc = new System.Net.WebClient())
                    {
                        imageData = wc.DownloadData(FileUrl);

                        wc.OpenRead(FileUrl);
                        Int64 bytes_total = Convert.ToInt64(wc.ResponseHeaders["Content-Length"]);
                        if (bytes_total > 1001)
                        {

                            string data = System.Convert.ToBase64String(imageData);

                            DataSet dsDocID = new DataSet();
                            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                            {
                                con.Open();

                                string queryStringPolicyURL = "DECLARE @DocumentType VARCHAR(100),@DocumentCategory VARCHAR(100) "

                  + "SELECT @DocumentType=DOC_ID FROM [ProductDB].[Customer].[DocumetIDs] (NOLOCK) "
                 + " WHERE REPLACE(DOC_DESCRIPTION,' ','')=REPLACE('" + DocCatType + "',' ','') "
                 + " AND REPLACE(PROOF_DESC,' ','')=REPLACE('" + DocCatType + "',' ','') "
                 + " and InsurerID=20 "

                 + " SELECT @DocumentCategory=DOC_ID FROM [ProductDB].[Customer].[DocumetIDs] (NOLOCK) "
                 + " WHERE REPLACE(DOC_DESCRIPTION,' ','')=REPLACE('" + DocCatType + "',' ','') "
                 + " AND REPLACE(PROOF_DESC,' ','')=REPLACE('" + DocType + "',' ','') "
                 + " and InsurerID=20 "

                 + " SELECT @DocumentType AS DocumentType,@DocumentCategory AS DocumentCategory "; ;
                                cmd = new SqlCommand(queryStringPolicyURL, con);
                                cmd.CommandTimeout = 1000000;
                                cmd.CommandType = CommandType.Text;
                                adp = new SqlDataAdapter();
                                dsDocID = new DataSet();
                                adp.SelectCommand = cmd;
                                adp.Fill(dsDocID);

                                //cmd = new SqlCommand("[Customer].[GetDocumentIDForTATA]", con);
                                //cmd.CommandType = CommandType.StoredProcedure;
                                //cmd.Parameters.Add("@MPDOC_DESCRIPTION", DbType.String).Value = DocCatType;
                                //cmd.Parameters.Add("@MPPROOF_DESC", DbType.String).Value = DocType;

                                //cmd.Parameters.Add("@Type", DbType.String).Value = 2;

                                //adp = new SqlDataAdapter(cmd);
                                //adp.Fill(dsDocID);
                            }
                            if (dsDocID != null && dsDocID.Tables.Count > 0 && dsDocID.Tables[0].Rows.Count > 0)
                            {

                                XElement xml = new XElement("PartnersDocumentDetails",
                                   new XElement("QuotationId",
                                       QuotationId),
                                   new XElement("ProposalId",
                                       ProposalId),
                                   new XElement("CustomerId",
                                       CustomerId),
                                   new XElement("PartnerCode",
                                       ConfigurationManager.AppSettings["PartnerCodeBAXA"]),
                                   new XElement("DocumentType",
                                        dsDocID.Tables[0].Rows[0]["DocumentType"].ToString()),
                                   new XElement("DocumentCategory",
                                        dsDocID.Tables[0].Rows[0]["DocumentCategory"].ToString()),
                                   new XElement("FileName",
                                       Path.GetFileName(FileUrl)),
                                   new XElement("FileInByte",
                                       data)
                                   );
                                XmlSerializer serializer = new XmlSerializer(typeof(PartnersDocumentDetails));
                                string xmlString = common.PostXmlAndGetResponse(xml.ToString(), ConfigurationManager.AppSettings["BAXADocSubmit"]);
                                PartnersDocumentDetails objPartnersDocumentDetails = (PartnersDocumentDetails)serializer.Deserialize(new StringReader(xmlString));
                                string ServiceReq = string.Empty;
                                ServiceReq = System.DateTime.Now.ToString();
                                ServiceReq = ServiceReq + " FileUrl:" + FileUrl + " QuotationId:" + QuotationId + " ProposalId:" + ProposalId + " CustomerId:" + CustomerId + " FileName:" + FileName + " DocCatType:" + DocCatType + " DocType:" + DocType;
                                ServiceReq = ServiceReq + " XML:" + xml.ToString();
                                //           FileUrl, string QuotationId, string ProposalId, string CustomerId,
                                //string FileName,string DocCatType,string DocType
                                if (objPartnersDocumentDetails.IsSuccess == "true")
                                {
                                    //common.ErrorNInfoLogMethod(ServiceReq, BookingID + "_BAXADocSubmit.txt");
                                    res = true;
                                }
                                else
                                {
                                    //common.ErrorNInfoLogMethod(xml.ToString(), BookingID + "_BAXADocSubmit.txt");
                                }

                            }
                            else
                            {
                                res = false;
                            }
                        }
                        else
                        {
                            res = false;
                        }
                    }
            }
            catch (Exception ex)
            {
                //var lineNumber = new System.Diagnostics.StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + "  Error in UploadToInsurerBAXA : " + ex.Message, "error.txt");
            }
            return res;
        }


        private int UploadToInsurerKotak(List<UpdateDocStatusReq> UpdateDocStatusReqList, string ProposalId)
        {
            common.ErrorNInfoLogMethod("\r\n Time=" + DateTime.Now +
                "\r\n Request=" + JsonConvert.SerializeObject(UpdateDocStatusReqList) +
                "\r\n ProposalId=" + Convert.ToString(ProposalId), "UploadToInsurerKotak.txt");
            try
            {
                int res = 0;
                FTPFilePush objFTPFilePush = new FTPFilePush();
                objFTPFilePush.ProposalNumber = ProposalId;
                List<FTPFileDetails> FTPFileDetailList = new List<FTPFileDetails>();
                FTPFileDetails objFTPFileDetails;
                foreach (UpdateDocStatusReq objUpdateDocStatusReq in UpdateDocStatusReqList)
                {
                    objFTPFileDetails = new FTPFileDetails();
                    objFTPFileDetails.FileURL = objUpdateDocStatusReq.FileUrl;
                    objFTPFileDetails.FTPFile = objUpdateDocStatusReq.DocCatTypeName + "/" + objUpdateDocStatusReq.DocCatTypeName + ".pdf";
                    FTPFileDetailList.Add(objFTPFileDetails);
                }

                //objFTPFileDetails = new FTPFileDetails();
                //objFTPFileDetails.FileURL = "";
                //objFTPFileDetails.FTPFile = "DNC format";

                //FTPFileDetailList.Add(objFTPFileDetails);
                objFTPFilePush.FTPFileDetailList = FTPFileDetailList;
                bool result = JsonConvert.DeserializeObject<bool>(common.MakeRequest(ConfigurationManager.AppSettings["KotakDocSubmit"], objFTPFilePush, "POST", "application/json"));
                res = UpdateDocStatusReqList.Count;
                return res;
            }
            catch (Exception ex)
            {
                var lineNumber = new System.Diagnostics.StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                common.ErrorNInfoLogMethod("Error in UploadToInsurerKotak : Line number : " + Convert.ToString(lineNumber) + " :" + ex.Message, "error.txt");
                return 0;
            }

        }
        public class PartnersDocumentDetails
        {
            public string IsSuccess { get; set; }
            public string ErrorMessage { get; set; }
        }

        public DocResponse GetPendingDocuments(string leadId, string applicationNumber)
        {
            try
            {
                DocResponse objDocResponse = JsonConvert.DeserializeObject<DocResponse>(
                    common.MakeRequest(ConfigurationManager.AppSettings["LifeURL"] + applicationNumber + "&leadid=" + leadId,
                     "", "GET", "application/json"));
                List<DocReqResp> objDocReqRespList = new List<DocReqResp>();
                DocReqResp objDocReqResp;

                KYCDocuments KYC_Documents = new KYCDocuments();
                NonKYCDocuments Non_KYC_Documents = new NonKYCDocuments();
                OtherDocuments Other_Documents = new OtherDocuments();
                DataRow[] DocCategoryID, DocumentID, DocUrl;

                List<DocumentForRes> DocumentForResListKYC = new List<DocumentForRes>();
                List<DocumentForRes> DocumentForResListNON = new List<DocumentForRes>();
                List<DocumentForRes> DocumentForResListOth = new List<DocumentForRes>();

                DocumentForRes objDocumentForRes;

                List<filesInfo> files;
                filesInfo objfilesInfo;
                if (objDocResponse != null)
                {
                    int cKYC = 0, cKYCSub = 0, cNON = 0, cOth = 0;
                    if (objDocResponse.KYC_Documents != null)
                    {
                        if (objDocResponse.KYC_Documents != null)
                        {
                            foreach (Availabledoc AvailabledocData in objDocResponse.KYC_Documents.availableDocs)
                            {

                                if (AvailabledocData.docs != null)
                                {
                                    foreach (filesInfo file in AvailabledocData.docs.files)
                                    {
                                        if (file.FileDetails != "NA" && AvailabledocData.docs.docTypeId != "NA")
                                        {
                                            objDocReqResp = new DocReqResp();
                                            objDocReqResp.DocumentType = "2";
                                            objDocReqResp.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocReqResp.docType = AvailabledocData.docs.docType;
                                            objDocReqResp.DocCategoryID = AvailabledocData.docCategoryId;
                                            objDocReqResp.DocumentID = AvailabledocData.docs.docTypeId;
                                            objDocReqResp.DocUrl = file.FileDetails;
                                            objDocReqResp.CreatedOn = AvailabledocData.lastModified;
                                            objDocReqRespList.Add(objDocReqResp);
                                        }
                                        else
                                        {
                                            objDocumentForRes = new DocumentForRes();
                                            objDocumentForRes.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocumentForRes.docType = AvailabledocData.docs.docType;
                                            objDocumentForRes.docCategoryId = AvailabledocData.docCategoryId;
                                            objDocumentForRes.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocumentForRes.docType = AvailabledocData.docs.docType;
                                            objDocumentForRes.docTypeId = AvailabledocData.docs.docTypeId;
                                            DocumentForResListKYC.Add(objDocumentForRes);
                                        }
                                    }

                                }
                            }
                        }
                        if (objDocResponse.Non_KYC_Documents != null)
                        {
                            foreach (Availabledoc AvailabledocData in objDocResponse.Non_KYC_Documents.availableDocs)
                            {

                                if (AvailabledocData.docs != null)
                                {
                                    foreach (filesInfo file in AvailabledocData.docs.files)
                                    {
                                        if (file.FileDetails != "NA" && AvailabledocData.docs.docTypeId != "NA")
                                        {

                                            objDocReqResp = new DocReqResp();
                                            objDocReqResp.DocumentType = "3";
                                            objDocReqResp.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocReqResp.docType = AvailabledocData.docs.docType;
                                            objDocReqResp.DocCategoryID = AvailabledocData.docCategoryId;
                                            objDocReqResp.DocumentID = AvailabledocData.docs.docTypeId;
                                            objDocReqResp.DocUrl = file.FileDetails;
                                            objDocReqResp.CreatedOn = AvailabledocData.lastModified;
                                            objDocReqRespList.Add(objDocReqResp);
                                        }
                                        else
                                        {
                                            objDocumentForRes = new DocumentForRes();
                                            objDocumentForRes.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocumentForRes.docType = AvailabledocData.docs.docType;
                                            objDocumentForRes.docCategoryId = AvailabledocData.docCategoryId;
                                            objDocumentForRes.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocumentForRes.docType = AvailabledocData.docs.docType;
                                            objDocumentForRes.docTypeId = AvailabledocData.docs.docTypeId;
                                            DocumentForResListNON.Add(objDocumentForRes);
                                        }
                                    }
                                }
                            }
                        }
                        if (objDocResponse.Other_Documents != null)
                        {
                            foreach (Availabledoc AvailabledocData in objDocResponse.Other_Documents.availableDocs)
                            {

                                if (AvailabledocData.docs != null)
                                {
                                    foreach (filesInfo file in AvailabledocData.docs.files)
                                    {
                                        if (file.FileDetails != "NA" && AvailabledocData.docs.docTypeId != "NA")
                                        {
                                            objDocReqResp = new DocReqResp();
                                            objDocReqResp.DocumentType = "4";
                                            objDocReqResp.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocReqResp.docType = AvailabledocData.docs.docType;
                                            objDocReqResp.DocCategoryID = AvailabledocData.docCategoryId;
                                            objDocReqResp.DocumentID = AvailabledocData.docs.docTypeId;
                                            objDocReqResp.DocUrl = file.FileDetails;
                                            objDocReqResp.CreatedOn = AvailabledocData.lastModified;
                                            objDocReqRespList.Add(objDocReqResp);
                                        }
                                        else
                                        {
                                            objDocumentForRes = new DocumentForRes();
                                            objDocumentForRes.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocumentForRes.docType = AvailabledocData.docs.docType;
                                            objDocumentForRes.docCategoryId = AvailabledocData.docCategoryId;
                                            objDocumentForRes.docCategoryName = AvailabledocData.docCategoryName;
                                            objDocumentForRes.docType = AvailabledocData.docs.docType;
                                            objDocumentForRes.docTypeId = AvailabledocData.docs.docTypeId;
                                            DocumentForResListOth.Add(objDocumentForRes);
                                        }
                                    }

                                }
                            }
                        }

                    }

                    int res = 0;
                    var xEle = new XElement("DocumentDetailIDsList",
                        from objDocReq in objDocReqRespList
                        select new XElement("DocumentDetailIDsList",
                            new XAttribute("DocumentType",
                                !string.IsNullOrEmpty(Convert.ToString(objDocReq.DocumentType))
                                    ? Convert.ToString(objDocReq.DocumentType)
                                    : string.Empty),
                            new XAttribute("DocCategoryID",
                                !string.IsNullOrEmpty(Convert.ToString(objDocReq.DocCategoryID))
                                    ? Convert.ToString(objDocReq.DocCategoryID)
                                    : string.Empty),
                            new XAttribute("DocumentID",
                                !string.IsNullOrEmpty(Convert.ToString(objDocReq.DocumentID))
                                    ? Convert.ToString(objDocReq.DocumentID)
                                    : string.Empty),
                            new XAttribute("docCategoryName",
                                !string.IsNullOrEmpty(Convert.ToString(objDocReq.docCategoryName))
                                    ? Convert.ToString(objDocReq.docCategoryName)
                                    : string.Empty),
                            new XAttribute("docType",
                                !string.IsNullOrEmpty(Convert.ToString(objDocReq.docType))
                                    ? Convert.ToString(objDocReq.docType)
                                    : string.Empty),
                            new XAttribute("DocUrl",
                                !string.IsNullOrEmpty(Convert.ToString(objDocReq.DocUrl))
                                    ? Convert.ToString(objDocReq.DocUrl)
                                    : string.Empty)
                            ,
                            new XAttribute("CreatedOn",
                                Convert.ToString(objDocReq.CreatedOn) == "NA" ||
                                string.IsNullOrEmpty(Convert.ToString(objDocReq.CreatedOn))
                                    ? System.DateTime.Now.ToString()
                                    : Convert.ToString(objDocReq.CreatedOn))
                            )).ToString();

                    //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                    using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                    {
                        con.Open();
                        cmd = new SqlCommand("[Customer].[CheckExistingDocument]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BookingID", DbType.Int64).Value = leadId;
                        cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;
                        cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.Output;
                        ds = new DataSet();
                        adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);
                        res = Convert.ToInt32(cmd.Parameters["@Return"].Value);
                    }
                    List<DocReqResp> objDocReqRespListNew = new List<DocReqResp>();

                    int count = 0;


                    string filter, mergingMsg;
                    List<string> strFile;
                    DataRow[] drDocCategoryID, drDocumentID, drFileUrl;
                    foreach (DataRow drDocumentType in ds.Tables[0].Rows)
                    {
                        filter = " DocumentType=" + drDocumentType["DocumentType"].ToString();

                        drDocCategoryID = ds.Tables[1].Select(filter);
                        foreach (DataRow drCat in drDocCategoryID)
                        {
                            filter = " DocumentType='" + drDocumentType["DocumentType"].ToString() + "' AND DocCategoryID='" + drCat["DocCategoryID"].ToString() + "'";
                            //filter = " DocumentType=" + drDocumentType["DocumentType"].ToString() + " AND DocCategoryID=" + drCat["DocCategoryID"].ToString();
                            drDocumentID = ds.Tables[2].Select(filter);
                            foreach (DataRow drDoc in drDocumentID)
                            {
                                mergingMsg = string.Empty;
                                filter = " DocumentType='" + drDocumentType["DocumentType"].ToString() + "' AND DocCategoryID='" + drCat["DocCategoryID"].ToString() + "' AND DocumentID='" + drDoc["DocumentID"].ToString() + "'";
                                drFileUrl = ds.Tables[3].Select(filter);
                                count = 0;
                                string MergedFileURL = string.Empty;
                                // string MergedFileURL = "";
                                strFile = new List<string>();
                                foreach (DataRow drURL in drFileUrl)
                                {

                                    count = count + 1;
                                    if (drURL["FileUrl"].ToString() != "NA")
                                    {
                                        objDocReqResp = new DocReqResp();
                                        objDocReqResp.DocumentType = drURL["DocumentType"].ToString();
                                        objDocReqResp.DocCategoryID = drURL["DocCategoryID"].ToString();
                                        objDocReqResp.docCategoryName = drURL["docCategoryName"].ToString();
                                        objDocReqResp.docType = drURL["docType"].ToString();

                                        objDocReqResp.DocumentID = drURL["DocumentID"].ToString();
                                        objDocReqResp.DocUrl = drURL["FileUrl"].ToString();
                                        objDocReqResp.DocName = drURL["FileUrl"].ToString() != "NA" ? Path.GetFileName(drURL["FileUrl"].ToString()) : "";
                                        objDocReqResp.DocExt = drURL["FileUrl"].ToString() != "NA" ? Path.GetExtension(drURL["FileUrl"].ToString()) : "";
                                        objDocReqResp.CreatedOn = drURL["CreatedOn"].ToString();


                                        strFile.Add(objDocReqResp.DocUrl);
                                        if (!string.IsNullOrEmpty(objDocResponse.supplierid))
                                        {
                                            MergedFileURL = ConfigurationManager.AppSettings["pbdocumentPath"]
                                                + common.GetDate()
                                                + "/MergeDoc/"
                                                + GetDocNameForInsurer(objDocResponse.applicationNumber, objDocResponse.sisid, drURL["docCategoryName"].ToString(), drURL["docType"].ToString(), objDocResponse.suppliertag, objDocResponse.supplierid);

                                            objDocReqResp.MergedURL = MergedFileURL;
                                            objDocReqResp.MergedName = Path.GetFileName(MergedFileURL);//Convert.ToString(builder).Split('/')[Convert.ToString(builder).Split('/').Length-1];
                                        }

                                        //<AppNo>_<AgentCode>_<SIS>_<DocID>_FORM.pdf

                                        //if (string.IsNullOrWhiteSpace(MergedFileURL))
                                        //{
                                        //    return null;
                                        //}

                                       
                                        if (drFileUrl.Length == count )
                                        {
                                            if (!string.IsNullOrEmpty(objDocResponse.supplierid))
                                            {
                                                mergingMsg = null;
                                                MergedFileURL = common.MergeFile(strFile, GetDocNameForInsurer(objDocResponse.applicationNumber, objDocResponse.sisid, drURL["docCategoryName"].ToString(), drURL["docType"].ToString(), objDocResponse.suppliertag, objDocResponse.supplierid), out mergingMsg);
                                                if (!string.IsNullOrEmpty(mergingMsg))
                                                {
                                                    objDocReqResp.MergingMsg = mergingMsg;
                                                    objDocReqResp.IsMerged = 0;
                                                    foreach (DocReqResp _objDocReqResp in objDocReqRespListNew)
                                                    {
                                                        if (_objDocReqResp.DocCategoryID == objDocReqResp.DocCategoryID && _objDocReqResp.docType == objDocReqResp.docType)
                                                        {
                                                            _objDocReqResp.IsMerged = 0;
                                                            _objDocReqResp.MergingMsg = mergingMsg;
                                                        }
                                                    }
                                                    //objDocReqRespListNew.ForEach(x => x.IsMerged = 0);
                                                    //objDocReqRespListNew.ForEach(x => x.MergingMsg = mergingMsg);

                                                }
                                                else
                                                {
                                                    objDocReqResp.IsMerged = 1;
                                                    objDocReqResp.MergingMsg = mergingMsg;
                                                    
                                                }
                                            }
                                            else
                                            {
                                                objDocReqResp.MergingMsg = "supplierid is null, E2E service error...";
                                                objDocReqResp.IsMerged = 0;
                                                foreach (DocReqResp _objDocReqResp in objDocReqRespListNew)
                                                {
                                                    if (_objDocReqResp.DocCategoryID == objDocReqResp.DocCategoryID && _objDocReqResp.docType == objDocReqResp.docType)
                                                    {
                                                        _objDocReqResp.IsMerged = 0;
                                                        _objDocReqResp.MergingMsg = "supplierid is null, E2E service error...";                                                     }
                                                }
                                                //objDocReqRespListNew.ForEach(x => x.IsMerged = 0);
                                                //objDocReqRespListNew.ForEach(x => x.MergingMsg = "supplierid is null, E2E service error...");
                                            }
                                            //string tempMsg = string.Empty;

                                           

                                           
                                        }
                                        objDocReqRespListNew.Add(objDocReqResp);
                                    }
                                }

                            }
                        }
                    }
                    DocResponse objDocResponseDetails = new DocResponse();

                    foreach (var item in objDocReqRespListNew)
                    {
                        if (item.IsMerged == 1)
                        {
                            foreach (var resp in objDocReqRespListNew.Where(resp => resp.DocCategoryID == item.DocCategoryID))
                            {
                                resp.IsMerged = item.IsMerged;
                            }
                        }
                    }

                    foreach (var item in objDocReqRespListNew)
                    {
                        if (!string.IsNullOrEmpty(item.MergingMsg))
                        {
                            foreach (var resp in objDocReqRespListNew.Where(resp => resp.DocCategoryID == item.DocCategoryID))
                            {
                                resp.MergingMsg = item.MergingMsg;
                            }
                        }
                    }

                    var xEleNew = new XElement("DocumentDetailIDsList",
                        from objDocReq in objDocReqRespListNew
                        select new XElement("DocumentDetailIDsList",
                            new XAttribute("DocumentType", !string.IsNullOrEmpty(objDocReq.DocumentType) ? Convert.ToString(objDocReq.DocumentType) : ""),
                            new XAttribute("DocCategoryID", !string.IsNullOrEmpty(objDocReq.DocCategoryID) ? Convert.ToString(objDocReq.DocCategoryID) : ""),
                            new XAttribute("DocumentID", !string.IsNullOrEmpty(objDocReq.DocumentID) ? Convert.ToString(objDocReq.DocumentID) : ""),
                            new XAttribute("DocUrl", !string.IsNullOrEmpty(objDocReq.DocUrl) ? Convert.ToString(objDocReq.DocUrl) : ""),
                            new XAttribute("DocExt", !string.IsNullOrEmpty(objDocReq.DocExt) ? Convert.ToString(objDocReq.DocExt) : ""),
                            new XAttribute("DocName", !string.IsNullOrEmpty(objDocReq.DocName) ? Convert.ToString(objDocReq.DocName) : ""),
                            new XAttribute("MergedUrl", !string.IsNullOrEmpty(objDocReq.MergedURL) ? Convert.ToString(objDocReq.MergedURL) : ""),
                            new XAttribute("MergedName", !string.IsNullOrEmpty(objDocReq.MergedName) ? Convert.ToString(objDocReq.MergedName) : ""),
                            new XAttribute("CreatedOn", !string.IsNullOrEmpty(objDocReq.CreatedOn) ? Convert.ToString(objDocReq.CreatedOn) : ""),
                            new XAttribute("docCategoryName", !string.IsNullOrEmpty(objDocReq.docCategoryName) ? Convert.ToString(objDocReq.docCategoryName) : ""),
                            new XAttribute("docType", !string.IsNullOrEmpty(objDocReq.docType) ? Convert.ToString(objDocReq.docType) : ""),
                            new XAttribute("IsMerged", !string.IsNullOrEmpty(Convert.ToString(objDocReq.IsMerged)) ? Convert.ToString(objDocReq.IsMerged) : ""),
                            new XAttribute("MergingMsg", !string.IsNullOrEmpty(objDocReq.MergingMsg) ? Convert.ToString(objDocReq.MergingMsg) : "")

                            )).ToString();

                    //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
                    DataSet dsRecord = new DataSet();
                    using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
                    {
                        con.Open();
                        cmd = new SqlCommand("[Customer].[UpdateDoumentStatus]", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@BookingID", DbType.Int64).Value = leadId;
                        cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEleNew;
                        cmd.Parameters.Add("@UserID", DbType.Xml).Value = 124;

                        adp = new SqlDataAdapter(cmd);
                        adp.Fill(dsRecord);
                    }
                    int TotalKYC = 0;
                    foreach (DataRow DocumentType in dsRecord.Tables[0].Rows)
                    {
                        objDocResponseDetails.applicationNumber = applicationNumber;
                        objDocResponseDetails.leadId = leadId;

                        if (DocumentType["DocumentType"].ToString() == "2")
                        {

                            filter = " DocumentType=" + DocumentType["DocumentType"].ToString();
                            DocCategoryID = dsRecord.Tables[1].Select(filter);
                            foreach (DataRow drDoc in DocCategoryID)
                            {
                                objDocumentForRes = new DocumentForRes();
                                objDocumentForRes.docCategoryId = drDoc["DocCategoryID"].ToString();
                                objDocumentForRes.docCategoryName = drDoc["Documentcategory"].ToString();
                                //objAvailabledoc.DocStatus = drDoc["DocCategoryID"].ToString();         

                                filter = " DocumentType=" + drDoc["DocumentType"].ToString() + " AND DocCategoryID='" + drDoc["DocCategoryID"].ToString() + "'";
                                DocumentID = dsRecord.Tables[2].Select(filter);
                                objDocumentForRes.DocPointsID = drDoc["DocPointsID"].ToString();
                                objDocumentForRes.docTypeId = drDoc["DocumentID"].ToString();
                                objDocumentForRes.statusID = drDoc["StatusID"].ToString();
                                objDocumentForRes.statusName = drDoc["StatusName"].ToString();
                                objDocumentForRes.docType = drDoc["Document"].ToString();
                                objDocumentForRes.CreatedON = drDoc["CreatedOn"].ToString();
                                objDocumentForRes.LastModified = drDoc["LastModified"].ToString();
                                objDocumentForRes.MergedFile = drDoc["MergedURL"].ToString();
                                objDocumentForRes.MergedFileName = drDoc["MergedName"].ToString();
                                objDocumentForRes.IsMerged = drDoc["IsMerged"].ToString();
                                objDocumentForRes.MergedMsg = drDoc["MergingMsg"].ToString();
                                if (!objDocumentForRes.MergedFileName.Contains("NotFound"))
                                {
                                    objDocumentForRes.isName = true;
                                }
                                if (objDocumentForRes.statusID == "6" || objDocumentForRes.statusID == "7")
                                {
                                    cKYC = cKYC + 1;
                                    if (objDocumentForRes.statusID == "7")
                                    {
                                        cKYCSub = cKYCSub + 1;
                                    }
                                }
                                TotalKYC = TotalKYC + 1;

                                files = new List<filesInfo>();

                                filter = " DocumentType='" + drDoc["DocumentType"].ToString() + "' AND DocCategoryID='" + drDoc["DocCategoryID"].ToString() + "' AND DocumentID='" + drDoc["DocumentID"].ToString() + "'";
                                DocUrl = dsRecord.Tables[2].Select(filter);
                                foreach (DataRow drFiles in DocUrl)
                                {
                                    objfilesInfo = new filesInfo();
                                    objfilesInfo.FileDetails = drFiles["DocUrl"].ToString();
                                    objfilesInfo.extension = drFiles["DocExt"].ToString();
                                    objfilesInfo.FileName = drFiles["DocName"].ToString();
                                    files.Add(objfilesInfo);
                                }
                                objDocumentForRes.files = files;


                                DocumentForResListKYC.Add(objDocumentForRes);
                            }
                        }
                        if (DocumentType["DocumentType"].ToString() == "3")
                        {

                            filter = " DocumentType=" + DocumentType["DocumentType"].ToString();
                            DocCategoryID = dsRecord.Tables[1].Select(filter);
                            foreach (DataRow drDoc in DocCategoryID)
                            {
                                objDocumentForRes = new DocumentForRes();
                                objDocumentForRes.docCategoryId = drDoc["DocCategoryID"].ToString();
                                objDocumentForRes.docCategoryName = drDoc["Documentcategory"].ToString();
                                //objAvailabledoc.DocStatus = drDoc["DocCategoryID"].ToString();


                                filter = " DocumentType='" + drDoc["DocumentType"].ToString() + "' AND DocCategoryID='" + drDoc["DocCategoryID"].ToString() + "'";
                                DocumentID = dsRecord.Tables[2].Select(filter);


                                objDocumentForRes.DocPointsID = drDoc["DocPointsID"].ToString();
                                objDocumentForRes.docTypeId = drDoc["DocumentID"].ToString();
                                objDocumentForRes.statusID = drDoc["StatusID"].ToString();
                                objDocumentForRes.statusName = drDoc["StatusName"].ToString();
                                objDocumentForRes.docType = drDoc["Document"].ToString();
                                objDocumentForRes.CreatedON = drDoc["CreatedOn"].ToString();
                                objDocumentForRes.LastModified = drDoc["LastModified"].ToString();
                                objDocumentForRes.MergedFile = drDoc["MergedURL"].ToString();
                                objDocumentForRes.MergedFileName = drDoc["MergedName"].ToString();
                                objDocumentForRes.IsMerged = drDoc["IsMerged"].ToString();
                                objDocumentForRes.MergedMsg = drDoc["MergingMsg"].ToString();

                                if (!objDocumentForRes.MergedFileName.Contains("NotFound"))
                                {
                                    objDocumentForRes.isName = true;
                                }
                                if (objDocumentForRes.statusID == "6")
                                {
                                    cNON = cNON + 1;
                                }
                                files = new List<filesInfo>();

                                filter = " DocumentType='" + drDoc["DocumentType"].ToString() + "' AND DocCategoryID='" + drDoc["DocCategoryID"].ToString() + "' AND DocumentID='" + drDoc["DocumentID"].ToString() + "'";
                                DocUrl = dsRecord.Tables[2].Select(filter);
                                foreach (DataRow drFiles in DocUrl)
                                {
                                    objfilesInfo = new filesInfo();
                                    objfilesInfo.FileDetails = drFiles["DocUrl"].ToString();
                                    objfilesInfo.extension = drFiles["DocExt"].ToString();
                                    objfilesInfo.FileName = drFiles["DocName"].ToString();
                                    files.Add(objfilesInfo);
                                }
                                objDocumentForRes.files = files;


                                DocumentForResListNON.Add(objDocumentForRes);
                            }
                        }
                        if (DocumentType["DocumentType"].ToString() == "4")
                        {

                            filter = " DocumentType='" + DocumentType["DocumentType"].ToString() + "'";
                            DocCategoryID = dsRecord.Tables[1].Select(filter);
                            foreach (DataRow drDoc in DocCategoryID)
                            {
                                objDocumentForRes = new DocumentForRes();
                                objDocumentForRes.docCategoryId = drDoc["DocCategoryID"].ToString();
                                objDocumentForRes.docCategoryName = drDoc["Documentcategory"].ToString();
                                //objAvailabledoc.DocStatus = drDoc["DocCategoryID"].ToString();


                                filter = " DocumentType='" + drDoc["DocumentType"].ToString() + "' AND DocCategoryID='" + drDoc["DocCategoryID"].ToString() + "'";
                                DocumentID = dsRecord.Tables[2].Select(filter);


                                objDocumentForRes.DocPointsID = drDoc["DocPointsID"].ToString();
                                objDocumentForRes.docTypeId = drDoc["DocumentID"].ToString();
                                objDocumentForRes.statusID = drDoc["StatusID"].ToString();
                                objDocumentForRes.statusName = drDoc["StatusName"].ToString();
                                objDocumentForRes.docType = drDoc["Document"].ToString();
                                objDocumentForRes.CreatedON = drDoc["CreatedOn"].ToString();
                                objDocumentForRes.LastModified = drDoc["LastModified"].ToString();
                                objDocumentForRes.MergedFile = drDoc["MergedURL"].ToString();
                                objDocumentForRes.MergedFileName = drDoc["MergedName"].ToString();
                                objDocumentForRes.IsMerged = drDoc["IsMerged"].ToString();
                                objDocumentForRes.MergedMsg = drDoc["MergingMsg"].ToString();
                                if (!objDocumentForRes.MergedFileName.Contains("NotFound"))
                                {
                                    objDocumentForRes.isName = true;
                                }
                                files = new List<filesInfo>();
                                if (objDocumentForRes.statusID == "6")
                                {
                                    cOth = cOth + 1;
                                }
                                filter = " DocumentType='" + drDoc["DocumentType"].ToString() + "' AND DocCategoryID='" + drDoc["DocCategoryID"].ToString() + "' AND DocumentID='" + drDoc["DocumentID"].ToString() + "'";
                                DocUrl = dsRecord.Tables[2].Select(filter);
                                foreach (DataRow drFiles in DocUrl)
                                {
                                    objfilesInfo = new filesInfo();
                                    objfilesInfo.FileDetails = drFiles["DocUrl"].ToString();
                                    objfilesInfo.FileName = drFiles["DocName"].ToString();
                                    objfilesInfo.extension = drFiles["DocExt"].ToString();
                                    files.Add(objfilesInfo);
                                }
                                objDocumentForRes.files = files;


                                DocumentForResListOth.Add(objDocumentForRes);
                            }
                        }
                    }

                    if (cKYC == DocumentForResListKYC.Count && cKYCSub != cKYC)
                    {
                        KYC_Documents.StatusID = 1;
                    }
                    if (cNON == DocumentForResListNON.Count)
                    {
                        Non_KYC_Documents.StatusID = 1;
                    }
                    if (cOth == DocumentForResListOth.Count)
                    {
                        Other_Documents.StatusID = 1;
                    }
                    //if (!string.IsNullOrEmpty(KYC_Documents.ErrorMsg))
                    //    DocumentForResListKYC.ForEach(x => x.MergedFileName = KYC_Documents.ErrorMsg);
                    //if (!string.IsNullOrEmpty(Non_KYC_Documents.ErrorMsg))
                    //    DocumentForResListNON.ForEach(x => x.MergedFileName = Non_KYC_Documents.ErrorMsg);
                    //if (!string.IsNullOrEmpty(Other_Documents.ErrorMsg))
                    //    DocumentForResListOth.ForEach(x => x.MergedFileName = Other_Documents.ErrorMsg);

                    KYC_Documents.DocumentForResList = DocumentForResListKYC;
                    Non_KYC_Documents.DocumentForResList = DocumentForResListNON;
                    Other_Documents.DocumentForResList = DocumentForResListOth;
                    objDocResponseDetails.KYC_Documents = KYC_Documents;
                    objDocResponseDetails.Non_KYC_Documents = Non_KYC_Documents;
                    objDocResponseDetails.Other_Documents = Other_Documents;
                    objDocResponseDetails.sisid = objDocResponse.sisid;
                    objDocResponseDetails.suppliertag = objDocResponse.suppliertag;
                    objDocResponseDetails.proposalid = objDocResponse.proposalid;
                    objDocResponseDetails.customerid = objDocResponse.customerid;
                    objDocResponseDetails.quotationid = objDocResponse.quotationid;
                    objDocResponseDetails.supplierid = objDocResponse.supplierid;
                    objDocResponseDetails.fid = objDocResponse.fid;
                    objDocResponseDetails.AgentCode = System.Configuration.ConfigurationManager.AppSettings["AgentCode"];
                    return objDocResponseDetails;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                var lineNumber = new System.Diagnostics.StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                common.ErrorNInfoLogMethod("Error in GetPendingDocuments : Line number : " + Convert.ToString(lineNumber) + " :" + ex.Message, "error.txt");
                return null;
            }
        }


        private string GetDocNameForInsurer(string applicationNumber, string sisid, string MPDOC_DESCRIPTION, string MPPROOF_DESC, string InsurerName, string InsurerID)
        {




            string DocID = MPDOC_DESCRIPTION + "_" + MPPROOF_DESC;
            DataSet dsDocID = new DataSet();
            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[GetDocumentIDForTATA]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MPDOC_DESCRIPTION", DbType.String).Value = MPDOC_DESCRIPTION;
                cmd.Parameters.Add("@MPPROOF_DESC", DbType.String).Value = MPPROOF_DESC;
                cmd.Parameters.Add("@InsurerID", DbType.String).Value = InsurerID;
                cmd.Parameters.Add("@InsurerName", DbType.String).Value = InsurerName;

                adp = new SqlDataAdapter(cmd);
                adp.Fill(dsDocID);
            }
            if (dsDocID != null && dsDocID.Tables.Count > 0 && dsDocID.Tables[0].Rows.Count > 0)
            {
                DocID = dsDocID.Tables[0].Rows[0]["DOC_ID"].ToString();//+ ".pdf";
            }
            else
            {
                DocID = "NotFound" + MPDOC_DESCRIPTION + "_" + MPPROOF_DESC;
            }
            DocID = DocID.Replace('/', '_');

            MPDOC_DESCRIPTION = MPDOC_DESCRIPTION.Replace('/', '_');
            MPPROOF_DESC = MPPROOF_DESC.Replace('/', '_');

            if (InsurerID == "20")  //baxa
            {
                DocID = applicationNumber.Trim() + "_" + MPDOC_DESCRIPTION.Trim() + "_" + MPPROOF_DESC.Trim() + ".pdf";
            }
            else if (InsurerID == "2")  //tata
            {
                DocID = applicationNumber + "_"
                                                 + ConfigurationManager.AppSettings["AgentCode"] + "_"
                                                 + sisid + "_"
                                                 + DocID
                                                 + "_FORM.pdf";
            }
            else if (InsurerID == "9")    //kotak
            {
                DocID = applicationNumber + "_" + MPDOC_DESCRIPTION.Replace(' ', '_') + "_" + MPPROOF_DESC.Replace(' ', '_') + ".pdf";
            }
            else
            {
                DocID = DocID + ".pdf";
            }

            return DocID;
        }

        private string GetDocID1(string MPDOC_DESCRIPTION, string MPPROOF_DESC)
        {
            string DocID = "NotFound" + MPDOC_DESCRIPTION + "_" + MPPROOF_DESC;
            DataSet dsDocID = new DataSet();
            //con = new SqlConnection(ConnectionClass.ProductDBsqlConnection());
            using (SqlConnection con = new SqlConnection(ConnectionClass.ProductDBsqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[Customer].[GetDocumentIDForTATA]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@MPDOC_DESCRIPTION", DbType.String).Value = MPDOC_DESCRIPTION;
                cmd.Parameters.Add("@MPPROOF_DESC", DbType.String).Value = MPPROOF_DESC;

                adp = new SqlDataAdapter(cmd);
                adp.Fill(dsDocID);
            }
            if (dsDocID != null && dsDocID.Tables.Count > 0 && dsDocID.Tables[0].Rows.Count > 0)
            {
                DocID = dsDocID.Tables[0].Rows[0]["DOC_ID"].ToString();
            }
            return DocID;
        }



    }
}
