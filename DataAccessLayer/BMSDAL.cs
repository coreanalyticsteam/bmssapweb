﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using PropertyLayers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccessLayer
{
    public class BMSDAL
    {
        Hashtable htData = null;
        DataSet _ds;
        DataTable _dt;
        SqlDataAdapter _adp;
        SqlConnection con;
        SqlCommand cmd;
        public ResultClass CommunicationHistory(Int64 BookingId)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[MTX].[CallHistoryReport]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BookingId", DbType.Int64).Value = BookingId;
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    con.Close();
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

        public ResultClass CommunicationHistoryNew(Int64 BookingId)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.ReplPBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[BMS].[GetCommHistoryByLeadID]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@LeadID", DbType.Int64).Value = BookingId;
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    con.Close();
                    result.Success = Newtonsoft.Json.JsonConvert.SerializeObject(_ds.Tables[0]);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

        public ResultClass SaveCommonIVRResponse(long LeadId, string Response, string AppIDSource)
        {
            ResultClass result = new ResultClass();

            try
            {
                result.IsAuth = true;
                //if (AppId == "HCR")
                //{
                //    if
                //}
                //else if (AppId == "")
                //{

                //}
                _ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
                {
                    //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                    con.Open();
                    cmd = new SqlCommand("[MTX].[SaveIVRCommonResponse]", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@LeadId", DbType.Int64).Value = LeadId;
                    cmd.Parameters.Add("@Response", DbType.String).Value = Response;
                    cmd.Parameters.Add("@AppIDSource", DbType.String).Value = AppIDSource;
                    _adp = new SqlDataAdapter(cmd);
                    _adp.Fill(_ds);
                    con.Close();
                    if (_ds.Tables[0].Rows.Count > 0)
                    {
                        result.Success = _ds.Tables[0].Rows[0][0].ToString();
                    }
                    else { result.Success = "0"; }
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
            }
            return result;
        }

        public  DataSet GetTicketDetailByLeadId(long LeadId)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
            {
                //con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                con.Open();
                cmd = new SqlCommand("[MTX].[LeadDetailsByLeadId]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LeadId", DbType.Int64).Value = LeadId;
                _adp = new SqlDataAdapter(cmd);
                _adp.Fill(ds);
                con.Close();
                return ds;
            }
            //var sqlparamNew = new Dictionary<object, object>
            //        {
            //            {"LeadId", LeadId}      
            //        };
            //DataSet ds = new DataSet();
            //ds = SqlHelper.SqlHelper.ExecuteDataset("PBConnectionString", CommandType.StoredProcedure, "[MTX].[LeadDetailsByLeadId]", sqlparamNew);
            //return ds;
        }

        public Byte[] PDFRawReportBMS(DateTime FromDate, DateTime ToDate, int ProductID)
        {

            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = ConnectionClass.ReplPBCromasqlConnection();
                    string ProcName = "";
                    if (ProductID == 117 || ProductID == 107 || ProductID == 114)
                    {
                        ProcName = "[BMS].[AllBookingsDataReport_Motor]";
                    }
                    else
                    {
                        ProcName = "[BMS].[AllBookingsDataReport]";
                    }
                    SqlCommand sqlCommand = new SqlCommand
                    {
                        Connection = connection,
                        CommandType = CommandType.StoredProcedure,
                        CommandText = ProcName
                    };
                    sqlCommand.Parameters.AddWithValue("@FromDate", DbType.DateTime).Value = FromDate;
                    sqlCommand.Parameters.AddWithValue("@ToDate", DbType.DateTime).Value = ToDate;
                    sqlCommand.Parameters.AddWithValue("@ProductID", DbType.Int16).Value = ProductID;
                    connection.Open();
                    _adp = new SqlDataAdapter(sqlCommand);
                    _dt = new DataTable();
                    _adp.Fill(_dt);
                }

                //ClosedXML.Excel.XLWorkbook wbook = new ClosedXML.Excel.XLWorkbook();
                //wbook.Worksheets.Add(_dt, "tab1");
                MemoryStream fs = new MemoryStream();
                if (_dt.Rows.Count > 0)
                {

                    //XLWorkbook wb = new XLWorkbook(XLEventTracking.Disabled);
                    ////if(_dt.Rows.Count > 30000)
                    ////    wb.Worksheets.Add();
                    //if (_dt.Rows.Count > 48000)
                    //{
                    //    int Sheet = 0;
                    //    int RowCount = 0;
                    //    DataTable dt = new DataTable();
                    //    do
                    //    {
                    //        dt = null;
                    //        dt = _dt.Rows.Cast<DataRow>().Where(r => r.Field<Int64>("SNo") > RowCount).OrderBy(x => x["SNo"]).Take(30000).CopyToDataTable();
                   
                    //        Sheet += 1;
                    //        wb.Worksheets.Add(dt, "WorksheetNo_" + Sheet.ToString());
                    //        RowCount += 30000;
                    //    } while (dt.Rows.Count != 0);
                    //}
                    //else
                    //{
                    //wb.Worksheets.Add(_dt, "WorksheetName");
                    //}

                    //wb.SaveAs(fs);
                    //using (var wb = new XLWorkbook(fs))
                    //{
                        XLWorkbook wb = new XLWorkbook();
                        wb.AddWorksheet("DATA");
                        using (var ws = wb.Worksheet("DATA"))
                        {
                            var tbl = ws.Table("tblDATA");

                      
                            bool firstRow = false;

                            foreach (IXLRangeRow row in tbl.Rows())
                            {
                                
                                    _dt.Rows.Add();
                                    int i = 0;

                                    foreach (IXLCell cell in row.Cells())
                                    {
                                        _dt.Rows[_dt.Rows.Count - 1][i] = cell.Value.ToString();
                                        i++;
                                    }
                                
                            }

                            //insert data several times - factor 5000 --> 50.000 rows
                            //var dt2 = dt.Copy();
                            //int factor = 5000;
                            //for (int i = 1; i < factor; i++)
                            //{
                            //    dt.Merge(dt2);
                            //}


                            tbl.InsertRowsBelow(_dt.Rows.Count);


                            tbl.DataRange.FirstCell().InsertData(_dt.AsEnumerable().Skip(1));

                            //tbl.Column(1).Style.NumberFormat.Format = "dd/mm/yyyy hh:mm";
                            //tbl.Column(2).Style.NumberFormat.NumberFormatId = 14;

                            ////hh:mm:ss
                            //tbl.Column(3).Style.NumberFormat.NumberFormatId = 21;
                            tbl.Theme = XLTableTheme.TableStyleMedium2;

                            tbl.AutoFilter.Column(4).AddFilter("sda");


                            ws.Columns().AdjustToContents(1, 20);
                        }

                        wb.SaveAs(fs);
                   // }
                    fs.Position = 0;


                }
                return fs.ToArray();
            }
            catch (Exception)
            {
                throw;
            }
        } 


    }
}
