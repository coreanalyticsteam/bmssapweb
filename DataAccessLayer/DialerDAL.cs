﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using PropertyLayers;
using System.Xml.Linq;
using System.Data.Common;
using System.IO;

namespace DataAccessLayer
{
    public  class DialerDAL
    {
        DataSet ds;
        DataTable dt;
        SqlDataAdapter adp;
        SqlConnection con;
        SqlCommand cmd;

        public List<ProductDetails> GetProductList(int AgentType)
        {
            ProductDetails objProductDetails;
            List<ProductDetails> objProductDetailsList = new List<ProductDetails>();  
            ds = new DataSet();
            try
            {
                con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                con.Open();
                cmd = new SqlCommand("[BMS].[CTGetMasterDetails]", con); 
                cmd.Parameters.Add("@AgentType", SqlDbType.Int).Value = AgentType;               
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 1;
                cmd.CommandType = CommandType.StoredProcedure;
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objProductDetails = new ProductDetails();
                        objProductDetails.ProductID = Convert.ToInt16(dr["ProductID"]);
                        objProductDetails.ProductName = Convert.ToString(dr["ProductName"]);
                        objProductDetailsList.Add(objProductDetails);
                    }
                }
            }
            catch
            {

            }
            return objProductDetailsList;
        }
        public List<GroupDetails> GetGroupList(int AgentType, int ProductID)
        {
            GroupDetails objGroupDetails;
            List<GroupDetails> objGroupDetailsList = new List<GroupDetails>();
            ds = new DataSet();
            try
            {
                con = new SqlConnection(ConnectionClass.PBCromasqlConnection());
                con.Open();
                cmd = new SqlCommand("[BMS].[CTGetMasterDetails]", con);
                cmd.Parameters.Add("@AgentType", SqlDbType.Int).Value = AgentType;
                cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = ProductID;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = 2;
                cmd.CommandType = CommandType.StoredProcedure;
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objGroupDetails = new GroupDetails();
                        objGroupDetails.ProductID = ProductID;
                        objGroupDetails.GroupID = Convert.ToInt16(dr["UserGroupID"]);
                        objGroupDetails.GroupName = Convert.ToString(dr["UserGroupName"]);
                        objGroupDetailsList.Add(objGroupDetails);
                    }
                }
            }
            catch
            {

            }
            return objGroupDetailsList;
        }
        public List<LeadAgentList> GetDialerAgentList(DialerSearchRequest objDialerSearchRequest)
        {
            LeadAgentList objLeadAgentList;
            List<LeadAgentList> objLeadAgentListList = new List<LeadAgentList>();

            ds = new DataSet();
            try
            {
                //cmd = new SqlCommand();
                con = new SqlConnection(ConnectionClass.PBCromasqlConnection());

                con.Open();
                cmd = new SqlCommand("[BMS].[CTGetAgentList]", con);
                cmd.Parameters.Add("@LeadID", SqlDbType.BigInt).Value = objDialerSearchRequest.LeadID;
                cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = objDialerSearchRequest.ProductID;
                cmd.Parameters.Add("@AgentType", SqlDbType.Int).Value = objDialerSearchRequest.AgentType;
                cmd.Parameters.Add("@GroupID", SqlDbType.Int).Value = objDialerSearchRequest.GroupID;
                cmd.Parameters.Add("@AgentNameEmpID", SqlDbType.VarChar).Value = objDialerSearchRequest.AgentNameEmpID;
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = objDialerSearchRequest.Type;
                cmd.CommandType = CommandType.StoredProcedure;
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        objLeadAgentList = new LeadAgentList();
                        if (objDialerSearchRequest.Type == 1)
                        {
                            objLeadAgentList.LeadID = Convert.ToInt64(dr["LeadID"]);
                            objLeadAgentList.LeadStatusID = Convert.ToInt16(dr["StatusID"]);
                            objLeadAgentList.LeadSubStatusID = Convert.ToInt16(dr["SubStatusID"]);
                            objLeadAgentList.LeadStatus = Convert.ToString(dr["LeadStatus"]);
                            objLeadAgentList.LeadProcessType = Convert.ToString(dr["LeadType"]);
                            objLeadAgentList.ProductName = Convert.ToString(dr["ProductName"]);
                            objLeadAgentList.ProductID = Convert.ToInt16(dr["ProductID"]);
                        }
                      
                        objLeadAgentList.AgentID = Convert.ToInt16(dr["UserID"]);
                        objLeadAgentList.AgentEmpID = Convert.ToString(dr["EmployeeId"]);
                        objLeadAgentList.AgentName = Convert.ToString(dr["UserName"]);                       
                        objLeadAgentList.AgentGroupName = Convert.ToString(dr["UserGroupName"]);                         
                        objLeadAgentListList.Add(objLeadAgentList);
                    }
                } 
            }
            catch
            {

            }     
            return objLeadAgentListList;
        }

        public string InsertCTCLog(LogReqDetails objLogReqDetails)
        {
            try
            {
                con = new SqlConnection(ConnectionClass.LoggingsqlConnection());

                con.Open();
                cmd = new SqlCommand("[BMS].[CTCInsertLogDetails]", con);
                cmd.Parameters.Add("@LeadID", SqlDbType.BigInt).Value = objLogReqDetails.LeadID;
                cmd.Parameters.Add("@FromAgentID", SqlDbType.VarChar).Value = objLogReqDetails.FromAgentID;
                cmd.Parameters.Add("@ToAgentID", SqlDbType.VarChar).Value = objLogReqDetails.ToAgentID;
                cmd.Parameters.Add("@MobileNo", SqlDbType.VarChar).Value = objLogReqDetails.MobileNo;
                cmd.Parameters.Add("@EventType", SqlDbType.Int).Value = objLogReqDetails.EventType;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            catch { }
            return "Success";
        }
        public string Loggin(string Request)
        {
            string path = @"D:\CTCLOG\Log.txt";
            Request = System.DateTime.Now.ToString() +"==>"+ Request;
            if (!System.IO.File.Exists(path))
            {
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {

                    sw.WriteLine(Request);
                       
                  
                }
            }
            else
            {
                StreamWriter sw = System.IO.File.AppendText(path);

                sw.WriteLine(Request);
                sw.Close();
            }
            return "0";
        }


    }
}
