﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLog
{
    public class Log
    {
        public static void ErrorNInfoLogMethod(String sMsg, String FileName)
        {
            if (ConfigurationSettings.AppSettings["SaveLogs"] != null && Convert.ToString(ConfigurationSettings.AppSettings["SaveLogs"]) == "true")
            {
                String FilePath = Convert.ToString(ConfigurationSettings.AppSettings["ErrorNInfoLogFilePath"]) + DateTime.Now.ToString("ddMMMyyyy") + @"\";
                if (!String.IsNullOrEmpty(FilePath.Trim()))
                {
                    if (!Directory.Exists(FilePath))
                        Directory.CreateDirectory(FilePath);
                    System.IO.File.AppendAllText(FilePath + FileName, sMsg + Environment.NewLine);
                }
            }
        }
        public static void ErrorNInfoLogMethod(String sMsg, String FileName, String FolderName)
        {
            if (ConfigurationSettings.AppSettings["SaveLogs"] != null && Convert.ToString(ConfigurationSettings.AppSettings["SaveLogs"]) == "true")
            {
                String FilePath = Convert.ToString(ConfigurationSettings.AppSettings["ErrorNInfoLogFilePath"]) + DateTime.Now.ToString("ddMMMyyyy") + FolderName + @"\";
                if (!String.IsNullOrEmpty(FilePath.Trim()))
                {
                    if (!Directory.Exists(FilePath))
                        Directory.CreateDirectory(FilePath);
                    System.IO.File.AppendAllText(FilePath + FileName, sMsg + Environment.NewLine);
                }
            }
        }
    }
}
