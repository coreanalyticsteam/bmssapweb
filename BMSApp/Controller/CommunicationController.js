﻿myApp.controller('CommunicationController', function ($scope, AjaxFactory, $stateParams) {
    $scope.LoadingImg = false;
    //debugger;
    $scope.BookingId = $stateParams.BookingId
    ///$scope.RequestURL = APIURL + 'ApplicationNo=' + $stateParams.Id + '&SupplierId=5&ProductId=7&LeadId=' + $stateParams.LeadID + '&Hit=1';
    $scope.RequestURL = APIURL + 'CommunicationHistory';
    AjaxFactory.postData($scope.RequestURL, { BookingId: $scope.BookingId }, function (data) {
        $scope.CommunicationHistory = JSON.parse(data.data.result.Success);
       // console.log($scope.Insurer.ApplicationNoInfo['Application History'])
    });
});