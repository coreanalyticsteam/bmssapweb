﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.IO;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace JOB
{
    public class ApplicationList
    {
        public string ApplicationNo { get; set; }
        public string LeadID { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ReMergeAllFile();
          // GetAllBookingAndApp( ReMergeAllFile());
           
        }
        public static string GetDate(DateTime dt)
        {
            string date = "";
            //DateTime dt = new System.DateTime();
            //dt = System.DateTime.Now;
            if (dt.Day.ToString().Length > 1)
            {
                date = dt.Day.ToString();
            }
            else
            {
                date = "0" + dt.Day.ToString();

            }
            if (dt.Month.ToString().Length > 1)
            {
                date = date + dt.Month.ToString();
            }
            else
            {
                date = date + "0" + dt.Month.ToString();

            }
            date = date + dt.Year.ToString();
            return date;
        }

        static void GetAllBookingAndApp(List<ApplicationList> ApplicationListList)
        {
            DataSet ds;
            DataTable dt;
            SqlDataAdapter adp;        
            SqlCommand cmd;   
            var xEle = new XElement("Application",
                                        from objDocReq in ApplicationListList
                                        select new XElement("Application",
                                                     new XAttribute("ApplicationNo", Convert.ToString(objDocReq.ApplicationNo))                                                   
                                                   )).ToString();

            using (SqlConnection con = new SqlConnection(ConnectionClass.PBCromasqlConnection()))
            {
                con.Open();
                cmd = new SqlCommand("[BMS].[GetCurruptedDocIDs]", con);
                cmd.CommandType = CommandType.StoredProcedure;               
                cmd.Parameters.Add("@XMLdata", DbType.Xml).Value = xEle;              
                ds = new DataSet();
                adp = new SqlDataAdapter(cmd);
                adp.Fill(ds);
               
            }
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DocVerify d = new DocVerify();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    d.GetPendingDocuments(dr["LeadID"].ToString(), dr["ApplicationNo"].ToString());
                }
            }
        }
        static List<ApplicationList> ReMergeAllFile()
        {
            List<ApplicationList> ApplicationListList = new List<ApplicationList>();
            //ApplicationList objApplicationList;
            string FilePAth = "";
             DateTime SDate = new DateTime(2017, 4, 28);
            DateTime CurDate=System.DateTime.Now;
            int count = Convert.ToInt16((CurDate - SDate).TotalDays);
            for (int i = 1; i <= count; i++)
            {
                //FilePAth = @"E:\pbDocuments\Doc\" + GetDate(SDate) + @"\MergeDoc";
                FilePAth = ConfigurationSettings.AppSettings["ReadDoc"] + GetDate(SDate) + @"\MergeDoc"; 
                if (Directory.Exists(FilePAth))
                {
                    Console.WriteLine("Exists Path:" + FilePAth);
                    ApplicationListList.AddRange(getAllApplicationNo(FilePAth));
                }
                else
                {
                    Console.WriteLine("Not Exists Path:" + FilePAth);
                }
                SDate = SDate.AddDays(1);
            }
            Console.Read();
            return ApplicationListList;
           
            //BAXA00002038_Address Proof_BANK ACCOUNT ST
        }
        static List<ApplicationList> getAllApplicationNo(string FilePath)
        {
            List<ApplicationList> ApplicationListList = new List<ApplicationList>();
            ApplicationList objApplicationList;
            string[] a = Directory.GetFiles(FilePath, "*.*");
            long b = 0;
            foreach (string name in a)
            {
                
                FileInfo info = new FileInfo(name);
                if (info.Length <= 15)
                {
                    objApplicationList = new ApplicationList();
                    objApplicationList.ApplicationNo = name.Substring(37, 12);
                    ApplicationListList.Add(objApplicationList);
                    Console.WriteLine("File:" + name);
                    Console.WriteLine("Size:" + info.Length);
                    //common.ErrorNInfoLogMethod("Application Number : " + objApplicationList.ApplicationNo, "ApplicationNo.txt");
                }
               
            }
            return ApplicationListList;
        }
    }
}
