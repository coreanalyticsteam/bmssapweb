﻿
MotorInspection.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
});
MotorInspection.factory('Excel', function ($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return {
        tableToExcel: function (tableId, worksheetName) {
            var table = $(tableId),
                ctx = { worksheet: worksheetName, table: table.html() },
                href = uri + base64(format(template, ctx));
            return href;
        }
    };
})
MotorInspection.service("VideoInspectionService", function ($http, $window) {
    this.FileData = {};

    this.GetSupplierList = function (Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/GetSupplierList"
             , headers: {
                 "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
             }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });
    }

    this.GetInspectionBookingDetail = function (inspectionId, token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/InspectionBookingDetail/" + inspectionId,
            headers: {
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function(data) {
            console.log(data);
        }).error(function(data, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });
    }

    this.InsurerInspectionDetail = function (objInspectionDetails, token) {
        var request = $http({
            method: "POST",
            url: config.serviceURL + "VideoInspectionService.svc/InsurerInspectionDetail",
            data: JSON.stringify(objInspectionDetails),
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                 "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {
            console.log(data);
        })
            .error(function (error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }

    this.InsurerInspectionHistory = function (inspectionId, token) {
        var request = $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/InsurerInspectionHistory/" + inspectionId,

            headers: {
                //"Content-Type": "application/json; charset=utf-8",
                 "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {
            console.log(data);
        })
            .error(function (error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }


    this.PbInspectionHistory = function (inspectionId, token) {
        var request = $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/PbInspectionHistory/" + inspectionId,

            headers: {
                //"Content-Type": "application/json; charset=utf-8",
                 "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {
            console.log(data);
        })
            .error(function (error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }

    this.UpdateBookingDetails = function (objInspectionDetails, token) {
        debugger;
        var request = $http({
            method: "POST",
            url: config.serviceURL + "VideoInspectionService.svc/UpdateStatus",
            data: JSON.stringify(objInspectionDetails),
            headers: {
                "Content-Type": "application/json; charset=utf-8"
                , "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {
            console.log(data);
        })
            .error(function (error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }

    this.UpdatePbBookingDetails = function (objInspectionDetails,token) {
        debugger;
        var request = $http({
                method: "POST",
                url: config.serviceURL + "VideoInspectionService.svc/UpdatePbInspectionStatus",
                data: JSON.stringify(objInspectionDetails),
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                    "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
                }
            }).success(function(data) {
                console.log(data);
            })
            .error(function(error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }

    this.SaveInspectionStatus = function (objInspectionRollbackDetails, token) {
        debugger;
        var request = $http({
            method: "POST",
            url: config.serviceURL + "VideoInspectionService.svc/InspectionRollback",
            data: JSON.stringify(objInspectionRollbackDetails),
            headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {
            console.log(data);
        })
            .error(function (error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }

    this.GetStatusAndSubStatus = function (userType, token) {
        var request = $http({
                method: "GET",
                url: config.serviceURL + "VideoInspectionService.svc/GetStatusCollection/" + userType,
                headers: {
                    //"Content-Type": "application/json"
                    "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)

                }
            })
            .success(function(data) {
                console.log(data);
            })
            .error(function(error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }

    this.GetInsurerCollection = function (token) {
        debugger;
        var request = $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/GetInsurerCollection",
            headers: {
                //"Content-Type": "application/json"
                 "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)

            }
        })
            .success(function (data) {
                console.log(data);
            })
            .error(function (error, status) {
                if (status == 401) {
                    $window.location.href = '/index.html';
                }
            });
        return request;
    }


    this.GetMediaFileUrl = function (inspectionId, token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/MediaFileUrl/" + inspectionId,
            headers: {
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });
    }

    this.GetPBInspectionDetails = function (objInspectionDetails, token) {
        debugger;
        var request = $http({
            method: "POST",
            dataType: 'application/json',
            url: config.serviceURL + "VideoInspectionService.svc/InspectionBookingDetails",
            data: JSON.stringify(objInspectionDetails)
            , headers: {
                //"Content-Type": "application/json; charset=utf-8"
                "Content-Type": "application/json; charset=utf-8", "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        })
            .success(function (data) {
                console.log(data);
            })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }
    this.GetPBPDFInspectionDetails = function (objInspectionDetails, token) {
        debugger;
        var request = $http({
            method: "POST",
            dataType: 'application/json',
            url: config.serviceURL + "VideoInspectionService.svc/PDFInspectionBookingDetails",
            data: JSON.stringify(objInspectionDetails)
            , headers: {
                "Content-Type": "application/json;charset=utf-8", "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID),

            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/home.html#/MotorInspection/Login';
           }
       });
        return request;
    }
    this.GetPBInsurerUnderwritingDetails = function (LeadID, Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/GetPBInsurerUnderwritingDetails/" + LeadID,
            headers: {
                "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });
    }

    this.Login = function (LoginData) {
        debugger;
        $window.localStorage.removeItem('UserId');
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/Login",
            data: JSON.stringify(LoginData)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    //this.GetLeadsForInsurerDashboard = function (objReqMarkExSearch, Token) {
    //    debugger;
    //    var request = $http({
    //        method: "post",
    //        dataType: 'json',
    //        url: config.serviceURL + "VideoInspectionService.svc/GetLeadsForInsurerDashboard",
    //        data: JSON.stringify(objReqMarkExSearch)
    //        , headers: {
    //            "Content-Type": "application/json"
    //            , "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
    //        }
    //    })
    //   .error(function (error, status) {
    //       if (status == 401) {
    //           $window.location.href = '/index.html';
    //       }
    //   });
    //    return request;
    //}


    //this.GETPaymentDetailsForEx = function (base64EncodedLeadID) {
    //    debugger;
    //    return $http({
    //        method: "GET",
    //        url: config.serviceURL + "CCTechService.svc/GETPaymentDetailsForEx/" + base64EncodedLeadID
    //    }).success(function (data) {

    //        console.log(data);
    //    }).error(function (data) {

    //    });
    //}

    this.ExLoginGetLeadData = function (LeadID) {
        debugger;
        var request = $http({
            method: "GET",
            dataType: 'json',
            url: config.serviceURL + "VideoInspectionService.svc/ExLoginGetLeadData/" + LeadID
            , headers: {
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }
    this.AddNewUsers = function (objAddNewUser, token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "VideoInspectionService.svc/AddNewUser",
            data: JSON.stringify(objAddNewUser)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
                //"auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/home.html#/MotorInspection/Login';
           }
       });
        return request;
    }

    this.GetSupplierListByProduct = function (ProductID, token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/MasterSupplier/" + ProductID
            , headers: {
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
                //"auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/home.html#/MotorInspection/Login';
            }
        });


    }

    this.GetHealthExUser = function (token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "VideoInspectionService.svc/GetHealthExUser"
            , headers: {
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
                //"auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/home.html#/MotorInspection/Login';
            }
        });


    }

    this.UpdateUserInfo = function (objUpdateUserInfo, token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "VideoInspectionService.svc/UpdateUserInfo",
            data: JSON.stringify(objUpdateUserInfo)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
                //"auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/home.html#/MotorInspection/Login';
           }
       });
        return request;
    }


    this.ChangePassword = function (objChangePassword, token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "VideoInspectionService.svc/ChangePassword",
            data: JSON.stringify(objChangePassword)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
                //"auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/home.html#/MotorInspection/Login';
           }
       });
        return request;
    }
    this.DownloadExcel = function (token) {
        debugger;
        var request = $http({
            method: "get",
            url: config.serviceURL + "VideoInspectionService.svc/Download2"
            , headers: {
               // "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID),
                //"content_type" : 'text/plain',
                //"Content-Disposition" : "attachment; filename=foo.txt"
                //"auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/home.html#/MotorInspection/Login';
           }
       });
        return request;
    }
    this.DownloadExcel1 = function (token) {
        debugger;
        var request = $http({
            method: "get",
            url: config.serviceURL + "VideoInspectionService.svc/Download1"
            , headers: {
                "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID),
               
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/home.html#/MotorInspection/Login';
           }
       });
        return request;
    }

  
});

