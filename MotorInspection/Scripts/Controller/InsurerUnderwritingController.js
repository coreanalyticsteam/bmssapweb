﻿MotorInspection.controller("InsurerUnderwritingCtrl",
    function ($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal, $window, AjaxCall) {
        debugger;
        $scope.AllStatusCollection = [];
        //$scope.IsVisible = false;
        $scope.urlButtonTxt = "View URL";
        $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));
        $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
        $scope.LabelInspectionID = $scope.BookingDetails.InspectionId;

        $scope.IsStatusDrpDwnEnable = false;
        $scope.IsSubStatusDrpDwnEnable = false;

        $scope.GetInsurerInspectionDetail = function () {
            debugger;
            var objInspectionDetails =
               {
                   "InspectionId": $scope.BookingDetails.InspectionId,
                   "VehicleNumber": $scope.BookingDetails.VehicleNumber,
                   "InsurerId": $scope.UserDetails.InsurerID,
                   "FromDate" : $scope.BookingDetails.FromDate,
                   "ToDate": $scope.BookingDetails.ToDate
               };
            //VideoInspectionService.InsurerInspectionDetail(objInspectionDetails).success(function (data) {
            VideoInspectionService.GetInspectionBookingDetail($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function(data) {
                $scope.AllInsurerInspectionDetails = data;
                localStorage.setItem('InsurerInspectionDetails', JSON.stringify(data));
            });
        };
        $scope.GetInsurerInspectionDetail();

        $scope.GetStatusAndSubStatus = function () {
            debugger;
            VideoInspectionService.GetStatusAndSubStatus(1, $scope.UserDetails.Token).success(function(data) {
                $scope.AllStatusCollection = data;
            });
        };
        $scope.GetStatusAndSubStatus();

        $scope.GetMediaFileUrl = function () {
            debugger;
            //$scope.IsVisible = $scope.IsVisible ? false : true;
            //if ($scope.IsVisible) {
            //    $scope.urlButtonTxt = "Hide URL";
            //} else {
            //    $scope.urlButtonTxt = "View URL";
            //}
            VideoInspectionService.GetMediaFileUrl($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function (data) {
                $scope.FileUrl = data;
            });

            AjaxCall.GET('VideoInspectionService.svc/GetInspectionsDocument/' + $scope.BookingDetails.InspectionId, function (data) {
                $scope.InspectionDocumentsURL = data.data;
            });
        };

        $scope.GetMediaFileUrl();

        $scope.UpdateBookingDetails = function () {
            debugger;
            var objInspectionDetails =
               {
                   "StatusId": $scope.Selected.InsurerStatus.StatusId,
                   "SubStatusId": $scope.Selected.InsurerSubStatus.SubStatusId,
                   "Remarks": $scope.Remarks,
                   "InspectionId": $scope.BookingDetails.InspectionId,
                   "UserID": $scope.UserDetails.UserID
               };
            if ($scope.Remarks != '') {
                VideoInspectionService.
                    UpdateBookingDetails(objInspectionDetails, $scope.UserDetails.Token).success(function (data) {
                        debugger;
                        alert('Updated');
                        $scope.Remarks = '';
                        $scope.GetInsurerInspectionHistory();
                        var storage = JSON.parse($window.localStorage.getItem('BookingDetails'));
                        storage.StatusName = $scope.Selected.InsurerStatus.StatusName;
                        storage.StatusId = $scope.Selected.InsurerStatus.StatusId;
                        storage.SubStatusName = $scope.Selected.InsurerSubStatus.SubStatusName;
                        storage.SubStatusId = $scope.Selected.InsurerSubStatus.SubStatusId;
                        localStorage.setItem('BookingDetails', JSON.stringify(storage));
                        $scope.GetInsurerInspectionDetail();
                        $scope.CheckSubStatus();
                    });
            }
        };

        $scope.DownloadFile = function (data) {
            debugger;
            var win = window.open(data, '_blank');
            win.focus();

        };

        $scope.GetInsurerInspectionHistory = function () {
            debugger;
            VideoInspectionService.InsurerInspectionHistory($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function(data) {
                $scope.AllInsurerInspectionHistory = data;
            });
        };
        $scope.GetInsurerInspectionHistory();

        $scope.Selected = {
            "InsurerStatus": { "StatusId": 0, "StatusName": "Select One" },
            "InsurerSubStatus": { "StatusId": 0, "StatusName": "Select One", "SubStatusId": 0, "SubStatusName": "Select One" }
        }
        var insurerStatus = {
            "StatusId": $scope.BookingDetails.StatusId,
            "StatusName": $scope.BookingDetails.StatusName,
            "SubStatusId": 0,
            "SubStatusName": null
        };
        var insurerSubStatus = {
            "StatusId": 0,
            "StatusName": null,
            "SubStatusId": $scope.BookingDetails.SubStatusId,
            "SubStatusName": $scope.BookingDetails.SubStatusName
        };

        $scope.Selected.InsurerStatus = insurerStatus;
        $scope.Selected.InsurerSubStatus = insurerSubStatus;

        $scope.CheckSubStatus = function() {
            debugger;
            var storage = JSON.parse($window.localStorage.getItem('BookingDetails'));
            var statusIdArray = ["2", "4", "6"];
            if (statusIdArray.indexOf(String($scope.Selected.InsurerStatus.StatusId)) == -1) {
                $scope.IsSubStatusVisible = false;
            } else {
                $scope.IsSubStatusVisible = true;
            }
            var statusId = ["3", "4", "8", "9", "14"];
            if ($.inArray(String(storage.StatusId), statusId) != -1) {
                $scope.IsStatusDrpDwnEnable = true;
                $scope.IsSubStatusDrpDwnEnable = true;
            } else {
                $scope.IsStatusDrpDwnEnable = false;
                $scope.IsSubStatusDrpDwnEnable = false;
            }
        };
        $scope.CheckSubStatus();
    });