﻿MotorInspection.controller("InspectionUnderwritingCtrl",
    function ($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal, $window, AjaxCall) {
        $scope.AllStatusCollection = [];
        $scope.InspectionFileData = [];
        //$scope.IsVisible = false;
        $scope.IsInsurerVisible = false;
        $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));
        $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
        $scope.urlButtonTxt = "View URL";
        $scope.LabelInspectionID = $scope.BookingDetails.InspectionId;
        $scope.AutoInspectionURL = labURL+ $scope.BookingDetails.InspectionId + "/";
        $scope.viewmf = false;
        $scope.showvehicleno = true;
        $scope.showedit = false;
        $scope.IsStatusDrpDwnEnable = false;
        $scope.IsSubStatusDrpDwnEnable = false;
        $scope.IsInsurerDrpDwnEnable = false ;

        $scope.GetPBInspectionDetail = function () {
            debugger;
            //var objInspectionDetails =
            //  {                
            //      //"CustomerId": $scope.BookingDetails.CustomerId,
            //      "InspectionId": $scope.BookingDetails.InspectionId,
            //      //"VehicleNumber": $scope.BookingDetails.VehicleNumber,
            //      //"StatusName": $scope.BookingDetails.StatusName
            //  };
            VideoInspectionService.GetInspectionBookingDetail($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function (data) {
                $scope.PBInspectionUnderWritingDetails = data;
                localStorage.setItem('PbInspectionDetails', JSON.stringify(data));
            });
           
        };
        $scope.GetPBInspectionDetail();

        $scope.GetUpdatedInspectionDetails = function () {
            debugger;
            var objInspectionDetails =
              {                
                  //"CustomerId": $scope.BookingDetails.CustomerId,
                  "InspectionId": $scope.BookingDetails.InspectionId
                  //"VehicleNumber": $scope.BookingDetails.VehicleNumber,
                  //"StatusName": $scope.BookingDetails.StatusName
              };
            VideoInspectionService.GetPBInspectionDetails(objInspectionDetails, $scope.UserDetails.Token).success(function (data) {
                $scope.AllPBInspectionDetails = data;
                //$scope.PBInspectionUnderWritingDetails = data;
                localStorage.setItem('PbInspectionDetails', JSON.stringify(data));
            });
        };

        debugger;
        $scope.Selected = {
            "InsurerStatus": { "StatusId": 0, "StatusName": "Select One" },
            "InsurerSubStatus": { "StatusId": 0, "StatusName": "Select One","SubStatusId": 0, "SubStatusName": "Select One" }
        }
        var insurerStatus = {
            "StatusId":$scope.BookingDetails.StatusId,
            "StatusName":$scope.BookingDetails.StatusName,
            "SubStatusId": 0,
            "SubStatusName":null
        };
        var insurerSubStatus = {
            "StatusId": 0,
            "StatusName": null,
            "SubStatusId": $scope.BookingDetails.SubStatusId,
            "SubStatusName": $scope.BookingDetails.SubStatusName
        };
        debugger;
        $scope.Selected.InsurerStatus = insurerStatus;
        $scope.Selected.InsurerSubStatus = insurerSubStatus;
        if ($scope.Selected.InsurerStatus != null) {
            if ($scope.Selected.InsurerStatus.StatusId != 13) {
                for (var i in $scope.AllStatusCollection) {
                    if ($scope.AllStatusCollection[i].StatusId == 13) {
                        $scope.AllStatusCollection.splice(i, 1);
                    }
                }
            }
        }
        $scope.GetStatusAndSubStatus = function () {
            debugger;
            var x = $scope.BookingDetails.StatusId;
            if (x != 9 && x != 3) {
                VideoInspectionService.GetStatusAndSubStatus(0, $scope.UserDetails.Token).success(function (data) {
                    debugger;
                    console.log(data);
                    $scope.AllStatusCollection = data;
                    if ($scope.Selected.InsurerStatus != null) {
                        if ($scope.Selected.InsurerStatus.StatusId != 13) {
                            for (var i in $scope.AllStatusCollection) {
                                if ($scope.AllStatusCollection[i].StatusId == 13) {
                                    $scope.AllStatusCollection.splice(i, 1);
                                }
                            }
                        }
                    }
                });
            }
            debugger;
            if (x == 3) {
                var insurer = {
                    "StatusId": 12,
                    "StatusName": "Issued"
                    //"SubStatusId": 0,
                    //"SubStatusName": null
                };

                var allStatus = [{
                    "StatusId": 12,
                    "StatusName": "Issued"
                    //"SubStatusId": 0,
                    //"SubStatusName": null
                }];
                $scope.AllStatusCollection = allStatus;
                $scope.Selected.InsurerStatus = insurer;
            }
            if (x == 9) {
                var _insurer = [{
                    "StatusId": 11,
                    "StatusName": "Pending cleared"
                },
                {
                    "StatusId": 14,
                    "StatusName": "Case Rejected"
                }];
                $scope.AllStatusCollection = _insurer;
            }
        };

        $scope.GetStatusAndSubStatus();

        $scope.GetInsurerCollection = function () {
            VideoInspectionService.GetInsurerCollection($scope.UserDetails.Token).success(function(data) {
                $scope.AllInsurerCollection = data;
            });
        };
        $scope.ReplicateData = function () {
            debugger;
            AjaxCall.postData('VideoInspectionService.svc/ReplicateInspection', { InspectionID: $scope.LabelInspectionID }, function (data) {
                console.log(data.data.result.Success)
                if (data.data.result.IsSuccess) {
                    alert("Replicate successfully");
                    var storage = JSON.stringify({
                        InspectionId: data.data.result.Success,
                        VehicleNumber: $scope.BookingDetails.VehicleNumber,
                        CustomerId: $scope.BookingDetails.CustomerId,
                        StatusName: "New",
                        StatusId: 13,
                        SubStatusName: null,
                        SubStatusId: 0,
                        InsurerId: 0,
                        InsurerName: null
                    });
                    localStorage.setItem('BookingDetails', storage);
                    $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
                    $window.location.reload();
                }

            })
        }

        $scope.UpdateVehicleNo = function (vehicleno) {
            debugger;           
            AjaxCall.postData('VideoInspectionService.svc/UpdateVehicleNo', { VehicleNumber: vehicleno, InspectionID: $scope.LabelInspectionID }, function (data) {
                console.log(data.data.result.Success)
                if (data.data.result.IsSuccess) {
                    alert("Vehicle number updated successfully!!");
                    //var storage = JSON.stringify({
                    //    InspectionId: data.data.result.Success,
                    //    VehicleNumber: $scope.BookingDetails.VehicleNumber,
                    //    CustomerId: $scope.BookingDetails.CustomerId,
                    //    StatusName: "New",
                    //    StatusId: 13,
                    //    SubStatusName: null,
                    //    SubStatusId: 0,
                    //    InsurerId: 0,
                    //    InsurerName: null
                    //});
                    //localStorage.setItem('BookingDetails', storage);
                    $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
                    $window.location.reload();
                }
                else {
                    alert("Vehicle number should not be blank and must be greater than 5 letters or it should not be same as previous or the vehicle number already exists in the system!!");
                    $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
                    $window.location.reload();
                }

            })
        }

        //$scope.EditVehicleNo = function () {
        //    debugger;
           
        //}

        $scope.GetMediaFileUrl = function () {
            debugger;
            //$scope.IsVisible = $scope.IsVisible ? false : true;
            //if ($scope.IsVisible) {
            //    $scope.urlButtonTxt = "Hide URL";
            //} else {
            //    $scope.urlButtonTxt = "View URL";
            //}
            VideoInspectionService.GetMediaFileUrl($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function(data) {
                $scope.FileUrl = data;
            });
            //$scope.GetInspectionsDocument = function () {
            AjaxCall.GET('VideoInspectionService.svc/GetInspectionsDocument/' + $scope.BookingDetails.InspectionId, function (data) {
                    $scope.InspectionDocumentsURL = data.data;
                });
             
           // };

        };
        $scope.GetMediaFileUrl();
        
        $scope.UpdatePbBookingDetails = function () {
            debugger;
            var insurerId = 0, subStatusId = 0;
            if (($scope.Selected.InsurerStatus.StatusId == 1 || $scope.Selected.InsurerStatus.StatusId == 16) && $scope.Selected.Insurer == undefined) {
                alert("Please select insurer !!!");
                return;
            }
            else if (($scope.Selected.InsurerStatus.StatusId == 1 || $scope.Selected.InsurerStatus.StatusId == 16) && $scope.Selected.Insurer != undefined) {
                insurerId = $scope.Selected.Insurer.InsurerId;
            }
            if ($scope.Remarks == '' || $scope.Remarks == undefined || $scope.Remarks == 'undefined' || $scope.Remarks == "" || $scope.Remarks == "undefined") {
                alert("Please Enter Remarks!!");
                return;
            }
                
            if ($scope.Selected.InsurerSubStatus != null) {
                subStatusId = $scope.Selected.InsurerSubStatus.SubStatusId;
            }

            var objInspectionDetails =
               {
                   "StatusId": $scope.Selected.InsurerStatus.StatusId,
                   "SubStatusId": subStatusId,
                   "Remarks": $scope.Remarks,
                   "InspectionId": $scope.BookingDetails.InspectionId,
                   "InsurerId": insurerId,
                   "UserID": $scope.UserDetails.UserID,

               };
            if ($scope.Remarks != '' || $scope.Remarks != undefined || $scope.Remarks != 'undefined' || $scope.Remarks != "" || $scope.Remarks != "undefined") {
                debugger;
                VideoInspectionService.UpdatePbBookingDetails(objInspectionDetails, $scope.UserDetails.Token).success(function (data) {
                    alert('Updated');
                    $scope.Remarks = '';
                    $scope.GetPbInspectionHistory();
                    var storage = JSON.parse($window.localStorage.getItem('BookingDetails'));
                    storage.StatusName = $scope.Selected.InsurerStatus.StatusName;
                    storage.StatusId = $scope.Selected.InsurerStatus.StatusId;
                    if ($scope.Selected.InsurerSubStatus != null) {
                        storage.SubStatusName = $scope.Selected.InsurerSubStatus.SubStatusName;
                        storage.SubStatusId = $scope.Selected.InsurerSubStatus.SubStatusId;
                    }
                    localStorage.setItem('BookingDetails', JSON.stringify(storage));
                    $scope.GetUpdatedInspectionDetails();
                    $scope.GetPBInspectionDetail();
                    $scope.CheckSubStatus();
                    if ($scope.Selected.InsurerStatus != null) {
                        if ($scope.Selected.InsurerStatus.StatusId != 13) {
                            for (var i in $scope.AllStatusCollection) {
                                if ($scope.AllStatusCollection[i].StatusId == 13) {
                                    $scope.AllStatusCollection.splice(i, 1);
                                }
                            }
                        }
                    }
                });

                // $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
                //$window.location.reload();
            }
          
        };

        $scope.SaveInspectionStatus = function () {
            debugger;
            var insurerId1 = 0, subStatusId1 = 0;
            if ($scope.Selected.Insurer1 == undefined) {
                alert("Please select insurer !!!");
                return;
            }
            else if ($scope.Selected.Insurer1 != undefined) {
                insurerId = $scope.Selected.Insurer1.InsurerId;
            }
            if ($scope.Selected.InsurerSubStatus1 != undefined && ($scope.Selected.InsurerStatus1.StatusId == 2 || $scope.Selected.InsurerStatus1.StatusId == 4 || $scope.Selected.InsurerStatus1.StatusId == 6)) {
                subStatusId = $scope.Selected.InsurerSubStatus1.SubStatusId;
            }
            else if ($scope.Selected.InsurerSubStatus1 == undefined && ($scope.Selected.InsurerStatus1.StatusId == 2 || $scope.Selected.InsurerStatus1.StatusId == 4 || $scope.Selected.InsurerStatus1.StatusId == 6)) {
                alert("Please select a substatus !!!");
                return;
            }
            else if ($scope.Selected.InsurerSubStatus1 == undefined && ($scope.Selected.InsurerStatus1.StatusId != 2 || $scope.Selected.InsurerStatus1.StatusId != 4 || $scope.Selected.InsurerStatus1.StatusId != 6)) {
                subStatusId=0;
                
            }

            var objInspectionRollbackDetails =
               {
                   "StatusId": $scope.Selected.InsurerStatus1.StatusId,
                   "SubStatusId": subStatusId,
                  // "Remarks": $scope.Remarks,
                   "InspectionId": $scope.BookingDetails.InspectionId,
                   "InsurerId":  $scope.Selected.Insurer1.InsurerId,
                   "UserID": $scope.UserDetails.UserID,

               };
            debugger;
            //if ($scope.Remarks != '') {
            VideoInspectionService.SaveInspectionStatus({ "objRollbackDetails": objInspectionRollbackDetails }, $scope.UserDetails.Token).success(function (data) {
                alert('Updated');
                //$scope.Remarks = '';
                debugger;
                $scope.GetPbInspectionHistory();
                var storage = JSON.parse($window.localStorage.getItem('BookingDetails'));
                storage.StatusName1 = $scope.Selected.InsurerStatus1.StatusName;
                storage.StatusId1 = $scope.Selected.InsurerStatus1.StatusId;
                if ($scope.Selected.InsurerSubStatus1 != null) {
                    storage.SubStatusName1 = $scope.Selected.InsurerSubStatus1.SubStatusName;
                    storage.SubStatusId1 = $scope.Selected.InsurerSubStatus1.SubStatusId;
                }
                   

                var storage = JSON.parse($window.localStorage.getItem('BookingDetails'));
                storage.StatusName = $scope.Selected.InsurerStatus1.StatusName;
                storage.StatusId = $scope.Selected.InsurerStatus1.StatusId;
                if ($scope.Selected.InsurerSubStatus1 != null) {
                    storage.SubStatusName = $scope.Selected.InsurerSubStatus1.SubStatusName;
                    storage.SubStatusId = $scope.Selected.InsurerSubStatus1.SubStatusId;
                }
                if ($scope.Selected.Insurer1 != null) {
                    storage.InsurerId = $scope.Selected.Insurer1.InsurerId;
                    storage.InsurerName = $scope.Selected.Insurer1.InsurerName;
                }
                    localStorage.setItem('BookingDetails', JSON.stringify(storage));
                    $scope.GetUpdatedInspectionDetails();
                    $scope.GetPBInspectionDetail();
                    $scope.GetInsurers();
                    $scope.CheckInsurer();
                    //$scope.Selected.InsurerStatus=$scope.Selected.InsurerStatus1.StatusId
                    //$scope.Selected.InsurerSubStatus= subStatusId

                    //if ($scope.Selected.InsurerStatus1.StatusId == 1) {

                    //    var insurer = {
                    //        "InsurerId": $scope.Selected.Insurer1.InsurerId,
                    //        "InsurerName": $scope.Selected.Insurer1.InsurerName
                    //    };
                    //    $scope.Selected.Insurer = insurer;
                    //}
                  
                    if ($scope.Selected.InsurerStatus1 != null) {
                        if ($scope.Selected.InsurerStatus1.StatusId != 13) {
                            for (var i in $scope.AllStatusCollection) {
                                if ($scope.AllStatusCollection[i].StatusId == 13) {
                                    $scope.AllStatusCollection.splice(i, 1);
                                }
                            }
                        }
                    }

                    $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
                    $window.location.reload();
                });
           // }
        };

        $scope.DownloadFile = function (data) {
            var win = window.open(data, '_blank');
            win.focus();

        };

        $scope.DeleteFile = function (data) {
            debugger;
            var confirmData = confirm("Do you want to delete the document?");
            if (confirmData == true) {
                AjaxCall.postData('VideoInspectionService.svc/DeleteInspectionDocument',({ "DocURL": data, "InspectionId": $scope.BookingDetails.InspectionId }), function (data) {
                    //$scope.InspectionDocumentsURL = data.data;
                    alert("Document Deleted Sucessfully!!");
                    $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
                    $window.location.reload();
                });
            }
            //var win = window.open(data, '_blank');
            //win.focus();

        };

        $scope.GetPbInspectionHistory = function () {
            debugger;
            VideoInspectionService.PbInspectionHistory($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function (data) {
                $scope.AllInsurerInspectionHistory = data;
            });
        };
        $scope.GetPbInspectionHistory();

        if ($scope.Selected.InsurerStatus.StatusId == 1 || $scope.Selected.InsurerStatus.StatusId == 16) {
            $scope.GetInsurerCollection();
            $scope.IsInsurerVisible = true;
        }

        $scope.GetInsurers = function () {
            debugger;
            $scope.CheckSubStatus();
            if ($scope.Selected.InsurerStatus.StatusId == 1 || $scope.Selected.InsurerStatus.StatusId == 16) {
                $scope.GetInsurerCollection();
                $scope.IsInsurerVisible = true;
                //$scope.Selected = {
                //    "Insurer": { "InsurerId": $scope.AllInsurerCollection[0].InsurerId, "InsurerName": $scope.AllInsurerCollection[0].InsurerName }
                //}
                //$scope.Selected.Insurer = $scope.Selected.Insurer;
            } else {
                $scope.IsInsurerVisible = false;
            }
            var statusIdArray = ["2", "4", "5", "6"];
            if (statusIdArray.indexOf(String($scope.Selected.InsurerStatus1.StatusId)) == -1)
            {
                $scope.InsurerStatus1Visible = false;
                $scope.GetInsurerCollection();
            }
            else {
                $scope.InsurerStatus1Visible = true;
                $scope.GetInsurerCollection();
            }
        };

        $scope.CheckInsurer = function () {
            debugger;
            if ($scope.Selected.InsurerStatus.StatusId == 1 || $scope.Selected.InsurerStatus.StatusId == 16) {

                var insurer = {
                    "InsurerId": $scope.BookingDetails.InsurerId,
                    "InsurerName": $scope.BookingDetails.InsurerName
                };
                $scope.Selected.Insurer = insurer;
            }
        };

        $scope.CheckInsurer();

        $scope.CheckSubStatus = function () {
            debugger;
            var statusIdArray = ["2", "4", "5", "6"];
            if (statusIdArray.indexOf(String($scope.Selected.InsurerStatus.StatusId)) == -1) {
                $scope.IsSubStatusVisible = false;
            } else {
                $scope.IsSubStatusVisible = true;
            }
            var storage = JSON.parse($window.localStorage.getItem('BookingDetails'));
            var statusId = ["1", "2", "3", "4", "5", "6", "10", "11","16"];
            if ($.inArray(String(storage.StatusId),statusId) != -1) {
                $scope.IsStatusDrpDwnEnable = true;
                $scope.IsSubStatusDrpDwnEnable = true;
                $scope.IsInsurerDrpDwnEnable = true;
            } else {
                $scope.IsStatusDrpDwnEnable = false;
                $scope.IsSubStatusDrpDwnEnable = false;
                $scope.IsInsurerDrpDwnEnable = false;
            }
          
        };
       
        $scope.CheckSubStatus();

        $scope.isEmpty = function (str) {
            return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
        };

        //$scope.DocumentList = [{
        //     "Selected": {
        //        "FileDetails":
        //            { "FileName": undefined, "Size": undefined, "FileStringData": undefined }
        //    }
        //}];

        var objDocumentList = [];

        $scope.ValidateDoc = function () {
            var msg = '';
            objDocumentList = [];
            if ($scope.InspectionFileData != undefined && $scope.InspectionFileData.length > 0) {
                for (var i in $scope.InspectionFileData) {

                    if ($scope.InspectionFileData[i] == undefined || $scope.isEmpty($scope.InspectionFileData[i].FileStringData)) {
                        alert("Please select a file.");
                        return false;
                    }
                    else {
                        objDocumentList.push({
                            "FileData": $scope.InspectionFileData[i].FileStringData, "FileName": $scope.InspectionFileData[i].FileName
                        })
                    }
                }
            }
            else {
                alert("Please select document.");
                return false;
            }
            if (msg == '') {
                return true;
            }
            else {
                //alert(msg);
                return false;
            }
        };

        $scope.UploadDocuments = function () {
            debugger;
            $scope.Error = '';
            $scope.Msg = '';    

            if ($scope.ValidateDoc()) {

                var confirmData = confirm("Do you want to upload document?");
                if (confirmData == true) {
                    $scope.PageLoad = true;
                    var objInspectionDocumentDetails =
                    {

                        "UserID": $scope.UserDetails.UserID,
                        "InspectionID": $scope.LabelInspectionID,
                        "LeadID": $scope.PBInspectionUnderWritingDetails[0].BookingId,
                        "CustomerID": 0,
                        "ProductID": 117,
                        "InspectionDocumentList": objDocumentList

                    };


                    AjaxCall.postData('CCTechService.svc/AddInspectionDocuments', { "objInspectionDocumentDetails": objInspectionDocumentDetails }, function (data) {                      
                        $scope.DocumentList = [{
                            "Selected": {
                                "FileDetails":
                              { "FileName": undefined, "Size": undefined, "FileStringData": undefined }
                            }
                        }];
                        
                        // alert('Reports Uploaded successfully.');
                        alert("Documents Uploaded Sucessfully!!");
                        $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
                        $window.location.reload();
                    })
                    //VideoInspectionService.AddInspectionDocuments(objInspectionDocumentDetails).success(function (response) {
                    //    $scope.DocumentList = [{
                    //         "Selected": {
                    //          "FileDetails":
                    //        { "FileName": undefined, "Size": undefined, "FileStringData": undefined }
                    //        }
                    //    }];
                    //    $scope.PageLoad = false;
                    //    // alert('Reports Uploaded successfully.');
                    //    $scope.Msg = 'Reports Uploaded successfully.';
                    //});
                }
          
            }


        }

        $scope.file_changed = function (element) {
            debugger;

            $scope.$apply(function (scope) {

                var documentfile = element.files[0];

                var fileReader = new FileReader();
                var InspectionData = {                   
                    "FileName": undefined,
                    "Size": undefined,
                    "FileStringData": undefined
                };
               
                fileReader.onload = function (fileLoadedEvent) {
                    debugger;
                    var chkIndex = undefined;
                    InspectionData.FileName = element.files[0].name;
                    InspectionData.Size = element.files[0].size
                    InspectionData.FileStringData = btoa(fileLoadedEvent.target.result);
                    if ($scope.InspectionFileData.length > 0) {
                        for (var i = 0; i < $scope.InspectionFileData.length; i++) {                            
                                chkIndex = i;
                        }
                    }
                    if (chkIndex != undefined) {
                        $scope.InspectionFileData.splice(chkIndex, 1);
                    }
                    $scope.InspectionFileData.push(InspectionData);
                    //
                };
                if (element.files.length > 0) {
                    fileReader.readAsBinaryString(element.files[0]);
                }
                else {
                    if ($scope.InspectionFileData.length > 0) {
                        for (var i = 0; i < $scope.InspectionFileData.length; i++) {                        
                                $scope.InspectionFileData.splice(i, 1);
                        }
                    }
                }

            });
        };

        //HEXDocUploadApp.service("HEXDocUploadAppService", function ($http, $window) {
        //    this.FileData = {};
            

           
        //});
       
    });