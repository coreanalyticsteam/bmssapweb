﻿MotorInspection.controller("LoginCtrl", function($scope, VideoInspectionService, $rootScope, $routeParams, $window) {

    debugger;
    $scope.isEmpty = function(str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };
    $scope.UserDetails = undefined;
    $scope.LeadDetails = undefined;
    $window.localStorage.removeItem('LeadDetails');
    $window.localStorage.removeItem('BookingDetails');
    $window.localStorage.removeItem('PbInspectionDetails');
    $window.localStorage.removeItem('UserId');
    $window.localStorage.removeItem('InsurerInspectionDetails');
    if (localStorage.UserDetails) {
        $window.localStorage.removeItem('UserDetails');
        $window.localStorage.removeItem('UserInfo');
        $window.location.reload();
    }
    
    //$rootScope.UserDetails = undefined;
    $scope.Msg = '';
    $scope.Login = function() {
        $scope.Msg = '';
        debugger;
        if (!$scope.isEmpty($scope.LoginID) && !$scope.isEmpty($scope.Password)) {
            var LoginData = { "LoginID": $scope.LoginID, "Password": $scope.Password, "LoginUserType": 1 };
            debugger;
            VideoInspectionService.Login(LoginData).success(function(data) {
                var storage = JSON.stringify({
                    EmailID: data.EmailID,
                    LoginID: data.LoginID,
                    LoginStatus: data.LoginStatus,
                    MobileNo: data.MobileNo,
                    Name: data.Name,
                    Token: data.Token,
                    UserID: data.UserID,
                    UserRole: data.UserRole,
                    UserType: data.UserType,
                    InsurerID: data.InsurerID,
                    InsurerName: data.InsurerName
                });
                debugger;
                $scope.$broadcast('GetUserDetails', storage);
                if (data.LoginStatus == 1) {
                    $window.localStorage.setItem('UserDetails', storage);
                    
                    if (data.UserType == 1) {
                        $window.location.href = '/home.html#/MotorInspection/PB/InspectionDashboard';
                        $window.location.reload();
                    } else if (data.UserType == 2) {
                         $window.localStorage.setItem('UserDetails', storage);
                         $window.location.href = '/home.html#/MotorInspection/Insurer/InsurerDashboard';
                         $window.location.reload();
                    }
                } else {
                    $scope.Msg = 'Invalid credentials';
                }

            });
        } else {
            $scope.Msg = 'Please enter Username and Password';
        }
    }

});

MotorInspection.controller("InternalLoginCtrl", function ($scope, VideoInspectionService, $rootScope, $routeParams, $window) {

    $scope.base64EncodedData = atob($routeParams.base64EncodedData);
    $scope.Page = $routeParams.Page;

    $scope.isEmpty = function (str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };
    $scope.Msg = '';
    $scope.Login = function () {
        var UrlData = $scope.base64EncodedData.split('~');
        debugger;
        if (!$scope.isEmpty(UrlData[0])) {
            var LoginData = { "LoginID": UrlData[0], "Password": '1234', "LoginUserType": UrlData[1] };
            VideoInspectionService.Login(LoginData).success(function (data) {
                debugger;
                var storage = JSON.stringify({
                    EmailID: data.EmailID,
                    LoginID: data.LoginID,
                    LoginStatus: data.LoginStatus,
                    MobileNo: data.MobileNo,
                    Name: data.Name,
                    Token: data.Token,
                    UserID: data.UserID,
                    UserRole: data.UserRole,
                    UserType: data.UserType,
                    InsurerID: data.InsurerID,
                    InsurerName: data.InsurerName
                });
                if (data.LoginStatus == 1) {
                    $window.localStorage.setItem('UserId', storage);
                    if ($scope.Page == 1)//Dashboard
                    {
                        $window.location.href = '/home.html#/HealthExchange/PB/PBDashboard';
                    }
                    else if ($scope.Page == 2)//Leadpage
                    {
                        VideoInspectionService.ExLoginGetLeadData(UrlData[2]).success(function (resdata) {
                            var storage = JSON.stringify({
                                LeadID: resdata.LeadID,
                                NewLeadID: resdata.NewLeadID,
                                ExID: resdata.ExID,
                                //SelectionID: data.SelectionID,
                                CustomerID: resdata.CustomerID,
                                ExLeadType: resdata.ExLeadType,
                                ProductID: '2'
                            });
                            $window.localStorage.setItem('LeadDetails', storage);
                            
                            $window.location.href = '/home.html#/HealthExchange/PB/InsurerUnderwritingResponse';
                        });
                       
                    }
                    
                }
                else {
                    $scope.Msg = 'Invalid credentials';
                }

            });
        }
        else {
            $scope.Msg = 'Please enter Username and Password';
        }
    }
    $scope.Login();
    //var storage = localStorage.getItem(UserId);
    //return JSON.parse(storage);

});