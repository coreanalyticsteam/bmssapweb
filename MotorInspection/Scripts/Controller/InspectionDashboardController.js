﻿MotorInspection.controller("InspectionDashboardCtrl", function ($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal,$location, $window, Excel, $timeout, $route, $compile) {
    debugger;
    var _this = this;
    var table1;
    $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));
    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
    $scope.clickTest = function () {
        $location.path("/MotorInspection/PB/InspectionUnderwriting")
    }
    //$location.path("/home")
    $scope.GetPBInspectionDetail = function () {
        debugger;
        var statusName = null;
        if ($scope.Selected.InsurerStatus != null) {
            if ($scope.Selected.InsurerStatus.StatusName != "-Select-") {
                statusName = $scope.Selected.InsurerStatus.StatusName;
            }
        }
        var objInspectionDetails =
        {
            "CustomerId": $scope.CustomerId,
            "InspectionId": $scope.InspectionId,
            "VehicleNumber": $scope.VehicleNumber,
            "StatusName": statusName,
            "MobileNo": $scope.MobileNumber,
            "FromDate": new Date($scope.FromDate).getTime(),
            "ToDate": new Date($scope.ToDate).getTime(),
            "BookingId": $scope.BookingId,
            "LeadId":$scope.LeadId
        };
        if ($scope.Selected.Insurer != undefined) {
            objInspectionDetails.Insurer = $scope.Selected.Insurer.InsurerId
        }
        var storage = JSON.stringify({
            CustomerId: $scope.CustomerId,
            InspectionId: $scope.InspectionId,
            VehicleNumber: $scope.VehicleNumber,
            Selected : $scope.Selected,
            MobileNo: $scope.MobileNumber,
            FromDate: $scope.FromDate,
            ToDate: $scope.ToDate,
            BookingId: $scope.BookingId,
            LeadId: $scope.LeadId
        });
        sessionStorage.setItem('LastSearchDetails', storage);
        $scope.PBInspectionLoad = true;
        VideoInspectionService.GetPBInspectionDetails(objInspectionDetails, $scope.UserDetails.Token).success(function (data) {
            debugger;
            $scope.PBInspectionLoad = false;
            $scope.AllPBInspectionDetails = data;
            $(function () {
                $("#example1").dataTable().fnDestroy()
                table1 = $('#example1').DataTable({
                    //"dom": 'Bfrtipl',
                    //"buttons": ['copy', 'csv', 'excel', 'pdf', 'print'],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                // columns: [ 0, ':visible' ]
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3,4,5,6,7,8 ]
                            }
                        },
                        //'colvis'
                    ],
                    "data": data,
                    "columns": [
                         {
                             "data": "id",
                             render: function (data, type, row, meta) {
                                 return meta.row + meta.settings._iDisplayStart + 1;
                             }
                         },
                        {
                            "data": "InspectionId"
                        },
                        {
                            "data": "UploadedDate",
                            "render": function (data) {
                                var date = new Date(data);
                                var month = date.getMonth();
                                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                                //return date.getDate() + "-" + months[parseInt(month.length > 1 ? month : "0" + month)] + "-" + date.getFullYear();
                                var NewData = (String(date.getDate()).length > 1 ? date.getDate() : "0" + '' + String(date.getDate())) + "-" + months[parseInt(month.length > 1 ? month : "0" + month)] + "-" + date.getFullYear();
                                var NewDateFormat = date.getFullYear() + '' + month + '' + date.getDate() +''+ (String(date.getHours()).length > 1 ? date.getHours() : "0" + date.getHours()) + ':' + (String(date.getMinutes()).length > 1 ? date.getMinutes() : "0" + date.getMinutes());
                                return '<span class="hide">' + NewDateFormat  + '</span>' + NewData + ' ' + (String(date.getHours()).length > 1 ? date.getHours() : "0" + date.getHours()) + ':' + (String(date.getMinutes()).length > 1 ? date.getMinutes() : "0" + date.getMinutes());
                               // return date.getDate() + "-" + parseInt(month.length > 1 ? month : "0" + month) + "-" + date.getFullYear();
                               // return data;
                            }
                        },
                        {
                            "data": "VehicleNumber"
                        },
                        {
                            "data": "CustomerName"
                        },
                        { "data": "BookingId" },
                        { "data": "InsurerName" },
                        { "data": "LeadId" },                      
                        { "data": "LeadSource" },
                        { "data": "StatusName" },
                        {
                            "data": "StatusName",
                            //"render": function (data, type, full, meta) { return '<em class="view_icon" ng-click="OpenPBBooking('+data+')"></em'; }
                            "render": function (data, type, full, meta) { return '<em class="view_icon" ng-click=\"Ctrl.dataTablesAlert()\"></em>'; }
                        }
                    ],
                    

                });
                
            });

            $('#example1 tbody').on('click', 'tr td', function () {
                debugger;
                //alert($(this).index());
                //alert(table1.cell(this).index());
                //console.log(table1.cell( this ).data())
                if ($(this).index() == 10) {
                    var data = table1.row(this).data();
                    $scope.OpenPBBooking(data);
                }
            });
           // localStorage.setItem('PbInspectionDetails', JSON.stringify(data));
        });
    };
    this.dataTablesAlert = function () {
        alert("Hello");
    };
    $scope.exportToExcel = function () {
        debugger;
        var statusName = null;
        if ($scope.Selected.InsurerStatus != null) {
            if ($scope.Selected.InsurerStatus.StatusName != "-Select-") {
                statusName = $scope.Selected.InsurerStatus.StatusName;
            }
        }
        var objInspectionDetails =
        {
            "CustomerId": $scope.CustomerId,
            "InspectionId": $scope.InspectionId,
            "VehicleNumber": $scope.VehicleNumber,
            "StatusName": statusName,
            "MobileNo": $scope.MobileNumber,
            "FromDate": new Date($scope.FromDate).getTime(),
            "ToDate": new Date($scope.ToDate).getTime(),
            "BookingId": $scope.BookingId,
            "LeadId": $scope.LeadId
        };
        if ($scope.Selected.Insurer != undefined) {
            objInspectionDetails.Insurer = $scope.Selected.Insurer.InsurerId
        }
        $scope.PDFPBInspectionLoad = true;
        VideoInspectionService.GetPBPDFInspectionDetails(objInspectionDetails, $scope.UserDetails.Token).success(function (data) {
            debugger;
            $scope.PDFPBInspectionLoad = false;
            var xml = data.split('>');
            var fields = xml[1].split('<');

            //window.open("data:application/vnd.ms-excel;base64," + fields[0]);
            var uri = "data:application/vnd.ms-excel;base64," + fields[0];
            var downloadLink = document.createElement("a");
            downloadLink.href = uri;
            downloadLink.download = "newFile.xls";

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
           // $scope.AllPBInspectionDetails = data;
           // localStorage.setItem('PbInspectionDetails', JSON.stringify(data));
        });
    };
    $scope.OpenPBBooking = function (data) {
        debugger;
        var storage = JSON.stringify({
            InspectionId: data.InspectionId,
            VehicleNumber: data.VehicleNumber,
            CustomerId: data.CustomerId,
            StatusName: data.StatusName,
            StatusId: data.StatusId,
            SubStatusName: data.SubStatusName,
            SubStatusId: data.SubStatusId,
            InsurerId: data.InsurerId,
            InsurerName: data.InsurerName
        });
        localStorage.setItem('BookingDetails', storage);
        $window.location.href = '/home.html#/MotorInspection/PB/InspectionUnderwriting';
    }

    //$scope.GetInspectionDetails = function () {
    //    debugger;
    //    if (localStorage.getItem("PbInspectionDetails") != null) {
    //        //var objInspectionDetails =
    //        //  {
    //        //      //"CustomerId": $scope.BookingDetails.CustomerId,
    //        //      "InspectionId": $scope.BookingDetails.InspectionId,
    //        //      //"VehicleNumber": $scope.BookingDetails.VehicleNumber,
    //        //      //"StatusName": $scope.BookingDetails.StatusName
    //        //  };
    //        VideoInspectionService.GetInspectionBookingDetail($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function (data) {
    //            //$scope.AllPBInspectionDetails = JSON.parse($window.localStorage.getItem('PbInspectionDetails'));
    //            $scope.AllPBInspectionDetails = data;
    //        });
    //    }
    //};
    $scope.exportToExcel1 = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
        $timeout(function () { location.href = exportHref; }, 100); // trigger download
    }
    $scope.GetStatusAndSubStatus = function () {
        debugger;
        VideoInspectionService.GetStatusAndSubStatus(0, $scope.UserDetails.Token).success(function (data) {
            $scope.AllStatusCollection = data;
        });
    };
    $scope.DownloadExcel1 = function () {
        debugger;
        VideoInspectionService.DownloadExcel($scope.UserDetails.Token).success(function (data) {
          
            //var blob = new Blob([data], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
            //var objectUrl = URL.createObjectURL(blob);
            //window.open(objectUrl);


            var blob = new Blob([data], { type: 'application/vnd.ms-excel' });
            var downloadUrl = URL.createObjectURL(blob);
            var a = document.createElement("a");
            a.href = downloadUrl;
            a.download = "data.xls";
            document.body.appendChild(a);
            a.click();

            //var blob = new Blob([data], { type: "application/vnd.ms-excel" });
            //alert('BLOB SIZE: ' + data.length);
            //var URL = window.URL || window.webkitURL;
            //var downloadUrl = URL.createObjectURL(blob);
            //document.location = downloadUrl;

            //var blob = new Blob([data],
            //        { type: 'application/vnd.openxmlformat-officedocument.spreadsheetml.sheet;' });
            //saveAs(blob, "abc");
            //var a = document.createElement('a');
            //a.href = 'data:attachment/xlsx,' + encodeURI(data);
            //a.target = '_blank';
            //a.download = 'out.xlsx';
            //console.log(a);
            //document.body.appendChild(a);
            //a.click();

        //    var a = document.createElement('a');
        //    var blob = new Blob([data], {'type':'application/octet-stream'});
        //a.href = window.URL.createObjectURL(blob);
        //a.download = 'filename.xls';
            //a.click();
           // window.open('http://localhost:55235/VideoInspectionService.svc/Download2');
        });
    };
    $scope.DownloadExcel = function () {
        debugger;
        VideoInspectionService.DownloadExcel1($scope.UserDetails.Token).success(function (data) {
        
            var xml = data.split('>');
            var fields = xml[1].split('<');
            
            //window.open("data:application/vnd.ms-excel;base64," + fields[0]);
            var uri = "data:application/vnd.ms-excel;base64," + fields[0];
            var downloadLink = document.createElement("a");
            downloadLink.href = uri;
            downloadLink.download = "newFile.xls";

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
          
        });
    };
    $scope.GetInsurer = function () {
        debugger;
        VideoInspectionService.GetInsurerCollection($scope.UserDetails.Token).success(function (data) {
            $scope.AllInsurerCollection = data;
            if (sessionStorage.getItem("LastSearchDetails")) {
                var Retrive = JSON.parse(sessionStorage.getItem("LastSearchDetails"));
                $scope.CustomerId = Retrive.CustomerId;
                $scope.InspectionId = Retrive.InspectionId;
                $scope.VehicleNumber = Retrive.VehicleNumber;
                $scope.Selected = Retrive.Selected;
                $scope.MobileNumber = Retrive.MobileNo;
                $scope.FromDate = new Date(Retrive.FromDate).valueOf();
                $scope.ToDate = Retrive.ToDate;
                $scope.BookingId = Retrive.BookingId;
                $scope.LeadId = Retrive.LeadId;

                $scope.GetPBInspectionDetail();
            }
        });
    }
    $scope.GetStatusAndSubStatus();
   // $scope.GetInspectionDetails();
    $scope.GetInsurer();
    //$scope.ClearFilter = function () {        
    //   /// localStorage.removeItem('BookingDetails');
    //    sessionStorage.clear();
    //    $route.reload();
    //}
    debugger;
    $scope.Selected = {
        "InsurerStatus": { "StatusId": 0, "StatusName": "-Select-" }
    }
    //if ($scope.BookingDetails == null) {
    //    var insurerStatus = {
    //        "StatusId": $scope.BookingDetails.StatusId,
    //        "StatusName": $scope.BookingDetails.StatusName,
    //        "SubStatusId": 0,
    //        "SubStatusName": null
    //    };
    //}
    debugger;

    $scope.Selected.InsurerStatus = $scope.Selected.InsurerStatus;

    $scope.ClearPBController = function () {
        sessionStorage.removeItem('LastSearchDetails');
        $route.reload();
    }
    ///////--------------------------------CALENDAR---------------------------------------------////
    
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    debugger;

    $scope.NumberOfDays = 0;
    $scope.ToDate = Date.now();
    $scope.FromDate = Date.now() + ($scope.NumberOfDays * 24 * 60 * 60 * 1000);

    $scope.DateChange = function () {
        debugger;
        if ($scope.Selected.DateRange.ID == 1) {
            $scope.NumberOfDays = 0;
        }
        else if ($scope.Selected.DateRange.ID == 2) {
            $scope.NumberOfDays = 7;
        }
        else if ($scope.Selected.DateRange.ID == 3) {
            $scope.NumberOfDays = 15;
        }
        else if ($scope.Selected.DateRange.ID == 4) {
            $scope.NumberOfDays = 30;
        }
        else {
            $scope.NumberOfDays = 0;
        }

        $scope.ToDate = Date.now();
        $scope.FromDate = Date.now() - ($scope.NumberOfDays * 24 * 60 * 60 * 1000);
    };
    $scope.dateOptions = {
        //dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
          mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
          date: tomorrow,
          status: 'full'
      },
      {
          date: afterTomorrow,
          status: 'partially'
      }
    ];

    function getDayClass(data) {
        var date = data.date,
          mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
    ////--------------------------------------------------------------------------------------////////////////////
});