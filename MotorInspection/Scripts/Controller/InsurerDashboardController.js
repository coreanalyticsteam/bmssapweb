﻿MotorInspection.controller("InsurerDashboardCtrl", function($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal, $window) {
    debugger;
    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
    $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));

    $scope.GetInsurerInspectionDetail = function() {
        debugger;
        var objInspectionDetails =
        {
            "InspectionId": $scope.inspectionId,
            "VehicleNumber": $scope.vehicleNumber,
            "InsurerId": $scope.UserDetails.InsurerID,
            "FromDate": new Date($scope.FromDate).getTime(),
            "ToDate": new Date($scope.ToDate).getTime()
        };
        VideoInspectionService.InsurerInspectionDetail(objInspectionDetails, $scope.UserDetails.Token).success(function(data) {
            $scope.AllInsurerInspectionDetails = data;
            localStorage.setItem('InsurerInspectionDetails', JSON.stringify(data));
        });
    };

    $scope.OpenBooking = function(data) {
        debugger;
        var storage = JSON.stringify({
            InspectionId: data.InspectionId,
            VehicleNumber: data.VehicleNumber,
            StatusName: data.StatusName,
            StatusId: data.StatusId,
            SubStatusName: data.SubStatusName,
            SubStatusId: data.SubStatusId,
            FromDate: new Date($scope.FromDate).getTime(),
            ToDate: new Date($scope.ToDate).getTime()
        });
        localStorage.setItem('BookingDetails', storage);
        $window.location.href = '/home.html#/MotorInspection/Insurer/InsurerUnderwriting';
    }

    $scope.GetInsurerInspDetails = function() {
        if (localStorage.getItem("BookingDetails") != null) {
            //var objInspectionDetails =
            //{
            //    "InspectionId": $scope.BookingDetails.InspectionId,
            //    //"VehicleNumber": $scope.BookingDetails.VehicleNumber,
            //    "InsurerId": $scope.UserDetails.InsurerID
            //};
            VideoInspectionService.GetInspectionBookingDetail($scope.BookingDetails.InspectionId, $scope.UserDetails.Token).success(function(data) {
                $scope.AllInsurerInspectionDetails = data;
            });
        }
        //if (localStorage.getItem("InsurerInspectionDetails") != null) {
        //    $scope.AllInsurerInspectionDetails = JSON.parse($window.localStorage.getItem('InsurerInspectionDetails'));;
        //}
    };
    $scope.GetInsurerInspDetails();

    /////////----------------------CALENDAR------------------------------------/////////////////////////////////
    $scope.today = function() {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    debugger;

    $scope.NumberOfDays = 0;
    $scope.ToDate = Date.now();
    $scope.FromDate = Date.now() + ($scope.NumberOfDays * 24 * 60 * 60 * 1000);

    $scope.DateChange = function() {
        debugger;
        if ($scope.Selected.DateRange.ID == 1) {
            $scope.NumberOfDays = 0;
        } else if ($scope.Selected.DateRange.ID == 2) {
            $scope.NumberOfDays = 7;
        } else if ($scope.Selected.DateRange.ID == 3) {
            $scope.NumberOfDays = 15;
        } else if ($scope.Selected.DateRange.ID == 4) {
            $scope.NumberOfDays = 30;
        } else {
            $scope.NumberOfDays = 0;
        }

        $scope.ToDate = Date.now();
        $scope.FromDate = Date.now() - ($scope.NumberOfDays * 24 * 60 * 60 * 1000);
    };
    $scope.dateOptions = {
        //dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
});