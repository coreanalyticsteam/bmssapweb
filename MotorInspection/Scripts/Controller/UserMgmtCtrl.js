﻿MotorInspection.controller("UserMgmtCtrl", function ($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal, $window, AjaxCall) {
    $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));
    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
    // $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserID'));
    $scope.chkLogin = function () {
        if ($window.localStorage.getItem('UserDetails') == null) {
            debugger;
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
        else {
            if ($scope.UserDetails.UserType == 2) {
                $window.location.href = '/home.html#/MotorInspection/Insurer/InsurerDashboard';
            }
        }
    };
    $scope.LogOut = function () {
        $window.localStorage.removeItem('UserID');
        if ($window.localStorage.getItem('UserID') == null) {
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
    };
    $scope.chkLogin();
    $scope.UserMgmtData = UserMgmtData;
    $scope.ValidateForm = function () {     
        var Msg = '';
        if ($scope.User.Name == undefined || $scope.User.Name == "") {
            Msg = 'Please Enter a valid Name';
        }
        else if ($scope.User.EmailID == undefined || $scope.User.EmailID == "")
        {
            Msg = 'Please Enter a valid EmailID';
        }
        //else if ($scope.User.MobileNo == undefined || $scope.User.MobileNo == "" || isNaN($scope.User.MobileNo) || $scope.User.MobileNo.length != 10) {
        //    Msg = 'Please Enter a valid MobileNo';
        //}
        else if ($scope.User.LoginID == undefined || $scope.User.LoginID == "") {
            Msg = 'Please Enter a valid LoginID';
        }
        else if ($scope.User.Password == undefined || $scope.User.Password == "") {
            Msg = 'Please Enter a valid Password';
        }
        else if ($scope.Selected == undefined || $scope.Selected.UserTypeName.ID == "" || $scope.Selected.UserTypeName.ID == 0) {
            Msg = 'Please select a valid UserType';
        }
        else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 2) {
            $scope.Selected.UserRoleName = { "ID": 0, "Name": "" };
            if ($scope.Selected.Insurer == 0 || $scope.Selected.Insurer == undefined) {
                Msg = 'Please select a valid Insurer';
            }
        }
        else if ($scope.Selected.UserRoleName == undefined || $scope.Selected.UserRoleName.ID == 0 || $scope.Selected.UserRoleName == null) {
            Msg = 'Please select a valid UserRole';
        }
            //else if ($scope.Selected.UserRole == undefined || $scope.Selected.UserRole == "" || $scope.Selected.UserRole.ID == 0) {
            //    Msg = 'Please select a valid UserRole';
            //}
            //else if ($scope.Selected.ApplicationType == undefined) {
            //    Msg = 'Please select a valid ApplicationType';
            //}
            // else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 2)
            // {
            // debugger;
            //  if ($scope.Selected.Insurer.InsurerID == 0 || $scope.Selected.Insurer.InsurerID == undefined)
            // {
            //     Msg = 'Please select a valid Insurer';
            //  }
            //}
        else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 1) {
            $scope.Selected.Insurer = {"InsurerID":0,"InsurerName":""};
        }
        

        if (Msg != '') {
            alert(Msg);
            return false;
        }
        else {
            //    Msg = 'You have added a new user!!';
            //    alert(Msg);
            return true;
        }
    }
    //$scope.ValidateForm = function () {     
    //    var Msg = '';
    //    if ($scope.Name ==undefined  || $scope.Name == "") {
    //        Msg = 'Please Enter a valid Name';
    //    }
    //    else if ($scope.EmailID == undefined || $scope.EmailID == "")
    //    {
    //        Msg = 'Please Enter a valid EmailID';
    //    }
    //    else if ($scope.MobileNo == undefined || $scope.MobileNo == "" || isNaN($scope.MobileNo) || $scope.MobileNo.length != 10) {
    //        Msg = 'Please Enter a valid MobileNo';
    //    }
    //    else if ($scope.LoginID == undefined || $scope.LoginID == "") {
    //        Msg = 'Please Enter a valid LoginID';
    //    }
    //    else if ($scope.Password == undefined || $scope.Password == "" ) {
    //        Msg = 'Please Enter a valid Password';
    //    }
    //    else if ($scope.Selected.UserTypeName == undefined || $scope.Selected.UserTypeName == "" || $scope.Selected.UserTypeName.ID == 0) {
    //        Msg = 'Please select a valid UserType';
    //    }
    //    else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 2) {
    //        $scope.Selected.UserRoleName = { "ID": 0, "Name": "" };
    //    }
    //    //else if ($scope.Selected.UserRole == undefined || $scope.Selected.UserRole == "" || $scope.Selected.UserRole.ID == 0) {
    //    //    Msg = 'Please select a valid UserRole';
    //    //}
    //    //else if ($scope.Selected.ApplicationType == undefined) {
    //    //    Msg = 'Please select a valid ApplicationType';
    //    //}
    //    else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 2)
    //    {
    //     if ($scope.Selected.Insurer == 0 || $scope.Selected.Insurer== undefined)
    //        {
    //            Msg = 'Please select a valid Insurer';
    //        }
    //    }
    //    else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 1) {
    //        $scope.Selected.Insurer = {"InsurerID":0,"InsurerName":""};
    //    }
        

    //    if (Msg != '') {
    //        alert(Msg);
    //        return false;
    //    }
    //    else {
    //    //    Msg = 'You have added a new user!!';
    //    //    alert(Msg);
    //      return true;
    //    }
    //}



    $scope.AddNewUsers = function () {
        
        if ($scope.ValidateForm()) {
            var Mssg = '';
            //var objAddNewUser =
            //   {
                  
            //       "Name": $scope.Name,
            //       "EmailID": $scope.EmailID,
            //       "MobileNo": $scope.MobileNo,
            //       "LoginID": $scope.LoginID,
            //       "Password": $scope.Password,
            //       "CreatedBy": $scope.UserDetails.UserID,
            //       "UserTypeName": $scope.Selected.UserTypeName.ID,
            //       "UserRoleName": $scope.Selected.UserRoleName.ID,
            //       "InsurerID": $scope.Selected.Insurer.InsurerID,
            //       "InsurerName": $scope.Selected.Insurer.InsurerName,
            //       //"ApplicationType": $scope.Selected.ApplicationType.ID,

            //       "Type": 1

            //   };
            //var objAddNewUser =
            //  {

            //      "Name": $scope.Name,
            //      "EmailID": $scope.EmailID,
            //      "MobileNo": $scope.MobileNo,
            //      "LoginID": $scope.LoginID,
            //      "Password": $scope.Password,
            //      "CreatedBy": $scope.UserDetails.UserID,
            //      "UserTypeName": $scope.Selected.UserTypeName.ID,
            //      "UserRoleName": $scope.Selected.UserRoleName.ID,
            //      "InsurerID": $scope.Selected.Insurer.InsurerID,
            //      "InsurerName": $scope.Selected.Insurer.InsurerName,
            //      //"ApplicationType": $scope.Selected.ApplicationType.ID,

            //      "Type": 1

            //  };
            $scope.User.CreatedBy = $scope.UserDetails.UserID;
            $scope.User.UserTypeName = $scope.Selected.UserTypeName.ID;
            $scope.User.UserRoleName = $scope.Selected.UserRoleName.ID;
            $scope.User.InsurerID = $scope.Selected.Insurer.InsurerID;
            $scope.User.InsurerName = $scope.Selected.Insurer.InsurerName;
            $scope.User.Type = 1;
            debugger;
            VideoInspectionService.AddNewUsers($scope.User, $scope.UserDetails.Token).success(function (data) {
                if(data == -1)
                {
                    Mssg = 'Either email ID, login ID or phone number already exists';
                    alert(Mssg);
                }
                else
                {
                    //Mssg = 'User Added Successfully';
                   // alert(Mssg);
                 $window.location.reload();
                 $scope.Msg = 'User Added Successfully';
                }
            });
        }
       
    };
    //$(function () {
    //    $('#UserType').change(function () {
    //        $('.insurer').hide();
    //        $('#' + $(this).val()).show();
    //    });
    //});
    //$scope.UserMgmtData.UserType.ID = '1'

    //$scope.$watch('UserMgmtData.UserType.ID', function () {
    //   $scope.isEnabled = $scope.UserMgmtData.UserType.ID == '1' ? true : false;
    //});

  //  $scope.inputType = 'password';
  
  //// Hide & show password function
  //$scope.hideShowPassword = function(){
  //  if ($scope.inputType == 'password')
  //    $scope.inputType = 'text';
  //  else
  //    $scope.inputType = 'password';
  //};


    $scope.GetSupplierListByProduct = function (ProductID) {
        VideoInspectionService.GetSupplierListByProduct(ProductID, $scope.UserDetails.Token).success(function (data) {
            debugger;
            $scope.SupplierList = data;
            //localStorage.setItem('SupplierList', data);
        });
    };
   
    $scope.CommonData = CommonData;

    $scope.GetSupplierListByProduct(117);


   // $scope.changeinsurer = function (item) {


   //   $scope.CommonData = CommonData;

   //   $scope.GetSupplierListByProduct(item.ID);
   //};
});





MotorInspection.controller("UserDetailsController", function ($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal, $window) {
    
    $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));
    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
    // $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserID'));
    $scope.chkLogin = function () {
        if ($window.localStorage.getItem('UserDetails') == null) {
            debugger;
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
        else {
            if ($scope.UserDetails.UserType == 2) {
                $window.location.href = '/home.html#/MotorInspection/Insurer/InsurerDashboard';
            }
        }
    };
    $scope.LogOut = function () {
        $window.localStorage.removeItem('UserID');
        if ($window.localStorage.getItem('UserID') == null) {
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
    };
    $scope.AddNewUser = function () {


        $window.location.href = '/home.html#/MotorInspection/Admin/AddNewUser';

    };
    $scope.chkLogin();

    $scope.GetHealthExUser = function () {
        debugger;
        VideoInspectionService.GetHealthExUser($scope.UserDetails.Token).success(function (data) {
            debugger;
            $scope.Userlist = data;
            
        });
    };
    $scope.GetHealthExUser();
    $scope.Edit = function (data) {

    
        var storage = JSON.stringify({
            Name: data.Name,
            EmailID: data.EmailID,
            MobileNo: data.MobileNo,
            LoginID: data.LoginID,
            Password: data.Password,
            UserTypeName: data.UserTypeName,
            UserRoleName: data.UserRoleName,
            InsurerID: data.InsurerID,
            InsurerName: data.InsurerName,
            //ApplicationType: data.ApplicationType,
            UserID: data.UserID
            
         
        });
        localStorage.setItem('UserInfo', storage);
        $scope.UserMgmtData = UserMgmtData;
        //$scope.currentPage = 1;
        //$scope.pageSize = 10;
        //$scope.data = $scope.Userlist;
        //$scope.search = '';
    
        //$scope.getData = function () {
           
        //    var arr = [];
        //    if ($scope.search == '') {
        //        arr = $scope.data;
        //    } else {
        //        for (var ea in $scope.data) {
        //            if ($scope.data[ea].indexOf($scope.search) > -1) {
        //                arr.push($scope.data[ea]);
        //            }
        //        }
        //    }
        //    return arr;
        //}
    
        //$scope.numberOfPages=function(){
        //    return Math.ceil($scope.getData().length/$scope.pageSize);                
        //}
        
    
        //for (var i=0; i<80; i++) {
        //    $scope.data.push("hello"+i);
        //}
    

  
    //app.filter('startFrom', function() {
    //    return function(input, start) {
    //        start = +start;
    //        return input.slice(start);
    //    }
    //});

   
        $window.location.href = '/home.html#/MotorInspection/Admin/UpdateUserInfo';
        
    }
    // $scope.CommonData = CommonData;
});

MotorInspection.controller("UpdateUserInfoCtrl", function ($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal, $window) {
    $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));
    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
    // $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserID'));
    $scope.chkLogin = function () {
        if ($window.localStorage.getItem('UserDetails') == null) {
            debugger;
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
        else {
            if ($scope.UserDetails.UserType == 2) {
                $window.location.href = '/home.html#/MotorInspection/Insurer/InsurerDashboard';
            }
        }
    };
    $scope.LogOut = function () {
        $window.localStorage.removeItem('UserID');
        if ($window.localStorage.getItem('UserID') == null) {
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
    };
    $scope.chkLogin();
   
    $scope.UserInfo = JSON.parse($window.localStorage.getItem('UserInfo'));
   // $scope.SupplierList = JSON.parse($window.localStorage.getItem('SupplierList'));
    $scope.UserMgmtData = UserMgmtData;
    $scope.Selected = {
        "UserTypeName": { "ID": $scope.UserInfo.UserTypeName },
        "UserRoleName": { "ID": $scope.UserInfo.UserRoleName },
        "InsurerName": { "InsurerID": $scope.UserInfo.InsurerID,"InsurerName": $scope.UserInfo.InsurerName }
    };
    //$scope.Selected1 = { "UserRoleName": { "ID": $scope.UserInfo.UserRoleName } };
    //$scope.Selected2 = { "InsurerName": { "InsurerName": $scope.UserInfo.InsurerName } };
    $scope.ValidateUpdateForm = function () {
        var Msg = '';
        if ($scope.UserInfo.Name == undefined || $scope.UserInfo.Name == "") {
            Msg = 'Please Enter a valid Name ';
        }
        else if ($scope.UserInfo.EmailID == undefined || $scope.UserInfo.EmailID == "") {
            Msg = 'Please Enter a valid EmailID';
        }
        else if ($scope.UserInfo.MobileNo == undefined || $scope.UserInfo.MobileNo == "" || isNaN($scope.UserInfo.MobileNo)) {
            Msg = 'Please Enter a valid MobileNo';
        }
        else if ($scope.UserInfo.LoginID == undefined || $scope.UserInfo.LoginID == "") {
            Msg = 'Please Enter a valid LoginID';
        }
        else if ($scope.UserInfo.Password == undefined || $scope.UserInfo.Password == "") {
            Msg = 'Please Enter a valid Password';
        }
        //else if ($scope.Selected == undefined) {
        //    Msg = 'Please select a valid UserType';
        //}
        else if ($scope.Selected.UserTypeName == undefined ) {
            Msg = 'Please select a valid UserType';
        }
        else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 2) {
            $scope.Selected.UserRoleName = { "ID": 0, "Name": "" };
        }
        //else if ($scope.Selected.UserRole == undefined ) {
        //    Msg = 'Please select a valid UserRole';
        //}
        //else if ($scope.Selected.ApplicationType == undefined) {
        //    Msg = 'Please select a valid ApplicationType';
        //}
        else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 2) {
            if ($scope.Selected.Insurer == 0 || $scope.Selected.Insurer == undefined) {
                Msg = 'Please select a valid Insurer';
            }
        }
        else if ($scope.Selected.UserTypeName.ID == 0 || $scope.Selected.UserTypeName.ID == 1) {
            $scope.Selected.Insurer = { "InsurerID": 0, "InsurerName": "" };
        }
       

        if (Msg != '') {
            alert(Msg);
            return false;
        }
        else {

            return true;
        }

    }


    $scope.GetSupplierListByProduct = function (ProductID) {
        VideoInspectionService.GetSupplierListByProduct(ProductID, $scope.UserDetails.Token).success(function (data) {
            debugger;
            $scope.SupplierList = data;
        });
    };

  

    $scope.GetSupplierListByProduct(117);
   
    $scope.UpdateUserInfo = function () {
        debugger;
        if ($scope.ValidateUpdateForm()) {
            var objUpdateUserInfo =
                  {

                      "Name": $scope.UserInfo.Name,
                      "EmailID": $scope.UserInfo.EmailID,
                      "MobileNo": $scope.UserInfo.MobileNo,
                      "LoginID": $scope.UserInfo.LoginID,
                      "Password": $scope.UserInfo.Password,
                      "CreatedBy": $scope.UserDetails.UserID,
                      "UserTypeName": $scope.Selected.UserTypeName.ID,
                      "UserRoleName": $scope.Selected.UserRoleName.ID,
                      //"UserTypeName": $scope.UserInfo.UserTypeName.ID,
                      //"UserRoleName": $scope.UserInfo.UserRoleName.ID,
                      "InsurerID": $scope.Selected.InsurerName.InsurerID,
                      "InsurerName": $scope.Selected.InsurerName.InsurerName,
                    //  "InsurerID": $scope.Selected.InsurerID,
                      //"InsurerName": $scope.Selected.InsurerName,
                      //"ApplicationType": $scope.Selected.ApplicationType.ID,
                      "UserID": $scope.UserInfo.UserID,
                      "Type": 2

                  };
            debugger;
            VideoInspectionService.UpdateUserInfo(objUpdateUserInfo, $scope.UserDetails.Token).success(function (data) {
                //if ($scope.ValidateUpdateForm()) {
                var Msgg = '';
                //    msg = 'Updated!!';
                //    alert(msg);
                //}
                //Msgg = 'You have updated the information successfully!!';
                //alert(Msgg);
                //$window.location.reload();
                $window.location.href = '/home.html#/MotorInspection/Admin/UserDetails';
                $scope.Msg = 'User Updated Successfully';
            });
        }


        //}
   
    
    }

    $scope.GetSupplierListByProduct = function (ProductID) {
        VideoInspectionService.GetSupplierListByProduct(ProductID, $scope.UserDetails.Token).success(function (data) {
            debugger;
            $scope.SupplierList = data;
        });
    };
    $scope.GetSupplierListByProduct(117);
    //$scope.changeinsurer1 = function (item) {


    //    $scope.CommonData = CommonData;

    //    $scope.GetSupplierListByProduct(item.ID);
    //};
});

MotorInspection.controller("ChangePasswordCtrl", function ($scope, VideoInspectionService, $rootScope, $routeParams, $uibModal, $window) {
    $scope.BookingDetails = JSON.parse($window.localStorage.getItem('BookingDetails'));
    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserDetails'));
   // $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserID'));
    $scope.chkLogin = function () {
        if ($window.localStorage.getItem('UserDetails') == null) {
            debugger;
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
        //else {
        //    if ($scope.UserDetails.UserType == 2) {
        //        $window.location.href = '/home.html#/MotorInspection/Insurer/InsurerDashboard';
        //    }
        //}
    };
    $scope.LogOut = function () {
        $window.localStorage.removeItem('UserID');
        if ($window.localStorage.getItem('UserID') == null) {
            $window.location.href = '/home.html#/MotorInspection/Login';
        }
    };
    $scope.chkLogin();

    $scope.UserInfo = JSON.parse($window.localStorage.getItem('UserInfo'));
    $scope.UserMgmtData = UserMgmtData;

    $scope.ValidateUpdateForm = function () {
        var Msg = '';
        
         if ($scope.OldPassword == undefined || $scope.OldPassword == "" ) {
            Msg = 'Please Enter a valid Password';
         }

         else if ($scope.NewPassword == undefined || $scope.NewPassword == "") {
             Msg = 'Please Enter a valid Password';
         }

         else if ($scope.NewPassword != $scope.ConfirmNewPassword || $scope.ConfirmNewPassword == "" || $scope.ConfirmNewPassword == undefined) {
             Msg = 'New Password do no match';
         }

         else if ($scope.NewPassword == $scope.OldPassword) {
             Msg = 'Old and New password cannot be same';
         }

        if (Msg != '') {
            alert(Msg);
            return false;
        }
        else {

            return true;
        }

    }


    $scope.ChangePassword = function () {

        if ($scope.ValidateUpdateForm()) {
            var objChangePassword =
                  {
                      
                      "OldPassword": $scope.OldPassword,
                      "NewPassword": $scope.NewPassword,
                      //"UserType": $scope.UserInfo.UserType,
                      "UserId": $scope.UserDetails.UserID,
                      
                  };
            debugger;
            VideoInspectionService.ChangePassword(objChangePassword,$scope.UserDetails.Token).success(function (data) {
              
                var Msgg = '';
                if (data == 1) {
                    Mssg = 'Old password did not match!! Try Again';
                    alert(Mssg);
                }
                else {
                    debugger;
                    $window.location.reload();
                    $scope.Msg = 'Password Changed Successfully';
                }
               

            });
        }


        //}


    }
});