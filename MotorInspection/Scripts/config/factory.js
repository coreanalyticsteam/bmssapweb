﻿
MotorInspection.factory('AjaxCall', function ($http, $location, $window) {
    var postData = function (url, data, callBackFn) {
        $http({ method: 'POST', url: config.serviceURL +url,data:JSON.stringify(data), headers: { "Content-Type": "application/json; charset=utf-8",
            //"auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID),
            "UserID": JSON.parse($window.localStorage.getItem('UserDetails')).UserID
        }
        })
           .then(function successCallback(response) {
               callBackFn(response);
           }, function errorCallBack(response) {
               console.log(response);
               //ErrorMsg();
               return null;
           });

    };
    var post = function (url, callBackFn) {
        $http({
            method: 'POST', url: config.serviceURL + url, headers: {
                "Content-Type": "application/json; charset=utf-8",
          // "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID),
            "UserID": JSON.parse($window.localStorage.getItem('UserDetails')).UserID
        }
        })
           .then(function successCallback(response) {
               callBackFn(response);
           }, function errorCallBack(response) {
               console.log(response);
              // ErrorMsg();
               return null;
           });

    };

    var GET = function (url, callBackFn) {
        $http({
            method: 'GET', url: config.serviceURL + url, headers: {
                "Content-Type": "application/json; charset=utf-8",
                // "auth-token": btoa(token + "~" + JSON.parse($window.localStorage.getItem('UserDetails')).UserID),
                "UserID": JSON.parse($window.localStorage.getItem('UserDetails')).UserID
            }
        })
           .then(function successCallback(response) {
               callBackFn(response);
           }, function errorCallBack(response) {
               console.log(response);
               // ErrorMsg();
               return null;
           });

    };
    return {
        postData: postData,
        post: post,
        GET: GET
        //checkToken: checkToken
    };
});