﻿var MotorInspection = angular.module("MotorInspection",);

MotorInspection.directive('fileModel', ['$parse', 'VideoInspectionService', function ($parse, VideoInspectionService) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    debugger;
                    VideoInspectionService.FileData = element[0].files[0];
                    modelSetter(scope.$parent.$parent.myFile, element[0].files[0]);
                });
            });
        }
    };
}]);
MotorInspection.directive("copyToClipboard", copyClipboardDirective);
function copyClipboardDirective() {
    var clip;
    function link(scope, element) {
        function clipboardSimulator() {
            var self = this,
                textarea,
                container;
            function createTextarea() {
                if (!self.textarea) {
                    container = document.createElement('div');
                    container.id = 'simulate-clipboard-container';
                    container.setAttribute('style', ['position: fixed;', 'left: 0px;', 'top: 0px;', 'width: 0px;', 'height: 0px;', 'z-index: 100;', 'opacity: 0;', 'display: block;'].join(''));
                    document.body.appendChild(container);
                    textarea = document.createElement('textarea');
                    textarea.setAttribute('style', ['width: 1px;', 'height: 1px;', 'padding: 0px;'].join(''));
                    textarea.id = 'simulate-clipboard';
                    self.textarea = textarea;
                    container.appendChild(textarea);
                }
            }
            createTextarea();
        }
        clipboardSimulator.prototype.copy = function() {
            this.textarea.innerHTML = '';
            this.textarea.appendChild(document.createTextNode(scope.textToCopy));
            this.textarea.focus();
            this.textarea.select();
            setTimeout(function() {
                document.execCommand('copy');
            }, 20);
        };
        clip = new clipboardSimulator();
        element[0].addEventListener('click', function() {
            clip.copy();
        });
    }
    return {
        restrict: 'A',
        link: link,
        scope: {
            textToCopy: '='
        }
    };
}
MotorInspection.directive('header', function () {
    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element. I don't like creating custom HTML elements
        replace: true,
        scope: { user: '=' }, // This is one of the cool things :). Will be explained in post.
        templateUrl: "/views/header.html",
        controller: "HeaderCtrl" 
    }
});

MotorInspection.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});



MotorInspection.filter('startFrom', function () {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
})
.filter('INR', function () {
    return function (input) {
        if (!isNaN(input)) {
            var currencySymbol = '₹';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            if (result.length > 1) {
                output += "." + result[1];
            }

            return currencySymbol + output;
        }
    }
});