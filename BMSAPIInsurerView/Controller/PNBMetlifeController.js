﻿myApp.controller('PNBMetlifeController', function ($scope, AjaxFactory, $stateParams, $interval) {
    //450708235
    $scope.Timer = 0
    $interval(function () {
        $scope.Timer = $scope.Timer + 1;
    }, 1000);
    $scope.LoadingImg = false;
    $scope.RequestURL = "http://integrationapi.policybazaar.com/Integration/GetBookingStatus/" + $stateParams.ApplicationNo;
    
    AjaxFactory.getData($scope.RequestURL, function (data) {
        $scope.LoadingImg = true;
         console.log(JSON.parse(data.data.Data))
         $scope.CurrentStatus = ""
         $scope.InsurerView = JSON.parse(data.data.Data)
         if($scope.InsurerView.tma.tma_body.additionalrequirements != undefined){
             if ($scope.InsurerView.tma.tma_body.additionalrequirements.counteroffer != undefined) {
                 if ($scope.InsurerView.tma.tma_body.additionalrequirements.counteroffer.currentstep == 'Y') {
                     $scope.InsurerCurrent = $scope.InsurerView.tma.tma_body.additionalrequirements.counteroffer;
                 }
             }
         
             if ($scope.InsurerView.tma.tma_body.additionalrequirements.pendingrequirements != undefined) {
                 if ($scope.InsurerView.tma.tma_body.additionalrequirements.pendingrequirements.currentstep == 'Y') {
                     $scope.InsurerCurrent = $scope.InsurerView.tma.tma_body.additionalrequirements.pendingrequirements;
                 }
             }
         }
         if ($scope.InsurerView.tma.tma_body.documentsuploaded != undefined) {
             if ($scope.InsurerView.tma.tma_body.documentsuploaded.currentstep == 'Y') {
                 $scope.InsurerCurrent = $scope.InsurerView.tma.tma_body.documentsuploaded;
             }
         }
         if ($scope.InsurerView.tma.tma_body.underprocess != undefined) {
             if ($scope.InsurerView.tma.tma_body.underprocess.cpuqcaccept != undefined) {
                 if ($scope.InsurerView.tma.tma_body.underprocess.cpuqcaccept.currentstep == 'Y') {
                     $scope.InsurerCurrent = $scope.InsurerView.tma.tma_body.underprocess.cpuqcaccept;
                 }

             }
             if ($scope.InsurerView.tma.tma_body.underprocess.underwriter != undefined){
                 if ($scope.InsurerView.tma.tma_body.underprocess.underwriter.currentstep == 'Y') {
                     $scope.InsurerCurrent = $scope.InsurerView.tma.tma_body.underprocess.underwriter;

                 }
             }
         }


         if ($scope.InsurerView.tma.tma_body.currentstep == "additionalrequirements" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "Pending Requirements")
             $scope.CurrentStatus = "Additional Requirements/Pending Requirements/WIP || Case Login(Additional Information Required)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "additionalrequirements" && $scope.InsurerCurrent.status == "completed" && $scope.InsurerCurrent.swiftstatus == "Pending Requirements")
             $scope.CurrentStatus = "Additional Requirements/Pending Requirements/completed || Case Login(Additional Information Required)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "additionalrequirements" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "Counter Offer")
             $scope.CurrentStatus = "Additional Requirements/WIP/Counter Offer || Case Login (Counter Offer Initiated)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "documentsuploaded" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "DFQC_Pending")
             $scope.CurrentStatus = "Documents uploaded/DFQC_Pending/WIP || Documents &Medical (Docs & Medical Pending)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "documentsuploaded" && $scope.InsurerCurrent.status == "Rejected" && $scope.InsurerCurrent.swiftstatus == "DFQC_Reject")
             $scope.CurrentStatus = "Documents uploaded/DFQC_Reject/Rejected || Documents &Medical (Docs & Medical Pending)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "documentsuploaded" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "DFQC_Accepted")
             $scope.CurrentStatus = "Documents uploaded/DFQC_Accepted/WIP || Case Login(Case Logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "epdstatus")
             $scope.CurrentStatus = "Epdstatus/EPD/not done"
         if ($scope.InsurerView.tma.tma_body.currentstep == "Field 5 @ Policy Issued")
             $scope.CurrentStatus = "Field 5 @ Policy Issued"
         if ($scope.InsurerView.tma.tma_body.currentstep == "Fields to Mapped")
             $scope.CurrentStatus = "Fields to Mapped"

         if ($scope.InsurerView.tma.tma_body.currentstep == "finaldecision" && $scope.InsurerCurrent.status == "completed" && $scope.InsurerCurrent.swiftstatus == "WITHDRAWN")
             $scope.CurrentStatus = "Finaldecision/WITHDRAWN/completed || Rejected(Post Login)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "finaldecision" && $scope.InsurerCurrent.status == "completed" && $scope.InsurerCurrent.swiftstatus == "DECLINED")
             $scope.CurrentStatus = "Final Decision/DECLINED/completed || Rejected(Post Login)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "finaldecision" && $scope.InsurerCurrent.status == "completed" && $scope.InsurerCurrent.swiftstatus == "INFORCE")
             $scope.CurrentStatus = "Finaldecision/INFORCE/completed || Policy Issued"

         if ($scope.InsurerView.tma.tma_body.currentstep == "opsreject")
             $scope.CurrentStatus = "Opsreject"
         if ($scope.InsurerView.tma.tma_body.currentstep == "pdreceived")
             $scope.CurrentStatus = "PD Received/PD Dispatch/completed || Policy Issued"
         if ($scope.InsurerView.tma.tma_body.currentstep == "pdsend")
             $scope.CurrentStatus = "PD send/completed || Policy Issued"
         if ($scope.InsurerView.tma.tma_body.currentstep == "Policy Term")
             $scope.CurrentStatus = "Policy Term"
         if ($scope.InsurerView.tma.tma_body.currentstep == "Remarks @ Policy Issued  -5")
             $scope.CurrentStatus = "Remarks @ Policy Issued  -5"

         if ($scope.InsurerView.tma.tma_body.currentstep == "underprocess" && $scope.InsurerCurrent.status == "completed" && $scope.InsurerCurrent.swiftstatus == "Underwriting")
             $scope.CurrentStatus = "Under Process/Underwriting/completed || Case Login (Case logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "underprocess" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "Underwriting")
             $scope.CurrentStatus = "Under Process/Underwriting/WIP || Case Login (Case logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "underprocess" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "Refer To Senior UW")
             $scope.CurrentStatus = "Under Process/Refer To Senior UW/WIP || Case Login (Case logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "underprocess" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "Refer To Supervisor")
             $scope.CurrentStatus = "Under Process/Refer To Supervisor/WIP || Case Login (Case logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "underprocess" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "Refer To Re-Insurer")
             $scope.CurrentStatus = "Under Process/Refer To Re-Insurer/WIP || Case Login (Case logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "underprocess" && $scope.InsurerCurrent.status == "WIP" && $scope.InsurerCurrent.swiftstatus == "Refer To Vendor")
             $scope.CurrentStatus = "Under Process/Refer To Vendor/WIP || Case Login (Case logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "underprocess" && $scope.InsurerCurrent.status == "completed" && $scope.InsurerCurrent.swiftstatus == "Refer To Senior UW")
             $scope.CurrentStatus = "Under Process/Refer To Senior UW/ completed || Case Login (Case logged)"
         if ($scope.InsurerView.tma.tma_body.currentstep == "waitingforpd")
             $scope.CurrentStatus = "Waiting for PD || Policy Issued"
         if($scope.CurrentStatus.length == 0)
             $scope.CurrentStatus = $scope.InsurerView.tma.tma_body.currentstep


        
    });
});