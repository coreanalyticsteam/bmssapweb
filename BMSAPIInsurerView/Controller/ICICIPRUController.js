﻿myApp.controller('ICICIPRUController', function ($scope, AjaxFactory, $stateParams) {
    $scope.LoadingImg = false;
    //debugger;
    $scope.ReqICICI = { InputKey: $stateParams.Id }
    $scope.BookingId = $stateParams.BookingId
    ///$scope.RequestURL = APIURL + 'ApplicationNo=' + $stateParams.Id + '&SupplierId=5&ProductId=7&LeadId=' + $stateParams.LeadID + '&Hit=1';
    $scope.RequestURL = APIURL + 'ICICIPru';
    AjaxFactory.postData($scope.RequestURL, { Json: $scope.ReqICICI }, function (data) {
        $scope.InsurerView = JSON.parse(data.data.result.Success);
        $scope.InsurerViewDetail = $scope.InsurerView[0];
        
        for (var i in $scope.InsurerView) {
            $scope.InsurerView[i].RECEIVEDDATE = new Date($scope.InsurerView[i].RECEIVEDDATE);
            $scope.InsurerView[i].REQUESTDATE = new Date($scope.InsurerView[i].REQUESTDATE);
            $scope.InsurerView[i].MED_REQUESTDATE = new Date($scope.InsurerView[i].MED_REQUESTDATE);
            $scope.InsurerView[i].EXAMDATE = new Date($scope.InsurerView[i].EXAMDATE);
            $scope.InsurerView[i].COPSRECDDATE = new Date($scope.InsurerView[i].COPSRECDDATE);
        }
        $scope.InsurerViewDetail.SUBMISSIONDATE = new Date($scope.InsurerViewDetail.SUBMISSIONDATE);
       // console.log($scope.Insurer.ApplicationNoInfo['Application History'])
    });
});