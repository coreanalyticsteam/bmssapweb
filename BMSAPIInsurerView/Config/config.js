﻿myApp.config(function ($stateProvider, $urlRouterProvider) {
    //var helloState = {
    //    name: 'hello',
    //    url: '/hello',
    //    template: '<h3>hello world!</h3>'
    //}

    //var aboutState = {
    //    name: 'about',
    //    url: '/about',
    //    template: '<h3>Its the UI-Router hello world app!</h3>'
    //}

    //$stateProvider.state(helloState);
    //$stateProvider.state(aboutState);
  //  $urlRouterProvider.otherwise("PNBMetlife");
    $stateProvider
        .state('ICICIPRU', {
            name: 'ICICIPRU',
            url: '/ICICIPRU/:Id/:BookingId',
            templateUrl: 'View/ICICIPRU.html',
            controller: 'ICICIPRUController'
        })
    .state('PNBMetlife', {
        name: 'PNBMetlife',
        url: '/PNBMetlife/:ApplicationNo',
        templateUrl: 'View/PNBMetlife.html',
        controller: 'PNBMetlifeController'
    })
    ;
});
