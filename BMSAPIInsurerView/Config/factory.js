﻿
myApp.factory('AjaxFactory', function ($http, $location) {
    var postData = function (url, data, callBackFn) {
        $http({ method: 'POST', url: url, headers: { 'Content-Type' :  'application/json'}, data: JSON.stringify(data) })
           .then(function successCallback(response) {
               callBackFn(response);
           }, function errorCallBack(response) {
               console.log(response);
              // ErrorMsg();
               return null;
           });

    };
    var getData = function (url, callBackFn) {
        $http({ method: 'GET', url: url, headers: { 'Content-Type': 'application/json', 'Authorization': 'cG9saWN5 YmF6YWFy' } })
           .then(function successCallback(response) {
               callBackFn(response);
           }, function errorCallBack(response) {
               console.log(response);
              // ErrorMsg();
               return null;
           });

    };
    return {
        postData: postData,
        getData: getData
        //checkToken: checkToken
    };
});