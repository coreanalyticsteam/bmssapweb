﻿CallTransfer.controller("CallTransferCtrl", function ($scope, CallTransferService, $routeParams, $uibModal) {
    
    $scope.UserID = $routeParams.UserID;
    $scope.EmployeeID = $routeParams.EmployeeID;
    $scope.LeadID = $routeParams.LeadID;
    $scope.MobileNo = atob($routeParams.MobileNo);    
    var objDialerSearchRequest=undefined;
    $scope.LeadAgentList = [];
    $scope.AgentStatusList = [];
    $scope.txtSearchAgentList = []
    $scope.ProductList = [{ "ProductID": "-1", "ProductName": "Select Product" }];
    $scope.GroupList = [{ "GroupID": "-1", "GroupName": "Select Group" }];
    $scope.ProcessType = ProcessType;
    $scope.Selected = { "ProcessType": {} };
    $scope.Selected.AgentNameEmpID = '';
    $scope.Selected.ProcessType = { "value": "-1", "text": "Select Process" };
    $scope.Selected.Product = { "ProductID": "-1", "ProductName": "Select Product" };
    $scope.Selected.Group = { "GroupID": "-1", "GroupName": "Select Group" };
    $scope.IsCustOnHold = false;

    $scope.Refresh = function (type) {
        //var confirmChk = confirm("Are you sure to refresh the status, after refresh all ");
        if(!$scope.IsCustOnHold){
            var count, LeadIDs = [];
            if (type == 1) {
                for (var i = 0; i < $scope.LeadAgentList.length; i++) {
                    count = i + 1;
                    LeadIDs.push($scope.LeadAgentList[i].AgentEmpID);
                }
            }
            else if (type == 2) {           
                for (var i = 0; i < $scope.SearchAgentList.length; i++) {
                    count = i + 1;
                    LeadIDs.push($scope.SearchAgentList[i].AgentEmpID);
                }
            }
            CallTransferService.GetDialerAgentStatus(LeadIDs).success(function (data) {
                debugger;
                if (type == 1) {
                    $scope.AgentStatusList = data;
                    for (var i = 0; i < $scope.LeadAgentList.length; i++) {
                        for (var j = 0; j < $scope.AgentStatusList.length; j++) {
                            if ($scope.AgentStatusList[j].agentId == $scope.LeadAgentList[i].AgentEmpID) {
                                $scope.LeadAgentList[i].DialerStatus = $scope.AgentStatusList[j].Status;
                                if ($scope.AgentStatusList[j].Status == 'READY' || $scope.AgentStatusList[j].Status == 'LOGGEDIN') {
                                    $scope.LeadAgentList[i].CallTransfer = true;
                                }
                                else {
                                    $scope.LeadAgentList[i].CallTransfer = false;
                                }
                                $scope.LeadAgentList[i].Unhold = false;
                                $scope.LeadAgentList[i].CallConferance = false;
                                $scope.LeadAgentList[i].DisconnectCall = false;                            
                            }
                        }
                    }
                }
                else if (type == 2) {
                    $scope.SearchAgentStatusList = data;
                    for (var i = 0; i < $scope.SearchAgentList.length; i++) {
                        for (var j = 0; j < $scope.SearchAgentStatusList.length; j++) {
                            if ($scope.SearchAgentList[j].agentId == $scope.LeadAgentList[i].AgentEmpID) {
                                $scope.SearchAgentList[i].DialerStatus = $scope.SearchAgentStatusList[j].Status;
                                if ($scope.SearchAgentStatusList[j].Status == 'READY' || $scope.SearchAgentStatusList[j].Status == 'LOGGEDIN') {
                                    $scope.SearchAgentList[i].CallTransfer = true;
                                }
                                else {
                                    $scope.SearchAgentList[i].CallTransfer = false;
                                }
                                $scope.SearchAgentList[i].Unhold = false;
                                $scope.SearchAgentList[i].CallConferance = false;
                                $scope.SearchAgentList[i].DisconnectCall = false;
                            }
                        }
                    }

                }
            });

        }
        else {
            alert("Please put customer on unhold then refresh.");
        }
    }
    $scope.onSelect = function ($item, $model, $label) {
        $scope.SearchAgentList = [];
        debugger;
        $scope.SearchAgentList .push( $scope.Selected.SearAgentDetails);
        debugger;
    };
    $scope.GetDialerAgentList = function () {
        objDialerSearchRequest = {
            "LeadID": $scope.LeadID, "ProductID": "0", "AgentType": "0", "GroupID": "0", "AgentNameEmpID": "0", "Type": "1"
        };
        CallTransferService.GetDialerAgentList(objDialerSearchRequest).success(function (data) {
            debugger;
            $scope.LeadAgentList = data;
            var count, LeadIDs=[];
            for (var i = 0; i < $scope.LeadAgentList.length; i++) {
                count = i + 1;
                LeadIDs.push($scope.LeadAgentList[i].AgentEmpID);
            }
            debugger;
            if (LeadIDs.length > 0) {
                CallTransferService.GetDialerAgentStatus(LeadIDs, $scope.UserID).success(function (data) {
                    debugger;
                    $scope.AgentStatusList = data;
                    for (var i = 0; i < $scope.LeadAgentList.length; i++) {
                        for (var j = 0; j < $scope.AgentStatusList.length; j++) {
                            if ($scope.AgentStatusList[j].agentId == $scope.LeadAgentList[i].AgentEmpID) {
                                $scope.LeadAgentList[i].DialerStatus = $scope.AgentStatusList[j].Status;
                                if ($scope.AgentStatusList[j].Status == 'READY' || $scope.AgentStatusList[j].Status == 'LOGGEDIN') {
                                    $scope.LeadAgentList[i].CallTransfer = true;
                                }
                                else {
                                    $scope.LeadAgentList[i].CallTransfer = false;
                                }
                                $scope.LeadAgentList[i].Unhold = false;
                                $scope.LeadAgentList[i].CallConferance = false;
                                $scope.LeadAgentList[i].DisconnectCall = false;
                                //$scope.LeadAgentList[i].StateTime = $scope.AgentStatusList[j].StateTime;
                            }
                        }
                    }
                });
            }
           
        });
    }

    $scope.fnCallTransfer = function (agent) {

        //Put Customer on Hold and establish a call between 1st Agent and 2nd agent.
        //http://10.0.9.66/callconfreance/new1.php?phone=8510022035&campaign=testibobtransfer&emp1=PW00735&emp2=ET01189&leadid=142536
        var confirmChk = confirm("Are you sure to transfer the call to " + agent.AgentName);
        if (confirmChk == true) {
            $scope.IsCustOnHold = true;
            var req = "phone=" + $scope.MobileNo + "&campaign=" + config.easydialCampaign + "&emp1=" + $scope.EmployeeID + "&emp2=" + agent.AgentEmpID + "&leadid=" + $scope.LeadID;
            CallTransferService.CallTransferAPI(req, $scope.UserID).success(function (data) {

                debugger;
                agent.CallTransfer = false;
                agent.Unhold = true;
                agent.CallConferance = true;
                agent.DisconnectCall = false;
                var LogObj = { "objLogReqDetails": { "LeadID": $scope.LeadID, "FromAgentID": $scope.EmployeeID, "ToAgentID": agent.AgentEmpID, "MobileNo": $scope.MobileNo, "EventType": 1 } };
                CallTransferService.InsertCTCLog(LogObj).success(function (data) {

                });
                debugger;
            });
        }
        else {

        }
    }
    
    $scope.fnUnhold = function (agent) {
        //Grab the customer from Hold and start Conversation
        //http://10.0.9.66/callconfreance/new3.php?phone=8510022035&campaign=testibobtransfer
        var confirmChk = confirm("Are you sure to unhold customer");
        if (confirmChk) {
            $scope.IsCustOnHold = false;
            var req = "phone=" + $scope.MobileNo + "&campaign=" + config.easydialCampaign;
            CallTransferService.UnholdAPI(req, $scope.UserID).success(function (data) {
                agent.CallTransfer = true;
                agent.Unhold = false;
                agent.CallConferance = false;
                agent.DisconnectCall = true;
                debugger;
            });
        }
    }
    $scope.fnCallConferance = function (agent) {
        //Put Everyone(1st agent, 2nd agent and Customer in Conference)
        //http://10.0.9.66/callconfreance/new2.php?phone=8510022035&campaign=testibobtransfer
        var confirmChk = confirm("Are you sure to enable call conference.");
        if (confirmChk) {
            $scope.IsCustOnHold = false;
            var req = "phone=" + $scope.MobileNo + "&campaign=" + config.easydialCampaign;
            CallTransferService.CallConferanceAPI(req, $scope.UserID).success(function (data) {
                agent.CallTransfer = false;
                agent.Unhold = false;
                agent.CallConferance = false;
                agent.DisconnectCall = true;
                debugger;
            });
        }
    }
    $scope.fnDisconnectCall = function (agent) {

        //http://10.0.9.66/callconfreance/new4.php?phone=8510967375&campaign=testibobtransfer
        var confirmChk = confirm("Are you sure to disconnect the call");
        if (confirmChk == true) {
            var req = "phone=" + $scope.MobileNo + "&campaign=" + config.easydialCampaign;
            CallTransferService.DisconnectCallAPI(req).success(function (data) {
                agent.CallTransfer = false;
                agent.Unhold = false;
                agent.CallConferance = false;
                agent.DisconnectCall = false;
                debugger;
            });
        }
    }

  

    $scope.GetSearchAgentList = function (searchText) {
        //$scope.txtSearchAgentList = [];
       // $scope.txtSearchAgentList = undefined;
        if (!searchText) {
            return [];
        };

        if ($scope.Selected.ProcessType.value != -1 && $scope.Selected.Product.ProductID != -1 && $scope.Selected.Group.GroupID != -1 && $.trim(searchText) != '') {
            chk = 1;
            objDialerSearchRequest = {
                "LeadID": $scope.LeadID,
                "ProductID": $scope.Selected.Product.ProductID,
                "AgentType": $scope.Selected.ProcessType.value,
                "GroupID": $scope.Selected.Group.GroupID,
                "AgentNameEmpID": $.trim(searchText),
                "Type": "2"
            };

        }
        else if ($scope.Selected.ProcessType.value != -1 && $scope.Selected.Product.ProductID != -1 && $scope.Selected.Group.GroupID == -1 && $.trim(searchText) != '') {
            chk = 1;
            objDialerSearchRequest = {
                "LeadID": $scope.LeadID,
                "ProductID": $scope.Selected.Product.ProductID,
                "AgentType": $scope.Selected.ProcessType.value,
                "GroupID": "0",
                "AgentNameEmpID": $.trim(searchText),
                "Type": "2"
            };

        }

       
        CallTransferService.GetDialerAgentList(objDialerSearchRequest, $scope.UserID).success(function (data) {
            //$scope.txtSearchAgentList = [];
            var TempData=data;
            //$scope.txtSearchAgentList = data;
            var count, LeadIDs = [];
            for (var i = 0; i < TempData.length; i++) {
                count = i + 1;
                LeadIDs.push(TempData[i].AgentEmpID);
            }
            if (LeadIDs.length > 0) {
                CallTransferService.GetDialerAgentStatus(LeadIDs, $scope.UserID).success(function (data) {

                    $scope.txtSearchAgentStatusList = data;
                    for (var i = 0; i < TempData.length; i++) {
                        for (var j = 0; j < $scope.txtSearchAgentStatusList.length; j++) {
                            if ($scope.txtSearchAgentStatusList[j].agentId == TempData[i].AgentEmpID) {
                                TempData[i].DialerStatus = $scope.txtSearchAgentStatusList[j].Status;
                                if ($scope.txtSearchAgentStatusList[j].Status == 'READY' || $scope.txtSearchAgentStatusList[j].Status == 'LOGGEDIN') {
                                    //debugger;
                                    TempData[i].CallTransfer = true;
                                }
                                else {
                                    TempData[i].CallTransfer = false;
                                }
                                TempData[i].Unhold = false;
                                TempData[i].CallConferance = false;
                                TempData[i].DisconnectCall = false;
                            }
                        }
                    }
                    $scope.txtSearchAgentList = TempData;
                    //debugger;

                });
            }
            else {
                $scope.txtSearchAgentList = TempData;
            }

        })
        
        return $scope.txtSearchAgentList;
        
    };

    $scope.SearchAgent = function () {
        debugger
        var chk = 0;
        var txtAgent = "";
        if ($scope.Selected.SearAgentDetails == "[object Object]") {
            txtAgent = $.trim($scope.Selected.SearAgentDetails.AgentEmpID);
        }
        else {
            var txtAgent = $.trim($scope.Selected.SearAgentDetails);
        }
        //var txtAgent = $.trim($scope.Selected.SearAgentDetails);
        if ($scope.Selected.ProcessType.value != -1 && $scope.Selected.Product.ProductID != -1 && $scope.Selected.Group.GroupID != -1 && txtAgent == '') {
            chk = 1;
            objDialerSearchRequest = {
                "LeadID": $scope.LeadID,
                "ProductID": $scope.Selected.Product.ProductID,
                "AgentType": $scope.Selected.ProcessType.value,
                "GroupID": $scope.Selected.Group.GroupID,
                //"AgentNameEmpID": "0",
                "Type": "2"
            };

        }
        else if ($scope.Selected.ProcessType.value != -1 && $scope.Selected.Product.ProductID != -1 && $scope.Selected.Group.GroupID != -1 && txtAgent != '') {
            chk = 1;
            objDialerSearchRequest = {
                "LeadID": $scope.LeadID,
                "ProductID": $scope.Selected.Product.ProductID,
                "AgentType": $scope.Selected.ProcessType.value,
                "GroupID": $scope.Selected.Group.GroupID,
                "AgentNameEmpID": txtAgent,
                "Type": "2"
            };

        }
        else if ($scope.Selected.ProcessType.value != -1 && $scope.Selected.Product.ProductID != -1 && $scope.Selected.Group.GroupID == -1 && txtAgent != '') {
            chk = 1;
            objDialerSearchRequest = {
                "LeadID": $scope.LeadID,
                "ProductID": $scope.Selected.Product.ProductID,
                "AgentType": $scope.Selected.ProcessType.value,
                "GroupID": "0",
                "AgentNameEmpID": txtAgent,
                "Type": "2"
            };

        }
        if (chk == 1) {
            CallTransferService.GetDialerAgentList(objDialerSearchRequest, $scope.UserID).success(function (data) {
                debugger;
                $scope.SearchAgentList = data;
                var count, LeadIDs = [];
                for (var i = 0; i < $scope.SearchAgentList.length; i++) {
                    count = i + 1;
                    LeadIDs.push($scope.SearchAgentList[i].AgentEmpID);
                }
                debugger;
                if ($scope.SearchAgentList.length > 0) {
                    if (LeadIDs.length > 0) {
                        CallTransferService.GetDialerAgentStatus(LeadIDs, $scope.UserID).success(function (data) {
                            debugger;
                            $scope.SearchAgentStatusList = data;
                            for (var i = 0; i < $scope.SearchAgentList.length; i++) {
                                for (var j = 0; j < $scope.SearchAgentStatusList.length; j++) {
                                    if ($scope.SearchAgentStatusList[j].agentId == $scope.SearchAgentList[i].AgentEmpID) {
                                        $scope.SearchAgentList[i].DialerStatus = $scope.SearchAgentStatusList[j].Status;
                                        if ($scope.SearchAgentStatusList[j].Status == 'READY' || $scope.SearchAgentStatusList[j].Status == 'LOGGEDIN') {
                                            $scope.SearchAgentList[i].CallTransfer = true;
                                        }
                                        else {
                                            $scope.SearchAgentList[i].CallTransfer = false;
                                        }
                                        $scope.SearchAgentList[i].Unhold = false;
                                        $scope.SearchAgentList[i].CallConferance = false;
                                        $scope.SearchAgentList[i].DisconnectCall = false;
                                    }
                                }
                            }
                        });
                    }
                }
                else {
                    alert('No record found.');                
                }

            });
           
        }
        else {
            alert('Please select valid input');
        }

       
    }
    $scope.BindProductList = function () {
        if ($scope.Selected.ProcessType.value != -1) {
            CallTransferService.GetProductList($scope.Selected.ProcessType.value).success(function (data) {
                debugger;
                $scope.ProductList = data;
                $scope.ProductList.push({ "ProductID": "-1", "ProductName": "Select Product" });
            });
            $scope.Selected.Product = { "ProductID": "-1", "ProductName": "Select Product" };
            $scope.Selected.Group = { "GroupID": "-1", "GroupName": "Select Group" };
        }
        else {
            $scope.Selected.Product = { "ProductID": "-1", "ProductName": "Select Product" };
            $scope.Selected.Group = { "GroupID": "-1", "GroupName": "Select Group" };
            $scope.ProductList = [{ "ProductID": "-1", "ProductName": "Select Product" }];
            $scope.GroupList = [{ "GroupID": "-1", "GroupName": "Select Group" }];
        }
    }

    $scope.BindGroupList = function () {
        if ($scope.Selected.ProcessType.value != -1 && $scope.Selected.Product.ProductID != -1) {
            CallTransferService.GetGroupList($scope.Selected.ProcessType.value, $scope.Selected.Product.ProductID).success(function (data) {
                debugger;
                $scope.GroupList = data;
                $scope.GroupList.push({ "GroupID": "-1", "GroupName": "Select Group" });
            });
        }
        else {
            $scope.GroupList = [{ "GroupID": "-1", "GroupName": "Select Group" }];
        }
    }


    $scope.GetDialerAgentList();   

});
