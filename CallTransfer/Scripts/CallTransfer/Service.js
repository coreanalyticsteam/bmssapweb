﻿
CallTransfer.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
});

CallTransfer.service("CallTransferService", function ($http) {
    
    this.GetDialerAgentList = function (objDialerSearchRequest) {
      
       // this.Loggin(config.easydialGetStatusURL + "::" +JSON.stringify( objDialerSearchRequest));
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "Dialer.svc/GetDialerAgentList",
            data: JSON.stringify(objDialerSearchRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.GetDialerAgentStatus = function (objDialerSearchRequest) {
        this.Loggin(config.easydialGetStatusURL + "::" + JSON.stringify(objDialerSearchRequest));    
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.easydialGetStatusURL ,
            data: JSON.stringify(objDialerSearchRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           debugger;
           alert(error);
       });
        return request;
    }

    this.Loggin = function (Request) {
        
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "Dialer.svc/Loggin",
            data: JSON.stringify(Request)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.CallTransferAPI = function (objDialer) {
       
       this.Loggin(config.easydialCallTransferURL + "::" + objDialer);     
        var request = $http({
            method: "get",           
            transformResponse: [function (data) {                
                return data;
            }],
            url: config.easydialCallTransferURL + objDialer,
            
        })
       .error(function (error) {
           debugger;
           alert(error);
       });
       return request;
     
    }
    this.UnholdAPI = function (objDialer) {
        this.Loggin(config.easydialUnholdURL + "::" + objDialer);
        debugger;
        var request = $http({
            method: "get",
            transformResponse: [function (data) {
                return data;
            }],
            url: config.easydialUnholdURL + objDialer
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }
    this.CallConferanceAPI = function (objDialer) {
        this.Loggin(config.easydialCallConferanceURL + "::" + objDialer);
        debugger;
        var request = $http({
            method: "get",
            transformResponse: [function (data) {
                return data;
            }],
            url: config.easydialCallConferanceURL + objDialer
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }
    this.DisconnectCallAPI = function (objDialer) {
        this.Loggin(config.easydialDisconnectCallURL + "::" + objDialer);
        debugger;
        var request = $http({
            method: "get",
            transformResponse: [function (data) {
                return data;
            }],
            url: config.easydialDisconnectCallURL + objDialer
        })
       .error(function (error) {
           //alert(error);
       });
        return request;
    }

   

    this.GetProductList = function (AgentType) {
        //this.Loggin(config.easydialGetStatusURL + "::" + JSON.stringify(objDialerSearchRequest));
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "Dialer.svc/GetProductList/" + AgentType
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    }

    this.GetGroupList = function (AgentType, ProductID) {
        //this.Loggin(config.easydialGetStatusURL + "::" + JSON.stringify(objDialerSearchRequest));
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "Dialer.svc/GetGroupList/" + AgentType + "/" + ProductID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    }

    this.InsertCTCLog = function (objLogReqDetails) {
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "Dialer.svc/InsertCTCLog",
            data: JSON.stringify(objLogReqDetails)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

   
   
});

