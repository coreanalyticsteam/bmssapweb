﻿DocumentMappingMaster.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
});
DocumentMappingMaster.service("DocumentMappingMasterService", function ($http) {
    
    this.FileData = [];
    this.GetMasterDropDownData = function (FilterID, ProductId, Type) {
        return $http({
            method: "GET",
            url: config.serviceURL + "DocRepositoryService.svc/GetMasterDropDownData/" + FilterID + "/" + ProductId + "/" + Type
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });
    }

    this.GetMasterDocumentDetails = function () {
        return $http({
            method: "GET",
            url: config.serviceURL + "DocRepositoryService.svc/GetMasterDocumentDetails"
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });
    }

    this.UpdateDocumentDetails = function (objReqDocUploadRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/UpdateDocumentDetails",
            data: JSON.stringify(objReqDocUploadRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }


    this.InsertDocumentMapping = function (objMasterDocumentMappingRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "DocRepositoryService.svc/InsertDocumentMapping",
            data: JSON.stringify(objMasterDocumentMappingRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }


    this.GetDocumentMasterDetails = function (ProductID, SupplierID) {
        return $http({
            method: "GET",
            url: config.serviceURL + "DocRepositoryService.svc/GetDocumentMasterDetails/" + ProductID + "/" + SupplierID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });
    }

    this.GetDocumentMasterDetailsEdit = function (GroupID ) {
        return $http({
            method: "GET",
            url: config.serviceURL + "DocRepositoryService.svc/GetDocumentMasterDetailsEdit/" + GroupID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });
    }
    
});

