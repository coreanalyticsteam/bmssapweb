﻿DocumentMappingMaster.controller("DocumentMappingMasterController", function ($scope, DocumentMappingMasterService, $routeParams) {
   
    $scope.UserID = $routeParams.userID;
    $scope.Selected = {
        "Product": undefined,
        //"Status": undefined, "SubStatus": undefined,
        "Supplier": [], "STP": [], "Portability": [], "MedicalOrInspection": [], "Document": [], "SelectedSupplier": undefined
    };
    $scope.Selected.Supplier = [];
    $scope.ProductList = [];
    $scope.SupplierList = [];
    //$scope.StatusList = [];
    //$scope.SubStatusList = [];
    $scope.DocCategoryList = [];
    $scope.DocumentList = [];
    $scope.DocumentMasterDetails = [];
    $scope.CommonData = CommonData;
   
    $scope.GetMasterDocumentDetails = function () {
        DocumentMappingMasterService.GetMasterDocumentDetails().success(function (data) {
            debugger;
            $scope.DocCategoryList = data.DocCategoryList;
            $scope.DocumentList = data.DocumentList;
        });
    }

    $scope.GetDocumentMasterDetails = function () {
        DocumentMappingMasterService.GetDocumentMasterDetails($scope.Selected.Product.id, $scope.Selected.SelectedSupplier.id).success(function (data) {
            debugger;
            $scope.DocumentMasterDetails = data;            
        });
    }

    $scope.GetMasterDocumentDetails();

    $scope.GetProductList = function (FilterID, Type) {
        var ProductId = 0;
        if (Type == 2) {
            FilterID = $scope.Selected.Product.id;
        }
        else if (Type == 4) {
            FilterID = $scope.Selected.Status.id;
            ProductId = $scope.Selected.Product.id;
        }

        DocumentMappingMasterService.GetMasterDropDownData(FilterID, ProductId, Type).success(function (FieldList) {
            if (Type == 1) {
                $scope.ProductList = FieldList;
            }
            else if (Type == 2) {
                if (FilterID != undefined) {

                    $scope.SupplierList = FieldList;

                    DocumentMappingMasterService.GetMasterDropDownData(FilterID, ProductId, 3).success(function (FieldList) {
                        $scope.StatusList = FieldList;
                    });
                }
            }
            else if (Type == 4) {
                if (FilterID != undefined) {
                    $scope.SubStatusList = FieldList;
                }
            }
            debugger;
        });
    }

    $scope.GetProductList(0, 1);
    
    $scope.SupplierSettings = {
        smartButtonMaxItems: 3,//selectionLimit: 2,
        smartButtonTextConverter: function (itemText, originalItem) {
            if (itemText === 'Jhon') {
                return 'Jhonny!';
            }
            return itemText;
        }
    };

    $scope.InsertDocumentMapping = function () {
        var SupplierIDs = "", StpIDs = "", PortabilityIDs = "", MedInspectionIDs = "", DocumentIDs = "";
        for (var i = 0 ; i < $scope.Selected.Supplier.length; i++)
        {
            if (i == $scope.Selected.Supplier.length - 1) {
                SupplierIDs =SupplierIDs+ $scope.Selected.Supplier[i].id ;
            }
            else {
                SupplierIDs = SupplierIDs + $scope.Selected.Supplier[i].id + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.STP.length; i++) {
            if (i == $scope.Selected.STP.length - 1) {
                StpIDs = StpIDs + $scope.Selected.STP[i];
            }
            else {
                StpIDs = StpIDs + $scope.Selected.STP[i] + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.Portability.length; i++) {
            if (i == $scope.Selected.Portability.length - 1) {
                PortabilityIDs = PortabilityIDs + $scope.Selected.Portability[i];
            }
            else {
                PortabilityIDs = PortabilityIDs + $scope.Selected.Portability[i] + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.MedicalOrInspection.length; i++) {
            if (i == $scope.Selected.MedicalOrInspection.length - 1) {
                MedInspectionIDs = MedInspectionIDs + $scope.Selected.MedicalOrInspection[i];
            }
            else {
                MedInspectionIDs = MedInspectionIDs + $scope.Selected.MedicalOrInspection[i] + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.Document.length; i++) {
            if (i == $scope.Selected.Document.length - 1) {
                DocumentIDs = DocumentIDs + $scope.Selected.Document[i];
            }
            else {
                DocumentIDs = DocumentIDs + $scope.Selected.Document[i] + ",";
            }
        }
        var objMasterDocumentMappingRequest = {
            "ProductID": $scope.Selected.Product.id,
            "SupplierIDs": SupplierIDs,
            //"StatusID": $scope.Selected.Status.id,
            //"SubStatusID": $scope.Selected.SubStatus.id,
            "StpIDs": StpIDs,
            "PortabilityIDs": PortabilityIDs,
            "MedInspectionIDs": MedInspectionIDs,
            "DocCategoryID": $scope.Selected.DocCategory.id,
            "DocumentIDs": DocumentIDs,
            "CreatedBy": $scope.UserID
        };

        

        DocumentMappingMasterService.InsertDocumentMapping(objMasterDocumentMappingRequest).success(function (data) {
            debugger;
            if (parseInt(data) > 0) {
                alert('Mapping updated successfully');
                $location.path("/DocumentMappingMaster/AddNew/" + $scope.UserID);;
            }
            else {
                alert('Document updated partially');
            }
        });
    }

    //"STP": [], "Portability": [], "MedicalOrInspection": [], "Document": []
    // toggle selection for a given employee by name
 
    $scope.toggleSelectionDocument = function (id) {
        var idx = $scope.Selected.Document.indexOf(id);
        // is currently selected
        if (idx > -1) {
            $scope.Selected.Document.splice(idx, 1);
        }
            // is newly selected
        else {
            $scope.Selected.Document.push(id);
        }
    };

    $scope.toggleSelectionSTP = function (a, type) {
        debugger;
        var chk = 0;


        if (type == 'STP') {

            if (a.Checked && a.id == -1) {
                if ($scope.Selected.STP.length > 0) {
                    chk = 1;
                }
            }
            else if (a.Checked && a.id != -1) {
                if ($scope.Selected.STP.indexOf("-1") > 0) {
                    chk = 1;
                }
            }
            if (chk == 0) {
                var idx = $scope.Selected.STP.indexOf(a.id);
                // is currently selected
                if (idx > -1) {
                    $scope.Selected.STP.splice(idx, 1);
                }
                    // is newly selected
                else {
                    $scope.Selected.STP.push(a.id);
                }
            }
        }
        else if (type == 'POR') {
            if (a.Checked && a.id == -1) {
                if ($scope.Selected.Portability.length > 0) {
                    chk = 1;
                }
            }
            else if (a.Checked && a.id != -1) {
                if ($scope.Selected.Portability.indexOf("-1") > 0) {
                    chk = 1;
                }
            }
            if (chk == 0) {
                var idx = $scope.Selected.Portability.indexOf(a.id);
                // is currently selected
                if (idx > -1) {
                    $scope.Selected.Portability.splice(idx, 1);
                }
                    // is newly selected
                else {
                    $scope.Selected.Portability.push(a.id);
                }
            }
        }
        else if (type == 'MED') {
            if (a.Checked && a.id == -1) {
                if ($scope.Selected.MedicalOrInspection.length > 0) {
                    chk = 1;
                }
            }
            else if (a.Checked && a.id != -1) {
                if ($scope.Selected.MedicalOrInspection.indexOf("-1") > 0) {
                    chk = 1;
                }
            }
            if (chk == 0) {
                var idx = $scope.Selected.MedicalOrInspection.indexOf(a.id);
                // is currently selected
                if (idx > -1) {
                    $scope.Selected.MedicalOrInspection.splice(idx, 1);
                }
                    // is newly selected
                else {
                    $scope.Selected.MedicalOrInspection.push(a.id);
                }
            }
        }
        if (chk == 1) {
            a.Checked = !a.Checked;
        }

    };
});



DocumentMappingMaster.controller("DocumentMappingMasterUpdateController", function ($scope, DocumentMappingMasterService, $routeParams, $location) {

    $scope.UserID = $routeParams.userID;
    $scope.GroupID = $routeParams.GroupID;
    $scope.Selected = {
        "Product": { "id": undefined }, "Status": { "id": undefined }, "SubStatus": { "id": undefined }
        , "Supplier": [], "STP": [], "Portability": [], "MedicalOrInspection": [], "Document": [], "SelectedSupplier": undefined, "DocCategory": { "id": undefined }
    };
    $scope.Selected.Supplier = [];
    $scope.ProductList = [];
    $scope.SupplierList = [];
    $scope.StatusList = [];
    $scope.SubStatusList = [];
    $scope.DocCategoryList = [];
    $scope.DocumentList = [];
    $scope.DocumentMasterDetails = [];
    $scope.CommonData = CommonData;

    $scope.GetMasterDocumentDetails = function () {
        DocumentMappingMasterService.GetMasterDocumentDetails().success(function (data) {
            debugger;
            $scope.DocCategoryList = data.DocCategoryList;
            $scope.DocumentList = data.DocumentList;
        });
    }

    $scope.GetDocumentMasterDetails = function () {
        DocumentMappingMasterService.GetDocumentMasterDetails($scope.Selected.Product.id, $scope.Selected.SelectedSupplier.id).success(function (data) {
            debugger;
            $scope.DocumentMasterDetails = data;
        });
    }

    $scope.GetMasterDocumentDetails();

    $scope.GetProductList = function (FilterID, Type) {
        var ProductId = 0;
        if (Type == 2) {
            FilterID = $scope.Selected.Product.id;
        }
        else if (Type == 4) {
            FilterID = $scope.Selected.Status.id;
            ProductId = $scope.Selected.Product.id;
        }

        DocumentMappingMasterService.GetMasterDropDownData(FilterID, ProductId, Type).success(function (FieldList) {
            if (Type == 1) {
                $scope.ProductList = FieldList;
            }
            else if (Type == 2) {
                if (FilterID != undefined) {

                    $scope.SupplierList = FieldList;

                    DocumentMappingMasterService.GetMasterDropDownData(FilterID, ProductId, 3).success(function (FieldList) {
                        $scope.StatusList = FieldList;
                    });
                }
            }
            else if (Type == 4) {
                if (FilterID != undefined) {
                    $scope.SubStatusList = FieldList;
                }
            }
            debugger;
        });
    }

    // $scope.GetProductList(0, 1);
    $scope.GetDocumentMasterDetailsEdit = function () {
        //$location.path("/DocumentMappingMaster/UserDetails/" + $scope.UserID);
        DocumentMappingMasterService.GetDocumentMasterDetailsEdit($scope.GroupID).success(function (data) {           
            $scope.DocumentMasterDetailsEdit = data;

            $scope.Selected.Supplier = data.SelectedSupplierIDRespList;
            $scope.Selected.STP = data.STPList;
            $scope.Selected.Portability = data.PortabilityList;
            $scope.Selected.MedicalOrInspection = data.MedInspectionList;
            $scope.Selected.Document = data.DcoumentIDList;

          
            for (var i = 0; i < $scope.CommonData.STP.length; i++) {
                if ($scope.Selected.STP.indexOf($scope.CommonData.STP[i].id) > 0) {
                    $scope.CommonData.STP[i].Checked = true;
                }
            }
            for (var i = 0; i < $scope.CommonData.Portability.length; i++) {
                if ($scope.Selected.Portability.indexOf($scope.CommonData.Portability[i].id) > 0) {
                    $scope.CommonData.Portability[i].Checked = true;
                }
            }
            for (var i = 0; i < $scope.CommonData.MedicalOrInspection.length; i++) {
                if ($scope.Selected.MedicalOrInspection.indexOf($scope.CommonData.MedicalOrInspection[i].id) > 0) {
                    $scope.CommonData.MedicalOrInspection[i].Checked = true;
                }
            }
        

            $scope.Selected.Product.id = $scope.DocumentMasterDetailsEdit.ProductID;
            $scope.Selected.Status.id = $scope.DocumentMasterDetailsEdit.StatusID
            $scope.Selected.SubStatus.id = $scope.DocumentMasterDetailsEdit.SubStatusID
            $scope.Selected.DocCategory.id = $scope.DocumentMasterDetailsEdit.DocCategoryID

            DocumentMappingMasterService.GetMasterDropDownData(0, 0, 1).success(function (FieldList) {
                $scope.ProductList = FieldList;
                DocumentMappingMasterService.GetMasterDropDownData($scope.DocumentMasterDetailsEdit.ProductID, 0, 2).success(function (FieldList) {
                    $scope.SupplierList = FieldList;
                    DocumentMappingMasterService.GetMasterDropDownData($scope.DocumentMasterDetailsEdit.ProductID, 0, 3).success(function (FieldList) {
                        $scope.StatusList = FieldList;
                    });
                    DocumentMappingMasterService.GetMasterDropDownData($scope.DocumentMasterDetailsEdit.StatusID, $scope.DocumentMasterDetailsEdit.ProductID, 4).success(function (FieldList) {
                        debugger;
                        $scope.SubStatusList = FieldList;
                    });                   
                });
            });
            
        });
    }
    $scope.GetDocumentMasterDetailsEdit();
    $scope.SupplierSettings = {
        smartButtonMaxItems: 3,//selectionLimit: 2,
        smartButtonTextConverter: function (itemText, originalItem) {
            if (itemText === 'Jhon') {
                return 'Jhonny!';
            }
            return itemText;
        }
    };

    $scope.InsertDocumentMapping = function () {
        var SupplierIDs = "", StpIDs = "", PortabilityIDs = "", MedInspectionIDs = "", DocumentIDs = "";
        for (var i = 0 ; i < $scope.Selected.Supplier.length; i++) {
            if (i == $scope.Selected.Supplier.length - 1) {
                SupplierIDs = SupplierIDs + $scope.Selected.Supplier[i].id;
            }
            else {
                SupplierIDs = SupplierIDs + $scope.Selected.Supplier[i].id + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.STP.length; i++) {
            if (i == $scope.Selected.STP.length - 1) {
                StpIDs = StpIDs + $scope.Selected.STP[i];
            }
            else {
                StpIDs = StpIDs + $scope.Selected.STP[i] + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.Portability.length; i++) {
            if (i == $scope.Selected.Portability.length - 1) {
                PortabilityIDs = PortabilityIDs + $scope.Selected.Portability[i];
            }
            else {
                PortabilityIDs = PortabilityIDs + $scope.Selected.Portability[i] + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.MedicalOrInspection.length; i++) {
            if (i == $scope.Selected.MedicalOrInspection.length - 1) {
                MedInspectionIDs = MedInspectionIDs + $scope.Selected.MedicalOrInspection[i];
            }
            else {
                MedInspectionIDs = MedInspectionIDs + $scope.Selected.MedicalOrInspection[i] + ",";
            }
        }
        for (var i = 0 ; i < $scope.Selected.Document.length; i++) {
            if (i == $scope.Selected.Document.length - 1) {
                DocumentIDs = DocumentIDs + $scope.Selected.Document[i];
            }
            else {
                DocumentIDs = DocumentIDs + $scope.Selected.Document[i] + ",";
            }
        }
        var objMasterDocumentMappingRequest = {
            "GroupID": $scope.GroupID,
            "ProductID": $scope.Selected.Product.id,
            "SupplierIDs": SupplierIDs,
            "StatusID": $scope.Selected.Status.id,
            "SubStatusID": $scope.Selected.SubStatus.id,
            "StpIDs": StpIDs,
            "PortabilityIDs": PortabilityIDs,
            "MedInspectionIDs": MedInspectionIDs,
            "DocCategoryID": $scope.Selected.DocCategory.id,
            "DocumentIDs": DocumentIDs,
            "CreatedBy": $scope.UserID
        };

        debugger

        DocumentMappingMasterService.InsertDocumentMapping(objMasterDocumentMappingRequest).success(function (data) {
            if (parseInt(data) > 0) {
                alert('Mapping updated successfully');
                $location.path("/DocumentMappingMaster/AddNew/" + $scope.UserID);;
            }
            else {
                alert('Document updated partially');
            }

        });
    }

    //"STP": [], "Portability": [], "MedicalOrInspection": [], "Document": []
    // toggle selection for a given employee by name

    $scope.toggleSelectionDocument = function (id) {
        var idx = $scope.Selected.Document.indexOf(id);
        // is currently selected
        if (idx > -1) {
            $scope.Selected.Document.splice(idx, 1);
        }
            // is newly selected
        else {
            $scope.Selected.Document.push(id);
        }
    };

    $scope.toggleSelectionSTP = function (a, type) {
        debugger;
        var chk = 0;


        if (type == 'STP') {

            if (a.Checked && a.id == -1) {
                if ($scope.Selected.STP.length > 0) {
                    chk = 1;
                }
            }
            else if (a.Checked && a.id != -1) {
                if ($scope.Selected.STP.indexOf("-1") > 0) {
                    chk = 1;
                }
            }
            if (chk == 0) {
                var idx = $scope.Selected.STP.indexOf(a.id);
                // is currently selected
                if (idx > -1) {
                    $scope.Selected.STP.splice(idx, 1);
                }
                    // is newly selected
                else {
                    $scope.Selected.STP.push(a.id);
                }
            }
        }
        else if (type == 'POR') {
            if (a.Checked && a.id == -1) {
                if ($scope.Selected.Portability.length > 0) {
                    chk = 1;
                }
            }
            else if (a.Checked && a.id != -1) {
                if ($scope.Selected.Portability.indexOf("-1") > 0) {
                    chk = 1;
                }
            }
            if (chk == 0) {
                var idx = $scope.Selected.Portability.indexOf(a.id);
                // is currently selected
                if (idx > -1) {
                    $scope.Selected.Portability.splice(idx, 1);
                }
                    // is newly selected
                else {
                    $scope.Selected.Portability.push(a.id);
                }
            }
        }
        else if (type == 'MED') {
            if (a.Checked && a.id == -1) {
                if ($scope.Selected.MedicalOrInspection.length > 0) {
                    chk = 1;
                }
            }
            else if (a.Checked && a.id != -1) {
                if ($scope.Selected.MedicalOrInspection.indexOf("-1") > 0) {
                    chk = 1;
                }
            }
            if (chk == 0) {
                var idx = $scope.Selected.MedicalOrInspection.indexOf(a.id);
                // is currently selected
                if (idx > -1) {
                    $scope.Selected.MedicalOrInspection.splice(idx, 1);
                }
                    // is newly selected
                else {
                    $scope.Selected.MedicalOrInspection.push(a.id);
                }
            }
        }
        if (chk == 1) {
            a.Checked = !a.Checked;
        }

    };
});