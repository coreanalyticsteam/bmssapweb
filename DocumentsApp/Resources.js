﻿//This will be used to prvide the config
//QA-http://10.0.29.223:5556
//Live-http://10.72.179.218:8850
//Dev-http://localhost:55235/
var config = {
    serviceURL: "http://localhost:55235/"
};
var CommonData = {
    "STP": [{ "id": -1, "label": "No Filter", "Checked": false }, { "id": 0, "label": "NSTP", "Checked": false }, { "id": 1, "label": "STP", "Checked": false }],
    "Portability": [{ "id": -1, "label": "No Filter", "Checked": false }, { "id": 1, "label": "Yes", "Checked": false }, { "id": 0, "label": "No", "Checked": false }],
    "MedicalOrInspection": [{ "id": -1, "label": "No Filter", "Checked": false }, { "id": 1, "label": "Yes", "Checked": false }, { "id": 0, "label": "No", "Checked": false }]
};
