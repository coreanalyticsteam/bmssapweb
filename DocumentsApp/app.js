﻿angular.module("DocumentMappingMaster", ["ngRoute", "ui.bootstrap", "angularjs-dropdown-multiselect"]).config(["$routeProvider",
  function ($routeProvider) {
      $routeProvider.
        when('/DocumentMappingMaster/AddNew/:userID', {
            templateUrl: 'views/DocumentMapping.html',
            controller: 'DocumentMappingMasterController'
        })
      .when('/DocumentMappingMaster/ViewAll/:userID', {
          templateUrl: 'views/ViewDocument.html',
          controller: 'DocumentMappingMasterController'
      })
      .when('/DocumentMappingMaster/Update/:userID/:GroupID', {
          templateUrl: 'views/DocumentMapping.html',
          controller: 'DocumentMappingMasterUpdateController'
      })
      ;
        //.when('/DocumentsApp/:id', {
        //    templateUrl: 'views/TicketDetails.htm',
        //    controller: 'TicketDetailsController'
        //}).
        //when('/DocumentsApp/:RefID/create', {
        //    templateUrl: 'views/TicketEdit.htm',
        //    controller: 'CreateTicketController'
        //}).
        //when('/DocumentsApp/User/:userid/:role/:system', {
        //    templateUrl: 'views/TicketList.htm',
        //    controller: 'UserLoginController'
        //}).
        //otherwise({
        //    redirectTo: '/Tickets'
        //});
  }]);