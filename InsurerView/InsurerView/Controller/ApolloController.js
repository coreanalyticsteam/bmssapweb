﻿myApp.controller('ApolloController', function ($scope, AjaxFactory, $stateParams) {
    $scope.LoadingImg = false;
    //2
    var s = $stateParams.Id;
    $scope.statusLd = false;
    //alert(s.substring(0, s.indexOf('-')));
    if (s.indexOf('-') != -1) {
        $scope.RequestURL = InsurerURL + 'ApplicationNo=' + s.substring(0, s.indexOf('-')) + '&SupplierId=16&ProductId=2&LeadId=' + $stateParams.LeadID + '&Hit=1';
        $scope.RequestURL1 = InsurerURL + 'ApplicationNo=' + s.substring(0, s.indexOf('-')) + '&SupplierId=16&ProductId=2&LeadId=' + $stateParams.LeadID + '&Hit=2';
    }
    else {
        $scope.RequestURL = InsurerURL + 'ApplicationNo=' + s + '&SupplierId=16&ProductId=2&LeadId=' + $stateParams.LeadID + '&Hit=1';
        $scope.RequestURL1 = InsurerURL + 'ApplicationNo=' + s + '&SupplierId=16&ProductId=2&LeadId=' + $stateParams.LeadID + '&Hit=2';
    }
    AjaxFactory.postData(LogURL, { Log: ' RequestHit : ' + $scope.RequestURL }, function (data) { })
    AjaxFactory.getData($scope.RequestURL, function (data) {
        
        
        console.log(data.data);
        var newData = data.data;
        //newData = newData.replace(/\)/g, '');
        //newData = newData.replace("ObjectId(", '');

        if (newData.constructor === Array) {
            $scope.Insurer = newData[0];
        }
        else { $scope.Insurer = newData; }
        if ($scope.Insurer.Stage != "No data found") {
            $scope.LoadingImg = true;
        }
        
        //$scope.Insurer = newData[0];
        AjaxFactory.getData($scope.RequestURL1, function (data) {
            var newData = data.data;
            //newData = newData.replace(/\)/g, '');
            //newData = newData.replace("ObjectId(", '');
            $scope.LoadingImg = true;
            $scope.statusLd = true;
            if (newData.constructor === Array) {
                $scope.NewInsurer = newData[0];
            }
            else { $scope.NewInsurer = newData; }
            $scope.Status = ($scope.NewInsurer.Stage == "NoDataChange")?"No Data Change":"New Data";
            if ($scope.NewInsurer.Stage != "NoDataChange")
            {
                $scope.Insurer = $scope.NewInsurer;
            }
            AjaxFactory.postData(LogURL, { Log: ' ResponseHit : ' + $scope.RequestURL + JSON.stringify($scope.Insurer) }, function (data) { })
        });
        //console.log($scope.Insurer);
        //console.log($scope.Insurer.ApplicationNoInfo['Application History'])
    });
});