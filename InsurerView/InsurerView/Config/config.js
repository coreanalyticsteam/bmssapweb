﻿myApp.config(function ($stateProvider) {
    //var helloState = {
    //    name: 'hello',
    //    url: '/hello',
    //    template: '<h3>hello world!</h3>'
    //}

    //var aboutState = {
    //    name: 'about',
    //    url: '/about',
    //    template: '<h3>Its the UI-Router hello world app!</h3>'
    //}

    //$stateProvider.state(helloState);
    //$stateProvider.state(aboutState);
    $stateProvider
        .state('MaxBupa', {
            name: 'MaxBupa',
            url: '/MaxBupa/:Id/:LeadID',
            templateUrl: 'View/MaxBupa.html',
            controller: 'MaxBupaController'
        })
        .state('MaxLife', {
            name: 'MaxLife',
            url: '/MaxLife/:Id/:LeadID',
            templateUrl: 'View/MaxLife.html',
            controller: 'MaxLifeController'
        })
        .state('Religare', {
            name: 'Religare',
            url: '/Religare/:Id/:LeadID',
            templateUrl: 'View/Religare.html',
            controller: 'ReligareController'
        })
        .state('Apollo', {
            name: 'Apollo',
            url: '/Apollo/:Id/:LeadID',
            templateUrl: 'View/Apollo.html',
            controller: 'ApolloController'
        })
        .state('Star', {
            name: 'Star',
            url: '/Star/:Id/:LeadID',
            templateUrl: 'View/Star.html',
            controller: 'StarController'
        })
        .state('ICICIPRU', {
            name: 'ICICIPRU',
            url: '/ICICIPRU/:Id/:LeadID',
            templateUrl: 'View/ICICIPRU.html',
            controller: 'ICICIPRUController'
        })
        .state('ReligareAPI', {
            name: 'ReligareAPI',
            url: '/ReligareAPI',
            templateUrl: 'View/ReligareAPI.html',
            controller: 'ReligareAPIController'
    });
});
