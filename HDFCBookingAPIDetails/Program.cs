﻿//using HDFCBookingAPIDetails.HDFCReqDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Data.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Globalization;
using HDFCBookingAPIDetails.SubmitHDFCDoc;
using System.IO;
using HDFCBookingAPIDetails.ProposalFormDetailsHDFC;
using Newtonsoft.Json;
using HDFCBookingAPIDetails.HDFCReqDetails;

namespace HDFCBookingAPIDetails
{
    class Program
    {
        private static string userid = ConfigurationSettings.AppSettings["HDFCUserId"];
        static void Main(string[] args)
        {
          //  SubmitDocDetails();
         //   HDFC();
            HDFCInsurerStatus();
           // GetDocDetails("1411001018370", true);
        }
        public static void HDFCInsurerStatus()
        {
            DataSet ds;
            SqlDataAdapter adp;
            SqlConnection con;
            SqlCommand cmd;
            string lStatus = string.Empty;
            ds = new DataSet();
            con = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]);
            con.Open();
            cmd = new SqlCommand(ConfigurationSettings.AppSettings["sqlQueryHDFC"], con);
            cmd.CommandType = CommandType.Text;
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);

            con.Close();
            Int64 a = 0;
            foreach (DataRow oDR in ds.Tables[0].Rows)
            {
                if (oDR["ApplicationNo"].ToString().Length > 0)
                {
                    a += 1;
                    Console.WriteLine(a + ")" + "App No: " + oDR["ApplicationNo"].ToString());
                    GetDocDetailsGetInsurerStatus(oDR["ApplicationNo"].ToString(), true, Convert.ToInt64(oDR["LEADID"]), Convert.ToInt32(oDR["ProductID"]), Convert.ToInt32(oDR["SupplierId"]));
                }
            }
        }

        public static void GetDocDetailsGetInsurerStatus(string appNo, bool IsDisplay, Int64 LeadId, int ProductId, int SupplierId)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {
                getProposalFormDetails obj = new getProposalFormDetails()
                {
                    getProposalFormDetailsRequest = new GetProposalFormDetailsRequest()
                    {
                        head = new HDFCBookingAPIDetails.ProposalFormDetailsHDFC.TEBTRequestHeader
                        {
                            source = "OCP_ONLINE",
                            // userid = "pindianbank1"
                            userid = "pb_ocp_001"
                        },
                        body = new GetProposalFormDetailsRequestBody
                        {
                            appnum = appNo
                        }
                    }
                };
                getProposalFormDetailsResponse objRes = new getProposalFormDetailsResponse();
                ApplicationmangmtExtInterfaceClient obj2 = new ApplicationmangmtExtInterfaceClient();
                objRes = obj2.getProposalFormDetails(obj);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                
                using (DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[HDFCUpdateInsurerStatus]"))
                {
                    Console.WriteLine(objRes.getProposalFormDetailsResponse1.body.body.applctndetails.basicappinfo.uwtype);
                    db.AddInParameter(dbCommand, "@InsurerStatus", DbType.String, objRes.getProposalFormDetailsResponse1.body.body.applctndetails.basicappinfo.magnumdcsn);
                    db.AddInParameter(dbCommand, "@LeadId", DbType.Int64, LeadId);

                    db.ExecuteNonQuery(dbCommand);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ërror IN LeadID : " + LeadId + " Execption : " + ex.ToString());
                oStringBuilder.Append("\r\n Execption=" + ex.ToString());
                ErrorNInfoLogMethod(oStringBuilder.ToString(), "HDFC.txt");
            }
            finally
            {

            }

            // return objRequirement.Where(a => a.IsDisplay == IsDisplay).ToList();
        }
        public static void HDFC()
        {
            DataSet ds;
            SqlDataAdapter adp;
            SqlConnection con;
            SqlCommand cmd;
            string lStatus = string.Empty;
            ds = new DataSet();
            con = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]);
            con.Open();
            cmd = new SqlCommand(ConfigurationSettings.AppSettings["sqlQueryHDFC"], con);
            cmd.CommandType = CommandType.Text;
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds);

            con.Close();
            Int64 a = 0;
            foreach (DataRow oDR in ds.Tables[0].Rows)
            {
                if (oDR["ApplicationNo"].ToString().Length > 0)
                {
                    a += 1;
                    Console.WriteLine(a + ")" + "App No: " + oDR["ApplicationNo"].ToString());
                    GetDocDetails( oDR["ApplicationNo"].ToString(), true, Convert.ToInt64(oDR["LEADID"]), Convert.ToInt32(oDR["ProductID"]), Convert.ToInt32(oDR["SupplierId"]));
                }
            }
        }
        public static void GetDocDetails(string appNo, bool IsDisplay, Int64 LeadId, int ProductId, int SupplierId)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {
                List<RequirementDetails> objRequirement = new List<RequirementDetails>();
                using (TEBT_Requirement_Management_ExtClient obj = new TEBT_Requirement_Management_ExtClient())
                {
                    getRequirement parameter = new getRequirement()
                    {
                        input = new HDFCReqDetails.GetRqmntMgmntReqBO()
                        {
                            head = new HDFCReqDetails.TEBTRequestHeader
                            {
                                //source = "OCP_OFFLINE",
                                //userid = userid
                                source = "OCP_ONLINE",
                                userid = userid
                            },
                            getrqmntreq = new HDFCReqDetails.GetRqmntMgmntReqBody
                            {
                                appnum = appNo //"1700040283227",

                            }
                        }
                    };
                    oStringBuilder.Append("\r\n objReq=" + parameter.ToString());
                    getRequirementResponse res = obj.getRequirement(parameter);
                    HDFCReqDetails.GetRqmntMgmntResBO ApptRes = res.output;
                    if (ApptRes.getrqmntres != null)
                    {
                        var XMLString = new XElement("HDFCBookingAPIDetails",
                                          from CustMobData in ApptRes.getrqmntres
                                          select new XElement("HDFCBookingAPIDetails",
                                                      CustMobData.appnum == null ? null : new XAttribute("appnum", CustMobData.appnum),
                                                        CustMobData.rqmnttrnskey == null ? null : new XAttribute("rqmnttrnskey", CustMobData.rqmnttrnskey),
                                                         CustMobData.rqmntcd == null ? null : new XAttribute("rqmntcd", CustMobData.rqmntcd),
                                                          CustMobData.rqmntdesc == null ? null : new XAttribute("rqmntdesc", CustMobData.rqmntdesc),
                                                         CustMobData.rqmntctgycd == null ? null : new XAttribute("rqmntctgycd", CustMobData.rqmntctgycd),
                                                         CustMobData.rqmntctgydesc == null ? null : new XAttribute("rqmntctgydesc", CustMobData.rqmntctgydesc),
                                                           CustMobData.rqmntsubctgycd == null ? null : new XAttribute("rqmntsubctgycd", CustMobData.rqmntsubctgycd),
                                                           CustMobData.rqmntsubctgydesc == null ? null : new XAttribute("rqmntsubctgydesc", CustMobData.rqmntsubctgydesc),
                                                         CustMobData.rqmntstatuscd == null ? null : new XAttribute("rqmntstatuscd", CustMobData.rqmntstatuscd),
                                                          CustMobData.rqmntstatusdesc == null ? null : new XAttribute("rqmntstatusdesc", CustMobData.rqmntstatusdesc),
                                                         CustMobData.rqmntraisedby == null ? null : new XAttribute("rqmntraisedby", CustMobData.rqmntraisedby),
                                                         CustMobData.rqmntgendt == null ? null : new XAttribute("rqmntgendt", Convert.ToDateTime(DateTime.ParseExact(CustMobData.rqmntgendt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))),
                                                          CustMobData.doccd == null ? null : new XAttribute("doccd", CustMobData.doccd),
                                                          CustMobData.docname == null ? null : new XAttribute("docname", CustMobData.docname),
                                                          CustMobData.fulfildt == null ? null : new XAttribute("fulfildt", Convert.ToDateTime(DateTime.ParseExact(CustMobData.fulfildt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))),
                                                          CustMobData.fulfilstatuscd == null ? null : new XAttribute("fulfilstatuscd", CustMobData.fulfilstatuscd),
                                                          CustMobData.rqmntupdtddt == null ? null : new XAttribute("rqmntupdtddt", Convert.ToDateTime(DateTime.ParseExact(CustMobData.rqmntupdtddt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))),
                                                          CustMobData.fulfilmntupdtddt == null ? null : new XAttribute("fulfilmntupdtddt", Convert.ToDateTime(DateTime.ParseExact(CustMobData.fulfilmntupdtddt, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture))),
                                                          CustMobData.rqmnttypecd == null ? null : new XAttribute("rqmnttypecd", CustMobData.rqmnttypecd),
                                                          CustMobData.clntid == null ? null : new XAttribute("clntid", CustMobData.clntid)
                                                     )).ToString();

                        Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                        using (DbCommand dbCommand = db.GetStoredProcCommand("[BMS].[BulkHDFCBookingAPIDetails]"))
                        {
                            db.AddInParameter(dbCommand, "@XMLdata", DbType.Xml, XMLString);
                            db.AddInParameter(dbCommand, "@LeadId", DbType.Int64, LeadId);
                            db.AddInParameter(dbCommand, "@ProductId", DbType.Int16, ProductId);
                            db.AddInParameter(dbCommand, "@SupplierId", DbType.Int16, SupplierId);

                            db.ExecuteNonQuery(dbCommand);

                        }


                        //foreach (var req in ApptRes.getrqmntres)
                        //{
                        //    objRequirement.Add(new RequirementDetails
                        //    {
                        //        Requirement = req.rqmntdesc,
                        //        DocumentSubmitted = req.docname,
                        //        RequirementStatus = req.rqmntstatusdesc,
                        //        DocumentSubmittedOn = req.rqmntupdtddt,
                        //        IsDisplay = req.displayflag == "Y" ? true : false
                        //    });

                        //}
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ërror IN LeadID : " + LeadId + " Execption : " + ex.ToString());
                oStringBuilder.Append("\r\n Execption=" + ex.ToString());
                ErrorNInfoLogMethod(oStringBuilder.ToString(), "HDFC.txt");
            }
            finally
            {
                
            }

           // return objRequirement.Where(a => a.IsDisplay == IsDisplay).ToList();
        }

            public static void SubmitDocDetails()
            {
                string appNo = "2700040226887";
                //SubmitHDFCDoc.SubmitDocumentExternal

                using (SubmitHDFCDoc.SubmitDocumentExternalClient obj = new SubmitHDFCDoc.SubmitDocumentExternalClient())
                {
                    SubmitHDFCDoc.documentdetailsreq[] ListDoc = new SubmitHDFCDoc.documentdetailsreq[1];
                    ListDoc[0] = new SubmitHDFCDoc.documentdetailsreq();
                    ListDoc[0].reqcode = "REQ_001";
                    ListDoc[0].clientid = "623358";
                    ListDoc[0].code = "DOC_0003";
                    ListDoc[0].path = "http://apiqa.policybazaar.com/cs/repo/getPolicyCopyById?docId=5aeb1b44e4b02921bff800fa";
                    ListDoc[0].filename = "bill.pdf";


                    submitDocumentExternal parameter = new submitDocumentExternal()
                    {
                        input = new SubmitHDFCDoc.UploadDocReq
                        {
                            head = new SubmitHDFCDoc.TEBTRequestHeader
                            {
                                source = "OCP_ONLINE",
                                userid = userid
                            },
                            uploaddocreq = new SubmitHDFCDoc.UploadDocReqBody
                            {
                                appno = appNo, //"1700040283227",
                                documentdetails = ListDoc

                            }
                        }

                    };
                    //SubmitDocumentExternalClient obj1 = new SubmitDocumentExternalClient();
                    submitDocumentExternalResponse abc = obj.submitDocumentExternal(parameter);
                }

            }
            public static void ErrorNInfoLogMethod(String sMsg, String FileName)
            {
                try
                {
                    if (ConfigurationSettings.AppSettings["SaveLogs"] != null && Convert.ToString(ConfigurationSettings.AppSettings["SaveLogs"]) == "true")
                    {
                        String FilePath = Convert.ToString(ConfigurationSettings.AppSettings["ErrorNInfoLogFilePath"]) + DateTime.Now.ToString("ddMMMyyyy") + @"\";
                        if (!String.IsNullOrEmpty(FilePath.Trim()))
                        {
                            if (!Directory.Exists(FilePath))
                                Directory.CreateDirectory(FilePath);
                            System.IO.File.AppendAllText(FilePath + FileName, sMsg + Environment.NewLine);
                        }
                    }
                }
                catch { }
            }
    }


    public class RequirementDetails
    {
        [DataMember]
        public string Requirement { get; set; }
        [DataMember]
        public string DocumentSubmitted { get; set; }
        [DataMember]
        public string RequirementStatus { get; set; }
        [DataMember]
        public string DocumentSubmittedOn { get; set; }
        [DataMember]
        public bool IsDisplay { get; set; }

    }
    
}
