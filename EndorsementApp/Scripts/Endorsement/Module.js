﻿var EndorsementApp = angular.module("EndorsementApp");

//EndorsementApp.directive('fileModel', ['$parse', 'DocumentsUploadAppAJService', function ($parse, DocumentsUploadAppAJService) {
//    return {
//        restrict: 'A',
//        link: function (scope, element, attrs) {
//            var model = $parse(attrs.fileModel);
//            var modelSetter = model.assign;

//            element.bind('change', function () {
//                scope.$apply(function () {
//                    debugger;
//                    DocumentsUploadAppAJService.FileData = element[0].files[0];
//                    modelSetter(scope.$parent.$parent.myFile, element[0].files[0]);
//                });
//            });
//        }
//    };
//}]);

EndorsementApp.directive('fileModel', ['$parse', 'EndorsementAJService', function ($parse, EndorsementAJService) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    debugger;
                    EndorsementAJService.FileData.push(element[0].files[0]);
                    modelSetter(scope.$parent.$parent.myFile, element[0].files[0]);
                });
            });
        }
    };
}]);