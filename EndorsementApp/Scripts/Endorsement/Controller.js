﻿
EndorsementApp.controller("EndorsementController", function ($scope, $rootScope, EndorsementAJService, $routeParams, $uibModal, $window) {
    var modalInstanceSameCtrl;
    $scope.RequestEndorsementDocList = { "LeadID": "", "UserID": "", "DetailId": "", "RequestEndorsementDocStatusList": [] };
    var NoChange = 0;
    var EndorsementFieldMasterList=undefined;
    var EndorsementStatus=undefined;
    var LeadDetail=undefined;
    var EndorsementAddtionalInformation = undefined;
    var RejectionReasonList = undefined;
    var EndorsementDocumentCopyList = undefined;
    //$scope.RejectionStatus = undefined;
    $scope.Role = $routeParams.Role;
    $scope.checkValue = function (selected) {
        debugger;
        selected.NewValue = selected.NewValue.toUpperCase();
        if (selected.FieldType == 'TINYINT' || selected.FieldType == 'DECIMAL' || selected.FieldType == 'SMALLINT') {
            if ($.isNumeric(selected.NewValue)) {
                if (selected.FieldType == 'TINYINT' && parseInt( selected.NewValue) > 255) { selected.NewValue = ''; }
                if (selected.FieldType == 'SMALLINT' && parseInt(selected.NewValue) > 32767) { selected.NewValue = ''; }
                if (selected.FieldType == 'DECIMAL' && selected.NewValue.length > 10) { selected.NewValue = ''; }
            }
            else{ selected.NewValue = '';}
          
        }       

    }
   

    $scope.EndorsementDetail = {
        "EndorsementFieldMasterList": EndorsementFieldMasterList,
        "EndorsementStatus": EndorsementStatus,
        "EndorsementAddtionalInformation": EndorsementAddtionalInformation, "RejectionReasonList": RejectionReasonList, "EndorsementDocumentCopyList": EndorsementDocumentCopyList
    };

    $scope.RequestEndorsementDetail = {
        "EndorsementFieldMasterList": EndorsementFieldMasterList,
        "EndorsementStatus": EndorsementStatus,
        "EndorsementAddtionalInformation": EndorsementAddtionalInformation, "RejectionReasonList": RejectionReasonList
    };
    $scope.RequestEndorsementDetail.LeadID = $routeParams.LeadID;
    $scope.RequestEndorsementDetail.DetailsID = $routeParams.DetailsID;
    $scope.RequestEndorsementDetail.UserID = $routeParams.userID;
    $scope.RequestEndorsementDetail.Type = $routeParams.Type;
    $scope.RequestEndorsementDetail.CustomerID = "";
    $scope.RequestEndorsementDetail.ProductID = "";
    $scope.RequestEndorsementDetail.SupplierID = "";
    $scope.RequestEndorsementDetail.PlanId = "";
    $scope.RequestEndorsementDetail.SupplierName = "";
    $scope.RequestEndorsementDetail.PlanName = "";
    //$scope.DocVerificationList.Show = 0;
    $scope.AddNewVisible = false;
    $scope.EndorsementDetail.DetailsID = $routeParams.DetailsID;
    $scope.EndorsementDetail.UserID = $routeParams.userID;
    $scope.EndorsementDetail.LeadID = $routeParams.LeadID;
   // $scope.EndorsementDetail.UserID = $routeParams.userID;

    debugger;

    $scope.NoChange = NoChange;
    $scope.RefundPending = 0;
    $scope.TempCollectedConfirm = 0;

    $scope.PremiumStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
    $scope.PremiumChangeStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
    $scope.SCVerifiedStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
    $scope.SCReceivedStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
    $scope.DocumentVerification = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };

    

    
    
    $scope.selected = { "EndorsementField": undefined, "RejectionStatus": undefined };
    var LeadID = '';
    
    var callType = 0;
    $scope.AddNewField = function () {
        $scope.AddNewVisible = true;
    };
    $scope.RemoveAdd = function () {
        $scope.AddNewVisible = false;
        $scope.selected.EndorsementField = undefined;
    };

    $scope.GetEndorsementFieldList = function (Type, fnCallType) {

        if (Type == undefined) { Type = 1;}
        if (LeadID == '') {
            LeadID = $routeParams.LeadID;
        }
        if ($.isNumeric(LeadID)) {
            
            EndorsementAJService.GetEndorsementDetails($scope.EndorsementDetail.LeadID, $scope.EndorsementDetail.DetailsID, Type).success(function (FieldList) {
                debugger;
                // $scope.EndorsementDetail = FieldList.data;
                if (FieldList.EndorsementFieldListDetails.length > 0) {
                    $scope.AddNewVisible = false;
                }
                else {
                    $scope.AddNewVisible = true;
                }

                $scope.EndorsementDetail.EndorsementFieldListDetails = FieldList.EndorsementFieldListDetails;

                $scope.EndorsementDetail.EndorsementFieldMasterList = FieldList.EndorsementFieldMasterList;
                $scope.EndorsementDetail.EndorsementStatus = FieldList.EndorsementStatus;
                if (fnCallType == 1) {
                    callType = 0;
                    if (FieldList.EndorsementStatus.StatusMasterID == 12 || FieldList.EndorsementStatus.StatusMasterID == 11) {
                        alert('Endorsement closed successfully.');
                    }
                }

                $scope.EndorsementDetail.EndorsementAddtionalInformation = FieldList.EndorsementAddtionalInformation;
                $scope.EndorsementDetail.RejectionReasonList = FieldList.RejectionReasonList;
                $scope.EndorsementDetail.EndorsementDocumentCopyList = FieldList.EndorsementDocumentCopyList;

                
                $scope.EndorsementDetail.IsDocReceived = FieldList.IsDocReceived;                
                $scope.EndorsementDetail.LeadID = FieldList.LeadID;
               // $scope.EndorsementDetail.UserID = $routeParams.userID;
                $scope.EndorsementDetail.CustomerID = FieldList.CustomerID;
                $scope.EndorsementDetail.ProductID = FieldList.ProductID;
                $scope.EndorsementDetail.SupplierID = FieldList.SupplierID;
                $scope.EndorsementDetail.PlanId = FieldList.PlanId;
                $scope.EndorsementDetail.SupplierName = FieldList.SupplierName;
                $scope.EndorsementDetail.PlanName = FieldList.PlanName;
                $scope.EndorsementDetail.ProductName = FieldList.ProductName;

              
                $scope.RequestEndorsementDetail.LeadID = FieldList.LeadID;
                $scope.RequestEndorsementDetail.UserID = $routeParams.userID;
                $scope.RequestEndorsementDetail.CustomerID = FieldList.CustomerID;
                $scope.RequestEndorsementDetail.ProductID = FieldList.ProductID;
                $scope.RequestEndorsementDetail.SupplierID = FieldList.SupplierID;
                $scope.RequestEndorsementDetail.PlanId = FieldList.PlanId;
                $scope.RequestEndorsementDetail.SupplierName = FieldList.SupplierName;
                $scope.RequestEndorsementDetail.PlanName = FieldList.PlanName;
                debugger;
                //Initiated By Agent,Resubmitted
                if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 1 || $scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 5) {
                    $scope.DocumentVerification = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                }
                    //Pending With Insurer
                else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 4 && $scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == -1) {
                    $scope.PremiumStatus = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                }
                    //Pending With Insurer
                else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 4 && $scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == 0) {
                    $scope.PremiumStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.SCVerifiedStatus = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                }
                    //Pending With Insurer
                else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 4 && $scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == 2) {
                    $scope.PremiumStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                }
                    //Rate Up Initiated,Refund Initiated
                else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 6 || $scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 8) {
                    $scope.PremiumStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                    if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 8 && $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded == 0) {
                        $scope.SCVerifiedStatus = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                        $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                    }

                }
                    //Rate Up Completed,Refund Completed
                else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 7 || $scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 9) {
                    $scope.PremiumStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.SCVerifiedStatus = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                }
                    //Soft Copy Received
                else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 10) {
                    $scope.PremiumStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.SCVerifiedStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.SCReceivedStatus = { "ContentDiv": true, "ButtonDiv": true, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                    if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == 2 && $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded == 0) {
                        $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
                    }
                }
                    //Closed
                else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 11) {
                    $scope.PremiumStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.PremiumChangeStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.SCVerifiedStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                    $scope.SCReceivedStatus = { "ContentDiv": true, "ButtonDiv": false, "Disabled": true, "Rejection": { "Display": false, "Disabled": false } };
                }





                if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == 0) {
                    $scope.NoChange = 1;
                }
                if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 8 && $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded == 0) {
                    debugger;
                    $scope.RefundPending = 1;
                }

                if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 12 && $scope.EndorsementDetail.EndorsementStatus.ReasonId != 0) {
                    $scope.PremiumStatus.ButtonDiv = false;
                    $scope.PremiumStatus.Disabled = true;
                    $scope.PremiumStatus.Rejection.Display = false;

                    $scope.PremiumChangeStatus.ButtonDiv = false;
                    $scope.PremiumChangeStatus.Disabled = true;
                    $scope.PremiumChangeStatus.Rejection.Display = false;

                    $scope.SCVerifiedStatus.ButtonDiv = false;
                    $scope.SCVerifiedStatus.Disabled = true;
                    $scope.SCVerifiedStatus.Rejection.Display = false;

                    $scope.SCReceivedStatus.ButtonDiv = false;
                    $scope.SCReceivedStatus.Disabled = true;
                    $scope.SCReceivedStatus.Rejection.Display = false;
                }

            });
        }
    }
    $scope.GetEndorsementFieldList($routeParams.Type, callType);
    $scope.EndorsementList = [];

    $scope.AddEndorsement = function () {
        var checkDuplicate = 0;
        debugger
        if ($scope.selected.EndorsementField != undefined) {
            if (parseInt($scope.selected.EndorsementField.FieldID) > 0 && $.trim($scope.selected.EndorsementField.NewValue) != '') {

                if ($.trim($scope.selected.EndorsementField.NewValue) != $.trim($scope.selected.EndorsementField.OldValue)) {
                    if ($scope.EndorsementDetail.EndorsementFieldListDetails != undefined && $scope.EndorsementDetail.EndorsementFieldListDetails.length > 0) {
                        for (var i = 0; i < $scope.EndorsementDetail.EndorsementFieldListDetails.length; i++) {
                            if ($scope.EndorsementDetail.EndorsementFieldListDetails[i].FieldID == $scope.selected.EndorsementField.FieldID) {
                                checkDuplicate = 1;
                            }

                        }
                    }
                    if (checkDuplicate == 0) {
                        if ($scope.selected.EndorsementField.MasterType == 1) {
                            $scope.selected.EndorsementField.NewValue = $scope.selected.EndorsementField.NewValue.Text
                        }
                        else if ($scope.selected.EndorsementField.MasterType == 2) {
                            $scope.selected.EndorsementField.NewValue = $scope.selected.EndorsementField.NewValue.Text
                        }
                        //$scope.selected.EndorsementField.Display = true;
                        $scope.selected.EndorsementField.SNo = 0;
                        var dateCheck = 0;
                        if ($scope.selected.EndorsementField.FieldType == 'DATETIME') {
                           
                            if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test($scope.selected.EndorsementField.NewValue))
                                dateCheck = 1;

                            // Parse the date parts to integers
                            var parts = $scope.selected.EndorsementField.NewValue.split("/");
                            var day = parseInt(parts[1], 10);
                            var month = parseInt(parts[0], 10);
                            var year = parseInt(parts[2], 10);

                            // Check the ranges of month and year
                            if (year < 1000 || year > 3000 || month == 0 || month > 12)
                                dateCheck = 1;

                            var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                            // Adjust for leap years
                            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                                monthLength[1] = 29;

                            // Check the range of the day
                            if (!(day > 0 && day <= monthLength[month - 1]))
                                dateCheck = 1;

                           
                        }


                    }

                    if (dateCheck == 0 && checkDuplicate == 0) {

                       
                        var NewRequestEndorsement = {

                            "UserID": $scope.RequestEndorsementDetail.UserID, "DetailsID": $scope.RequestEndorsementDetail.DetailsID,
                            "LeadID": $scope.RequestEndorsementDetail.LeadID, "FieldID": $scope.selected.EndorsementField.FieldID, "OldValue": $scope.selected.EndorsementField.OldValue,
                            "NewValue": $scope.selected.EndorsementField.NewValue

                        };
                        debugger;
                        EndorsementAJService.AddNewEndorsementField(NewRequestEndorsement).success(function (response) {
                            $scope.EndorsementDetail.DetailsID = response.DetailsID;
                            $scope.RequestEndorsementDetail.DetailsID = response.DetailsID
                            $scope.GetEndorsementFieldList(1, callType);
                            $scope.FieldDocsDetails = response;
                            if (!$scope.FieldDocsDetails.IsDocReceived) {

                                $scope.SelectedDocUploadField = {
                                    "LeadID": $scope.RequestEndorsementDetail.LeadID, "FieldID": $scope.selected.EndorsementField.FieldID,
                                    "DetailsID": response.DetailsID, "DocReq": $scope.selected.EndorsementField.DocReq
                                }
                                $scope.EndorsementFileData = [];
                                debugger;
                                $scope.FieldDocsDetails = response;
                                var chkExistingDoc = 0;
                                for (var i = 0; i < $scope.FieldDocsDetails.EndorsementDocReqList.length; i++) {
                                    $scope.FieldDocsDetails.EndorsementDocReqList[i].NewHostName = '';
                                    if ($scope.FieldDocsDetails.EndorsementDocReqList[i].HostName != '') {
                                        chkExistingDoc = chkExistingDoc + 1;
                                    }
                                }
                                if (chkExistingDoc > 0) {
                                    $scope.FieldDocsDetails.chkExistingDoc = 1;
                                }

                                modalInstanceSameCtrl = $uibModal.open({
                                    animation: true,
                                    size: 'lg',
                                    scope: $scope,
                                    templateUrl: 'views/UploadFieldDoc.html'

                                });
                                $scope.FieldDocsDetails.Show = true;

                            }
                            $scope.selected.EndorsementField = undefined;



                        });

                    }
                    else if (dateCheck == 1) { alert('Please enter in MM/DD/YYYY format'); }
                    else if (dateCheck == 2) { alert('Date should be less than current date.'); }
                    else if (checkDuplicate == 1) { alert('Duplicate record'); }

                }
                else
                    alert("New value can not be equal to existing value !!!");
            }
            else
                alert("Please fill valid input !!!");

        }
       
    };


    $scope.DeleteRow = function (index) {        
        $scope.EndorsementList.splice(index, 1);
      
    }

    $scope.RejectEndorsement = function (value) {        
        if (value == 2) {
            $scope.PremiumStatus.ButtonDiv = false;
            $scope.PremiumStatus.Rejection.Display = true;
            $scope.PremiumStatus.Disabled = true;
        }
        else if (value == 3) {
            $scope.PremiumChangeStatus.ButtonDiv = false;
            $scope.PremiumChangeStatus.Rejection.Display = true;
            $scope.PremiumChangeStatus.Disabled = true;
        }
        else if (value == 4) {
            $scope.SCVerifiedStatus.ButtonDiv = false;
            $scope.SCVerifiedStatus.Rejection.Display = true;
            $scope.SCVerifiedStatus.Disabled = true;
        }
        else if (value == 5) {
            $scope.SCReceivedStatus.ButtonDiv = false;
            $scope.SCReceivedStatus.Rejection.Display = true;
            $scope.SCReceivedStatus.Disabled = true;
        }
        else if (value == 6) {
            $scope.DocumentVerification.ButtonDiv = false;
            $scope.DocumentVerification.Rejection.Display = true;
            $scope.DocumentVerification.Disabled = true;
        }
    }
    $scope.CancelRejection = function (value) {
        if (value == 2) {
            $scope.PremiumStatus.ButtonDiv = true;
            $scope.PremiumStatus.Rejection.Display = false;
            $scope.PremiumStatus.Disabled = false ;
        }
        else if (value == 3) {
            $scope.PremiumChangeStatus.ButtonDiv = true;
            $scope.PremiumChangeStatus.Rejection.Display = false;
            $scope.PremiumChangeStatus.Disabled = false;
        }
        else if (value == 4) {
            $scope.SCVerifiedStatus.ButtonDiv = true;
            $scope.SCVerifiedStatus.Rejection.Display = false;
            $scope.SCVerifiedStatus.Disabled = false;
        }
        else if (value == 5) {
            $scope.SCReceivedStatus.ButtonDiv = true;
            $scope.SCReceivedStatus.Rejection.Display = false;
            $scope.SCReceivedStatus.Disabled = false;
        }
        else if (value == 6) {
            $scope.DocumentVerification.ButtonDiv = true;
            $scope.DocumentVerification.Rejection.Display = false;
            $scope.DocumentVerification.Disabled = false;
        }
    }

    $scope.SaveRejection = function () {
        debugger;
        if ($scope.selected.RejectionStatus.StatusMasterID > 0) {
            var r = confirm("Are you sure you want to reject the endorsement?");
            if (r == true) {
                $scope.EndorsementDetail.EndorsementStatus.ReasonId = $scope.selected.RejectionStatus.StatusMasterID;
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 12;

                var updateEndorsementStatus = EndorsementAJService.UpdateEndorsementStatus($scope.EndorsementDetail, $routeParams.userID);
                updateEndorsementStatus.then(function (msg) {
                    $scope.GetEndorsementFieldList(1, callType);
                    alert(msg.data);
                    debugger;
                   
                }, function () {
                    alert(msg.data);
                });
            }
        }
    }

    $scope.ChangePremium = function (value) {
        $scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange = value;
        if (value == 0) {
            $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumChange = 0.0;            
        }
        debugger;
    }

    

    $scope.CollectedConfirm = function (value) {
        debugger;
        if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 6 || $scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 8) {
            $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded = value;
        }
        else { $scope.TempCollectedConfirm = value; }
    }
    $scope.SCVerifiedConfirm = function (value) {
        $scope.EndorsementDetail.EndorsementAddtionalInformation.SCVerified = value;
    }
    $scope.SCReceivedConfirm = function (value) {
        $scope.EndorsementDetail.EndorsementAddtionalInformation.SCReceived = value;
    }
    
    
    $scope.ConfirmClick = function (type) {
        debugger;
        var res = 0;
        if (type == 1001) {
            if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 1001) {
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 1;//Initiated By Agent
                res = 1;
            }
            else if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 3) {
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 5;//Resubmitted
                res = 1;
            }
           
        }
        else if (type == 1) {
            $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 4;//Pending With Insurer
            res = 1;
        }
        else if (type == 1) {
            $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 4;//Pending With Insurer
            res = 1;
        }
        else if (type == 2) {
            if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == "0") {

                $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumChange = 0.0;
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 0;
                res = 1;
            }
            else if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange != "0") {

                if (parseFloat($scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumChange) > 0.0) {

                    if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == "1") {
                        $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 6;//Rate Up Initiated
                        res = 1;
                    }
                    else {
                        $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 8;//Refund Initiated
                        res = 1;
                    }
                } else { alert("Please enter amount"); res = 0; }
            }
        }
        else if (type == 3) {
            if ($scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded == "0")
            {
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 0;
                res = 1;
            }
            else {
                if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == "1") {
                    if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 6) {
                        $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 7;//Rate Up Completed
                    }
                    else { $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 0; }
                    res = 1;
                }
                else if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == "2") {
                    if ($scope.EndorsementDetail.EndorsementStatus.StatusMasterID == 8) {
                        $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 9;//Refund Completed
                    }
                    else { $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 0; }
                    res = 1;
                }
                
            }            
        }
        else if (type == 4) {
            if ($scope.EndorsementDetail.EndorsementAddtionalInformation.SCVerified == "0") {
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 0;
                res = 1;
            }
            else if ($scope.EndorsementDetail.EndorsementAddtionalInformation.SCVerified == "1") {
                if ($scope.EndorsementDetail.EndorsementDocumentCopyList != undefined && $scope.EndorsementDetail.EndorsementDocumentCopyList.length > 0) {
                    $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 10;//Soft Copy Received
                    res = 1;
                }
                else {
                    alert("Please upload endorsement soft copy first.");
                    $scope.EndorsementDetail.EndorsementAddtionalInformation.SCVerified = -1;
                    return false;
                }
               
            }
        }
        else if (type == 5) {
            debugger;
            if ($scope.EndorsementDetail.EndorsementAddtionalInformation.SCReceived == "0") {
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 0;
                res = 1;
            }
            else if ($scope.EndorsementDetail.EndorsementAddtionalInformation.SCReceived == "1") {
                if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == 2 && $scope.TempCollectedConfirm == 0 && $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded == 0) {
                    alert("Please mark Refund Completed then close the endorsement.");
                    res = 0;
                }
                else if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == 2 && $scope.TempCollectedConfirm == 1) {
                    $scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded = $scope.TempCollectedConfirm;
                    $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 11;//Closed
                    res = 2;
                }
                else if ($scope.EndorsementDetail.EndorsementAddtionalInformation.IsPremiumChange == 0) {                    
                    $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 11;//Closed
                    res = 1;
                }
                else if ($scope.EndorsementDetail.EndorsementAddtionalInformation.PremiumCollectedRefunded == 1) {
                    $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 11;//Closed
                    res = 1;
                }
                else {
                    res = 0;
                }
                
            }
        }
        debugger;
        
        if (res == 1 || res == 2) {
            EndorsementAJService.UpdateEndorsementStatus($scope.EndorsementDetail).success(function (msg) {
                $scope.GetEndorsementFieldList(1, callType);
                if (msg == 'No changes found.') {
                    alert('No option is selected');
                } else {
                    alert(msg);
                }
                debugger;
                callType = 1;
                

            });
        }
        else { alert('Please choose option for save');}
        debugger;
    }

    $scope.RaiseEndorsement = function () {
        $scope.RequestEndorsementDetail.EndorsementFieldMasterList = $scope.EndorsementList;
        $scope.RequestEndorsementDetail.LeadID = $scope.EndorsementDetail.LeadDetail.LeadId;
        $scope.RequestEndorsementDetail.UserID = $scope.EndorsementDetail.UserID;
        if ($scope.EndorsementList!=[]) {
            var RaiseEndorsementStatus = EndorsementAJService.RaiseEndorsement($scope.RequestEndorsementDetail);
            RaiseEndorsementStatus.then(function (msg) {
                return msg;
                       
            }, function () {
            });
        }
        else
            alert("Please fill valid input !!!");
       

    };

   
    $scope.onSelect = function ($item, $model, $label) {
        debugger;
        if($model.MasterType == 1){
            EndorsementAJService.GetFieldMasterDataList($model.FieldID).success(function (FieldList) {
                $model.FieldMasterDataList = FieldList;
            });
        }
        else if ($model.MasterType == 2) {
            EndorsementAJService.GetFieldMasterDataList($model.FieldID).success(function (FieldList) {
                $model.FieldMasterDataList = FieldList;
            });
        }
       
    };
    $scope.DocumentsUploadApp = function (callType) {

        if (callType == 0) {
            $scope.AddEndorsement();
            $scope.RequestEndorsementDetail.EndorsementFieldMasterList = $scope.EndorsementList;
            $scope.RequestEndorsementDetail.LeadID = $scope.EndorsementDetail.LeadID;
            if ($scope.EndorsementList.length >0) {
                for (var i = 0; i < $scope.EndorsementList.length; i++) {
                    $scope.EndorsementList[i].FieldMasterDataList = undefined;
                }

                EndorsementAJService.RaiseEndorsement($scope.RequestEndorsementDetail).success(function (response) {
                    if (response != "error") {
                        EndorsementAJService.GetEndorsementDetails($scope.EndorsementDetail.LeadID,1).success(function (FieldList) {
                            debugger;
                            $scope.EndorsementDetail = {
                                "EndorsementFieldMasterList": FieldList.EndorsementFieldMasterList,
                                "EndorsementStatus": FieldList.EndorsementStatus,
                                "EndorsementAddtionalInformation": FieldList.EndorsementAddtionalInformation, "RejectionReasonList": FieldList.RejectionReasonList
                            }

                            //$scope.EndorsementDetail.UserID = $routeParams.userID;
                            $scope.EndorsementDetail.LeadID = FieldList.LeadID;
                            //$scope.EndorsementDetail.UserID = $routeParams.userID;
                            $scope.EndorsementDetail.CustomerID = FieldList.CustomerID;
                            $scope.EndorsementDetail.ProductID = FieldList.ProductID;
                            $scope.EndorsementDetail.SupplierID = FieldList.SupplierID;
                            $scope.EndorsementDetail.PlanId = FieldList.PlanId;
                            $scope.EndorsementDetail.SupplierName = FieldList.SupplierName;
                            $scope.EndorsementDetail.PlanName = FieldList.PlanName;

                            $scope.RequestEndorsementDetail.UserID = $routeParams.userID;
                            $scope.RequestEndorsementDetail.LeadID = FieldList.LeadID;
                            $scope.RequestEndorsementDetail.UserID = $routeParams.userID;
                            $scope.RequestEndorsementDetail.CustomerID = FieldList.CustomerID;
                            $scope.RequestEndorsementDetail.ProductID = FieldList.ProductID;
                            $scope.RequestEndorsementDetail.SupplierID = FieldList.SupplierID;
                            $scope.RequestEndorsementDetail.PlanId = FieldList.PlanId;
                            $scope.RequestEndorsementDetail.SupplierName = FieldList.SupplierName;
                            $scope.RequestEndorsementDetail.PlanName = FieldList.PlanName;


                            var modalInstance = $uibModal.open({
                                animation: true,
                                size: 'lg',
                                templateUrl: 'views/DocumentUpload.html',
                                controller: 'DocumentsUploadApp',
                                resolve: {
                                    params: function () {
                                        return {
                                            LeadID: $scope.RequestEndorsementDetail.LeadID, CustomerID: $scope.RequestEndorsementDetail.CustomerID, userID: $scope.RequestEndorsementDetail.UserID, Type: "2"
                                            , EndorsementID: "ertre", EndorsementDetail: $scope.EndorsementDetail

                                        };
                                    }
                                }
                            });
                        });
                    }
                    else { alert("Error occured !!!!"); }
                });
              
            }
            else
                alert("Please fill valid input !!!");
            
        }
        else {
            var modalInstance = $uibModal.open({
                animation: true,
                size: 'lg',
                templateUrl: 'views/DocumentUpload.html',
                controller: 'DocumentsUploadApp',
                resolve: {
                    params: function () {
                        return {
                            LeadID: $scope.RequestEndorsementDetail.LeadID, CustomerID: $scope.RequestEndorsementDetail.CustomerID, userID: $scope.RequestEndorsementDetail.UserID, Type: "2"
                            , EndorsementID: "ertre", EndorsementDetail: $scope.EndorsementDetail

                        };
                    }
                }
            });

        }
    };
    $scope.EndorsementFileData = [];

    $scope.RemoveEndorsementField = function (FieldID) {
        var EndorsementFieldUpdate = {
            "BookingID": $scope.EndorsementDetail.LeadID,
            "DetailsID": $scope.EndorsementDetail.DetailsID,
            "FieldID": FieldID,
            "UserID": $scope.EndorsementDetail.UserID,
            "TYPE": 1
        };
        var r = confirm("Are you sure to remove the field??");
        if (r == true) {
            EndorsementAJService.UpdateEndorsementFieldDetails(EndorsementFieldUpdate).success(function (response) {
                $scope.GetEndorsementFieldList(1, callType);
                debugger;
            });
        }
    };

    $scope.SaveFieldDetails = function () {
        var EndorsementFieldUpdate = {
            "BookingID": $scope.EndorsementDetail.LeadID,
            "DetailsID": $scope.SelectedDocUploadField.DetailsID,
            "FieldID": $scope.SelectedDocUploadField.FieldID,
            "UserID": $scope.EndorsementDetail.UserID,          
            "TYPE": 2
        };
        EndorsementAJService.UpdateEndorsementFieldDetails(EndorsementFieldUpdate).success(function (response) {
            modalInstanceSameCtrl.close();
            $scope.GetEndorsementFieldList(1, callType);
            debugger;
        });
    };


   
    $scope.RaiseFreshEndorsement = function () {
        debugger;
        var r = confirm("Are you sure you want to create Endorsement ??");
        if (r == true) {
            var requestEndorsement = undefined;
            requestEndorsement = $scope.EndorsementDetail;


            if (requestEndorsement.EndorsementStatus.StatusMasterID == 3) {
                $scope.EndorsementDetail.EndorsementStatus.StatusMasterID = 5;
            }
            else { requestEndorsement.EndorsementStatus.StatusMasterID = 1; }

            $scope.buttonClick = true;
            EndorsementAJService.UpdateEndorsementStatus(requestEndorsement).success(function (msg) {                
                $scope.GetEndorsementFieldList(1, callType);
                alert(msg);
                
            });
        }
    };
    $scope.cancelSave = function () {
        $scope.FieldDocsDetails.Show = true;
    };


    $scope.SaveEndorsementDocs = function () {
        debugger;
        
        var chkValidation = '';
        var objReqDocUploadRequest = {
           
                "DetailsID": $scope.SelectedDocUploadField.DetailsID,
                "UserID": $scope.EndorsementDetail.UserID,
                "LeadID": $scope.EndorsementDetail.LeadID,
                "FieldID": $scope.SelectedDocUploadField.FieldID,
                "CustomerID": $scope.EndorsementDetail.CustomerID,
                "ProductID": $scope.EndorsementDetail.ProductID
                ,"ReqDocRequestList": []
           

        };
        debugger;
        var chkDocCount = 0;
        for (var i = 0; i < $scope.FieldDocsDetails.EndorsementDocReqList.length; i++) {
            if ($.trim($scope.FieldDocsDetails.EndorsementDocReqList[i].NewHostName) != '') {
                chkDocCount = chkDocCount + 1;
                var chkDocReq = 0;
                if ($scope.EndorsementFileData.length > 0) {
                    for (var j = 0; j < $scope.EndorsementFileData.length ; j++) {

                        if ($scope.FieldDocsDetails.EndorsementDocReqList[i].DocumentID == $scope.EndorsementFileData[j].DocumentID) {
                            chkDocReq = 1;
                            var docDetails = undefined;
                            docDetails = {
                                "DocumentID": $scope.FieldDocsDetails.EndorsementDocReqList[i].DocumentID
                                , "DocCategoryID": $scope.FieldDocsDetails.EndorsementDocReqList[i].DocCategoryID
                                , "HostName": $scope.FieldDocsDetails.EndorsementDocReqList[i].NewHostName
                                , "FileName": $scope.EndorsementFileData[j].FileName
                                , "FileStream": $scope.EndorsementFileData[j].FileStringData

                            };
                            objReqDocUploadRequest.ReqDocRequestList.push(docDetails);
                        }
                       // else { chkValidation = chkValidation + 'Please choose a file for ' + $scope.FieldDocsDetails.EndorsementDocReqList[i].Document + '. '; }
                        if (j == $scope.EndorsementFileData.length -1) {
                            if (chkDocReq == 0) {
                                chkValidation = chkValidation + 'Please choose a file for ' + $scope.FieldDocsDetails.EndorsementDocReqList[i].Document + '. ';
                            } 
                        }
                    }
                }
                else {
                    chkValidation = chkValidation + 'Please choose a file for ' + $scope.FieldDocsDetails.EndorsementDocReqList[i].Document + '. ';
                }
            }
        }
        if ($.trim(chkValidation) != '') {
            alert(chkValidation);
        }
        else if (chkDocCount == 0) {
            alert('Please choose a file and Doc Name.');
        }
        else {            
            EndorsementAJService.UpdateDocumentDetails(objReqDocUploadRequest).success(function (response) {
                alert("Docs uploaded successfully.");
                debugger;
                $scope.FieldDocsDetails = response;
                var chkExistingDoc = 0;
                for (var i = 0; i < $scope.FieldDocsDetails.EndorsementDocReqList.length; i++) {
                    $scope.FieldDocsDetails.EndorsementDocReqList[i].NewHostName = '';
                    if ($scope.FieldDocsDetails.EndorsementDocReqList[i].HostName != '') {
                        chkExistingDoc = chkExistingDoc + 1;
                    }
                }
                if (chkExistingDoc>0) {
                    $scope.FieldDocsDetails.chkExistingDoc = 1;
                }
                if ($scope.FieldDocsDetails.IsDocReceived) {
                    $scope.FieldDocsDetails.Show = false;
                }
                else {
                    $scope.FieldDocsDetails.Show = true;
                }
            });
        }
    };

    $scope.SaveExistingDocs = function (doc) {

        var objReqDocUploadRequest = {

            "DetailsID": $scope.SelectedDocUploadField.DetailsID,
            "UserID": $scope.EndorsementDetail.UserID,
            "LeadID": $scope.EndorsementDetail.LeadID,
            "FieldID": $scope.SelectedDocUploadField.FieldID,
            "CustomerID": $scope.EndorsementDetail.CustomerID,
            "ProductID": $scope.EndorsementDetail.ProductID
              , "ReqDocRequestList": []


        };

        var docDetails = {
            "DocumentID": "0"
                              , "DocCategoryID": "0"
                              , "HostName": "0"
                              , "FileName": "0"
                              , "FileStream": "0"
            , "ParentCustDocumentID": doc.CustDocumentID

        };
        objReqDocUploadRequest.ReqDocRequestList.push(docDetails);

        if (doc.Checked != false) {
            debugger;
            var r = confirm("Are you sure to upload the document??");
            if (r == true) {
                EndorsementAJService.UpdateDocumentDetails(objReqDocUploadRequest).success(function (response) {
                    debugger;
                    $scope.FieldDocsDetails = response;
                    var chkExistingDoc = 0;
                    for (var i = 0; i < $scope.FieldDocsDetails.EndorsementDocReqList.length; i++) {
                        $scope.FieldDocsDetails.EndorsementDocReqList[i].NewHostName = '';
                        if ($scope.FieldDocsDetails.EndorsementDocReqList[i].HostName != '') {
                            chkExistingDoc = chkExistingDoc + 1;
                        }
                    }
                    if (chkExistingDoc > 0) {
                        $scope.FieldDocsDetails.chkExistingDoc = 1;
                    }
                    if ($scope.FieldDocsDetails.IsDocReceived) {
                        $scope.FieldDocsDetails.Show = false;
                    }
                    else {
                        $scope.FieldDocsDetails.Show = true;
                    }
                });
            }
            else { doc.Checked = false; }
        }
    };


    $scope.DocExistingFilter = function (docs) {
        if (docs.DocumentUrl != 0)
            return undefined
        else
            docs
    };

    $scope.file_changed = function (element) {
        debugger;
       
        $scope.$apply(function (scope) {
            
            var photofile = element.files[0];

            var fileReader = new FileReader();
            var EndorsementData = {
                "DocCategoryID": undefined,
                "DocumentID": undefined,
                "FileName": undefined,
                "Size": undefined,
                "FileStringData": undefined
            };
            EndorsementData.DocCategoryID = element.id.split(':')[0];
            EndorsementData.DocumentID = element.id.split(':')[1];

            fileReader.onload = function (fileLoadedEvent) {
                var chkIndex = undefined;
                EndorsementData.FileName = element.files[0].name;
                EndorsementData.Size = element.files[0].size
                EndorsementData.FileStringData = btoa(fileLoadedEvent.target.result);
                if ($scope.EndorsementFileData.length > 0) {
                    for (var i = 0; i < $scope.EndorsementFileData.length; i++) {
                        if ($scope.EndorsementFileData[i].DocumentID == element.id.split(':')[1]) {
                            chkIndex = i;
                        }
                    }
                }
                if (chkIndex != undefined) {
                    $scope.EndorsementFileData.splice(chkIndex, 1);
                }
                $scope.EndorsementFileData.push(EndorsementData);
                //
            };
            if (element.files.length > 0) {
                fileReader.readAsBinaryString(element.files[0]);
            }
            else {
                if ($scope.EndorsementFileData.length > 0) {
                    for (var i = 0; i < $scope.EndorsementFileData.length; i++) {
                        if ($scope.EndorsementFileData[i].DocumentID == element.id.split(':')[1]) {
                            $scope.EndorsementFileData.splice(i, 1);
                        }
                    }
                }
            }

        });
    };
    $scope.UploadFieldDocs = function (LeadID, FieldID, DetailsID, DocReq) {
        $scope.SelectedDocUploadField = { "LeadID": LeadID, "FieldID": FieldID, "DetailsID": DetailsID, "DocReq": DocReq }
        var NewRequestEndorsement = {
            "UserID": $scope.EndorsementDetail.UserID, "DetailsID": DetailsID,
            "LeadID": LeadID, "FieldID": FieldID
        };
        
        EndorsementAJService.GetNewFieldDocs(NewRequestEndorsement).success(function (response) {
            $scope.EndorsementFileData = [];
            debugger;
            if (response != "error") {
                $scope.FieldDocsDetails = response;
                var chkExistingDoc = 0;
                for (var i = 0; i < $scope.FieldDocsDetails.EndorsementDocReqList.length; i++) {
                    $scope.FieldDocsDetails.EndorsementDocReqList[i].NewHostName = '';
                    if ($scope.FieldDocsDetails.EndorsementDocReqList[i].HostName != '') {
                        chkExistingDoc = chkExistingDoc + 1;
                    }
                }
                if (chkExistingDoc > 0) {
                    $scope.FieldDocsDetails.chkExistingDoc = 1;
                }
                if ($scope.FieldDocsDetails.IsDocReceived) {
                    $scope.FieldDocsDetails.Show = false;
                }
                else {
                    $scope.FieldDocsDetails.Show = true;
                }

                modalInstanceSameCtrl = $uibModal.open({
                    animation: true,
                    size: 'lg',
                    scope: $scope,
                    templateUrl: 'views/UploadFieldDoc.html'

                });
            }

        });

    };
        
    $scope.CreateFreshEndorsement = function () {
        $scope.EndorsementDetail = {
            "EndorsementFieldMasterList": undefined,
            "EndorsementStatus": undefined,
            "EndorsementAddtionalInformation": undefined, "RejectionReasonList": undefined, "EndorsementDocumentCopyList": undefined
        };

        $scope.RequestEndorsementDetail = {
            "EndorsementFieldMasterList": undefined,
            "EndorsementStatus": undefined,
            "EndorsementAddtionalInformation": undefined, "RejectionReasonList": undefined
        };
        $scope.NoChange = NoChange;
        $scope.RefundPending = 0;
        $scope.TempCollectedConfirm = 0;
        $scope.PremiumStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
        $scope.PremiumChangeStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
        $scope.SCVerifiedStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
        $scope.SCReceivedStatus = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };
        $scope.DocumentVerification = { "ContentDiv": false, "ButtonDiv": false, "Disabled": false, "Rejection": { "Display": false, "Disabled": false } };





        $scope.selected = { "EndorsementField": undefined, "RejectionStatus": undefined };
        $scope.GetEndorsementFieldList(2, callType);
    };
    $scope.GetEndorsementDocList = function (type) {
        var EndorsementFieldUpdate = {
            "BookingID": $scope.EndorsementDetail.LeadID,
            "DetailsID": $scope.EndorsementDetail.DetailsID
        };
        EndorsementAJService.GetEndorsementDocList(EndorsementFieldUpdate).success(function (response) {
            $scope.DocVerificationList = response;

            $scope.DocVerificationList.Show = type;

        }
        );
    };

    $scope.UploadEndorsementSoftCopy = function () {
        debugger;
        var objReqDocUploadRequest = {

            "DetailsID": $scope.EndorsementDetail.DetailsID,
            "UserID": $scope.EndorsementDetail.UserID,
            "LeadID": $scope.EndorsementDetail.LeadID,
            "CustomerID": $scope.EndorsementDetail.CustomerID,
            "ProductID": $scope.EndorsementDetail.ProductID
              , "ReqDocRequestList": []


        };

        var checkUpload = 0;


        if ($window.EndorsementFile.FileName != '') {
            checkUpload = 1;           
            docDetails = {
                "DocumentID": "42"
                                , "DocCategoryID": "1"
                                , "FileName": $window.EndorsementFile.FileName
                                , "FileStream": $window.EndorsementFile.AttachemntContent

            };
            objReqDocUploadRequest.ReqDocRequestList.push(docDetails);
        }
        if ($window.PolicyFile.FileName != '') {
            checkUpload = checkUpload + 1;          
            docDetails = {
                "DocumentID": "43"
                                , "DocCategoryID": "1"
                                , "FileName": $window.PolicyFile.FileName
                                , "FileStream": $window.PolicyFile.AttachemntContent

            };
            objReqDocUploadRequest.ReqDocRequestList.push(docDetails);
        }
        if (checkUpload == 0) {
            alert('Please choose atleast one file');
        }
        else {
            debugger;
            EndorsementAJService.UpdateEndorsementCopyDetails(objReqDocUploadRequest).success(function (response) {
                alert(response);
                $scope.GetEndorsementFieldList(1, callType);
                modalInstanceSameCtrl.close();
                modalInstanceSameCtrl.close();
            });
        }
    };
  
    $scope.DocVerify = function (type) {
               
        $scope.GetEndorsementDocList(type);

        modalInstanceSameCtrl = $uibModal.open({
            animation: true,
            size: 'lg',
            scope: $scope,
            templateUrl: 'views/docverification.html'           
          
        });
        debugger;
       
    };
    $scope.SaveDocVerification = function () {
        debugger;
        $scope.RequestEndorsementDocList = { "LeadID": "", "UserID": "", "DetailId": "", "RequestEndorsementDocStatusList": [] };
        $scope.RequestEndorsementDocList.LeadID = $scope.RequestEndorsementDetail.LeadID;
        $scope.RequestEndorsementDocList.UserID = $scope.RequestEndorsementDetail.UserID;
        $scope.RequestEndorsementDocList.DetailId = $scope.EndorsementDetail.EndorsementAddtionalInformation.DetailId;
        var cAll=0,cSel=0;
        for (var i = 0; i < $scope.DocVerificationList.DocCategoryList.length; i++) {

            for (var j = 0; j < $scope.DocVerificationList.DocCategoryList[i].DocumentsList.length; j++) {

                if ($scope.DocVerificationList.DocCategoryList[i].DocumentsList[j].SelectedStatus.VerificationStatusName != 'To Be Verified') {
                    cSel = cSel + 1;
                    $scope.RequestEndorsementDocList.RequestEndorsementDocStatusList.push({ "CustDocumentId": $scope.DocVerificationList.DocCategoryList[i].DocumentsList[j].CustDocumentId, "VerificationStatusId": $scope.DocVerificationList.DocCategoryList[i].DocumentsList[j].SelectedStatus.VerificationStatusId });
                }
                cAll = cAll + 1;


            }

        }

        
        if (cAll == cSel) {
            debugger;           
            EndorsementAJService.SaveDocVerification($scope.RequestEndorsementDocList).success(function (response) {
                if (response == 1) {
                    $scope.GetEndorsementFieldList(1, callType);
                    alert('Documents verified successfully.');

                  
                    modalInstanceSameCtrl.close();
                }
                else {
                    $scope.GetEndorsementFieldList(1, callType);
                    alert('Document verification status updated successfully.');                  
                    modalInstanceSameCtrl.close();
                    
                }

                debugger;



            });
        }
        else { alert('Please select status for all documents.');}
    };

    $scope.UploadEndorsementCopy = function () {

        modalInstanceSameCtrl = $uibModal.open({
            animation: true,
            size: 'lg',
            scope: $scope,
            templateUrl: 'views/uploadpolicydoc.html'

        });
        debugger;       
    };
    $scope.cancel = function () {
        modalInstanceSameCtrl.close();
    };
    $scope.$on('saveStatus', function (event, args) {
        $scope.GetEndorsementFieldList(1, callType);
        $scope.ConfirmClick(1001);
       
    });
   

});

EndorsementApp.controller("UploadDocController", function ($scope, params) {

    $scope.URL = "34535345345";
    $scope.params = params;

});

EndorsementApp.controller("EndorsementListController", function ($scope, $routeParams, $rootScope, $uibModal, EndorsementListAJService) {

    var EndorsementList = undefined;
    var EndorsementStatus = undefined;
    var LeadDetail = undefined;
    var EndorsementAddtionalInformation = undefined;
    var RejectionReasonList = undefined;
    var EndorsementDocumentCopyList = undefined;
    var LeadID = undefined;
    var UserId = $routeParams.userID;
   // $scope.SelectedStatusForRollback = undefined;

    $scope.EndorsementListDetails = {
        "EndorsementList": EndorsementList       
    };
    $scope.userID = $routeParams.userID;
    $scope.Role = $routeParams.Role;
    $scope.GetEndorsementListDetails = function () {
     
        if (LeadID == '' || LeadID == undefined) {
            LeadID = $routeParams.LeadID;
        }
        if ($.isNumeric(LeadID)) {
            EndorsementListAJService.GetEndorsementListDetails(LeadID).success(function (FieldList) {
                debugger;
                $scope.EndorsementListDetails.EndorsementList = FieldList.EndorsementList;

                $scope.EndorsementListDetails.LeadID = FieldList.LeadID;
                $scope.EndorsementListDetails.CustName = FieldList.CustName;
                $scope.EndorsementListDetails.CustomerID = FieldList.CustomerID;
                $scope.EndorsementListDetails.SupplierID = FieldList.SupplierID;
                $scope.EndorsementListDetails.PlanId = FieldList.PlanId;
                $scope.EndorsementListDetails.PlanName = FieldList.PlanName;
                $scope.EndorsementListDetails.SupplierName = FieldList.SupplierName;
                $scope.EndorsementListDetails.ProductName = FieldList.ProductName;
                $scope.EndorsementListDetails.IsEligible = FieldList.IsEligible;
            });
        }
    }
    var modalInstanceSameCtrl;
    var EndorsementStatusdetails = {       
        "DetailsID": undefined,
        "StatusDetails": []
    };
    $scope.OpenDetails = function (DetailsID) {
        debugger;
        EndorsementStatusdetails.DetailsID = DetailsID;
        EndorsementListAJService.GetEndorsementStatusdetails(DetailsID).success(function (data) {
            EndorsementStatusdetails.StatusDetails = data;
            $scope.EndorsementStatusdetails = EndorsementStatusdetails;
            modalInstanceSameCtrl = $uibModal.open({
                animation: true,
                size: 'lg',
                scope: $scope,
                templateUrl: 'views/EndorsementStatusDetails.html'

            });
        });
    };
    $scope.cancel = function () {
        modalInstanceSameCtrl.close();
    };
    $scope.GetEndorsementListDetails();

    $scope.RollbackEndorsement = function () {
        debugger;
        var r = confirm("Are you sure to rollback to '" + $scope.SelectedStatusForRollback.StatusMasterName + "' Status?");
        if (r == true) {
            if ($scope.SelectedStatusForRollback != undefined) {
                var objEndorsementRollBackRequest = {
                    "DetailsID": EndorsementStatusdetails.DetailsID,
                    "BookingID": LeadID,
                    "StatusMasterID": $scope.SelectedStatusForRollback.StatusMasterID,
                    "UserID": UserId
                };
                EndorsementListAJService.RollbackEndorsement(objEndorsementRollBackRequest).success(function (response) {
                    debugger;
                    EndorsementStatusdetails.StatusDetails = response;
                    $scope.EndorsementStatusdetails = EndorsementStatusdetails;
                });
            }
            else {
                alert('Please select Rollback Status');
            }
        }
    };

    $scope.getVal = function (data) {
        $scope.SelectedStatusForRollback = data;
    }

});
