﻿
EndorsementApp.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
});

EndorsementApp.service("EndorsementAJService", function ($http) {   

    this.FileData = [];
    this.GetEndorsementDetails = function (LeadID, DetailsID, Type) {
        return $http({
            method: "GET",            
            url: config.serviceURL + "EndorsementService.svc/GetEndorsementDetails/" + LeadID + "/" + DetailsID + "/" + Type
        }).success(function (data) {
           
            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    }

    this.GetFieldMasterDataList = function (FieldID) {          
        return $http({
            method: "GET",
            url: config.serviceURL + "EndorsementService.svc/GetFieldMasterDataList/" + FieldID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    }

    //this.GetEndorsementDocList = function (EndorsementFieldUpdate) {
    //    return $http.get(config.serviceURL + "EndorsementService.svc/GetEndorsementDocList/" + LeadID);

    //}

    this.GetEndorsementDocList = function (EndorsementFieldUpdate) {
        debugger;
        var request = $http({
            method: "POST",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/GetEndorsementDocList",
            data: JSON.stringify(EndorsementFieldUpdate)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.AddNewEndorsementField = function (NewRequestEndorsement) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/AddNewEndorsementField",
            data: JSON.stringify(NewRequestEndorsement)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.UpdateDocumentDetails = function (objReqDocUploadRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/UpdateDocumentDetails",
            data: JSON.stringify(objReqDocUploadRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.UpdateEndorsementCopyDetails = function (objReqDocUploadRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/UpdateEndorsementCopyDetails",
            data: JSON.stringify(objReqDocUploadRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.UpdateEndorsementFieldDetails = function (EndorsementFieldUpdate) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/UpdateEndorsementFieldDetails",
            data: JSON.stringify(EndorsementFieldUpdate)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.GetNewFieldDocs = function (NewRequestEndorsement) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/GetNewFieldDocs",
            data: JSON.stringify(NewRequestEndorsement)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.RaiseEndorsement = function (EndorsementList) {
        debugger;
            var request = $http({
                method: "post",
                dataType: 'json',
                url: config.serviceURL + "EndorsementService.svc/RaiseEndorsement",
                data: JSON.stringify(EndorsementList)
                ,headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }
            })
           .error(function (error) {
               alert(error);
           });
        
            return request;
    }
    this.UploadFileDetails = function (PayLoad) {
        var uploadUrl = config.docUploadURL + "BMSWCFRest.svc/UploadFileDetails";
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: uploadUrl,
            data: JSON.stringify(PayLoad)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           debugger;
           console.log(error);
       });

        return request;

    }
    this.SaveDocVerification = function (RequestEndorsementDocList) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/InsertEndorsementDocumentStatus",
            data: JSON.stringify(RequestEndorsementDocList)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
       
        return request;
    }

    this.UpdateEndorsementStatus = function (EndorsementDetail) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/UpdateEndorsementDetails",
            data: JSON.stringify(EndorsementDetail)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });       
        return request;
    }


    
});


EndorsementApp.service("DocumentsUploadAppAJService", function ($http) {
    this.FileData = {};
    this.GetDocuments = function (LeadID, CustomerID, Type) {
        return $http.get("http://10.0.29.223:8870/BMSWCFRest.svc/GetDocumentDetails?LeadID=" + LeadID + "&CustomerID=" + CustomerID + "&Type=2");
    }
    
    this.UploadFileDetails = function (PayLoad, uploadUrl) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: uploadUrl,
            data: JSON.stringify(PayLoad)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           debugger;
           console.log(error);
       });
     
        return request;
        
    }

    this.UpdateEndorsementStatus = function (EndorsementDetail) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/UpdateEndorsementDetails",
            data: JSON.stringify(EndorsementDetail)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });

        return request;
      
    }


});


EndorsementApp.service("EndorsementListAJService", function ($http) {


    this.GetEndorsementListDetails = function (LeadID) {
        return $http({
            method: "GET",
            url: config.serviceURL + "EndorsementService.svc/GetEndorsementListDetails/" + LeadID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    }

    this.GetEndorsementStatusdetails = function (DetailsID) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "EndorsementService.svc/GetEndorsementStatusdetails/" + DetailsID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    }

    this.RollbackEndorsement = function (objEndorsementRollBackRequest) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "EndorsementService.svc/RollbackEndorsement",
            data: JSON.stringify(objEndorsementRollBackRequest)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

});