﻿angular.module("EndorsementApp", ["ngRoute", "ui.bootstrap"]).config(["$routeProvider",
  function ($routeProvider) {
      $routeProvider.
        when('/EndorsementApp/User/:page/:userID/:Role/:LeadID/:DetailsID/:Type', {
            templateUrl: 'views/endorsement.html',
            controller: 'EndorsementController'
        })
      .when('/EndorsementApp/User/:page/:userID/:Role/:LeadID', {
          templateUrl: 'views/EndorsementList.html',
          controller: 'EndorsementListController'
      })
      ;       
  }]);
