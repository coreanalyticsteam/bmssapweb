﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropertyLayers;
namespace BusinessLogicLayer
{
    public interface IHealthExBL
    {
        List<LeadDetails> GetAllExLead(ReqMarkExSearch objReqMarkExSearch);

        List<MasterSupplier> GetSupplierList();

        //List<SupplierPlanDetails> GetSupplierPlanDetails(string LeadID);

        int MarkLeadForHealthEx(ReqMarkHealthEx objReqMarkHealthEx);

        ResUploadCaseToInsurer GetExLeadDetailsForCaseSend(long ExID);

        int AddSupplierPlanForEx(PostPlanSelectionData objPostPlanSelectionData);

        int AddMemberDouments(PostMemberDocumentDetails objPostMemberDocumentDetails);

        List<LeadDetails> GetLeadsForPBDashboard(ReqMarkExSearch objReqMarkExSearch);

        ResInsurerUnderwriting GetExLeadDetailsForInsurer(long ExID, long SelectionID, int InsurerID, int UserID);

        int UpdateInsurerAction(PostInsurerActionData objPostInsurerActionData);

        ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(long ExID, int Type = 0);

        UserDetails Login(UserLoginRequest objUserLoginRequest);

        List<LeadDetails> GetLeadsForInsurerDashboard(ReqMarkExSearch objReqMarkExSearch);

        int RemoveFromHealthEx(ReqRemoveFromHealthEx objReqRemoveFromHealthEx);

        bool UpdateHealthExStatus(long NewLeadID, long OldLeadID, long SelectionID, int Type, int UserID);

        bool MarkNewLeadForHealthEx(ReqMarkNewLead objReqMarkNewLead);

        bool InsertSupplierPlanNewHEx(List<HealthExSelections> HealthExSelectionsList);

        bool UpdatePaymentDetails(PaymentDetailsData objPaymentDetailsData);

        bool AcceptOrRejectInsurer(PostInsurerActionData objPostInsurerActionData);

        List<MedicalDetails> CustomerMedicalDetails(string base64EncodedLeadID);

        List<PaymentDetailsForEx> GETPaymentDetailsForEx(string base64EncodedLeadID);

        bool RequestAuthenticate(string Token, string UserID);

        LeadDetails ExLoginGetLeadData(long LeadID);

        int SendCommunicationSync(long leadId, string TriggerName, string MobileNo, string EmailID, string BookingType, int ProductID, string SelectionID);

        bool UpdatePremiumForSelection(UpdatePremiumReq objUpdatePremiumReq);

        int InsertInsurerRemarks(InsurerRemarksDetails objInsurerRemarksDetails);

        ResCustomerDocUpload GetCustomerDocUploadDetails(string ExID);

        int AddCustomerDouments(PostMemberDocumentDetails objPostMemberDocumentDetails);

        LeadDocumentDetails GetHExLeadMemberDocDetails(Int64 NewLeadID, long SelectionID);

        ResPBInsurerUnderwriting InsurerUnderwritingDecisionDetails(Int64 LeadId, long SelectionID);

        int AddNewUsers(AddNewUser objAddNewUser);

        int ChangePassword(ChangePassword objChangePassword);

        List<MasterSupplier> GetSupplierListByProduct(string ProductID);

        List<AddNewUser> GetHealthExUser();

        int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo);

        int AddInspectionDocuments(InspectionDocumentDetails objInspectionDocumentDetails);

    }
}
