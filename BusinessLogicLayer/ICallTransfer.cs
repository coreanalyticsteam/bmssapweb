﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropertyLayers;
namespace BusinessLogicLayer
{
    public interface ICallTransfer
    {
        List<LeadAgentList> GetDialerAgentList(DialerSearchRequest objDialerSearchRequest);

        List<ProductDetails> GetProductList(int AgentType);

        List<GroupDetails> GetGroupList(int AgentType, int ProductID);

        string Loggin(string Request);

        string InsertCTCLog(LogReqDetails objLogReqDetails);
    }
}
