﻿using PropertyLayers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Data;
using System.Net.Http;
using MongoDB.Bson;

namespace BusinessLogicLayer
{
    public class BMSBL : IBMSBL
    {
        public ResultClass CommunicationHistory(Int64 BookingId)
        {
            BMSDAL d = new BMSDAL();
            return d.CommunicationHistory(BookingId);
        }
        public ResultClass CommunicationHistoryNew(Int64 BookingId)
        {
            BMSDAL d = new BMSDAL();
            return d.CommunicationHistoryNew(BookingId);
        }
        public ResultClass SaveCommonIVRResponse(long LeadId, string Response, string AppIDSource, string APICreateTicket)
        {
            BMSDAL d = new BMSDAL();
            ResultClass Rc = new ResultClass();
            Rc = d.SaveCommonIVRResponse(LeadId, Response,AppIDSource);
            string[] lista = ConfigurationSettings.AppSettings["TicketCreate"].ToString().Split(',');
            int[] intList = Array.ConvertAll<string, int>(lista, int.Parse);
            //int[] intList = { 3, 5, 8, 13, 15, 16, 18,19 };
                if (Rc.Success.Length > 0)
                {
                    if (intList.Contains(Convert.ToInt32(Rc.Success)))// Rc.Success == "3" || Rc.Success == "5" || Rc.Success == "8")
                    {
                        IVRCreateTicketByLeadId(LeadId, Convert.ToInt16(Rc.Success), APICreateTicket);
                    }
                    try
                    {
                        try
                        {
                            string[] SendSMSTo = ConfigurationSettings.AppSettings["SendSMSTo"].ToString().Split(',');
                            int[] intSendSMSTo = Array.ConvertAll<string, int>(SendSMSTo, int.Parse);
                            if (intSendSMSTo.Contains(Convert.ToInt32(Rc.Success)))
                            {
                                SendSms(LeadId, Convert.ToInt16(Rc.Success));
                            }
                        }
                        catch (Exception ex) { }
                        try { 
                        string[] SendEmailTo = ConfigurationSettings.AppSettings["SendEmailTo"].ToString().Split(',');
                        int[] intSendEmailTo = Array.ConvertAll<string, int>(SendEmailTo, int.Parse);
                      
                        if (intSendEmailTo.Contains(Convert.ToInt32(Rc.Success)))
                        {
                            SendEmail(LeadId, Convert.ToInt16(Rc.Success));
                        }
                        }
                        catch (Exception ex) { }
                    }
                    catch (Exception ex) { }
                }
                
                return Rc;
        }
        public void SendSms(long LeadId, Int16 responseId)
        {
            DataSet ds = new DataSet();
            BMSDAL d = new BMSDAL();
            ds = d.GetTicketDetailByLeadId(LeadId);
            DataRow oDr = ds.Tables[0].Rows[0];
            string content = "{\"CommunicationDetails\":{\"LeadID\":" + LeadId + ",\"ProductID\":" + oDr["ProductID"].ToString() + ",\"IsBooking\":true,\"Conversations\":[{\"ToReceipent\":[\"" + oDr["MobileNo"].ToString() + "\"],\"TriggerName\":\"" + ConfigurationSettings.AppSettings["SMSTrigger_" + responseId.ToString()] + "\",\"UserID\":124,\"AutoTemplate\":true}] ,\"CommunicationType\":2}}";
            Communication.SendEmailSms_Comm(content);
        }
        public void SendEmail(long LeadId, Int16 responseId)
        {
            DataSet ds = new DataSet();
            BMSDAL d = new BMSDAL();
            ds = d.GetTicketDetailByLeadId(LeadId);
            DataRow oDr = ds.Tables[0].Rows[0];
            string content = "{\"CommunicationDetails\":{\"LeadID\":" + LeadId + ",\"ProductID\":" + oDr["ProductID"].ToString() + ",\"IsBooking\":true,\"Conversations\":[{\"ToReceipent\":[\"" + oDr["EmailID"].ToString() + "\"],\"TriggerName\":\"" + ConfigurationSettings.AppSettings["SMSTrigger_" + responseId.ToString()] + "\",\"UserID\":124,\"AutoTemplate\":true}] ,\"CommunicationType\":1}}";
            Communication.SendEmailSms_Comm(content);
        }
        public void IVRCreateTicketByLeadId(long LeadId, Int16 responseId,string APICreateTicket)
        {
            try
            {
                Tickets oTick = new Tickets();

                DataSet ds = new DataSet();
                BMSDAL d = new BMSDAL();
                ds = d.GetTicketDetailByLeadId(LeadId);
                DataRow oDr = ds.Tables[0].Rows[0];
                oTick.LeadID = Convert.ToInt64(oDr["LeadID"]);
                oTick.ProductID = Convert.ToInt32(oDr["ProductID"]);
                oTick.ProductName = Convert.ToString(oDr["ProductName"]);
                oTick.InsurerID = Convert.ToInt32(oDr["SupplierId"]);
                oTick.InsurerName = Convert.ToString(oDr["SupplierName"]);
                oTick.CustomerID = Convert.ToInt32(oDr["CustomerID"]);
                oTick.Name = Convert.ToString(oDr["Name"]);
                try
                {
                    oTick.MobileNo = Convert.ToInt64(oDr["MobileNo"]);
                }
                catch (Exception)
                {
                    oTick.MobileNo = 0;
                }
                oTick.EmailID = Convert.ToString(oDr["EmailID"]);
                //if (responseId == 13)
                //{
                //    //oTick.FollowUpDate = DateTime.Now.AddDays(3).Ticks;
                //    oTick.FollowUpDate = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                //    oTick.Subject = "IVR: PYP Required due to wrong NCB, LeadId : " + oTick.LeadID.ToString();
                //    oTick.Comments = "IVR: PYP Required due to wrong NCB and customer needs Assistance ";
                //    oTick.Source = "IVR";
                //}
                //if (responseId == 15)
                //{
                //    //oTick.FollowUpDate = DateTime.Now.AddDays(3).Ticks;
                //    oTick.FollowUpDate = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                //    oTick.Subject = "IVR:  PYP Required due to wrong NCB, LeadId : " + oTick.LeadID.ToString();
                //    oTick.Comments = "IVR:  PYP Required due to wrong NCB and customer said it was correct";
                //    oTick.Source = "IVR";
                //    oTick.IsAutoResolved = "1";
                //}
                //if (responseId == 16)
                //{
                //    //oTick.FollowUpDate = DateTime.Now.AddDays(3).Ticks;
                //    oTick.FollowUpDate = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                //    oTick.Subject = "IVR: PYP Required due to wrong NCB, LeadId: " + oTick.LeadID.ToString();
                //    oTick.Comments = "IVR: PYP Required due to wrong NCB and customer said it was done";
                //    oTick.Source = "IVR";
                //    oTick.IsAutoResolved = "1";
                //}
                //if (responseId == 18)
                //{
                //    //oTick.FollowUpDate = DateTime.Now.AddDays(3).Ticks;
                //    oTick.FollowUpDate = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                //    oTick.Subject = "IVR: Wrong NCB, LeadId : " + oTick.LeadID.ToString();
                //    oTick.Comments = "IVR: Wrong NCB and customer needs support";
                //    oTick.Source = "IVR";
                //}
                //if (responseId == 19)
                //{
                //    //oTick.FollowUpDate = DateTime.Now.AddDays(3).Ticks;
                //    oTick.FollowUpDate = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                //    oTick.Subject = "IVR: Wrong NCB, LeadId : " + oTick.LeadID.ToString();
                //    oTick.Comments = "IVR: Wrong NCB and Info ";
                //    oTick.Source = "IVR";
                //}
                if (responseId == 3)
                {
                    //oTick.FollowUpDate = DateTime.Now.AddDays(3).Ticks;
                    oTick.FollowUpDate = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                    oTick.Subject = "IVR: HardCopy not received, LeadId : " + oTick.LeadID.ToString();
                    oTick.Comments = "IVR: HardCopy not received, please followup with the insurer to get the exact status, LeadId : " + oTick.LeadID.ToString();
                    oTick.Source = "IVR";
                }
                else if (responseId == 5)
                {
                    DateTime now = DateTime.Now;
                    DateTime dtCompare = new DateTime(now.Year, now.Month, now.Day, 19, 0, 0);
                    if (TimeSpan.Compare(DateTime.Now.AddHours(3).TimeOfDay, dtCompare.TimeOfDay) <= 0)
                    {
                        //oTick.FollowUpDate = DateTime.Now.AddHours(4).TimeOfDay.Ticks;
                        oTick.FollowUpDate = DateTime.Now.AddHours(3).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                    }
                    else
                    {
                        DateTime tomorrow = DateTime.Now.AddDays(1);
                        //oTick.FollowUpDate = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 10, 0, 0).Ticks;
                        oTick.FollowUpDate = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, 10, 0, 0).ToString("yyyy-MM-dd HH:mm:ss.zzz"); ;
                    }
                    oTick.Subject = "IVR: HardCopy received and having Query, LeadId : " + oTick.LeadID.ToString();
                    oTick.Comments = "IVR: Customer has received the hard copy and has asked for a callback to resolve the query in an hour , LeadId : " + oTick.LeadID.ToString();
                    oTick.Source = "IVR";
                }
                else if (responseId == 8)
                {
                    //oTick.FollowUpDate = DateTime.Now.AddDays(3).Ticks;
                    oTick.FollowUpDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                    oTick.Subject = "IVRDOC: Term Document Submission, LeadId : " + oTick.LeadID.ToString();
                    oTick.Comments = "IVRDOC: The customer confirmed the submission of documents via IVR call. Please confirm the same with insurer and followup accordingly.LeadId : " + oTick.LeadID.ToString();
                    oTick.Source = "IVRDOC";
                }
                else
                {
                    oTick.FollowUpDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss.zzz");
                    oTick.Source = "IVR";
                    string Subject = ConfigurationSettings.AppSettings["Subject_" + responseId.ToString()].ToString();
                    oTick.Subject = Subject.Replace("##LeadId##",oTick.LeadID.ToString());
                    string Comments = ConfigurationSettings.AppSettings["Comments_" + responseId.ToString()].ToString();
                    oTick.Comments = Comments.Replace("##LeadId##", oTick.LeadID.ToString());

                }
                
                try
                {
                    string[] IsAutoResolved = ConfigurationSettings.AppSettings["IsAutoResolved"].ToString().Split(',');
                    int[] intIsAutoResolved = Array.ConvertAll<string, int>(IsAutoResolved, int.Parse);

                    if (intIsAutoResolved.Contains(responseId))
                    {
                        oTick.IsAutoResolved = "1";
                    }
                }
                catch (Exception ex) { }
                oTick.PolicyNumber = Convert.ToString(oDr["PolicyNo"]);
                


                oTick.Issue = 0;
                try {

                    string[] IssueTypeMaster = ConfigurationSettings.AppSettings["IssueTypeMaster"].ToString().Split(',');
                    int[] intIssueTypeMaster = Array.ConvertAll<string, int>(IssueTypeMaster, int.Parse);

                    if (intIssueTypeMaster.Contains(responseId))
                    {
                        string[] ProductIssue = ConfigurationSettings.AppSettings["Product_IssueType_" + responseId].ToString().Split(',');

                       foreach (string a in ProductIssue)
                       {
                           string[] ProductIssueSplit = a.ToString().Split('-');
                           if (Convert.ToInt32(ProductIssueSplit[0]) == oTick.ProductID)
                           {
                               oTick.Issue = Convert.ToInt32(ProductIssueSplit[1]);
                           }
                       }
                    }
                }
                catch (Exception ex)
                {
                    oTick.Issue = 0;
                }
                oTick.AutoClosure = 1;//mannual 
                oTick.AddressDetails = Convert.ToString(oDr["Address"]);
                oTick.Pincode = Convert.ToInt64(oDr["PostCode"]);

                fnCreateTicket(APICreateTicket, oTick);
            }
            catch (Exception ex)
            {

            }
            //return oTick;
        }
        public string fnCreateTicket(string APICreateTicket,Tickets objTicketDetail)
        {
            try
            {

                string AuthenticationKey = "";
                string ServiceUrl =  APICreateTicket;
                //ConfigurationManager.AppSettings["APICreateTicket"];

                //JavaScriptSerializer jss = new JavaScriptSerializer();
                string Data = Newtonsoft.Json.JsonConvert.SerializeObject(objTicketDetail);

                //Service Returned Data
                //WebHelper oWeb = new WebHelper();

                string response = PostAsyncByHttpclient(ServiceUrl, Data, 10000, false);


                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }
        public  string PostAsyncByHttpclient(string url, string content, int timeout, bool IsAsync = false, Dictionary<object, object> Headers = null)
        {
            try
            {
                HttpClient client = new HttpClient();
                System.Net.Http.HttpContent contentPost = new StringContent(content, Encoding.UTF8, "application/json");
                client.Timeout = TimeSpan.FromMilliseconds(timeout);
                //client.DefaultRequestHeaders.Add("auth", "gFH8gPXbCWaW8WqUefmFBcyRj0XIw");
                if (Headers != null)
                {
                    foreach (var header in Headers)
                    {
                        client.DefaultRequestHeaders.Add(header.Key.ToString(), header.Value.ToString());
                    }
                }
                if (IsAsync)
                {
                    client.PostAsync(url, contentPost);
                    return string.Empty;
                }
                else
                {
                    var response = client.PostAsync(url, contentPost).Result;
                    return response.Content.ReadAsStringAsync().Result;
                }
            }

            catch (Exception ex)
            {
                ex.Source += "\nMethodName : Matrix.WebUtility.WebHelper.PostAsync"
                            + "\n" + "Input : " + Newtonsoft.Json.JsonConvert.SerializeObject(url);
                throw ex;
            }

        }
        public Byte[] PDFRawReportBMS(DateTime FromDate, DateTime ToDate, int ProductID)
        {
            try
            {
                BMSDAL dataDal = new BMSDAL();
                return dataDal.PDFRawReportBMS(FromDate,ToDate,ProductID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultClass GetNotification(int AgentId)
        {
            ResultClass result = new ResultClass();

            return result;
        }
        public ResultClass InsertNotification()
        {
            ResultClass result = new ResultClass();
            try
            {
                
                CommunicationModel objCommunicationModel = new CommunicationModel();
                //var CommID = ObjectId.GenerateNewId();
                //objCommunicationModel.id = CommID;

                objCommunicationModel.AgentID = 121;
                objCommunicationModel.CreatedOn = DateTime.Now.Ticks;
                objCommunicationModel.CustomerID = 656565;
                objCommunicationModel.LeadID = 656565;
                objCommunicationModel.ProductID = 7;
                objCommunicationModel.Seen = false;


                MongoHelper.InsertCommDetails(objCommunicationModel, "CareEmails");
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
