﻿using System.Collections.Generic;
using PropertyLayers;

namespace BusinessLogicLayer
{
    public interface IVideoInspection
    {
        bool UpdateMediaFileDetails(MediaFileInfo info);

        List<InspectionBookingDetails> GetInspectionBookingDetails(string inspectionId);

        List<InspectionBookingDetails> InspectionDetailsList(InspectionBookingDetails objDetails);

        byte[] PDFInspectionDetailsList(InspectionBookingDetails objDetails);

        List<StatusCollection> GetStatusCollection(string userType);

        List<InspectionBookingDetails> GetInsurerInspectionDetails(InspectionBookingDetails objDetails);

        List<MediaFileInfo> MediaFileUrl(string inspectionId);

        List<InspectionDocument> GetInspectionsDocument(string inspectionId);

        ResultClass DeleteInspectionDocument(string DocURL, string InspectionId);

        bool UpdateStatus(InspectionBookingDetails objDetails);

        List<InspectionBookingDetails> InsurerInspectionHistory(string inspectionId);

        bool UpdatePbInspectionStatus(InspectionBookingDetails objDetails);

        bool InspectionRollback(InspectionRollbackDetails objRollbackDetails);

        List<InspectionBookingDetails> PbInspectionHistory(string inspectionId);

        List<InsurerCollection> GetInsurerCollection();

        int AddNewUsers(AddNewUser objAddNewUser);

        int ChangePassword(ChangePassword objChangePassword);

        List<MasterSupplier> GetSupplierListByProduct(string ProductID);

        List<AddNewUser> GetHealthExUser();

        int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo);

        ResultClass GetApplicationAccess(int UserID);
        ResultClass GetProducts();
        ResultClass GetSuppliers(int ProductId);
        ResultClass GetExUser();
        ResultClass GetExUserTicket();

        ResultClass GetUserDetails(int UserID);
        ResultClass SaveUserDetails(UserDetail Json);
        ResultClass GetUserDetailsTicket(int UserID);
        ResultClass SaveUserDetailsTicket(UserDetail Json);
        ResultClass ReplicateInspection(string InspectionID);
        ResultClass UpdateVehicleNo(string VehicleNumber, string InspectionID);
    }
}
