﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropertyLayers;
namespace BusinessLogicLayer
{
    public interface IEndorsement
    {
        string UpdateEndorsementDetails(EndorsementDetail ReqEndorsementDetail);

        EndorsementDocList GetEndorsementDocList(EndorsementFieldUpdateReq EndorsementFieldUpdate);

        EndorsementListDetails GetEndorsementListDetails(long LeadID);

        string InsertCustomerEndorsement(EndorsementDetail RequestEndorsementList);

        EndorsementAddFieldResponse AddNewEndorsementField(EndorsementFieldAddRequest NewRequestEndorsement);

        EndorsementAddFieldResponse GetNewFieldDocs(EndorsementFieldAddRequest NewRequestEndorsement);

        EndorsementDetail GetEndorsementDetails(long LeadID, long DetailsID, int Type);

        int InsertEndorsementDocumentStatus(RequestEndorsementDocList objRequestEndorsementDocList);

        List<FieldMasterData> GetFieldMasterDataList(int FieldID);

        EndorsementAddFieldResponse UpdateDocumentDetails(ReqDocUploadRequest objReqDocUploadRequest);

        string UpdateEndorsementCopyDetails(ReqDocUploadRequest objReqDocUploadRequest);

        string UpdateEndorsementFieldDetails(EndorsementFieldUpdateReq EndorsementFieldUpdate);

        List<EndorsementStatusdetailsResponse> GetEndorsementStatusdetails(long DetailsID);

        List<EndorsementStatusdetailsResponse> RollbackEndorsement(EndorsementRollBackRequest objEndorsementRollBackRequest);

        //List<DropDownData> GetMasterDropDownData(int FilterID, int Type);
    }
}
