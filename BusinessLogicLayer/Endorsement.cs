﻿using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
namespace BusinessLogicLayer
{
    public class Endorsement : IEndorsement 
    {
        public string UpdateEndorsementFieldDetails(EndorsementFieldUpdateReq EndorsementFieldUpdate)
        {
            dal d = new dal();
            return d.UpdateEndorsementFieldDetails(EndorsementFieldUpdate);
        }
        public string UpdateEndorsementDetails(EndorsementDetail ReqEndorsementDetail )
        {
            dal d = new dal();
            return d.UpdateEndorsementDetails(ReqEndorsementDetail);
        }

        public EndorsementListDetails GetEndorsementListDetails(long LeadID)
        {
            dal d = new dal();
            return d.GetEndorsementListDetails(LeadID);
        }

        public EndorsementDetail GetEndorsementDetails(long LeadID, long DetailsID, int Type)
        {
            dal d = new dal();
            return d.GetEndorsementDetails(LeadID, DetailsID, Type);
        }

        public string InsertCustomerEndorsement(EndorsementDetail RequestEndorsementList)
        {
            dal d = new dal();
            return d.InsertCustomerEndorsement(RequestEndorsementList);
        }

        public EndorsementAddFieldResponse AddNewEndorsementField(EndorsementFieldAddRequest NewRequestEndorsement)
        {
            dal d = new dal();
            return d.AddNewEndorsementField(NewRequestEndorsement);
        }

        public EndorsementAddFieldResponse GetNewFieldDocs(EndorsementFieldAddRequest NewRequestEndorsement)
        {
            dal d = new dal();
            return d.GetNewFieldDocs(NewRequestEndorsement);
        }

        public int InsertEndorsementDocumentStatus(RequestEndorsementDocList objRequestEndorsementDocList) 
        {
            dal d = new dal();
            return d.InsertEndorsementDocumentStatus(objRequestEndorsementDocList);
        }

        public EndorsementDocList GetEndorsementDocList(EndorsementFieldUpdateReq EndorsementFieldUpdate)
        {
            dal d = new dal();
            return d.GetEndorsementDocList(EndorsementFieldUpdate);
        }

        public List<FieldMasterData> GetFieldMasterDataList(int FieldID)
        {
            dal d = new dal();
            return d.GetFieldMasterDataList(FieldID);

        }

        public EndorsementAddFieldResponse UpdateDocumentDetails(ReqDocUploadRequest objReqDocUploadRequest)
        {
            FileUpload d = new FileUpload();
            return d.UpdateDocumentDetails(objReqDocUploadRequest);

        }

        public string UpdateEndorsementCopyDetails(ReqDocUploadRequest objReqDocUploadRequest)
        {
            FileUpload d = new FileUpload();
            return d.UpdateEndorsementCopyDetails(objReqDocUploadRequest);

        }

        public List<EndorsementStatusdetailsResponse> GetEndorsementStatusdetails(long DetailsID)
        {
            dal d = new dal();
            return d.GetEndorsementStatusdetails(DetailsID);
        }
        public List<EndorsementStatusdetailsResponse> RollbackEndorsement(EndorsementRollBackRequest objEndorsementRollBackRequest)
        {
            dal d = new dal();
            return d.RollbackEndorsement(objEndorsementRollBackRequest);
        }
    }
}
