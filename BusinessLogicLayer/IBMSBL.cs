﻿using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public interface IBMSBL
    {
        ResultClass CommunicationHistory(Int64 BookingId);
        ResultClass CommunicationHistoryNew(Int64 BookingId);
        ResultClass SaveCommonIVRResponse(long LeadId, string Response, string AppIDSource, string APICreateTicket);
        Byte[] PDFRawReportBMS(DateTime FromDate, DateTime ToDate, int ProductID);


        ResultClass InsertNotification();
    }
}
