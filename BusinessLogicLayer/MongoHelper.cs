﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class MongoHelper
    {
        public static IMongoCollection<BsonDocument> InsertCommDetails(CommunicationModel objCommModel, string collectionTable)
        {
            //MongoDBCon
            string connectionString = ConfigurationSettings.AppSettings["MongoDBCon"];//"mongodb://commdbUser:commdbUser%40123@10.0.8.62:27017";
            MongoClient client = new MongoClient(connectionString);
            var DB = client.GetDatabase("communicationDB");
            var collection = DB.GetCollection<BsonDocument>("BMSNotification");
            IMongoCollection<BsonDocument> CareEmailData = DB.GetCollection<BsonDocument>(collectionTable);
            var EmailDetails = objCommModel.ToBsonDocument();
            CareEmailData.InsertOne(EmailDetails);
            return CareEmailData;
        }
    }
}
