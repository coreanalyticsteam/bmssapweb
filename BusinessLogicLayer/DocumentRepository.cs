﻿using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
namespace BusinessLogicLayer
{
    public class DocumentRepository : IDocumentRepository
    {
        public List<DropDownData> GetMasterDropDownData(int FilterID, int ProductId, int Type)
        {
            dal d = new dal();
            return d.GetMasterDropDownData(FilterID, ProductId, Type);
        }

        public DocumentDetails GetMasterDocumentDetails()
        {
            dal d = new dal();
            return d.GetMasterDocumentDetails();
        }

        public int InsertDocumentMapping(MasterDocumentMappingRequest objMasterDocumentMappingRequest)
        {
            dal d = new dal();
            return d.InsertDocumentMapping(objMasterDocumentMappingRequest);
        }

        public CustomerDocDetails GetCustomerDocumentDetails(string LeadID)
        {
            dal d = new dal();
            return d.GetCustomerDocumentDetails(LeadID);
        }

        public int UploadCustomerDocuments(UploadDocReq objUploadDocReq)
        {
            FileUpload d = new FileUpload();
            return d.UploadCustomerDocuments(objUploadDocReq);
        }

        public List<DocumentMasterDetailsResp> GetDocumentMasterDetails(int ProductID, int SupplierID)
        {
            dal d = new dal();
            return d.GetDocumentMasterDetails(ProductID, SupplierID);
        }

        public GetDocumentEditResp GetDocumentMasterDetailsEdit(string GroupID)
        {
            dal d = new dal();
            return d.GetDocumentMasterDetailsEdit(GroupID);
        }

        public DocumentDetailsResponse InsertUpdateDocumentsForBooking(DocumentDetailsRequest objDocumentDetailsRequest)
        {
            dal d = new dal();
            return d.InsertUpdateDocumentsForBooking(objDocumentDetailsRequest);
        }

        public DocumentDetailsResponse GETRequiredDocumentsForBooking(long BookingID)
        {
            dal d = new dal();
            return d.GETRequiredDocumentsForBooking(BookingID);
        }

        public DocumentDetailsResponse UpdateDocumentsForBooking(ExistingDocumentDetailsRequest objExistingDocumentDetailsRequest)
        {
            dal d = new dal();
            return d.UpdateDocumentsForBooking(objExistingDocumentDetailsRequest);
        }

        public List<DocumentAuditTrailResponse> GetDocumentAuditTrail(long BookingID)
        {
            dal d = new dal();
            return d.GetDocumentAuditTrail(BookingID);
        }


        public DocumentDetailsResponse UpdateDocStatus(DocumentDetailsRequest objDocumentDetailsRequest)
        {
            dal d = new dal();
            return d.UpdateDocStatus(objDocumentDetailsRequest);
        }

        public DocResponse GetPendingDocuments(string leadId, string applicationNumber)
        {
            DocVerify d = new DocVerify();
            return d.GetPendingDocuments(leadId, applicationNumber);
        }

        public DocResponse UpdateSingleDocStatus(UpdateDocStatusReq objUpdateDocStatusReq)
        {
            DocVerify d = new DocVerify();
            return d.UpdateSingleDocStatus(objUpdateDocStatusReq);

        }

        public string MergeFile(MergeFile objMergeFile)
        {
            string mergingMsg;
            return common.MergeFile(objMergeFile.FileList, objMergeFile.FileName, out mergingMsg);

        }

        public DocResponse SubmitDoc(UpdateDocReq objUpdateDocReq)
        {
            DocVerify d = new DocVerify();
            return d.SubmitDoc(objUpdateDocReq);

        }
    }
}
