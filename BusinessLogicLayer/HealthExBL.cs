﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropertyLayers;
using DataAccessLayer;
using System.Runtime.Caching;
using System.Collections;
namespace BusinessLogicLayer
{
    public class HealthExBL : IHealthExBL
    {

        public List<LeadDetails> GetAllExLead(ReqMarkExSearch objReqMarkExSearch)
        {
            HealthExDL d = new HealthExDL();
            return d.GetAllExLead(objReqMarkExSearch);
        }


        public List<MasterSupplier> GetSupplierList()
        {
            HealthExDL d = new HealthExDL();
            return d.GetSupplierList();
        }

        //public List<SupplierPlanDetails> GetSupplierPlanDetails(string LeadID)
        //{
        //    HealthExDL d = new HealthExDL();
        //    return d.GetSupplierPlanDetails(LeadID);
        //}

        public int MarkLeadForHealthEx(ReqMarkHealthEx objReqMarkHealthEx)
        {
            HealthExDL d = new HealthExDL();
            return d.MarkLeadForHealthEx(objReqMarkHealthEx);
        }

        public ResUploadCaseToInsurer GetExLeadDetailsForCaseSend(long ExID)
        {
            HealthExDL d = new HealthExDL();
            return d.GetExLeadDetailsForCaseSend(ExID);
        }

        public int AddSupplierPlanForEx(PostPlanSelectionData objPostPlanSelectionData)
        {
            HealthExDL d = new HealthExDL();
            return d.AddSupplierPlanForEx(objPostPlanSelectionData);
        }

        public int AddMemberDouments(PostMemberDocumentDetails objPostMemberDocumentDetails)
        {
            HealthExDL d = new HealthExDL();
            return d.AddMemberDouments(objPostMemberDocumentDetails);
        }

        public List<LeadDetails> GetLeadsForPBDashboard(ReqMarkExSearch objReqMarkExSearch)
        {
            HealthExDL d = new HealthExDL();
            return d.GetLeadsForPBDashboard(objReqMarkExSearch);
        }

        public ResInsurerUnderwriting GetExLeadDetailsForInsurer(long ExID, long SelectionID, int InsurerID, int UserID)
        {
            HealthExDL d = new HealthExDL();
            return d.GetExLeadDetailsForInsurer(ExID, SelectionID, InsurerID, UserID);
        }

        public int UpdateInsurerAction(PostInsurerActionData objPostInsurerActionData)
        {
            HealthExDL d = new HealthExDL();
            return d.UpdateInsurerAction(objPostInsurerActionData);
        }

        public ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(long ExID, int Type = 0)
        {
            HealthExDL d = new HealthExDL();
            return d.GetPBInsurerUnderwritingDetails(ExID, Type);
        }

        public UserDetails Login(UserLoginRequest objUserLoginRequest)
        {
            HealthExDL d = new HealthExDL();

            Token objToken = new Token();
            TokenEntity objTokenEntity=new TokenEntity();
            
            UserDetails objUserDetails = new UserDetails();

            objTokenEntity      =objToken.GenerateToken(objUserLoginRequest.LoginID) ;
            objUserLoginRequest.Token = objTokenEntity .AuthToken;
            objUserLoginRequest.ExpireOn = objTokenEntity.ExpiresOn;
            objUserDetails=d.Login(objUserLoginRequest);
            objTokenEntity.UserId = objUserDetails.UserID;
            
            //ObjectCache USERTOKEN = MemoryCache.Default;

            //Hashtable hashtable; 
            //hashtable = (Hashtable)USERTOKEN.Get("USERTOKEN");
            
            //if(hashtable ==null)
            //    hashtable = new Hashtable();

            //hashtable[objUserLoginRequest.Token]=  objTokenEntity;

            //CacheItemPolicy objCachePolicies = new CacheItemPolicy();
            //USERTOKEN["USERTOKEN"]=  hashtable;
               //USERTOKEN.Add("USERTOKEN", hashtable, objCachePolicies);

             //(hashtable.ContainsKey("tokenId"))
                

            return objUserDetails;
        }

        public List<LeadDetails> GetLeadsForInsurerDashboard(ReqMarkExSearch objReqMarkExSearch)
        {
            HealthExDL d = new HealthExDL();
            return d.GetLeadsForInsurerDashboard(objReqMarkExSearch);
        }

        public int RemoveFromHealthEx(ReqRemoveFromHealthEx objReqRemoveFromHealthEx)
        {
            HealthExDL d = new HealthExDL();
            return d.RemoveFromHealthEx(objReqRemoveFromHealthEx);
        }
        public bool UpdateHealthExStatus(long NewLeadID, long OldLeadID, long SelectionID, int Type, int UserID)
        {
            HealthExDL d = new HealthExDL();
            return d.UpdateHealthExStatus(NewLeadID, OldLeadID, SelectionID, Type, UserID);
        }

        public bool MarkNewLeadForHealthEx(ReqMarkNewLead objReqMarkNewLead)
        {
            HealthExDL d = new HealthExDL();
            return d.MarkNewLeadForHealthEx(objReqMarkNewLead);
        }

        public bool InsertSupplierPlanNewHEx(List<HealthExSelections> HealthExSelectionsList)
        {
            HealthExDL d = new HealthExDL();
            return d.InsertSupplierPlanNewHEx(HealthExSelectionsList);
        }

        public bool UpdatePaymentDetails(PaymentDetailsData objPaymentDetailsData)
        {
            HealthExDL d = new HealthExDL();
            return d.UpdatePaymentDetails(objPaymentDetailsData);
        }

        public bool AcceptOrRejectInsurer(PostInsurerActionData objPostInsurerActionData)
        {
            HealthExDL d = new HealthExDL();
            return d.AcceptOrRejectInsurer(objPostInsurerActionData);
        }

        public List<MedicalDetails> CustomerMedicalDetails(string base64EncodedLeadID)
        {
            HealthExDL d = new HealthExDL();
            return d.CustomerMedicalDetails(base64EncodedLeadID);
        }

        public List<PaymentDetailsForEx> GETPaymentDetailsForEx(string base64EncodedLeadID)
        {
            HealthExDL d = new HealthExDL();
            return d.GETPaymentDetailsForEx(base64EncodedLeadID);
        }

        public bool RequestAuthenticate(string Token, string UserID)
        {
            HealthExDL d = new HealthExDL();
            return d.RequestAuthenticate(Token, UserID);
        }

        public LeadDetails ExLoginGetLeadData(long LeadID)
        {
            HealthExDL d = new HealthExDL();
            return d.ExLoginGetLeadData(LeadID);
        }

        public int SendCommunicationSync(long leadId, string TriggerName, string MobileNo, string EmailID, string BookingType, int ProductID, string SelectionID)
        {
            Communication.SendCommunicationSync(leadId, TriggerName, MobileNo, EmailID, BookingType, ProductID, SelectionID);
            return 1;
        }

        public bool UpdatePremiumForSelection(UpdatePremiumReq objUpdatePremiumReq)
        {
            HealthExDL d = new HealthExDL();
            return d.UpdatePremiumForSelection(objUpdatePremiumReq);
        }

        public int InsertInsurerRemarks(InsurerRemarksDetails objInsurerRemarksDetails)
        {
            HealthExDL d = new HealthExDL();
            return d.InsertInsurerRemarks(objInsurerRemarksDetails);
        }

        public ResCustomerDocUpload GetCustomerDocUploadDetails(string ExID)
        {
            HealthExDL d = new HealthExDL();
            return d.GetCustomerDocUploadDetails(ExID);
        }

        public int AddCustomerDouments(PostMemberDocumentDetails objPostMemberDocumentDetails)
        {
            HealthExDL d = new HealthExDL();
            return d.AddCustomerDouments(objPostMemberDocumentDetails);
        }

        public LeadDocumentDetails GetHExLeadMemberDocDetails(Int64 NewLeadID,Int64 SelectionID)
        {
            HealthExDL d = new HealthExDL();
            try
            {
                return d.GetHExLeadMemberDocDetails(NewLeadID, SelectionID);
            }
            catch
            {
                return new LeadDocumentDetails();
            }
        }

        public ResPBInsurerUnderwriting InsurerUnderwritingDecisionDetails(Int64 LeadId, long SelectionID)
        {
            ResPBInsurerUnderwriting data = GetPBInsurerUnderwritingDetails(LeadId, 1);
            List<ResInsurerUnderwriting> SelectData = data.InsurerUnderwritingDetails;
            data.InsurerUnderwritingDetails = SelectData.Where(a => a.InsurerDetails.SelectionID == SelectionID).ToList();
            return data;
        }
        public int AddNewUsers(AddNewUser objAddNewUser)
        {
            HealthExDL d = new HealthExDL();
            return d.AddNewUsers(objAddNewUser);
        }

        public int ChangePassword(ChangePassword objChangePassword)
        {
            HealthExDL d = new HealthExDL();
            return d.ChangePassword(objChangePassword);
        }

        public List<MasterSupplier> GetSupplierListByProduct(string ProductID)
        {
            HealthExDL d = new HealthExDL();
            return d.GetSupplierListByProduct(ProductID);
        }

        public List<AddNewUser> GetHealthExUser()
        {
            HealthExDL d = new HealthExDL();
            return d.GetHealthExUser();
        }

        public int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo)
        {
            HealthExDL d = new HealthExDL();
            return d.UpdateUserInfo(objUpdateUserInfo);
        }

        public int AddInspectionDocuments(InspectionDocumentDetails objInspectionDocumentDetails)
        {
            HealthExDL d = new HealthExDL();
            return d.AddInspectionDocuments(objInspectionDocumentDetails);
        }
    }
}
