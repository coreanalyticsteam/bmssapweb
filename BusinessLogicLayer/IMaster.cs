﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyLayers;

namespace BusinessLogicLayer
{
    public interface IMaster
    {
        ResultClass GetTicketIssueMaster();
        ResultClass GetProducts();
        ResultClass SaveTicketIssueTypeMaster(TicketIssueTypeMaster Json);
        ResultClass GetAllIssues();
        ResultClass GetTicketSubIssueMaster(String Json);
    }
}
