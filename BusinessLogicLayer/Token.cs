﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Caching;
using PropertyLayers;
namespace BusinessLogicLayer
{
   public  class Token :IToken
    {
       public TokenEntity GenerateToken(string LoginID)
       {
           string token = Guid.NewGuid().ToString();
           DateTime issuedOn = DateTime.Now;
           DateTime expiredOn = DateTime.Now.AddSeconds(
           Convert.ToDouble("20"));
           var tokendomain = new TokenEntity
           {
               LoginID = LoginID,
               AuthToken = token,
               IssuedOn = issuedOn,
               ExpiresOn = expiredOn
           };

           //_unitOfWork.TokenRepository.Insert(tokendomain);
           //_unitOfWork.Save();

           var tokenModel = new TokenEntity()
           {
               LoginID = LoginID,
               IssuedOn = issuedOn,
               ExpiresOn = expiredOn,
               AuthToken = token
           };

           return tokenModel;
       }
       public bool ValidateToken(string tokenId)
       {
           ObjectCache USERTOKEN = MemoryCache.Default;

           Hashtable hashtable; //= new Hashtable();
           hashtable = (Hashtable)USERTOKEN.Get("USERTOKEN");
           TokenEntity objTokenEntity ;//= new TokenEntity();

           ///dictionary = new Dictionary<string, string>();
           hashtable = new Hashtable();
           CacheItemPolicy objCachePolicies = new CacheItemPolicy();

           if (hashtable.ContainsKey("tokenId"))
               objTokenEntity = (TokenEntity)hashtable[tokenId];
           else
               return false;

           if (objTokenEntity != null && objTokenEntity.ExpiresOn > DateTime.Now)

               return true;

           else

               return false;


           //USERTOKEN.Add("USERTOKEN", hashtable, objCachePolicies);
           //var token = _unitOfWork.TokenRepository.Get(t => t.AuthToken == tokenId && t.ExpiresOn > DateTime.Now);
           //if (token != null && !(DateTime.Now > token.ExpiresOn))
           //{
           //    token.ExpiresOn = token.ExpiresOn.AddSeconds(
           //    Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"]));
           //    _unitOfWork.TokenRepository.Update(token);
           //    _unitOfWork.Save();
           //    return true;
           //}          
       }
       //public bool Kill(string tokenId)
       //{
       //    _unitOfWork.TokenRepository.Delete(x => x.AuthToken == tokenId);
       //    _unitOfWork.Save();
       //    var isNotDeleted = _unitOfWork.TokenRepository.GetMany(x => x.AuthToken == tokenId).Any();
       //    if (isNotDeleted) { return false; }
       //    return true;
       //}
       //public bool DeleteByUserId(int userId)
       //{
       //    _unitOfWork.TokenRepository.Delete(x => x.UserId == userId);
       //    _unitOfWork.Save();

       //    var isNotDeleted = _unitOfWork.TokenRepository.GetMany(x => x.UserId == userId).Any();
       //    return !isNotDeleted;
       //}

    }
}
