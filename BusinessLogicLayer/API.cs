﻿using Newtonsoft.Json;
using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using DataAccessLayer;
using System.Configuration;

namespace BusinessLogicLayer
{
    public class API : IAPI
    {
        const int ICICIPRUIterationcount = 450;
        const string ICICIPRUAuthenticationKey = "!i@*6@($%(";
        public ResultClass ICICIPru(ICICIPRUMob Json)
        {
            ResultClass result = new ResultClass();
            try
            {
                Json.webservicename = "POLICY_BAAZAR_APP_TRACKER";
                using (var client = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    client.Headers[HttpRequestHeader.Authorization] = "Basic d2Vic2l0ZTp3ZWJzaXRl";
                    client.Headers[HttpRequestHeader.Accept] = "application/json";

                    var data = EncryptString(Newtonsoft.Json.JsonConvert.SerializeObject(Json), ICICIPRUAuthenticationKey, ICICIPRUIterationcount);
                    result.Success = client.UploadString(ConfigurationSettings.AppSettings["ICICPRUAPI"], "POST", data);
                    //XmlDocument xdoc = JsonConvert.DeserializeXmlNode("{\"root\":" + DecryptString(result.Success, ICICIPRUAuthenticationKey, ICICIPRUIterationcount) + "}", "root");
                    //XmlDocument doc = JsonConvert.DeserializeXmlNode(GetXMLFromJson(DecryptString(result.Success, ICICIPRUAuthenticationKey, ICICIPRUIterationcount)));
                    APIDAL oAPI = new APIDAL();
                    result = oAPI.ICICIPru(DecryptString(result.Success, ICICIPRUAuthenticationKey, ICICIPRUIterationcount));
                    result.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                result.Error = ex.ToString();
            }
            return result;
        }

        public static string GetXMLFromJson(string jsonString)
        {
            string parentNote = "Person";
            XDocument doc = (XDocument)JsonConvert.DeserializeXNode("{\"" + parentNote + "\":" + jsonString + "}", "DocumentElement");
            return doc.ToString();
        }
        public static string EncryptString(string strdata, string key, int iteration)
        {
            // TODO: Parameterize the Password, Salt, and Iterations.  They should be encrypted with the machine key and stored in the registry
            if (string.IsNullOrEmpty(strdata))
            {
                return strdata;
            }

            byte[] salt = { 0xA9, 0x9B, 0xC8, 0x32, 0x56, 0x35, 0xE3, 0x03 };

            PKCSKeyGenerator crypto = new PKCSKeyGenerator(key, salt, iteration, 1);

            ICryptoTransform cryptoTransform = crypto.Encryptor;
            byte[] cipherBytes = cryptoTransform.TransformFinalBlock(Encoding.UTF8.GetBytes(strdata), 0, strdata.Length);
            return Convert.ToBase64String(cipherBytes);
        }
        public static string DecryptString(string strdata, string key, int iteration)
        {
            if (string.IsNullOrEmpty(strdata))
            {
                return strdata;
            }

            byte[] salt = { 0xA9, 0x9B, 0xC8, 0x32, 0x56, 0x35, 0xE3, 0x03 };

            PKCSKeyGenerator crypto = new PKCSKeyGenerator(key, salt, iteration, 1);

            ICryptoTransform cryptoTransform = crypto.Decryptor;
            var strdataBytes = Convert.FromBase64String(strdata);
            var clearBytes = cryptoTransform.TransformFinalBlock(strdataBytes, 0, strdataBytes.Length);
            return Encoding.UTF8.GetString(clearBytes);
        }
    }
}
