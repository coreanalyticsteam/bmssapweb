﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyLayers;
using DataAccessLayer;
namespace BusinessLogicLayer
{
    public class Master : IMaster
    {
        public ResultClass GetTicketIssueMaster()
        {
            
            MasterDal dataDal = new MasterDal();
            return dataDal.GetTicketIssueMaster();
        }

        public ResultClass GetProducts()
        {
            MasterDal dataDal = new MasterDal();
            return dataDal.GetProducts();
        }
        public ResultClass SaveTicketIssueTypeMaster(TicketIssueTypeMaster Json)
        {
            MasterDal dataDal = new MasterDal();
            return dataDal.SaveTicketIssueTypeMaster(Json);
        }
        public ResultClass GetAllIssues()
        {
            MasterDal dataDal = new MasterDal();
            return dataDal.GetAllIssues();
        }
        public ResultClass GetTicketSubIssueMaster(String Json)
        {
            MasterDal dataDal = new MasterDal();
            return dataDal.GetTicketSubIssueMaster(Json);
        }
    }
}
