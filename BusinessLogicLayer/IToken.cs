﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropertyLayers;
namespace BusinessLogicLayer
{
    public interface IToken
    {
        TokenEntity GenerateToken(string LoginID);

        bool ValidateToken(string tokenId);

        //bool Kill(string tokenId);

        //bool DeleteByUserId(int userId);
    }
}
