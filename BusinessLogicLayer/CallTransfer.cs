﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropertyLayers;
using DataAccessLayer;
namespace BusinessLogicLayer
{
    public class CallTransfer : ICallTransfer
    {

        public List<LeadAgentList> GetDialerAgentList(DialerSearchRequest objDialerSearchRequest)
        {
            DialerDAL d = new DialerDAL();
            return d.GetDialerAgentList(objDialerSearchRequest);
        }

        public List<ProductDetails> GetProductList(int AgentType)
        {
            DialerDAL d = new DialerDAL();
            return d.GetProductList(AgentType);
        }

        public List<GroupDetails> GetGroupList(int AgentType, int ProductID)
        {
            DialerDAL d = new DialerDAL();
            return d.GetGroupList(AgentType, ProductID);
        }

        public string Loggin(string Request)
        {
            DialerDAL d = new DialerDAL();
            return d.Loggin(Request);
        }

        public string InsertCTCLog(LogReqDetails objLogReqDetails)
        {
            DialerDAL d = new DialerDAL();
            return d.InsertCTCLog(objLogReqDetails);
        }
    }
}
