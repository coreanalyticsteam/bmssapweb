﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.Http;
//using System.Net.Http.Headers;
using System.Web;
using DataAccessLayer;
using PropertyLayers;

namespace BusinessLogicLayer
{
    public class VideoInspection: IVideoInspection
    {
        public bool UpdateMediaFileDetails(MediaFileInfo objMediaFileInfo)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.UpdateMediaFileDetails(objMediaFileInfo);
        }

        /// <summary>
        /// Get InspectionBooking Details
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <returns></returns>
        public List<InspectionBookingDetails> GetInspectionBookingDetails(string inspectionId)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.GetInspectionBookingDetails(inspectionId);
            }
            catch (Exception)
            {              
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDetails"></param>
        /// <returns></returns>
        public List<InspectionBookingDetails> InspectionDetailsList(InspectionBookingDetails objDetails)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.InspectionDetailsList(objDetails);
            }
            catch (Exception)
            {                
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDetails"></param>
        /// <returns></returns>
        public Byte[] PDFInspectionDetailsList(InspectionBookingDetails objDetails)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.PDFInspectionDetailsList(objDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Status
        /// </summary>
        /// <returns></returns>
        public List<StatusCollection> GetStatusCollection(string userType)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.GetStatusCollection(userType);
            }
            catch (Exception)
            {               
                throw;
            }
        }


        public List<InspectionBookingDetails> GetInsurerInspectionDetails(InspectionBookingDetails objDetails)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.GetInsurerInspectionDetails(objDetails);
            }
            catch (Exception)
            {                
                throw;
            }
        }


        public List<MediaFileInfo> MediaFileUrl(string inspectionId)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.GetMediaFileUrl(inspectionId);
            }
            catch (Exception)
            {               
                throw;
            }
        }

        public List<InspectionDocument> GetInspectionsDocument(string inspectionId)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.GetInspectionsDocument(inspectionId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update Status, SubStatus, Remarks
        /// </summary>
        /// <param name="objDetails"></param>
        /// <returns></returns>
        public bool UpdateStatus(InspectionBookingDetails objDetails)
        {
            try
            {
                InspectionDal dataDal = new InspectionDal();
                return dataDal.UpdateStatus(objDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InspectionBookingDetails> InsurerInspectionHistory(string inspectionId)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.InsurerInspectionHistory(inspectionId, 2);
        }


        public bool UpdatePbInspectionStatus(InspectionBookingDetails objDetails)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.UpdatePbInspectionStatus(objDetails);
        }

        public bool InspectionRollback(InspectionRollbackDetails objRollbackDetails)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.InspectionRollback(objRollbackDetails);
        }

        public List<InspectionBookingDetails> PbInspectionHistory(string inspectionId)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.InsurerInspectionHistory(inspectionId, 1);
        }


        public List<InsurerCollection> GetInsurerCollection()
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetInsurerCollection();
        }

        public int AddNewUsers(AddNewUser objAddNewUser)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.AddNewUsers(objAddNewUser);
        }

        public int ChangePassword(ChangePassword objChangePassword)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.ChangePassword(objChangePassword);
        }

        public List<MasterSupplier> GetSupplierListByProduct(string ProductID)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetSupplierListByProduct(ProductID);
        }

        public List<AddNewUser> GetHealthExUser()
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetHealthExUser();
        }

        public int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.UpdateUserInfo(objUpdateUserInfo);
        }
        public ResultClass GetApplicationAccess(int UserID)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetApplicationAccess(UserID);
        }
        public ResultClass GetProducts()
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetProducts();
        }
        public ResultClass GetSuppliers(int ProductId)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetSuppliers(ProductId);
        }
        public ResultClass GetExUser()
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetExUser();
        }
        public ResultClass GetExUserTicket()
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetExUserTicket();
        }
        public ResultClass GetUserDetails(int UserID)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetUserDetails(UserID);
        }
        public ResultClass SaveUserDetails(UserDetail Json)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.SaveUserDetails(Json);
        }

        public ResultClass GetUserDetailsTicket(int UserID)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.GetUserDetailsTicket(UserID);
        }
        public ResultClass SaveUserDetailsTicket(UserDetail Json)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.SaveUserDetailsTicket(Json);
        }
        public ResultClass ReplicateInspection(string InspectionID) {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.ReplicateInspection(InspectionID);
        }
        public ResultClass UpdateVehicleNo(string VehicleNumber,string InspectionID)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.UpdateVehicleNo(VehicleNumber, InspectionID);
        }

        public ResultClass DeleteInspectionDocument(string DocURL, string InspectionId)
        {
            InspectionDal dataDal = new InspectionDal();
            return dataDal.DeleteInspectionDocument(DocURL,InspectionId);
        }
        
    }
}
