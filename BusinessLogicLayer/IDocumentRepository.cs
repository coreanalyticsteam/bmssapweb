﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropertyLayers;
namespace BusinessLogicLayer
{
    public interface IDocumentRepository
    {
        List<DropDownData> GetMasterDropDownData(int FilterID, int ProductId, int Type);

        DocumentDetails GetMasterDocumentDetails();

        int InsertDocumentMapping(MasterDocumentMappingRequest objMasterDocumentMappingRequest);

        CustomerDocDetails GetCustomerDocumentDetails(string LeadID);

        int UploadCustomerDocuments(UploadDocReq objUploadDocReq);

        List<DocumentMasterDetailsResp> GetDocumentMasterDetails(int ProductID, int SupplierID);

        GetDocumentEditResp GetDocumentMasterDetailsEdit(string GroupID);

        DocumentDetailsResponse InsertUpdateDocumentsForBooking(DocumentDetailsRequest objDocumentDetailsRequest);

        DocumentDetailsResponse GETRequiredDocumentsForBooking(long BookingID);

        DocumentDetailsResponse UpdateDocumentsForBooking(ExistingDocumentDetailsRequest objExistingDocumentDetailsRequest);

        List<DocumentAuditTrailResponse> GetDocumentAuditTrail(long BookingID);

        DocumentDetailsResponse UpdateDocStatus(DocumentDetailsRequest objDocumentDetailsRequest);

        DocResponse GetPendingDocuments(string leadId, string applicationNumber);

        DocResponse UpdateSingleDocStatus(UpdateDocStatusReq objUpdateDocStatusReq);

        string MergeFile(MergeFile objMergeFile);

        DocResponse SubmitDoc(UpdateDocReq objUpdateDocReq);
    }

}
