﻿using DataAccessLayer;
using System;

namespace BusinessLogicLayer
{
    public class PbFileUpload : IPbFileUpload
    {
        public byte[] GetFile(string id)
        {
            try
            {
                PbDal objPbDal = new PbDal();
                return objPbDal.GetFile(id);


            }
            catch (Exception)
            {                
                throw;
            }
        }


        public string UploadFile(byte[] imageData, PropertyLayers.MediaFileInfo info)
        {
            try
            {
                PbDal objPbDal = new PbDal();
                return objPbDal.UploadFile(imageData, info);
            }
            catch (Exception)
            {               
                throw;
            }
        }
    }
}
