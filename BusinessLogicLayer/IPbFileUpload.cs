﻿using PropertyLayers;

namespace BusinessLogicLayer
{
    public interface IPbFileUpload
    {
        byte[] GetFile(string id);

        string UploadFile(byte[] imageData, MediaFileInfo info);



    }
}
