﻿<?xml version=\"1.0\" encoding=\"utf-8\"?>
<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">
  <xsl:output method=\"html\" version=\"1.0\" encoding=\"ISO-8859-1\" indent=\"yes\" doctype-public=\"-//W3C//DTD XHTML 1.0 Transitional//EN\" doctype-system=\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"/>
  <xsl:template match=\"LeadDetailResponse\">
    <xsl:variable name=\"LeadID\" select=\"./LeadData/LeadID\"/>
    <xsl:variable name=\"EnquiryId\" select=\"./LeadData/EnquiryId\"/>
    <xsl:variable name=\"EncrypEnquiryId\" select=\"./LeadData/EncrypEnquiryId\"/>
    <xsl:variable name=\"AgentName\" select=\"./LeadData/AgentName\"/>
    <xsl:variable name=\"AgentID\" select=\"./LeadData/AgentID\"/>
    <xsl:variable name=\"ProductID\" select=\"./LeadData/ProductID\"/>
    <xsl:variable name=\"CustomerName\" select=\"./LeadData/CustomerName\"/>
    <xsl:variable name=\"MobileNo\" select=\"./LeadData/MobileNo\"/>
    <xsl:variable name=\"ShortMobileNo\" select=\"./LeadData/ShortMobileNo\"/>
    <xsl:variable name=\"SupplierShortName\" select=\"./PolicyDetails/SupplierShortName\"/>
    <xsl:variable name=\"PlanShortName\" select=\"./PolicyDetails/PlanShortName\"/>
    <xsl:variable name=\"PlanReviewURL\" select=\"./PolicyDetails/PlanReviewURL\"/>
    <xsl:variable name=\"Lcletters\">abcdefghijklmnopqrstuvwxyz</xsl:variable>
    <xsl:variable name=\"Ucletters\">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
    <xsl:variable name=\"CustCareEmailTo\" select=\"./CompanyContactInfo/CustCareEmailTo\"/>
    <xsl:variable name=\"Fax\" select=\"./CompanyContactInfo/Fax\"/>
    <xsl:variable name=\"TelPhoneNo\" select=\"./CompanyContactInfo/TelPhoneNo\"/>
    <xsl:variable name=\"TollFreeNo\" select=\"./CompanyContactInfo/TollFreeNo\"/>
    <xsl:variable name=\"WhatsApNo\" select=\"./CompanyContactInfo/WhatsApNo\"/>
    <xsl:variable name=\"WebSiteLink\" select=\"./CompanyContactInfo/WebSiteLink\"/>
    <xsl:variable name=\"MobileWebSiteLink\" select=\"./CompanyContactInfo/MobileWebSiteLink\"/>
    <xsl:variable name=\"ImageLogoURL\" select=\"./CompanyContactInfo/ImageLogoURL\"/>
    <xsl:variable name=\"ContactEmailTo\" select=\"./CompanyContactInfo/ContactEmailTo\"/>
    <xsl:variable name=\"FooterEmail\" select=\"./CompanyContactInfo/FooterEmail\"/>
    <xsl:variable name=\"FooterContactNo\" select=\"./CompanyContactInfo/FooterContactNo\"/>
    <xsl:variable name=\"TriggerName\" select=\"./LeadData/TriggerName\"/>
    <xsl:variable name=\"HEXInsurerName\" select=\"./HEXSelectionDetails/InsurerName\"/>
    <xsl:variable name=\"HEXInsurerID\" select=\"./HEXSelectionDetails/InsurerID\"/>
    <xsl:variable name=\"HEXPlanID\" select=\"./HEXSelectionDetails/PlanID\"/>
    <xsl:variable name=\"EncryptPlanID\" select=\"./HEXSelectionDetails/EncryptPlanID\"/>
    <xsl:variable name=\"HEXPlanName\" select=\"./HEXSelectionDetails/PlanName\"/>
    <xsl:variable name=\"HEXIsDiseaseExlusion\" select=\"./HEXSelectionDetails/IsDiseaseExlusion\"/>
    <xsl:variable name=\"HEXIsDiseaseExlusionPerm\" select=\"./HEXSelectionDetails/IsDiseaseExlusionPerm\"/>
    <xsl:variable name=\"HEXDiseaseExclusionPerm\" select=\"./HEXSelectionDetails/DiseaseExclusionPerm\"/>
    <xsl:variable name=\"HEXIsDiseaseExlusionTemp\" select=\"./HEXSelectionDetails/IsDiseaseExlusionTemp\"/>
    <xsl:variable name=\"HEXDiseaseExclusionTemp\" select=\"./HEXSelectionDetails/DiseaseExclusionTemp\"/>
    <xsl:variable name=\"HEXIsLoadPremium\" select=\"./HEXSelectionDetails/IsLoadPremium\"/>
    <xsl:variable name=\"HEXFinalPremium\" select=\"./HEXSelectionDetails/FinalPremium\"/>
    <xsl:variable name=\"HEXIsCoPay\" select=\"./HEXSelectionDetails/IsCoPay\"/>
    <xsl:variable name=\"HEXCoPay\" select=\"./HEXSelectionDetails/CoPay\"/>
    <xsl:variable name=\"HEXSumInsured\" select=\"./HEXSelectionDetails/SumInsured\"/>
    <xsl:variable name=\"HEXIsMemberExclusion\" select=\"./HEXSelectionDetails/IsMemberExclusion\"/>
    <xsl:variable name=\"HEXMembers\" select=\"./HEXSelectionDetails/Members\"/>
    <html>
      <head>
        <meta content=\"follow, index, all\" name=\"robots\"/>
        <meta name=\"keywords\" content=\"\"/>
        <meta name=\"description\" content=\"\"/>
      </head>
      <body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">
        <table id=\"Header\" width=\"600\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
            <td>
              <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                <tbody>
                  <tr>
                    <td style=\"border-bottom:2px solid #1a4961;\">
                      <a href=\"http://www.policybazaar.com/\">
                        <img src=\"{$ImageLogoURL}pb-logo.jpg\" alt=\"\" width=\"237\" height=\"54\" border=\"0\"/>
                      </a>
                    </td>
                    <td align=\"right\" valign=\"bottom\" style=\"line-height:22px;border-bottom:2px solid #1a4961; \">
                      <strong style=\"font-weight:bold;\">
                        Call Us : <xsl:value-of select=\"$TollFreeNo\"/>
                      </strong>
                      <br></br>
                      <a href=\"http://www.youtube.com/user/PolicybazaarIndia\">
                        <img src= \"{$ImageLogoURL}youtube-icon.jpg\" width=\"30\" height=\"32\" alt=\"\"/>
                      </a>
                      <a href=\"https://twitter.com/policybazaar_in\">
                        <img src=\"{$ImageLogoURL}twitter-icon.jpg\" width=\"30\" height=\"32\" alt=\"\"/>
                      </a>
                      <a href=\"https://www.linkedin.com/company/policybazaar\">
                        <img src=\"{$ImageLogoURL}linkedin-icon.jpg\" width=\"30\" height=\"32\" alt=\"\"/>
                      </a>
                      <a href=\"https://www.facebook.com/PolicyBazaarIndia?filter=1\">
                        <img src=\"{$ImageLogoURL}facebook-icon.jpg\" width=\"31\" height=\"32\" alt=\"\"/>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <tr>
            <td height=\"30\" colspan=\"2\" valign=\"middle\" style=\"padding:7px 0 0 0;\">             Hi,            </td>
          </tr>
          <xsl:choose>
            <xsl:when test=\"$TriggerName='MPFDetailsHEX'\">
              <tr>
                <td height=\"5\" colspan=\"2\" valign=\"middle\"></td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">                  Thanks for choosing Policy Bazaar. As discussed with you over the phone, we are going to submit your case to 3 insurers. To help them make a decision, please fill in the following form with your personal and medical details.                </td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">
                  <br></br>
                  <a href=\"http://health.policybazaar.com/proposal?enquiryid={$EncrypEnquiryId}\" target=\"_blank\">
                    http://health.policybazaar.com/proposal?enquiryid=<xsl:value-of select=\"$EncrypEnquiryId\"/>
                  </a>
                  <!--<xsl:call-template name=\"DisplayLink\">                <xsl:with-param name=\"TriggerName\" select=\"TriggerName\"/>                <xsl:with-param name=\"EncrypEnquiryId\" select=\"EncrypEnquiryId\"/>              </xsl:call-template>-->
                </td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">
                  <br></br>                  Additional tests may be required based on the response provided.                  <br/>                  We will get back to you with the insurer’s decision once we hear from them.                  <br/>                  Look forward to a great partnership.
                </td>
              </tr>
            </xsl:when>
            <xsl:when test=\"$TriggerName='PaymentLinkHEX'\">
              <tr>
                <td height=\"5\" colspan=\"2\" valign=\"middle\"></td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">
                  Thanks for choosing Policy Bazaar. As discussed over the phone, you have decided to proceed with                    <xsl:value-of select=\"$HEXInsurerName\" /> (<xsl:value-of select=\"$HEXPlanName\" />).                  Please click on the following link to verify your details and make the payment.
                </td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">
                  <br></br>
                  <a href=\"http://health.policybazaar.com/nstpproposal?enquiryid={$EncrypEnquiryId}&amp;planid={$EncryptPlanID}\" target=\"_blank\">
                    http://health.policybazaar.com/nstpproposal?enquiryid=<xsl:value-of select=\"$EncrypEnquiryId\"/>&amp;planid=<xsl:value-of select=\"$EncryptPlanID\"/>
                  </a>
                  <!--<xsl:call-template name=\"DisplayLink\">                <xsl:with-param name=\"TriggerName\" select=\"TriggerName\"/>                <xsl:with-param name=\"EncrypEnquiryId\" select=\"EncrypEnquiryId\"/>              </xsl:call-template>-->
                </td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">
                  <br></br>                                    Look forward to a great partnership.
                </td>
              </tr>
            </xsl:when>
            <xsl:when test=\"$TriggerName='MarkHealthEX'\">
              <tr>
                <td height=\"5\" colspan=\"2\" valign=\"middle\"></td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">                  Thanks for choosing Policy Bazaar. As discussed over the phone, you have decided to proceed with Health Exchange . Please click the below link                </td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">
                  <br></br>
                  <a href=\"http://health.policybazaar.com/proposal?enquiryid={$EncrypEnquiryId}\" target=\"_blank\">
                    http://health.policybazaar.com/proposal?enquiryid=<xsl:value-of select=\"$EncrypEnquiryId\"/>
                  </a>
                  <!--<xsl:call-template name=\"DisplayLink\">                <xsl:with-param name=\"TriggerName\" select=\"TriggerName\"/>                <xsl:with-param name=\"EncrypEnquiryId\" select=\"EncrypEnquiryId\"/>              </xsl:call-template>-->
                </td>
              </tr>
              <tr>
                <td colspan=\"2\" valign=\"middle\">
                  <br></br>                  Look forward to a great partnership.
                </td>
              </tr>
            </xsl:when>
          </xsl:choose>
        </table>
        <table id=\"Content\" width=\"600\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
            <td>
              <spam id=\"SplitContent\"></spam>
            </td>
          </tr>
        </table>
        <table id=\"Footer\" width=\"600\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
            <td colspan=\"2\" valign=\"middle\" style=\"font-size:11px;\">
              <br></br>              For any further assistance call us at <xsl:value-of select=\"$TollFreeNo\"/> or you can also reply back on this email without changing the subject.            <xsl:choose>
                <xsl:when test=\"$ProductID=7\">
                  You can also WhatsApp us at <xsl:value-of select=\"$WhatsApNo\"/>.
                </xsl:when>
                <xsl:when test=\"$ProductID=115\">
                  You can also WhatsApp us at <xsl:value-of select=\"$WhatsApNo\"/>.
                </xsl:when>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td colspan=\"2\" valign=\"middle\"  style=\"font-size:11px;\">              Look forward to hear from you soon. Have an awesome day!!!            </td>
          </tr>
          <tr>
            <td height=\"5\" valign=\"middle\"></td>
          </tr>
          <tr>
            <td height=\"50\" colspan=\"2\" valign=\"middle\"  style=\"font-size:11px; color:#7d7c7c;\" >
              <strong style=\"font-weight:bold;\">Best Wishes,</strong>
              <br></br>
              <xsl:value-of select=\"$AgentName\"/>
              <br></br>
              <xsl:value-of select=\"$AgentID\"/>
              <br></br>
            </td>
          </tr>
          <tr>
            <td height=\"5\" valign=\"middle\">            </td>
          </tr>
          <tr>
            <td height=\"5\" valign=\"middle\">            </td>
          </tr>
          <tr>
            <td align=\"middle\" style=\"font-size: 11px; border-top: 1px solid #ccc;            color: #7d7c7c; padding: 5px 0 0 0;\">
              <b>Disclaimer</b>: This communication (including any attachments) is intended solely for the person or entity to which it is addressed and may contain confidential and/or privileged material.  If you are not the intended recipient, any disclosure, copying, distribution or use of the contents of this communication is prohibited.If you have received this in error, please contact the sender immediately and delete this communication without copying..<br />
            </td>
          </tr>
          <tr>
            <td colspan=\"2\" valign=\"middle\">            </td>
          </tr>
          <tr>
            <td height=\"50\" colspan=\"2\" align=\"left\" valign=\"middle\" style=\"font-size:11px; color:#7d7c7c;\">              </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
  <xsl:template name=\"DisplayLink\">
    <xsl:param name=\"EncrypEnquiryId\"/>
    <xsl:param name=\"TriggerName\"/>
    <results>
      <xsl:choose>
        <xsl:when test=\"$TriggerName='MPFDetailsHEX'\">
          Address Proof ((Passport, Aadhaar Card, Driving License, Voter ID, Utility Bill etc..…)<br/>          <!--<a href=\"{concat('http://health.policybazaar.com/proposal?enquiryid=','<xsl:value-of select='$EncrypEnquiryId'/>')}\" target=\"_blank\">           http://health.policybazaar.com/proposal?enquiryid=\"<xsl:value-of select=\"$EncrypEnquiryId\"/>          </a>-->          <!--<xsl:value-of select=\"$DocComment\"/>-->                    http://health.policybazaar.com/proposal?enquiryid=\"          <xsl:value-of select=\"$EncrypEnquiryId\"/>
        </xsl:when>
      </xsl:choose>
    </results>
  </xsl:template>
</xsl:stylesheet>