﻿//http://localhost:50366/api/EndorsementApi/GetEndorsementFieldList
HealthExchange.config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
});

HealthExchange.service("HealthExchangeService", function ($http, $window) {

    //var UserDetails = JSON.parse($window.localStorage.getItem('UserId'));

    this.FileData = {};  

    this.GetSupplierList = function (Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "HealthExService.svc/GetSupplierList"
             , headers: {
                 "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
             }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                 $window.location.href = '/index.html';
            }
        });;
    }

    this.GetExLeadDetailsForCaseSend = function (LeadID, Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "HealthExService.svc/GetExLeadDetailsForCaseSend/" + LeadID
            , headers: {
                "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                 $window.location.href = '/index.html';
            }
        });;
    }
    
    this.GetAllExLead = function (objReqMarkExSearch, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/GetAllExLead",
            data: JSON.stringify(objReqMarkExSearch)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
                $window.location.href = '/index.html';
           }
       });
        return request;
    }
    
    this.MarkLeadForHealthEx = function (objReqMarkHealthEx, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/MarkLeadForHealthEx",
            data: JSON.stringify(objReqMarkHealthEx)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
                $window.location.href = '/index.html';
           }
       });
        return request;
    }

    this.AddSupplierPlanForEx = function (objPostPlanSelectionData, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/AddSupplierPlanForEx",
            data: JSON.stringify(objPostPlanSelectionData)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
                , "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
                $window.location.href = '/index.html';
           }
       });
        return request;
    }

    this.AddMemberDouments = function (objPostMemberDocumentDetails, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/AddMemberDouments",
            data: JSON.stringify(objPostMemberDocumentDetails)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
                , "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
                $window.location.href = '/index.html';
           }
       });
        return request;
    }

    this.GetLeadsForPBDashboard = function (objReqMarkExSearch, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/GetLeadsForPBDashboard",
            data: JSON.stringify(objReqMarkExSearch)
            , headers: {
                "Content-Type": "application/json"
                , "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }


    this.GetExLeadDetailsForInsurer = function (ExID, SelectionID, InsurerID, UserID, Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "HealthExService.svc/GetExLeadDetailsForInsurer/" + ExID + "/" + SelectionID + "/" + InsurerID + "/" + UserID,
            headers: {
                "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                 $window.location.href = '/index.html';
            }
        });
    }

    this.UpdateInsurerAction = function (objPostInsurerActionData, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/UpdateInsurerAction",
            data: JSON.stringify(objPostInsurerActionData)
            , headers: {
                "Content-Type": "application/json; charset=utf-8", "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
                $window.location.href = '/index.html';
           }
       });
        return request;
    }

    this.GetPBInsurerUnderwritingDetails = function (LeadID, Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "HealthExService.svc/GetPBInsurerUnderwritingDetails/" + LeadID,
            headers: {
                "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                 $window.location.href = '/index.html';
            }
        });
    }

    this.Login = function (LoginData) {
        debugger;
        $window.localStorage.removeItem('UserId');
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/Login",
            data: JSON.stringify(LoginData)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }

    this.GetLeadsForInsurerDashboard = function (objReqMarkExSearch, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/GetLeadsForInsurerDashboard",
            data: JSON.stringify(objReqMarkExSearch)
            , headers: {
                "Content-Type": "application/json"
                , "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
                $window.location.href = '/index.html';
           }
       });
        return request;
    }
    this.RemoveFromHealthEx = function (objReqRemoveFromHealthEx, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/RemoveFromHealthEx",
            data: JSON.stringify(objReqRemoveFromHealthEx)
            , headers: {
                "Content-Type": "application/json; charset=utf-8", "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
                $window.location.href = '/index.html';
           }
       });
        return request;
    }
    this.AcceptOrRejectInsurer = function (objPostInsurerActionData, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/AcceptOrRejectInsurer",
            data: JSON.stringify(objPostInsurerActionData)
            , headers: {
                "Content-Type": "application/json; charset=utf-8", "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }


    this.CustomerMedicalDetails = function (base64EncodedLeadID) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "CCTechService.svc/CustomerMedicalDetails/" + base64EncodedLeadID            
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
           
        });
    }

    this.GETPaymentDetailsForEx = function (base64EncodedLeadID) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "CCTechService.svc/GETPaymentDetailsForEx/" + base64EncodedLeadID
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {

        });
    }

    this.GetPBInsurerUnderwritingDetailsForSV = function (LeadID) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "CCTechService.svc/GetPBInsurerUnderwritingDetails/" + LeadID,
           
        }).success(function (data) {

            console.log(data);
        }).error(function (data) {
           
        });
    }

    this.ExLoginGetLeadData = function (LeadID) {
        debugger;
        var request = $http({
            method: "GET",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/ExLoginGetLeadData/" + LeadID           
            , headers: {
                "auth-token": btoa(JSON.parse($window.localStorage.getItem('UserId')).Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }

    this.SendEmail = function (CommunicationDetails) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.commService + "send",
            data: JSON.stringify(CommunicationDetails)
            , headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        })
       .error(function (error) {
           alert(error);
       });
        return request;
    }



    this.CommSend = function (leadId, TriggerName, MobileNo, EmailID, BookingType, ProductID, SelectionID) {
        debugger;
        var request = $http({
            method: "GET",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/SendCommunicationSync/" + leadId + "/" + TriggerName + "/" + "0" + "/" + EmailID + "/" + BookingType + "/" + ProductID + "/" + SelectionID
             , headers: {
                 "auth-token": btoa(JSON.parse($window.localStorage.getItem('UserId')).Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
             }
        })
        .error(function (error, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });
        return request;
    }

    this.InsertInsurerRemarks = function (objInsurerRemarksDetails, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/InsertInsurerRemarks",
            data: JSON.stringify(objInsurerRemarksDetails)
            , headers: {
                "Content-Type": "application/json; charset=utf-8", "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }

    this.AddNewUsers = function (objAddNewUser, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/AddNewUser",
            data: JSON.stringify(objAddNewUser)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(JSON.parse($window.localStorage.getItem('UserId')).Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }

    this.GetSupplierListByProduct = function (ProductID, Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "HealthExService.svc/MasterSupplier/" + ProductID
            , headers: {
                "auth-token": btoa(JSON.parse($window.localStorage.getItem('UserId')).Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });


    }

    this.GetHealthExUser = function (Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "HealthExService.svc/GetHealthExUser"
            , headers: {
                "auth-token": btoa(JSON.parse($window.localStorage.getItem('UserId')).Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });


    }

    this.UpdateUserInfo = function (objUpdateUserInfo, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/UpdateUserInfo",
            data: JSON.stringify(objUpdateUserInfo)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(JSON.parse($window.localStorage.getItem('UserId')).Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }


    this.ChangePassword = function (objChangePassword, Token) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: config.serviceURL + "HealthExService.svc/ChangePassword",
            data: JSON.stringify(objChangePassword)
            , headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(JSON.parse($window.localStorage.getItem('UserId')).Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
            }
        })
       .error(function (error, status) {
           if (status == 401) {
               $window.location.href = '/index.html';
           }
       });
        return request;
    }
});


HealthExchange.service("MedPayDetailService", function ($http, $window) {

    this.FileData = {};

    this.GetSupplierList = function (Token) {
        debugger;
        return $http({
            method: "GET",
            url: config.serviceURL + "HealthExService.svc/GetSupplierList"
             , headers: {
                 "auth-token": btoa(Token + "~" + JSON.parse($window.localStorage.getItem('UserId')).UserID)
             }
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            if (status == 401) {
                $window.location.href = '/index.html';
            }
        });;
    }


});