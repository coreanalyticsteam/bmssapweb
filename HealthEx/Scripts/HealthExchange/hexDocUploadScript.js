﻿var HEXDocUploadApp = angular.module("HEXDocUploadApp");

HEXDocUploadApp.service("HEXDocUploadAppService", function ($http, $window) {
    this.FileData = {};
    this.GetCustomerDocUploadDetails = function (ExID) {
        debugger;
        return $http({
            method: "GET",
            url: "http://localhost:55235/" + "CCTechService.svc/GetCustomerDocUploadDetails/" + ExID
        }).success(function (data) {

            console.log(data);
        }).error(function (data, status) {
            //if (status == 401) {
            //    $window.location.href = '/index.html';
            //}
        });;
    }

    this.AddCustomerDouments = function (objPostMemberDocumentDetails) {
        debugger;
        var request = $http({
            method: "post",
            dataType: 'json',
            url: "http://localhost:55235/" + "CCTechService.svc/AddCustomerDouments",
            data: JSON.stringify(objPostMemberDocumentDetails)           
        })
       .error(function (error, status) {
           //if (status == 401) {
           //    $window.location.href = '/index.html';
           //}
       });
        return request;
    }
});

HEXDocUploadApp.directive('fileModel', ['$parse', 'HEXDocUploadAppService', function ($parse, HEXDocUploadAppService) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    debugger;
                    HEXDocUploadAppService.FileData = element[0].files[0];
                    modelSetter(scope.$parent.$parent.myFile, element[0].files[0]);
                });
            });
        }
    };
}]);

HEXDocUploadApp.controller("HEXDocUploadAppCtrl", function ($scope, HEXDocUploadAppService, $rootScope, $routeParams, $uibModal, $window,$location) {
    $scope.ExID = $location.search().id;
    //alert($location.search().id);
    $scope.GetCustomerDocUploadDetails = function () {
        HEXDocUploadAppService.GetCustomerDocUploadDetails($scope.ExID).success(function (response) {
            $scope.CustomerDocUploadDetails = response;
        });
    };
    $scope.GetCustomerDocUploadDetails();
    //alert($location.search().id);
    $scope.isEmpty = function (str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };


    $scope.DocumentList = [{
        "Sno": 0, "Selected": {
            "Member": undefined, "Document": undefined, "FileDetails":
                { "FileName": undefined, "Size": undefined, "FileStringData": undefined }, "Comments": ""
        }
    }];

    $scope.AddMoreDouments = function () {
        var id = $scope.DocumentList.length;
        $scope.DocumentList.push({
            "Sno": id, "Selected": {
                "Member": undefined, "Document": undefined, "FileDetails":
                    { "FileName": undefined, "Size": undefined, "FileStringData": undefined }, "Comments": ""
            }
        });
    };

    var objDocumentList = [];

    $scope.ValidateDoc = function () {
        var msg = '';
        objDocumentList = [];
        if ($scope.DocumentList != undefined && $scope.DocumentList.length > 0) {
            for (var i in $scope.DocumentList) {

                if ($scope.DocumentList[i].Selected.Member == undefined || $scope.DocumentList[i].Selected.Member.MemberId == 0) {
                    $scope.Error = 'Please select Member';
                    return false;
                }
                else if ($scope.DocumentList[i].Selected.Document == undefined || $scope.DocumentList[i].Selected.Document.DocumentId == undefined || $scope.DocumentList[i].Selected.Document.DocumentId == 0) {
                    $scope.Error = 'Please select document';
                    return false;
                }
                else if ($scope.DocumentList[i].Selected.FileDetails == undefined || $scope.isEmpty($scope.DocumentList[i].Selected.FileDetails.FileStringData)) {
                    $scope.Error = 'Please select a file';
                    return false;
                }
                else if ($scope.DocumentList[i].Selected.Comments == undefined || $scope.isEmpty($scope.DocumentList[i].Selected.Comments)) {
                    $scope.Error = 'Please enter comments';
                    return false;
                }

                else {
                    objDocumentList.push({
                        "DocumentID": $scope.DocumentList[i].Selected.Document.DocumentId, "DocumentName": $scope.DocumentList[i].Selected.Document.Document,
                        "MemberID": $scope.DocumentList[i].Selected.Member.MemberId, "MemberName": $scope.DocumentList[i].Selected.Member.Name,
                        "Comments": $scope.DocumentList[i].Selected.Comments, "FileData": $scope.DocumentList[i].Selected.FileDetails.FileStringData, "FileName": $scope.DocumentList[i].Selected.FileDetails.FileName
                    })
                }
            }
        }
        else {
            $scope.Error = 'Please select document';
            return false;
        }
        if (msg == '') {
            return true;
        }
        else {
            //alert(msg);
            return false;
        }
    };

    $scope.AddMemberDouments = function () {

        $scope.Error = '';
        $scope.Msg = '';
        //for (var i in $scope.DocumentList) {
        //    objDocumentList.push( {
        //        "DocumentID": $scope.DocumentList[i].Selected.Document.DocumentId, "DocumentName":$scope.DocumentList[i].Selected.Document.Document ,
        //        "MemberID": $scope.DocumentList[i].Selected.Member.MemberId, "MemberName": $scope.DocumentList[i].Selected.Member.Name,
        //        "Comments": $scope.DocumentList[i].Selected.Comments, "FileData": $scope.DocumentList[i].Selected.FileDetails.FileStringData, "FileName": $scope.DocumentList[i].Selected.FileDetails.FileName
        //        })
        //}

        if ($scope.ValidateDoc()) {

            var confirmData = confirm("Do you want to save reports.");
            if (confirmData == true) {
                $scope.PageLoad = true;
                var objPostMemberDocumentDetails =
                {

                    "UserID": 0,
                    "EncryptExID": $scope.CustomerDocUploadDetails.EncryptEnquiryID,
                    "LeadID": $scope.CustomerDocUploadDetails.LeadID,
                    "ExID": $scope.CustomerDocUploadDetails.ExID,
                    "CustomerID": $scope.CustomerDocUploadDetails.CustomerID,//$scope.CustomerID,
                    "ProductID": $scope.CustomerDocUploadDetails.ProductID,//$scope.ProductID,
                    "Type": 1,
                    "MemberDocumentDetailsList": objDocumentList

                };

                HEXDocUploadAppService.AddCustomerDouments(objPostMemberDocumentDetails).success(function (response) {
                    $scope.GetCustomerDocUploadDetails();
                    $scope.DocumentList = [{
                        "Sno": 0, "Selected": {
                            "Member": undefined, "Document": undefined, "FileDetails":
                        { "FileName": undefined, "Size": undefined, "FileStringData": undefined }, "Comments": ""
                        }
                    }];
                    $scope.PageLoad = false;
                    // alert('Reports Uploaded successfully.');
                    $scope.Msg = 'Reports Uploaded successfully.';
                });
            }
        }



    }



    $scope.DeleteDoc = function (index) {
        $scope.DocumentList.splice(index, 1);
    }

    $scope.file_changed = function (element) {
        debugger;

        $scope.$apply(function (scope) {
            //$scope.DocumentList

            var photofile = element.files[0];

            var fileReader = new FileReader();


            fileReader.onload = function (fileLoadedEvent) {
                debugger;
                for (var i in $scope.DocumentList) {
                    if (element.id == i) {
                        $scope.DocumentList[i].Selected.FileDetails.FileName = element.files[0].name;
                        $scope.DocumentList[i].Selected.FileDetails.Size = element.files[0].size
                        $scope.DocumentList[i].Selected.FileDetails.FileStringData = btoa(fileLoadedEvent.target.result);
                    }
                }

            };
            if (element.files.length > 0) {
                fileReader.readAsBinaryString(element.files[0]);
            }

        });
    };




});