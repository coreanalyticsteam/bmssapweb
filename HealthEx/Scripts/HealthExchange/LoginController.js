﻿HealthExchange.controller("LoginCtrl", function ($scope, HealthExchangeService, $rootScope, $routeParams, $window) {
    
    $scope.isEmpty = function (str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };
    $scope.UserDetails = undefined;
    $scope.LeadDetails = undefined;
    $window.localStorage.removeItem('StorFilter');
    $window.localStorage.removeItem('LeadDetails');
    $window.localStorage.removeItem('UserId');
    //$rootScope.UserDetails = undefined;
    $scope.Msg = '';
    $scope.Login = function () {
        $scope.Msg = '';
        debugger;
        if ( !$scope.isEmpty($scope.LoginID) && !$scope.isEmpty($scope.Password) ) {
            var LoginData = { "LoginID": $scope.LoginID, "Password": $scope.Password, "LoginUserType": 1 };
            HealthExchangeService.Login(LoginData).success(function (data) {
                var storage = JSON.stringify({
                    EmailID: data.EmailID,
                    LoginID: data.LoginID,
                    LoginStatus: data.LoginStatus,
                    MobileNo: data.MobileNo,
                    Name: data.Name,
                    Token: data.Token,
                    UserID: data.UserID,
                    UserRole: data.UserRole,
                    UserType: data.UserType,
                    InsurerID: data.InsurerID,
                    InsurerName: data.InsurerName
                });
                $scope.$broadcast('GetUserDetails', storage);
                if (data.LoginStatus == 1) {
                    $window.localStorage.setItem('UserId', storage);
                  
                    //$rootScope.UserDetails = JSON.parse($window.localStorage.getItem('UserId'));
                    if (data.UserType == 1) {
                        $window.location.href = '/home.html#/HealthExchange/PB/PBDashboard';
                    }
                    else if (data.UserType == 2) {
                        $window.location.href = '/home.html#/HealthExchange/Insurer/InsurerDashboard';
                    }
                 
                }
                else {
                    $scope.Msg='Invalid credentials';
                }               

            });
        }
        else {
            $scope.Msg = 'Please enter Username and Password';
        }
    }

    //var storage = localStorage.getItem(UserId);
    //return JSON.parse(storage);

});

HealthExchange.controller("InternalLoginCtrl", function ($scope, HealthExchangeService, $rootScope, $routeParams, $window) {

    $scope.base64EncodedData = atob($routeParams.base64EncodedData);
    $scope.Page = $routeParams.Page;
    
    //base64EncodedData - split by ~ => UserID~UserType(2-Matrix,3-BMS)~LeadID
    //--page
    //1 - Dashboard
    //2 - Leadpage




    $scope.isEmpty = function (str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };
    $scope.Msg = '';
    $scope.Login = function () {
        var UrlData = $scope.base64EncodedData.split('~');
        debugger;
        if (!$scope.isEmpty(UrlData[0])) {
            var LoginData = { "LoginID": UrlData[0], "Password": '1234', "LoginUserType": UrlData[1] };
            HealthExchangeService.Login(LoginData).success(function (data) {
                debugger;
                var storage = JSON.stringify({
                    EmailID: data.EmailID,
                    LoginID: data.LoginID,
                    LoginStatus: data.LoginStatus,
                    MobileNo: data.MobileNo,
                    Name: data.Name,
                    Token: data.Token,
                    UserID: data.UserID,
                    UserRole: data.UserRole,
                    UserType: data.UserType,
                    InsurerID: data.InsurerID,
                    InsurerName: data.InsurerName
                });
                if (data.LoginStatus == 1) {
                    $window.localStorage.setItem('UserId', storage);
                    if ($scope.Page == 1)//Dashboard
                    {
                        $window.location.href = '/home.html#/HealthExchange/PB/PBDashboard';
                    }
                    else if ($scope.Page == 2)//Leadpage
                    {
                        HealthExchangeService.ExLoginGetLeadData(UrlData[2]).success(function (resdata) {
                            var storage = JSON.stringify({
                                LeadID: resdata.LeadID,
                                NewLeadID: resdata.NewLeadID,
                                ExID: resdata.ExID,
                                //SelectionID: data.SelectionID,
                                CustomerID: resdata.CustomerID,
                                ExLeadType: resdata.ExLeadType,
                                ProductID: '2'
                            });
                            $window.localStorage.setItem('LeadDetails', storage);
                            $window.location.href = '/home.html#/HealthExchange/PB/InsurerUnderwritingResponse';
                            //if (resdata.ExStatusID == 4) {
                            //    $window.location.href = '/home.html#/HealthExchange/PB/InsurerUnderwritingResponse';
                            //}
                            //else if (resdata.ExStatusID == 2 || resdata.ExStatusID == 3) {
                            //    $window.location.href = '/home.html#/HealthExchange/PB/UploadCaseToInsurer';
                            //}
                        });
                       
                    }
                    
                }
                else {
                    $scope.Msg = 'Invalid credentials';
                }

            });
        }
        else {
            $scope.Msg = 'Please enter Username and Password';
        }
    }
    $scope.Login();
    //var storage = localStorage.getItem(UserId);
    //return JSON.parse(storage);

});