﻿
HealthExchange.controller("InsurerDashboardCtrl", function ($scope, HealthExchangeService, $rootScope, $routeParams, $uibModal, $window) {

    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserId'));
   
    $scope.chkLogin = function () {
        if ($window.localStorage.getItem('UserId') == null) {
            $window.location.href = '/index.html';
        }
        else {
            if ($scope.UserDetails.UserType == 1) {
                $window.location.href = '/home.html#/HealthExchange/PB/PBDashboard';
            }
        }
    };
    $scope.LogOut = function () {
        $window.localStorage.removeItem('UserId');
        $window.localStorage.removeItem('StorFilter');
        if ($window.localStorage.getItem('UserId') == null) {
            $window.location.href = '/home.html';
        }
    };
    $scope.chkLogin();
    //$scope.userID = $routeParams.UserID;
   
    $scope.CommonData = CommonData;
    $scope.Selected = {
        "SearchParameterInsurerDashboard": { "ID": 0, "Name": "Select One" }, "DateRange": { "ID": 0, "Name": "Select One" }
        , "InsurerDashboardKey": { "ID": 0, "Name": "Select One" }, "InsurerDashboardStatus": { "ID": 0, "Name": "Select One" }
    }

  
    $scope.isEmpty = function (str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };

    $scope.GetPagePreData = function () {
        if ($window.localStorage.getItem('StorFilter') != null) {
            debugger;
            $scope.StorFilter = JSON.parse($window.localStorage.getItem('StorFilter'));
            $scope.FilterParam = $scope.StorFilter.FilterParam;
            $scope.Selected = $scope.StorFilter.FilterType;
            $scope.FromDate = new Date($scope.StorFilter.FromDate);
            $scope.ToDate = new Date($scope.StorFilter.ToDate);
            debugger;
            if ($scope.Selected == undefined) {
                $scope.Selected = {
                    "SearchParameterInsurerDashboard": { "ID": 0, "Name": "Select One" },
                    "DateRange": { "ID": 0, "Name": "Select One" },
                    "InsurerDashboardKey": { "ID": 0, "Name": "Select One" },
                    "InsurerDashboardStatus": { "ID": 0, "Name": "Select One" }
                }
            }
            $scope.GetLeadsForInsurerDashboard();
        }
    };

    
    $scope.GetLeadsForInsurerDashboard = function () {
        var msg="";
        if ($scope.Selected.SearchParameterInsurerDashboard.ID > 0 && $scope.isEmpty($scope.FilterParam)) {
            msg = 'Please enter ' + $scope.Selected.SearchParameterInsurerDashboard.Name;
        }
        if (msg == '') {
            $scope.CommonData.ButtonLabel.Search = "Please wait...";
            var objReqMarkExSearch =
            {
                "objReqMarkExSearch": {
                    "FilterParam": $scope.FilterParam,
                    "FilterType": $scope.Selected.SearchParameterInsurerDashboard.ID,
                    "FromDate": $scope.FromDate,
                    "ToDate": $scope.ToDate,
                    "Status": $scope.Selected.InsurerDashboardStatus.ID,
                    "KeyMatrix": $scope.Selected.InsurerDashboardKey.ID,
                    "InsurerID": $scope.UserDetails.InsurerID
                }
            };

            var StorFilter = {
                "FilterParam": $scope.FilterParam, "FilterType": $scope.Selected, "FromDate": $scope.FromDate, "ToDate": $scope.ToDate
            };

            debugger;
            HealthExchangeService.GetLeadsForInsurerDashboard(objReqMarkExSearch, $scope.UserDetails.Token).success(function (data) {
                $scope.AllLeadForHealth = data;
                $scope.CommonData.ButtonLabel.Search = "Search";
                $window.localStorage.setItem('StorFilter', JSON.stringify(StorFilter));
            });
        }
        else { alert(msg); }
    };
    
    
    $scope.OpenLead = function (data) {
        //alert('');
        var storage = JSON.stringify({
            //LeadID: data.OldLeadID,
            //ExID: data.ExID,
            //SelectionID: data.SelectionID,
            //CustomerID: data.CustomerID,
            //ExLeadType: data.ExLeadType,
            //ProductID: '2'
            LeadID: data.OldLeadID,
            NewLeadID: data.NewLeadID,
            ExID: data.ExID,
            SelectionID: data.SelectionID,
            CustomerID: data.CustomerID,
            ExLeadType: data.ExLeadType,
            ProductID: '2'
        });
        debugger;
        localStorage.setItem('LeadDetails', storage);
        $window.location.href = '/home.html#/HealthExchange/Insurer/InsurerUnderwriting';
    }
    //$scope.GetLeadsForPBDashboard();

    $scope.resetSelection = function () {
        $scope.FilterParam = '';
    };

    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.data = [];
    $scope.q = '';

    $scope.getData = function () {
        return $filter('filter')($scope.AllLeadForHealthEx.GetLeadsForPBDashboardResult, $scope.q)
    }

    $scope.numberOfPages = function () {
        return Math.ceil($scope.getData().length / $scope.pageSize);
    }

    $scope.$watch('q', function (newValue, oldValue) {
        if (oldValue != newValue) {
            $scope.currentPage = 0;
        }
    }, true);

    $scope.exportData = function () {

        alasql('SELECT HealthExMarkDate  AS DateTime,NewLeadID AS LeadID,CustomerID,Name AS ProposerName,PlanName ,Remarks AS Status,KeyMatrix INTO XLSX("Data_' + Date.now() + '.xlsx",{headers:true}) FROM ?', [$scope.AllLeadForHealth.GetLeadsForInsurerDashboardResult]);
    };


    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    $scope.NumberOfDays = 0;
    $scope.ToDate = Date.now();
    $scope.FromDate = Date.now() + ($scope.NumberOfDays * 24 * 60 * 60 * 1000);
    $scope.DateChange = function () {
        if ($scope.Selected.DateRange.ID == 1) {
            $scope.NumberOfDays = 0;
        }
        else if ($scope.Selected.DateRange.ID == 2) {
            $scope.NumberOfDays = 7;
        }
        else if ($scope.Selected.DateRange.ID == 3) {
            $scope.NumberOfDays = 15;
        }
        else if ($scope.Selected.DateRange.ID == 4) {
            $scope.NumberOfDays = 30;
        }

        $scope.ToDate = Date.now();
        $scope.FromDate = Date.now() - ($scope.NumberOfDays * 24 * 60 * 60 * 1000);
    };
    $scope.dateOptions = {
        //dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
          mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
          date: tomorrow,
          status: 'full'
      },
      {
          date: afterTomorrow,
          status: 'partially'
      }
    ];

    function getDayClass(data) {
        var date = data.date,
          mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
    $scope.GetPagePreData();


});
//PBDashboardCtrl
HealthExchange.controller("InsurerUnderwritingCtrl", function ($scope, HealthExchangeService, $rootScope, $routeParams, $uibModal, $window) {
    //:UserID/:InsurerID/:LeadID
    $scope.CommonData = CommonData;
    $scope.UserDetails = JSON.parse($window.localStorage.getItem('UserId'));

    $scope.LeadDetails = JSON.parse($window.localStorage.getItem('LeadDetails'));
    $scope.SetInsurerImage = function () {
        if ($scope.UserDetails != undefined) {
            if ($scope.UserDetails.InsurerID != 0) {
               // $scope.UserDetails.InsurerImage = InsurerImage.;
            }
        }
    };
    $scope.chkLogin = function () {
        if ($window.localStorage.getItem('UserId') == null) {
            $window.location.href = '/index.html';
        }
        else {
            if ($scope.UserDetails.UserType == 1) {
                $window.location.href = '/home.html#/HealthExchange/PB/PBDashboard';
            }
        }
    };
    $scope.LogOut = function () {
        $window.localStorage.removeItem('UserId');
        if ($window.localStorage.getItem('UserId') == null) {
            $window.location.href = '/index.html';
        }
    };
    $scope.chkLogin();

    $scope.base64EncodedLeadID = btoa($scope.LeadDetails.NewLeadID);

    $scope.CustomerMedicalDetails = function () {
        if ($scope.LeadDetails.ExLeadType == 1) {
            HealthExchangeService.CustomerMedicalDetails($scope.base64EncodedLeadID).success(function (data) {
                debugger;
                $scope.CustomerMedicalDetails = data;
            });
        }
    };

    $scope.GETPaymentDetailsForEx = function () {
        if ($scope.LeadDetails.ExLeadType == 1) {
            HealthExchangeService.GETPaymentDetailsForEx($scope.base64EncodedLeadID).success(function (data) {
                debugger;
                $scope.GETPaymentDetailsForEx = data;
            });
        }
    };

    $scope.Calculate = function (type) {
        if (type == 1) {
            //$scope.ExLeadDetailsForInsurer.PremLoadingPerc
            //$scope.ExLeadDetailsForInsurer.PremLoadingAmount
            //$scope.ExLeadDetailsForInsurer.FinalPremium
            if (parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingPerc) >= 100 || parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingPerc) < 0) {
                $scope.ExLeadDetailsForInsurer.PremLoadingPerc = 0;
            }
            $scope.ExLeadDetailsForInsurer.PremLoadingAmount =parseInt( parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingPerc) * .01 * parseFloat($scope.ExLeadDetailsForInsurer.InsurerDetails.BasePremium));
            $scope.ExLeadDetailsForInsurer.FinalPremium =parseInt( parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingAmount) + parseFloat($scope.ExLeadDetailsForInsurer.InsurerDetails.BasePremium));
        }
        else if (type == 2) {
            if (parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingAmount) >= 99999 || parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingAmount) < 0) {
                $scope.ExLeadDetailsForInsurer.PremLoadingAmount = 0;
            }
            //$scope.ExLeadDetailsForInsurer.PremLoadingPerc
            //$scope.ExLeadDetailsForInsurer.PremLoadingAmount
            //$scope.ExLeadDetailsForInsurer.FinalPremium
            $scope.ExLeadDetailsForInsurer.PremLoadingPerc =parseInt( parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingAmount) /( .01 * parseFloat($scope.ExLeadDetailsForInsurer.InsurerDetails.BasePremium)));
            $scope.ExLeadDetailsForInsurer.FinalPremium = parseInt(parseFloat($scope.ExLeadDetailsForInsurer.PremLoadingAmount) + parseFloat($scope.ExLeadDetailsForInsurer.InsurerDetails.BasePremium));
        }
    };

    $scope.CustomerMedicalDetails();
    $scope.GETPaymentDetailsForEx();
    //$scope.UserID = $routeParams.UserID;
    //$scope.InsurerID = $routeParams.InsurerID;
    //$scope.LeadID = $routeParams.LeadID;
    $scope.CommonData = CommonData;
    $scope.TaskPending = TaskPending;
    $scope.GetExLeadDetailsForInsurer = function () {
        HealthExchangeService.GetExLeadDetailsForInsurer($scope.LeadDetails.ExID, $scope.LeadDetails.SelectionID,
                                        $scope.UserDetails.InsurerID, $scope.UserDetails.UserID, $scope.UserDetails.Token).success(function (data) {
            debugger;
            $scope.ExLeadDetailsForInsurer = data;
            $scope.ExLeadDetailsForInsurer.InsurerDetails.ExcPlan = btoa($scope.ExLeadDetailsForInsurer.InsurerDetails.PlanID);
            $scope.ExLeadDetailsForInsurer.NewInsurerRemarks = $scope.ExLeadDetailsForInsurer.InsurerComment;
            //alert($scope.ExLeadDetailsForInsurer.InsurerStatusID);
            if ($scope.ExLeadDetailsForInsurer.InsurerStatusID == 2 || $scope.ExLeadDetailsForInsurer.InsurerStatusID == 3
                || $scope.ExLeadDetailsForInsurer.InsurerStatusID == 4 || $scope.ExLeadDetailsForInsurer.InsurerStatusID == 7
                || $scope.ExLeadDetailsForInsurer.InsurerStatusID == 8 || $scope.ExLeadDetailsForInsurer.InsurerStatusID == 9)
            {
                $scope.ControlPageAction = 1;
            }
            else {
                $scope.ControlPageAction = 0;
            }
           
        });
    }
    $scope.CheckAction = function (id) {
        if (id == 1) {
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Accept = true;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Reject = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AdditionalInfoRequired = false;
        }
        else if (id == 2) {
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Accept = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Reject = true;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AdditionalInfoRequired = false;
        }
        else if (id == 3) {
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Accept = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Reject = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions = true;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AdditionalInfoRequired = false;
        }
        else if (id == 4) {
            $scope.InsurerAdditionalInfoDeatilsList = [];
            $scope.AddTaskPending();
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Accept = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Reject = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions = false;
            $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AdditionalInfoRequired = true;
        }
    }
    $scope.GetExLeadDetailsForInsurer();
    $scope.isEmpty = function (str) {
        return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
    };
    var MemberDetailsList = [];
    $scope.PageValidate = function () {
        MemberDetailsList = [];
        var msg = '';
        var chk = 0;
        if ($scope.ExLeadDetailsForInsurer.InsurerActionDetails.Accept || $scope.ExLeadDetailsForInsurer.InsurerActionDetails.Reject || $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions || $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AdditionalInfoRequired) {
            chk = 1;
            if (
                ($scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions || $scope.ExLeadDetailsForInsurer.InsurerActionDetails.AdditionalInfoRequired)
                && ($scope.isEmpty($scope.ExLeadDetailsForInsurer.NewInsurerRemarks))
                ) {
                msg = 'Please enter comments';
            }
        }
        
        if (chk == 0) {
            msg = 'Please select a status';
        }
        else {
            if ($scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions) {
                if ($scope.ExLeadDetailsForInsurer.IsCoPay) {
                    if ($scope.ExLeadDetailsForInsurer.CoPay == undefined || $scope.ExLeadDetailsForInsurer.CoPay == 0 && $scope.ExLeadDetailsForInsurer.CopayType != 2) {
                        msg = 'Please enter a valid CoPay value';
                    }
                }
                else {
                    $scope.ExLeadDetailsForInsurer.CoPay = 0;
                }
                if($scope.ExLeadDetailsForInsurer.IsDiseaseExclusion)
                {
                    if ($scope.ExLeadDetailsForInsurer.IsDiseaseExclusionPerm || $scope.ExLeadDetailsForInsurer.IsDiseaseExclusionTemp) {
                        if ($scope.ExLeadDetailsForInsurer.IsDiseaseExclusionPerm) {
                            if ($scope.ExLeadDetailsForInsurer.DiseaseExclusionPerm == undefined || $scope.isEmpty($scope.ExLeadDetailsForInsurer.DiseaseExclusionPerm)) {
                                msg = 'Please enter Disease Exclusion reason';
                            }
                        }
                        if ($scope.ExLeadDetailsForInsurer.IsDiseaseExclusionTemp) {
                            if ($scope.ExLeadDetailsForInsurer.DiseaseExclusionTemp == undefined || $scope.isEmpty($scope.ExLeadDetailsForInsurer.DiseaseExclusionTemp)) {
                                msg = 'Please enter Disease Exclusion reason';
                            }
                        }
                    }
                    else {
                        msg = 'Please select Disease Exclusion type';
                    }
                }
                else {
                    $scope.ExLeadDetailsForInsurer.DiseaseExclusionTemp = '';
                    $scope.ExLeadDetailsForInsurer.DiseaseExclusionPerm = '';
                }
                if ($scope.ExLeadDetailsForInsurer.IsMemberExclusion) {
                    for (var i in $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers) {
                        if ($scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].IsExcluded == true) {
                            if ($scope.isEmpty($scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].ExclusionReason)) {
                                msg =  ' Please enter reason for ' + $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].Name;
                            }
                            else {
                                MemberDetailsList.push({
                                    "CustMemId": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].MemberId,
                                    "InsuredName": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].Name == null ? '' : $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].Name,
                                    "ExclusionReason": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].ExclusionReason
                                });
                            }
                        }
                    }
                    if (MemberDetailsList.length == 0) {
                        if (msg == '') {
                            msg = 'Please select Member';
                        }
                    }
                }
                else {
                    MemberDetailsList = [];
                }

                if ($scope.ExLeadDetailsForInsurer.IsLoadPremium) {
                    if ($scope.ExLeadDetailsForInsurer.LoadingType == 1) {
                        if ($scope.isEmpty($scope.ExLeadDetailsForInsurer.FinalPremium) || $scope.ExLeadDetailsForInsurer.FinalPremium == 0) {
                            msg = 'Please enter a valid Loading premium';
                        }
                        else if (!$scope.isEmpty($scope.ExLeadDetailsForInsurer.FinalPremium) && $scope.ExLeadDetailsForInsurer.FinalPremium <= $scope.ExLeadDetailsForInsurer.InsurerDetails.BasePremium) {
                            msg = 'Loading premium should be greater than base premium';
                        }
                    }
                    //ExLeadDetailsForInsurer.InsurerDetails.BasePremium

                }
                else {
                    $scope.ExLeadDetailsForInsurer.FinalPremium = 0;
                }
            }
        }

        if (msg == '') {
            return true;
        }
        else {
            alert(msg);
            return false;
        }

    };
    $scope.InsurerAdditionalInfoDeatilsList = [];
    $scope.UpdateInsurerAction = function () {
        var statusID = 0;
        var msg = '';
        var acceptWithCondition = 0;
       
        if ($scope.PageValidate()) {
            var confirmData = confirm("Do you want to save changes");
            if (confirmData == true) {

                if ($scope.ExLeadDetailsForInsurer.InsurerActionDetails.Accept) {
                    statusID = 2;
                }
                else if ($scope.ExLeadDetailsForInsurer.InsurerActionDetails.Reject) {
                    statusID = 3;
                }
                else if ($scope.ExLeadDetailsForInsurer.InsurerActionDetails.AcceptWithConditions) {
                    statusID = 4;
                }
                else if ($scope.ExLeadDetailsForInsurer.InsurerActionDetails.AdditionalInfoRequired) {
                    statusID = 5;
                }
                debugger;
                //if ((statusID == 4 || statusID == 5) && ($scope.isEmpty($scope.ExLeadDetailsForInsurer.NewInsurerRemarks))) {
                //    msg = 'Please enter remarks';
                //}
                //else if (statusID == 4 && $scope.ExLeadDetailsForInsurer.IsCoPay && ($scope.ExLeadDetailsForInsurer.CoPay == undefined)) {
                //    msg = 'CoPay should be between  0 to 100';
                //}

                //if (statusID == 4 && $scope.ExLeadDetailsForInsurer.IsMemberExclusion) {
                //    for (var i in $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers) {
                //        if ($scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].IsExcluded == true) {
                //            MemberDetailsList.push({
                //                "CustMemId": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].MemberId, "InsuredName": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].Name,
                //                "ExclusionReason": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[i].ExclusionReason
                //            });
                //        }
                //    }
                //}
                if ($scope.InsurerAdditionalInfoDeatilsList.length > 0) {
                    var InsurerAdditionalInfoDeatilsList = [];
                    if (statusID == 5) {
                        for (var i = 0; i < $scope.InsurerAdditionalInfoDeatilsList.length; i++) {
                            var dataInsurerAdditionalInfoDeatilsList = $scope.InsurerAdditionalInfoDeatilsList[i];
                            //msg
                            if (dataInsurerAdditionalInfoDeatilsList.Selected.TaskPending != undefined && dataInsurerAdditionalInfoDeatilsList.Selected.Member != undefined && dataInsurerAdditionalInfoDeatilsList.Selected.Comments != '') {
                                InsurerAdditionalInfoDeatilsList.push({
                                    "InfoType": dataInsurerAdditionalInfoDeatilsList.Selected.TaskPending.TaskPendingID
                                                                        , "InfoName": dataInsurerAdditionalInfoDeatilsList.Selected.TaskPending.TaskPending
                                                                        , "MemberID": dataInsurerAdditionalInfoDeatilsList.Selected.Member.MemberId
                                                                        , "Relation": dataInsurerAdditionalInfoDeatilsList.Selected.Member.Relation.RelationShipName
                                                                        , "MemberName": dataInsurerAdditionalInfoDeatilsList.Selected.Member.Name == null ? "" : dataInsurerAdditionalInfoDeatilsList.Selected.Member.Name
                                                                        , "Comments": dataInsurerAdditionalInfoDeatilsList.Selected.Comments
                                });
                            }
                            else {
                                msg = 'Please enter all details.';
                            }
                        }
                    }
                }
                var objPostInsurerActionData;
                if (statusID == 4) {
                    var CopayDetailsList = [];
                    if ($scope.ExLeadDetailsForInsurer.IsCoPay) {
                        if ($scope.ExLeadDetailsForInsurer.CopayType == 2) {
                            for (var j = 0; j < $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers.length; j++) {
                                CopayDetailsList.push(
                                    {
                                     "MemberID": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].MemberId
                                     , "MemberName": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].Name == null ? "" : $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].Name
                                     , "Copay": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].CoPay
                                     , "RelationShip": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].Relation.RelationShipName
                                    });
                            }
                            $scope.ExLeadDetailsForInsurer.CoPay = 0;
                            //CopayDetailsList.push({"MemberID":,"MemberName":,"Copay"});
                        }
                       
                    }
                    else {
                        $scope.ExLeadDetailsForInsurer.CoPay = 0;
                    }
                    if ($scope.ExLeadDetailsForInsurer.IsLoadPremium
                        || $scope.ExLeadDetailsForInsurer.IsDiseaseExclusion
                        || $scope.ExLeadDetailsForInsurer.IsCoPay
                        || $scope.ExLeadDetailsForInsurer.IsMemberExclusion) {
                        acceptWithCondition = 1;
                    }
                    var LoadingDetailsList = [];
                    if ($scope.ExLeadDetailsForInsurer.IsLoadPremium) {
                        if ($scope.ExLeadDetailsForInsurer.LoadingType == 2) {
                            for (var j = 0; j < $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers.length; j++) {
                                if ($scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].MemberLoading > 0) {
                                    LoadingDetailsList.push(
                                        {
                                            "MemberID": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].MemberId
                                         , "MemberName": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].Name == null ? "" : $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].Name
                                         , "MemberLoading": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].MemberLoading
                                         , "RelationShip": $scope.ExLeadDetailsForInsurer.ProposerDetails.CoveredMembers[j].Relation.RelationShipName
                                        });
                                }
                            }
                            $scope.ExLeadDetailsForInsurer.Loading = 0;
                        }

                    }
                    objPostInsurerActionData = {
                        "UserID": $scope.UserDetails.UserID,
                        "InsurerID": $scope.UserDetails.InsurerID,

                        "NewLeadID": $scope.ExLeadDetailsForInsurer.CustomerLead.NewLeadID,
                        "EmailID": $scope.ExLeadDetailsForInsurer.CustomerLead.EmailID,
                       // "InsurerID": $scope.ExLeadDetailsForInsurer.CustomerLead.EnquiryID,

                        "ExID": $scope.LeadDetails.ExID,
                        "EnquiryID":$scope.ExLeadDetailsForInsurer.CustomerLead.EnquiryID,
                        "SelectionID": $scope.LeadDetails.SelectionID,
                        "LeadID": $scope.LeadDetails.LeadID,
                        "IsLoadPremium": $scope.ExLeadDetailsForInsurer.IsLoadPremium,
                        "FinalPremium": $scope.ExLeadDetailsForInsurer.FinalPremium,
                        "IsDiseaseExclusion": $scope.ExLeadDetailsForInsurer.IsDiseaseExclusion,
                        //"DiseaseExclusionType": $scope.Selected.DiseaseType.value,
                        //"DiseaseExclusion": $scope.ExLeadDetailsForInsurer.DiseaseExclusion,
                        "IsDiseaseExclusionPerm": $scope.ExLeadDetailsForInsurer.IsDiseaseExclusionPerm,
                        "IsDiseaseExclusionTemp": $scope.ExLeadDetailsForInsurer.IsDiseaseExclusionTemp,
                        "DiseaseExclusionTemp": $scope.ExLeadDetailsForInsurer.DiseaseExclusionTemp,
                        "DiseaseExclusionPerm": $scope.ExLeadDetailsForInsurer.DiseaseExclusionPerm,
                        "IsCoPay": $scope.ExLeadDetailsForInsurer.IsCoPay,
                        "CoPay": $scope.ExLeadDetailsForInsurer.CoPay,
                        "IsMemberExclusion": $scope.ExLeadDetailsForInsurer.IsMemberExclusion,
                        "StatusID": statusID,
                        "Remarks": $scope.ExLeadDetailsForInsurer.NewInsurerRemarks,
                        "MemberDetailsList": MemberDetailsList,
                        "CopayDetailsList": CopayDetailsList,
                        "LoadingDetailsList": LoadingDetailsList,
                        "CopayType": $scope.ExLeadDetailsForInsurer.CopayType,
                        "LoadingType": $scope.ExLeadDetailsForInsurer.LoadingType,
                        "PremLoadingAmount": $scope.ExLeadDetailsForInsurer.PremLoadingAmount,
                        "PremLoadingPerc": $scope.ExLeadDetailsForInsurer.PremLoadingPerc,
                    };
                }
                else {
                    objPostInsurerActionData = {
                        "UserID": $scope.UserDetails.UserID,
                        "InsurerID": $scope.UserDetails.InsurerID,
                        "NewLeadID": $scope.ExLeadDetailsForInsurer.CustomerLead.NewLeadID,
                        "EmailID": $scope.ExLeadDetailsForInsurer.CustomerLead.EmailID,
                        "ExID": $scope.LeadDetails.ExID,
                        "SelectionID": $scope.LeadDetails.SelectionID,
                        "EnquiryID": $scope.ExLeadDetailsForInsurer.CustomerLead.EnquiryID,
                        "LeadID": $scope.LeadDetails.LeadID,
                        "IsLoadPremium": false,
                        "FinalPremium": $scope.ExLeadDetailsForInsurer.InsurerDetails.BasePremium,
                        "IsDiseaseExlusion": false,
                        //"DiseaseExclusionType": $scope.Selected.DiseaseType.value,
                        //"DiseaseExclusion": $scope.ExLeadDetailsForInsurer.DiseaseExclusion,
                        "IsDiseaseExlusionPerm": false,
                        "IsDiseaseExlusionTemp": false,
                        "DiseaseExclusionTemp": "",
                        "DiseaseExclusionPerm": "",
                        "IsCoPay": false,
                        "CoPay": "0",
                        "IsMemberExclusion": false,
                        "StatusID": statusID,
                        "Remarks": $scope.ExLeadDetailsForInsurer.NewInsurerRemarks,
                        "MemberDetailsList": MemberDetailsList,
                        "InsurerAdditionalInfoDeatilsList": InsurerAdditionalInfoDeatilsList
                    };
                }
                debugger;
                if (msg == '') {
                    if ((acceptWithCondition == 1 && statusID == 4) || statusID != 4) {
                        HealthExchangeService.UpdateInsurerAction(objPostInsurerActionData, $scope.UserDetails.Token).success(function (response) {
                            if (response > 0) {
                                $scope.GetExLeadDetailsForInsurer();
                                $scope.InsurerAdditionalInfoDeatilsList = [];
                                alert('Submitted successfully.');
                                $scope.msg = 'Submitted successfully.';
                            }
                            else {
                                alert('Some error occured, please try again.');
                            }
                        });
                    }
                    else {
                        alert('Please select one condition.');
                    }
                }
                else {
                    alert(msg);
                    $scope.msg = msg;
                }
            }
        }
    };
    $scope.AddTaskPending = function () {
        //$scope.ExLeadDetailsForInsurer.InsurerAdditionalInfoDeatilsList
        $scope.InsurerAdditionalInfoDeatilsList.push({ "Selected": { "TaskPending": undefined, "Member": undefined, "Comments": undefined } });
    };
   
    $scope.DeleteAddInfo = function (index) {
        $scope.InsurerAdditionalInfoDeatilsList.splice(index, 1);
    }


    $scope.InsertInsurerRemarks = function () {
        var d = new Date();
        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        var date = month[d.getMonth()] + " " + d.getDate() + " " + d.getFullYear();
        var time = d.toLocaleTimeString().toLowerCase();


        var statusID = 0;
        var msg = '';
        var curDate=new Date();
            var confirmData = confirm("Do you want to save changes");
            if (confirmData == true) {
                var objInsurerRemarksDetails;                
                    objInsurerRemarksDetails = {
                        "UserID": $scope.UserDetails.UserID,
                        "InsurerID": $scope.UserDetails.InsurerID,
                        "ExID": $scope.LeadDetails.ExID,
                        "SelectionID": $scope.LeadDetails.SelectionID,
                        "LeadID": $scope.LeadDetails.LeadID,
                        "Remarks": $scope.MyRemarks,
                        "CreatedOn": date + " " + time
                    };
                              
              
                if (msg == '') {
                    HealthExchangeService.InsertInsurerRemarks(objInsurerRemarksDetails, $scope.UserDetails.Token).success(function (response) {
                        if (response > 0) {
                            $scope.MyRemarks = '';
                            $scope.ExLeadDetailsForInsurer.InsurerRemarksDetailsList.unshift(objInsurerRemarksDetails);
                        }
                        //$scope.GetExLeadDetailsForInsurer();
                        //alert('Submitted successfully.');
                        //$scope.msg = 'Submitted successfully.';
                    });
                }
                else {
                    alert(msg);
                    $scope.msg = msg;
                }
            }
        
    };
   

});
