﻿
angular.module("HealthExchange", ["ngRoute", "ui.bootstrap"]).config(["$routeProvider",
  function ($routeProvider) {
      $routeProvider.
        when('/Details/MedicalDetails/:base64EncodedLeadID', {
            templateUrl: 'views/medicaldetails.html',
            controller: 'MedicalDetailsCtrl'
       })
      .when('/InternalLogin/:base64EncodedData/:Page', {
          templateUrl: 'views/InternalLogin.html',
          controller: 'InternalLoginCtrl'
      })
      .when('/Details/PaymentDetails/:base64EncodedLeadID', {
          templateUrl: 'views/paymentdetails.html',
          controller: 'PaymentDetailsCtrl'
      });

      //.otherwise('/HealthExchange/Login', {
      //    templateUrl: 'views/login.html',
      //    controller: 'LoginCtrl'
      //});
  }]);