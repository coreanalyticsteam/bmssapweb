﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
using System.ServiceModel.Activation;
namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MasterService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MasterService.svc or MasterService.svc.cs at the Solution Explorer and start debugging.
    //[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class MasterService : IMasterService
    {
        public ResultClass GetTicketIssueMaster()
        {
            IMaster objMasterService = new Master();
            return objMasterService.GetTicketIssueMaster();
        }
        public ResultClass GetProducts()
        {
            IMaster objMasterService = new Master();
            return objMasterService.GetProducts();
        }
        public ResultClass SaveTicketIssueTypeMaster(TicketIssueTypeMaster Json)
        {
            IMaster objMasterService = new Master();
            return objMasterService.SaveTicketIssueTypeMaster(Json);
        }
        public ResultClass GetAllIssues()
        {
            IMaster objMasterService = new Master();
            return objMasterService.GetAllIssues();
        }
        public ResultClass GetTicketSubIssueMaster(String Json)
        {
            IMaster objMasterService = new Master();
            return objMasterService.GetTicketSubIssueMaster(Json);
        }
    }
}
