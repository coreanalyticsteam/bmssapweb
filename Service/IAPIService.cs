﻿using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAPIService" in both code and config file together.
    [ServiceContract]
    public interface IAPIService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ICICIPru")]
        [return: MessageParameter(Name = "result")]
        ResultClass ICICIPru(ICICIPRUMob Json);
    }
}
