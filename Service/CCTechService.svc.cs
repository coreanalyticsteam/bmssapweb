﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
using DataAccessLayer;
using Newtonsoft.Json;
using System.Security.Cryptography;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CCTechService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CCTechService.svc or CCTechService.svc.cs at the Solution Explorer and start debugging.
    public class CCTechService : ICCTechService
    {
        public bool UpdateHealthExStatusForExternal(string NewLeadID, string OldLeadID, string SelectionID, string Type, string UserID)
        {
            bool res = false;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "UpdateHealthExStatusForExternal" +
                   "\r\n NewLeadID=" + NewLeadID +
                   "\r\n OldLeadID=" + OldLeadID +
                   "\r\n SelectionID=" + SelectionID +
                    "\r\n Type=" + Type +
             "\r\n UserID=" + UserID);
            IHealthExBL bll = new HealthExBL();
            long _NewLeadID;
            long _OldLeadID;
            long _SelectionID;
            int _Type;
            int _UserID;
            if (long.TryParse(NewLeadID, out _NewLeadID) && long.TryParse(OldLeadID, out _OldLeadID) 
                && long.TryParse(SelectionID, out _SelectionID) && int.TryParse(Type, out _Type) && int.TryParse(UserID, out _UserID))
            {
                res = bll.UpdateHealthExStatus(_NewLeadID, _OldLeadID, _SelectionID, _Type, _UserID);
                oStringBuilder.Append("\r\n Result=" + res.ToString());
                common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "UpdateHealthExStatusForExternal.txt");
                return res;
            }
            else
            {
                return false;
            }
          
        }

        public bool MarkNewLeadForHealthEx(ReqMarkNewLead objReqMarkNewLead)
        {
            bool res = false;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "MarkNewLeadForHealthEx" +
                   "\r\n Request=" + JsonConvert.SerializeObject(objReqMarkNewLead));
            IHealthExBL bll = new HealthExBL();
            res = bll.MarkNewLeadForHealthEx(objReqMarkNewLead);
            oStringBuilder.Append("\r\n Result=" + res.ToString());
            common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "External.txt");
             return res;
        }

        public bool InsertSupplierPlanNewHEx(List<HealthExSelections> HealthExSelectionsList)
        {
            bool res = false;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "InsertSupplierPlanNewHEx" +
                   "\r\n Request=" + JsonConvert.SerializeObject(HealthExSelectionsList));

            IHealthExBL bll = new HealthExBL();
            res = bll.InsertSupplierPlanNewHEx(HealthExSelectionsList);
            oStringBuilder.Append("\r\n Result=" + res.ToString());
            common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "External.txt");
            return res;
        }

        public bool UpdatePaymentDetails(PaymentDetailsData objPaymentDetailsData)
        {
            bool res = false;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "UpdatePaymentDetails" +
                   "\r\n Request=" + JsonConvert.SerializeObject(objPaymentDetailsData));
            IHealthExBL bll = new HealthExBL();
            res = bll.UpdatePaymentDetails(objPaymentDetailsData);
            oStringBuilder.Append("\r\n Result=" + res.ToString());
            common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "External.txt");

            return res;
        }

        public List<MedicalDetails> CustomerMedicalDetails(string base64EncodedLeadID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.CustomerMedicalDetails(base64EncodedLeadID);
        }

        public List<PaymentDetailsForEx> GETPaymentDetailsForEx(string base64EncodedLeadID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GETPaymentDetailsForEx(base64EncodedLeadID);
        }

        public ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(string ExID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetPBInsurerUnderwritingDetails(Convert.ToInt64(ExID), 1);
        }

        public bool UpdatePremiumForSelection(UpdatePremiumReq objUpdatePremiumReq)
        {
            bool res = false;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "UpdatePremiumForSelection" +
                   "\r\n Request=" + JsonConvert.SerializeObject(objUpdatePremiumReq));

            IHealthExBL bll = new HealthExBL();
            res = bll.UpdatePremiumForSelection(objUpdatePremiumReq);
            oStringBuilder.Append("\r\n Result=" + res.ToString());
            common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "External.txt");    
            return res;
        }

        public ResCustomerDocUpload GetCustomerDocUploadDetails(string ExID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetCustomerDocUploadDetails(ExID);
        }
        public int AddCustomerDouments(PostMemberDocumentDetails objPostMemberDocumentDetails)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AddCustomerDouments(objPostMemberDocumentDetails);
        }
        public LeadDocumentDetails GetHExLeadMemberDocDetails(string NewLeadID, string SelectionID)
        {
            if (Convert.ToInt64(NewLeadID) > 0) {
                IHealthExBL bll = new HealthExBL();
                return bll.GetHExLeadMemberDocDetails(Convert.ToInt64(NewLeadID), Convert.ToInt64(SelectionID));
            }
            return new LeadDocumentDetails();
        }
        public ResPBInsurerUnderwriting InsurerUnderwritingDecisionDetails(string LeadId, string SelectionID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.InsurerUnderwritingDecisionDetails(Convert.ToInt64(LeadId), Convert.ToInt64(SelectionID));
        }
        public void LogInsurerView(string Log)
        {
        try
            {
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " " + Log, "InsurerViewLog.txt");
                
            }
            catch (Exception ex)
            {
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " " + ex.ToString(), "InsurerViewLog.txt");
                throw;
            }
        }

        public int AddInspectionDocuments(InspectionDocumentDetails objInspectionDocumentDetails)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AddInspectionDocuments(objInspectionDocumentDetails);
        }

        //public string EncryptString(string strdata, string key, int iteration)
        //{
        //    // TODO: Parameterize the Password, Salt, and Iterations.  They should be encrypted with the machine key and stored in the registry
        //    if (string.IsNullOrEmpty(strdata))
        //    {
        //        return strdata;
        //    }

        //    byte[] salt = { 0xA9, 0x9B, 0xC8, 0x32, 0x56, 0x35, 0xE3, 0x03 };

        //    PKCSKeyGenerator crypto = new PKCSKeyGenerator(key, salt, iteration, 1);

        //    ICryptoTransform cryptoTransform = crypto.Encryptor;
        //    byte[] cipherBytes = cryptoTransform.TransformFinalBlock(Encoding.UTF8.GetBytes(strdata), 0, strdata.Length);
        //    return Convert.ToBase64String(cipherBytes);
        //}
        //public string DecryptString(string strdata, string key, int iteration)
        //{
        //    if (string.IsNullOrEmpty(strdata))
        //    {
        //        return strdata;
        //    }

        //    byte[] salt = { 0xA9, 0x9B, 0xC8, 0x32, 0x56, 0x35, 0xE3, 0x03 };

        //    PKCSKeyGenerator crypto = new PKCSKeyGenerator(key, salt, iteration, 1);

        //    ICryptoTransform cryptoTransform = crypto.Decryptor;
        //    var strdataBytes = Convert.FromBase64String(strdata);
        //    var clearBytes = cryptoTransform.TransformFinalBlock(strdataBytes, 0, strdataBytes.Length);
        //    return Encoding.UTF8.GetString(clearBytes);
        //}
        
    }
}
