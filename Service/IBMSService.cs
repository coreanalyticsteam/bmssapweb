﻿using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBMSService" in both code and config file together.
    [ServiceContract]
    public interface IBMSService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "CommunicationHistory")]
        [return: MessageParameter(Name = "result")]
        ResultClass CommunicationHistory(Int64 BookingId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "CommunicationHistoryNew")]
        [return: MessageParameter(Name = "result")]
        ResultClass CommunicationHistoryNew(Int64 BookingId);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,  UriTemplate = "SaveCommonIVRResponse")]
        [return: MessageParameter(Name = "result")]
        ResultClass SaveCommonIVRResponse(IVRResponse Json);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertNotification")]
        [return: MessageParameter(Name = "result")]
        ResultClass InsertNotification();

        
    }
}
