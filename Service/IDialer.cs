﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDialer" in both code and config file together.
    [ServiceContract]
    public interface IDialer
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetDialerAgentList")]
        List<LeadAgentList> GetDialerAgentList(DialerSearchRequest objDialerSearchRequest);


        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetProductList/{AgentType}")]
        List<ProductDetails> GetProductList(string AgentType);


        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetGroupList/{AgentType}/{ProductID}")]
        List<GroupDetails> GetGroupList(string AgentType, string ProductID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Loggin")]        
        string Loggin(string Request);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "InsertCTCLog")]   
        string InsertCTCLog(LogReqDetails objLogReqDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "InsertCTCLogGetAPI/{LeadID}/{FromAgentID}/{ToAgentID}/{MobileNo}")]
        string InsertCTCLogGetAPI(string LeadID, string FromAgentID, string ToAgentID, string MobileNo);

    }
}
