﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
namespace Service
{
    [ServiceContract]
    public interface IEndorsementService
    {

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetEndorsementDetails/{LeadID}/{DetailsID}/{Type}")]
        EndorsementDetail GetEndorsementDetails(string LeadID, string DetailsID, string Type);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetEndorsementListDetails/{LeadID}")]
        EndorsementListDetails GetEndorsementListDetails(string LeadID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetFieldMasterDataList/{FieldID}")]
        List<FieldMasterData> GetFieldMasterDataList(string FieldID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetEndorsementDocList")]
        EndorsementDocList GetEndorsementDocList(EndorsementFieldUpdateReq EndorsementFieldUpdate);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,

            UriTemplate = "InsertEndorsementDocumentStatus")]
        int InsertEndorsementDocumentStatus(RequestEndorsementDocList objRequestEndorsementDocList);


        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "UpdateEndorsementFieldDetails")]
        string UpdateEndorsementFieldDetails(EndorsementFieldUpdateReq EndorsementFieldUpdate);



        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,            
            UriTemplate = "RaiseEndorsement")]
        string RaiseEndorsement(EndorsementDetail RequestEndorsementList);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,           
            UriTemplate = "AddNewEndorsementField")]
        EndorsementAddFieldResponse AddNewEndorsementField(EndorsementFieldAddRequest NewRequestEndorsement);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            //BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "UpdateDocumentDetails")]
        EndorsementAddFieldResponse UpdateDocumentDetails(ReqDocUploadRequest objReqDocUploadRequest);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            //BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "UpdateEndorsementCopyDetails")]
        string UpdateEndorsementCopyDetails(ReqDocUploadRequest objReqDocUploadRequest);


        
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "GetNewFieldDocs")]
        EndorsementAddFieldResponse GetNewFieldDocs(EndorsementFieldAddRequest NewRequestEndorsement);


        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,           
            UriTemplate = "UpdateEndorsementDetails")]
        string UpdateEndorsementDetails(EndorsementDetail objEndorsementDetail);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //    ResponseFormat = WebMessageFormat.Json,
        //    RequestFormat = WebMessageFormat.Json,
        //    UriTemplate = "UpdateEndorsementDetails")]
        //string UpdateEndorsementDetails(EndorsementDetail objEndorsementDetail);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetEndorsementStatusdetails/{DetailsID}")]
        List<EndorsementStatusdetailsResponse> GetEndorsementStatusdetails(string DetailsID);


        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            UriTemplate = "RollbackEndorsement")]
        List<EndorsementStatusdetailsResponse> RollbackEndorsement(EndorsementRollBackRequest objEndorsementRollBackRequest);

    }
}
