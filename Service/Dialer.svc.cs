﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Dialer" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Dialer.svc or Dialer.svc.cs at the Solution Explorer and start debugging.
    public class Dialer : IDialer
    {
        public List<LeadAgentList> GetDialerAgentList(DialerSearchRequest objDialerSearchRequest)
        {
            ICallTransfer bll = new CallTransfer();
            return bll.GetDialerAgentList(objDialerSearchRequest);
        }

        public List<ProductDetails> GetProductList(string AgentType)
        {
            ICallTransfer bll = new CallTransfer();
            return bll.GetProductList(Convert.ToInt16(AgentType));
        }

        public List<GroupDetails> GetGroupList(string AgentType, string ProductID)
        {
            ICallTransfer bll = new CallTransfer();
            return bll.GetGroupList(Convert.ToInt16(AgentType), Convert.ToInt16(ProductID));
        }

        public string Loggin(string Request)
        {
            ICallTransfer bll = new CallTransfer();
            return bll.Loggin(Request);
        }

        public string InsertCTCLog(LogReqDetails objLogReqDetails)
        {
            ICallTransfer bll = new CallTransfer();
            return bll.InsertCTCLog(objLogReqDetails);
        }

        public string InsertCTCLogGetAPI(string LeadID, string FromAgentID, string ToAgentID, string MobileNo)
        {
            //LogReqDetails objLogReqDetails = new LogReqDetails();
            //objLogReqDetails.LeadID = Convert.ToInt64(LeadID);
            //objLogReqDetails.FromAgentID = FromAgentID;
            //objLogReqDetails.ToAgentID = ToAgentID;
            //objLogReqDetails.MobileNo = MobileNo;
            //objLogReqDetails.EventType = 1;
            ICallTransfer bll = new CallTransfer();

            bll.Loggin("InsertCTCLogGetAPI" + LeadID + ":" + FromAgentID + ":" + ToAgentID + ":" + MobileNo);
            return "";
            //return bll.InsertCTCLog(objLogReqDetails);
        }
    }
}
