﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
using DataAccessLayer;
using Newtonsoft.Json;
namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "HealthExService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select HealthExService.svc or HealthExService.svc.cs at the Solution Explorer and start debugging.
    public class HealthExService : IHealthExService
    {
        public List<LeadDetails> GetAllExLead(ReqMarkExSearch objReqMarkExSearch)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetAllExLead(objReqMarkExSearch);
        }

        public int MarkLeadForHealthEx(ReqMarkHealthEx objReqMarkHealthEx)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.MarkLeadForHealthEx(objReqMarkHealthEx);
        }

        public int AddSupplierPlanForEx(PostPlanSelectionData objPostPlanSelectionData)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AddSupplierPlanForEx(objPostPlanSelectionData);
        }

        public int AddMemberDouments(PostMemberDocumentDetails objPostMemberDocumentDetails)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AddMemberDouments(objPostMemberDocumentDetails);
        }

        public List<MasterSupplier> GetSupplierList()
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetSupplierList();
        }


        public ResUploadCaseToInsurer GetExLeadDetailsForCaseSend(string ExID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetExLeadDetailsForCaseSend(Convert.ToInt64(ExID));
        }

        public List<LeadDetails> GetLeadsForPBDashboard(ReqMarkExSearch objReqMarkExSearch)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetLeadsForPBDashboard(objReqMarkExSearch);
        }

        public ResInsurerUnderwriting GetExLeadDetailsForInsurer(string ExID, string SelectionID, string InsurerID, string UserID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetExLeadDetailsForInsurer(Convert.ToInt64(ExID), Convert.ToInt64(SelectionID), Convert.ToInt16(InsurerID), Convert.ToInt16(UserID));
        }

        public int UpdateInsurerAction(PostInsurerActionData objPostInsurerActionData)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.UpdateInsurerAction(objPostInsurerActionData);
        }

        public ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(string ExID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetPBInsurerUnderwritingDetails(Convert.ToInt64(ExID));
        }

        public UserDetails Login(UserLoginRequest objUserLoginRequest)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.Login(objUserLoginRequest);

        }

        public List<LeadDetails> GetLeadsForInsurerDashboard(ReqMarkExSearch objReqMarkExSearch)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetLeadsForInsurerDashboard(objReqMarkExSearch);
        }

        public int RemoveFromHealthEx(ReqRemoveFromHealthEx objReqRemoveFromHealthEx)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.RemoveFromHealthEx(objReqRemoveFromHealthEx);
        }

        public bool UpdateHealthExStatus(string NewLeadID, string OldLeadID, string SelectionID, string Type, string UserID)
        {
            IHealthExBL bll = new HealthExBL();
            long _NewLeadID;
            long _OldLeadID;            
            int _SelectionID;
            int _Type;
            int _UserID;
            if (long.TryParse(NewLeadID, out _NewLeadID) && long.TryParse(OldLeadID, out _OldLeadID) && int.TryParse(SelectionID, out _SelectionID)
                && int.TryParse(Type, out _Type) && int.TryParse(UserID, out _UserID))
            {
                return bll.UpdateHealthExStatus(_NewLeadID, _OldLeadID, _SelectionID, _Type, _UserID);
            }
            else
            {
                return false;
            }
        }

        public bool UpdateHealthExStatusForExternal(string NewLeadID, string OldLeadID, string SelectionID, string Type, string UserID)
        {
            bool res = false;
            StringBuilder oStringBuilder = new StringBuilder();
            oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                   "\r\n Service=" + "UpdateHealthExStatusForExternal" +
                   "\r\n NewLeadID=" + NewLeadID +
                   "\r\n OldLeadID=" + OldLeadID +
                   "\r\n SelectionID=" + SelectionID +
                    "\r\n Type=" + Type +
             "\r\n UserID=" + UserID);
            IHealthExBL bll = new HealthExBL();
            long _NewLeadID;
            long _OldLeadID;
            long _SelectionID;
            int _Type;
            int _UserID;
            if (long.TryParse(NewLeadID, out _NewLeadID) && long.TryParse(OldLeadID, out _OldLeadID)
                && long.TryParse(SelectionID, out _SelectionID) && int.TryParse(Type, out _Type) && int.TryParse(UserID, out _UserID))
            {
                res = bll.UpdateHealthExStatus(_NewLeadID, _OldLeadID, _SelectionID, _Type, _UserID);
                oStringBuilder.Append("\r\n Result=" + res.ToString());
                common.ErrorNInfoLogMethod(oStringBuilder.ToString(), "UpdateHealthExStatusForExternal.txt");
                return res;
            }
            else
            {
                return false;
            }
        }

        public bool MarkNewLeadForHealthEx(ReqMarkNewLead objReqMarkNewLead)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.MarkNewLeadForHealthEx(objReqMarkNewLead);
        }

        public bool AcceptOrRejectInsurer(PostInsurerActionData objPostInsurerActionData)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AcceptOrRejectInsurer(objPostInsurerActionData);
        }

        public List<MedicalDetails> CustomerMedicalDetails(string base64EncodedLeadID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.CustomerMedicalDetails(base64EncodedLeadID);
        }

        public List<PaymentDetailsForEx> GETPaymentDetailsForEx(string base64EncodedLeadID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GETPaymentDetailsForEx(base64EncodedLeadID);
        }

        //public bool InsertSupplierPlanNewHEx(PostPlanSelectionData objPostPlanSelectionData)
        //{
        //    IHealthExBL bll = new HealthExBL();
        //    return bll.InsertSupplierPlanNewHEx(objPostPlanSelectionData);
        //}

        public LeadDetails ExLoginGetLeadData(string LeadID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.ExLoginGetLeadData(Convert.ToInt64(LeadID));
        }

        public int SendCommunicationSync(string leadId, string TriggerName, string MobileNo, string EmailID, string BookingType, string ProductID, string SelectionID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.SendCommunicationSync(Convert.ToInt64(leadId), TriggerName, MobileNo, EmailID, BookingType, Convert.ToInt16(ProductID), SelectionID);
        }

        public int InsertInsurerRemarks(InsurerRemarksDetails objInsurerRemarksDetails)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.InsertInsurerRemarks(objInsurerRemarksDetails);
        }

        public int AddNewUsers(AddNewUser objAddNewUser)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.AddNewUsers(objAddNewUser);
        }

        public int ChangePassword(ChangePassword objChangePassword)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.ChangePassword(objChangePassword);
        }

        public List<MasterSupplier> GetSupplierListByProduct(string ProductID)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetSupplierListByProduct(ProductID);
        }

        public List<AddNewUser> GetHealthExUser()
        {
            IHealthExBL bll = new HealthExBL();
            return bll.GetHealthExUser();
        }

        public int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo)
        {
            IHealthExBL bll = new HealthExBL();
            return bll.UpdateUserInfo(objUpdateUserInfo);
        }
    }

}
