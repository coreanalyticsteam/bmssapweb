﻿using BusinessLogicLayer;
using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Report" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Report.svc or Report.svc.cs at the Solution Explorer and start debugging.
    public class Report : IReport
    {
        public Byte[] PDFRawReportBMS(BMSReport Json)
        {
            //DateTime FromDate = Convert.ToDateTime("2018-01-01");
            //DateTime ToDate = Convert.ToDateTime("2018-01-05");
            //int ProductID = 7;
            try
            {
                BMSBL o = new BMSBL();
                return o.PDFRawReportBMS(Convert.ToDateTime(Json.FromDate), Convert.ToDateTime(Json.ToDate), Json.ProductID);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
