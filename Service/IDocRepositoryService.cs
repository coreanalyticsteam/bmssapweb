﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDocRepositoryService" in both code and config file together.
    [ServiceContract]
    public interface IDocRepositoryService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetMasterDropDownData/{FilterID}/{ProductId}/{Type}")]
        List<DropDownData> GetMasterDropDownData(string FilterID, string ProductId, string Type);


        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetMasterDocumentDetails")]
        DocumentDetails GetMasterDocumentDetails();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "InsertDocumentMapping")]
        int InsertDocumentMapping(MasterDocumentMappingRequest objMasterDocumentMappingRequest);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetCustomerDocumentDetails/{LeadID}")]
        CustomerDocDetails GetCustomerDocumentDetails(string LeadID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UploadCustomerDocuments")]
        int UploadCustomerDocuments(UploadDocReq objUploadDocReq);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetDocumentMasterDetails/{ProductID}/{SupplierID}")]
        List<DocumentMasterDetailsResp> GetDocumentMasterDetails(string ProductID, string SupplierID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetDocumentMasterDetailsEdit/{GroupID}")]
        GetDocumentEditResp GetDocumentMasterDetailsEdit(string GroupID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "InsertUpdateDocumentsForBooking")]
        DocumentDetailsResponse InsertUpdateDocumentsForBooking(DocumentDetailsRequest objDocumentDetailsRequest);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GETRequiredDocumentsForBooking/{BookingID}")]
        DocumentDetailsResponse GETRequiredDocumentsForBooking(string BookingID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateDocumentsForBooking")]
        DocumentDetailsResponse UpdateDocumentsForBooking(ExistingDocumentDetailsRequest objExistingDocumentDetailsRequest);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetDocumentAuditTrail/{BookingID}")]
        List<DocumentAuditTrailResponse> GetDocumentAuditTrail(string BookingID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateDocStatus")]
        DocumentDetailsResponse UpdateDocStatus(DocumentDetailsRequest objDocumentDetailsRequest);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetPendingDocuments/{leadId}/{applicationNumber}")]
        DocResponse GetPendingDocuments(string leadId, string applicationNumber);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateSingleDocStatus")]
        DocResponse UpdateSingleDocStatus(UpdateDocStatusReq objUpdateDocStatusReq);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "MergeFile")]
        string MergeFile(MergeFile objMergeFile);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "SubmitDoc")]
        DocResponse SubmitDoc(UpdateDocReq objUpdateDocReq);
    }
}
