﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHealthExService" in both code and config file together.
    [ServiceContract]
    public interface IHealthExService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "GetAllExLead")]
        List<LeadDetails> GetAllExLead(ReqMarkExSearch objReqMarkExSearch);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "MarkLeadForHealthEx")]
        int MarkLeadForHealthEx(ReqMarkHealthEx objReqMarkHealthEx);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AddSupplierPlanForEx")]
        int AddSupplierPlanForEx(PostPlanSelectionData objPostPlanSelectionData);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AddMemberDouments")]
        int AddMemberDouments(PostMemberDocumentDetails objPostMemberDocumentDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetSupplierList")]
        List<MasterSupplier> GetSupplierList();  

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetExLeadDetailsForCaseSend/{ExID}")]
        ResUploadCaseToInsurer GetExLeadDetailsForCaseSend(string ExID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, 
            BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "GetLeadsForPBDashboard")]
        List<LeadDetails> GetLeadsForPBDashboard(ReqMarkExSearch objReqMarkExSearch);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetExLeadDetailsForInsurer/{ExID}/{SelectionID}/{InsurerID}/{UserID}")]
        ResInsurerUnderwriting GetExLeadDetailsForInsurer(string ExID, string SelectionID, string InsurerID, string UserID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateInsurerAction")]
        int UpdateInsurerAction(PostInsurerActionData objPostInsurerActionData);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetPBInsurerUnderwritingDetails/{ExID}")]
        ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(string ExID);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "Login")]
        UserDetails Login(UserLoginRequest objUserLoginRequest);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "GetLeadsForInsurerDashboard")]
        List<LeadDetails> GetLeadsForInsurerDashboard(ReqMarkExSearch objReqMarkExSearch);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "RemoveFromHealthEx")]
        int RemoveFromHealthEx(ReqRemoveFromHealthEx objReqRemoveFromHealthEx);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateHealthExStatus/{NewLeadID}/{OldLeadID}/{SelectionID}/{Type}/{UserID}")]
        bool UpdateHealthExStatus(string NewLeadID, string OldLeadID, string SelectionID, string Type, string UserID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateHealthExStatusForExternal/{NewLeadID}/{OldLeadID}/{SelectionID}/{Type}/{UserID}")]
        bool UpdateHealthExStatusForExternal(string NewLeadID, string OldLeadID, string SelectionID, string Type, string UserID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,UriTemplate = "MarkNewLeadForHealthEx")]
        bool MarkNewLeadForHealthEx(ReqMarkNewLead objReqMarkNewLead);

        //[OperationContract]
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
        //    BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "InsertSupplierPlanNewHEx")]
        //bool InsertSupplierPlanNewHEx(PostPlanSelectionData objPostPlanSelectionData);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AcceptOrRejectInsurer")]
        bool AcceptOrRejectInsurer(PostInsurerActionData objPostInsurerActionData);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "CustomerMedicalDetails/{base64EncodedLeadID}")]
        List<MedicalDetails> CustomerMedicalDetails(string base64EncodedLeadID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GETPaymentDetailsForEx/{base64EncodedLeadID}")]
        List<PaymentDetailsForEx> GETPaymentDetailsForEx(string base64EncodedLeadID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ExLoginGetLeadData/{LeadID}")]
        LeadDetails ExLoginGetLeadData(string LeadID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "SendCommunicationSync/{leadId}/{TriggerName}/{MobileNo}/{EmailID}/{BookingType}/{ProductID}/{SelectionID}")]
        int SendCommunicationSync(string leadId, string TriggerName, string MobileNo, string EmailID, string BookingType, string ProductID, string SelectionID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "InsertInsurerRemarks")]
        int InsertInsurerRemarks(InsurerRemarksDetails objInsurerRemarksDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AddNewUser")]
        int AddNewUsers(AddNewUser objAddNewUser);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ChangePassword")]
        int ChangePassword(ChangePassword objChangePassword);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "MasterSupplier/{ProductID}")]
        List<MasterSupplier> GetSupplierListByProduct(string ProductID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetHealthExUser")]
        List<AddNewUser> GetHealthExUser();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateUserInfo")]
        int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo);
    }
}
