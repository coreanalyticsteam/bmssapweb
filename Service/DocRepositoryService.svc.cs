﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DocRepositoryService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DocRepositoryService.svc or DocRepositoryService.svc.cs at the Solution Explorer and start debugging.
    public class DocRepositoryService : IDocRepositoryService
    {
        public List<DropDownData> GetMasterDropDownData(string FilterID, string ProductId, string Type)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GetMasterDropDownData(Convert.ToInt16(FilterID), Convert.ToInt16(ProductId), Convert.ToInt16(Type));
        }

        public DocumentDetails GetMasterDocumentDetails()
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GetMasterDocumentDetails();
        }

        public int InsertDocumentMapping(MasterDocumentMappingRequest objMasterDocumentMappingRequest)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.InsertDocumentMapping(objMasterDocumentMappingRequest);
        }

        public CustomerDocDetails GetCustomerDocumentDetails(string LeadID)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GetCustomerDocumentDetails(LeadID);
        }

        public int UploadCustomerDocuments(UploadDocReq objUploadDocReq)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.UploadCustomerDocuments(objUploadDocReq);
        }

        public List<DocumentMasterDetailsResp> GetDocumentMasterDetails(string ProductID, string SupplierID)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GetDocumentMasterDetails(Convert.ToInt16(ProductID), Convert.ToInt16(SupplierID));
        }

        public GetDocumentEditResp GetDocumentMasterDetailsEdit(string GroupID)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GetDocumentMasterDetailsEdit(GroupID);
        }

        public DocumentDetailsResponse InsertUpdateDocumentsForBooking(DocumentDetailsRequest objDocumentDetailsRequest)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.InsertUpdateDocumentsForBooking(objDocumentDetailsRequest);
        }

        public DocumentDetailsResponse GETRequiredDocumentsForBooking(string BookingID)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GETRequiredDocumentsForBooking(Convert.ToInt64(BookingID));
        }

        public DocumentDetailsResponse UpdateDocumentsForBooking(ExistingDocumentDetailsRequest objExistingDocumentDetailsRequest)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.UpdateDocumentsForBooking(objExistingDocumentDetailsRequest);
        }

        public List<DocumentAuditTrailResponse> GetDocumentAuditTrail(string BookingID)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GetDocumentAuditTrail(Convert.ToInt64(BookingID));
        }


        public DocumentDetailsResponse UpdateDocStatus(DocumentDetailsRequest objDocumentDetailsRequest)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.UpdateDocStatus(objDocumentDetailsRequest);
        }

        public DocResponse GetPendingDocuments(string leadId, string applicationNumber)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.GetPendingDocuments(leadId, applicationNumber);
        }

        public DocResponse UpdateSingleDocStatus(UpdateDocStatusReq objUpdateDocStatusReq)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.UpdateSingleDocStatus(objUpdateDocStatusReq);
        }

        public string MergeFile(MergeFile objMergeFile)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.MergeFile(objMergeFile);
        }

        public DocResponse SubmitDoc(UpdateDocReq objUpdateDocReq)
        {
            IDocumentRepository bll = new DocumentRepository();
            return bll.SubmitDoc(objUpdateDocReq);
        }
    }
}
