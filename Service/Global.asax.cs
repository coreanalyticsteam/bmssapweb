﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.ServiceModel.Activation;
using System.Web.Routing;
using System.ServiceModel.Web;
using System.Net;
using System.Runtime.Caching;
using System.Collections;
using BusinessLogicLayer;
namespace Service
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //RouteTable.Routes.Add(new ServiceRoute("", new WebServiceHostFactory(), typeof(EndorsementService)));
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            EnableCrossDmainAjaxCall();
            if (HttpContext.Current.Request.CurrentExecutionFilePath == "/HealthExService.svc")
            {
                string URL = HttpContext.Current.Request.PathInfo.ToString();

                if ("/Login" != URL && !URL.Contains("/UpdateHealthExStatusForExternal/"))
                {
                    //var aa = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
                    string token = HttpContext.Current.Request.Headers["auth-token"];
                    //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //HttpContext.Current.Response.Cache.SetNoStore();
                    ////Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    //ObjectCache USERTOKEN = MemoryCache.Default;

                    //Hashtable hashtable;
                    //hashtable = (Hashtable)USERTOKEN.Get("USERTOKEN");
                    if (token != null)
                    {
                        string[] tokenArr = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(token)).Split('~');
                        IHealthExBL bll = new HealthExBL();
                        if (bll.RequestAuthenticate(tokenArr[0], tokenArr[1]))
                        {

                        }
                        else
                        {
                            Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        }
                    }
                    else
                    {
                        Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                    }
                }
                else if ("/UpdateHealthExStatusForExternal" == URL)
                {
                    

                }
            }
        }

        private void EnableCrossDmainAjaxCall()
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            //HttpContext.Current.Response.AddHeader("auth-token", "");
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept,auth-token,UserID,Token");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}