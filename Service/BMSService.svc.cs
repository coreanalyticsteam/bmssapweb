﻿using PropertyLayers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BusinessLogicLayer;
using System.Configuration;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BMSService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BMSService.svc or BMSService.svc.cs at the Solution Explorer and start debugging.
    public class BMSService : IBMSService
    {
        public ResultClass CommunicationHistory(Int64 BookingId)
        {
            BMSBL d = new BMSBL();
            return d.CommunicationHistory(BookingId);
        }

        public ResultClass CommunicationHistoryNew(Int64 BookingId)
        {
            BMSBL d = new BMSBL();
            return d.CommunicationHistoryNew(BookingId);
        }

        public ResultClass SaveCommonIVRResponse(IVRResponse Json)
        {
            Log.WriteLog("SaveCommonIVRResponse: " + Newtonsoft.Json.JsonConvert.SerializeObject(Json));
            BMSBL o = new BMSBL();
            string APICreateTicket = ConfigurationManager.AppSettings["APICreateTicket"];
            return o.SaveCommonIVRResponse(Json.LeadId, Json.Response, Json.AppIDSource, APICreateTicket);
        }

        public ResultClass GetNotification(int AgentId)
        {
            BMSBL d = new BMSBL();
            return d.GetNotification(AgentId);
        }

        public ResultClass InsertNotification()
        {
            BMSBL oBL = new BMSBL();
            return oBL.InsertNotification();
        }
       
    }
}
