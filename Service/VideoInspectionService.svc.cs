﻿using System;
using System.Collections.Generic;
using System.IO;
using BusinessLogicLayer;
using DataAccessLayer;
using Newtonsoft.Json;
using PropertyLayers;
using System.ServiceModel.Web;
using System.Data;
//using ClosedXML.Excel;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "VideoInspection" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select VideoInspection.svc or VideoInspection.svc.cs at the Solution Explorer and start debugging.
    public class VideoInspectionService : IVideoInspectionService
    {
        /// <summary>
        /// Upload MediaFiles
        /// </summary>
        /// <param name="objMediaFileInfo"></param>
        /// <returns></returns>
        public bool UploadMediaFile(MediaFileInfo objMediaFileInfo)
        {
            try
            {
                common.ErrorNInfoLogMethod(JsonConvert.SerializeObject(objMediaFileInfo), "InspectionServiceRequest.txt");
                //byte[] imageData = null;
                //using (var wc = new System.Net.WebClient())
                //    imageData = wc.DownloadData(objMediaFileInfo.Fileinfo.FileUrl);

                //objMediaFileInfo.Fileinfo.MediaData = new MemoryStream(imageData);
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.UpdateMediaFileDetails(objMediaFileInfo);
            }
            catch (Exception ex)
            {
                common.ErrorNInfoLogMethod(ex.Message, "InspectionService.txt");
                throw;
            }
        }

        /// <summary>
        /// Get Inspection Booking Details
        /// </summary>
        /// <param name="inspectionId"></param>
        /// <returns></returns>
        public List<InspectionBookingDetails> InspectionBookingDetail(string inspectionId)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.GetInspectionBookingDetails(inspectionId);
            }
            catch (Exception)
            {               
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDetails"></param>
        /// <returns></returns>
        public List<InspectionBookingDetails> InspectionBookingDetails(InspectionBookingDetails objDetails)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.InspectionDetailsList(objDetails);
            }
            catch (Exception)
            {               
                throw;
            }
        }

        /// <summary>
        /// Get Status
        /// </summary>
        /// <returns></returns>
        public List<StatusCollection> GetStatusCollection(string userType)
        {
            try
            {
                IVideoInspection objInspection = new VideoInspection();
                return objInspection.GetStatusCollection(userType);
            }
            catch (Exception)
            {                   
                throw;
            }
        }

        public List<InspectionBookingDetails> InsurerInspectionDetail(InspectionBookingDetails objDetails)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.GetInsurerInspectionDetails(objDetails);
            }
            catch (Exception)
            {               
                throw;
            }
        }

        public List<MediaFileInfo> MediaFileUrl(string inspectionId)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.MediaFileUrl(inspectionId);
            }
            catch (Exception)
            {               
                throw;
            }
        }

        public List<InspectionDocument> GetInspectionsDocument(string inspectionId)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.GetInspectionsDocument(inspectionId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update Status, SubStatus, Remarks
        /// </summary>
        /// <param name="objDetails"></param>
        /// <returns></returns>
        public bool UpdateStatus(InspectionBookingDetails objDetails)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.UpdateStatus(objDetails);
            }
            catch (Exception)
            {
                
                throw;
            }
            return false;
        }


        public List<InspectionBookingDetails> InsurerInspectionHistory(string inspectionId)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.InsurerInspectionHistory(inspectionId);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool UpdatePbInspectionStatus(InspectionBookingDetails objDetails)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.UpdatePbInspectionStatus(objDetails);
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public bool InspectionRollback(InspectionRollbackDetails objRollbackDetails)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.InspectionRollback(objRollbackDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InspectionBookingDetails> PbInspectionHistory(string inspectionId)
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.PbInspectionHistory(inspectionId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<InsurerCollection> GetInsurerCollection()
        {
            try
            {
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.GetInsurerCollection();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int AddNewUsers(AddNewUser objAddNewUser)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.AddNewUsers(objAddNewUser);
        }

        public int ChangePassword(ChangePassword objChangePassword)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.ChangePassword(objChangePassword);
        }

        public List<MasterSupplier> GetSupplierListByProduct(string ProductID)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetSupplierListByProduct(ProductID);
        }

        public List<AddNewUser> GetHealthExUser()
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetHealthExUser();
        }

        public int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.UpdateUserInfo(objUpdateUserInfo);
        
        }

        public Byte[] PDFInspectionBookingDetails(InspectionBookingDetails objDetails)
        {
            try
            {
                //Log.WriteLog("Inside PDFInspectionBookingDetails Video inspection");
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + "  Inside PDFInspectionBookingDetails Video inspection ", "GenralLog.txt");
                IVideoInspection objVideoInspection = new VideoInspection();
                return objVideoInspection.PDFInspectionDetailsList(objDetails);
            }
            catch (Exception ex)
            {
                //Log.WriteLog(ex.ToString());
                common.ErrorNInfoLogMethod(System.DateTime.Now.ToString() + " " + ex.ToString(), "GenralLog.txt");
                throw;
            }
        }
        public ResultClass GetApplicationAccess(int UserID)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetApplicationAccess(UserID);
        }

        public ResultClass GetProducts()
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetProducts();
        }

        public ResultClass GetSuppliers(int ProductId)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetSuppliers(ProductId);
        }
        public ResultClass GetExUser()
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetExUser();
        }
        public ResultClass GetExUserTicket()
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetExUserTicket();
        }
        public ResultClass GetUserDetails(int UserID)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetUserDetails(UserID);
        }
        public ResultClass SaveUserDetails(UserDetail Json)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.SaveUserDetails(Json);

        }
        public ResultClass GetUserDetailsTicket(int UserID)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.GetUserDetailsTicket(UserID);
        }
        public ResultClass SaveUserDetailsTicket(UserDetail Json)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.SaveUserDetailsTicket(Json);

        }
        public ResultClass ReplicateInspection(string InspectionID)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.ReplicateInspection(InspectionID);
        }

        public ResultClass UpdateVehicleNo(string VehicleNumber, string InspectionID)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.UpdateVehicleNo(VehicleNumber, InspectionID);
        }

        public ResultClass DeleteInspectionDocument(string DocURL, string InspectionId)
        {
            IVideoInspection objVideoInspection = new VideoInspection();
            return objVideoInspection.DeleteInspectionDocument(DocURL,InspectionId);
        }
        //public Stream DownloadFile2()
        //{

        //    DataTable dt = new DataTable();

        //    dt.Columns.Add("ID", typeof(int));
        //    dt.Columns.Add("FirstName", typeof(string));
        //    dt.Columns.Add("LastName", typeof(string));
        //    dt.Rows.Add(new object[] { 16, "Aman", "Rossi" });
        //    dt.Rows.Add(new object[] { 17, "Arya", "Rossi" });
        //    dt.Rows.Add(new object[] { 100, "John", "Doe" });
        //    dt.Rows.Add(new object[] { 101, "Fred", "Nurk" });
        //    dt.Rows.Add(new object[] { 103, "Hans", "Meier" });
        //    dt.Rows.Add(new object[] { 104, "Ivan", "Horvat" });
        //    dt.Rows.Add(new object[] { 105, "Jean", "Dupont" });
        //    dt.Rows.Add(new object[] { 106, "Mario", "Rossi" });


        //    XLWorkbook wb = new XLWorkbook();

        //    wb.Worksheets.Add(dt, "WorksheetName");

        //    //string downloadFilePath = Path.Combine(HostingEnvironment.MapPath("~/Files"), "NewSheet.xlsx");

        //    // wb.SaveAs(downloadFilePath);


        //    //System.IO.FileInfo fileInfo = new System.IO.FileInfo(downloadFilePath);


        //    //if (!fileInfo.Exists)
        //    //    throw new System.IO.FileNotFoundException("File not found",
        //    //                                              "NewSheet.xlsx");
        //    //String headerInfo = "attachment; filename=NewSheet2.xlsx";
        //    //WebOperationContext.Current.OutgoingResponse.Headers["Content-Disposition"] = headerInfo;

        //    WebOperationContext.Current.OutgoingResponse.ContentType = "application/octet-stream";

        //    //System.IO.FileStream stream = new System.IO.FileStream(downloadFilePath,
        //    //          System.IO.FileMode.Open, System.IO.FileAccess.Read);
        //    Stream fs = new MemoryStream();
        //    wb.SaveAs(fs);
        //    fs.Position = 0;
        //    return fs;
        //}

        //public Byte[] DownloadFile()
        //{

        //    DataTable dt = new DataTable();

        //    dt.Columns.Add("ID", typeof(int));
        //    dt.Columns.Add("FirstName", typeof(string));
        //    dt.Columns.Add("LastName", typeof(string));
        //    dt.Columns.Add("Date", typeof(DateTime));
        //    dt.Rows.Add(new object[] { 16, "Aman", "Rossi",DateTime.Now });
        //    dt.Rows.Add(new object[] { 17, "Arya", "Rossi", DateTime.Now });
        //    dt.Rows.Add(new object[] { 100, "John", "Doe", DateTime.Now });
        //    dt.Rows.Add(new object[] { 101, "Fred", "Nurk", DateTime.Now });
        //    dt.Rows.Add(new object[] { 103, "Hans", "Meier", DateTime.Now });
        //    dt.Rows.Add(new object[] { 104, "Ivan", "Horvat", DateTime.Now });
        //    dt.Rows.Add(new object[] { 105, "Jean", "Dupont", DateTime.Now });
        //    dt.Rows.Add(new object[] { 106, "Mario", "Rossi", DateTime.Now });


        //    XLWorkbook wb = new XLWorkbook();

        //    wb.Worksheets.Add(dt, "WorksheetName");

        //    MemoryStream fs = new MemoryStream();
        //    wb.SaveAs(fs);
        //    fs.Position = 0;
           
        //    return fs.ToArray();
        //}
    }
}
