﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Service
{
    public class Log
    {
        private static readonly bool IsLogEnabled = true;// Convert.ToBoolean((string)ConfigurationManager.AppSettings["EnableLog"]);

        private static readonly string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);


        public static bool WriteLog(string logMessage, string fileName = "GeneralLog")
        {
            if (!IsLogEnabled || string.IsNullOrEmpty(logMessage)) return false;
            try
            {
                var dt = DateTime.Now;
                var date = "_" + dt.Day + "_" + dt.Month + "_" + dt.Year;
                if (!Directory.Exists(path + @"Logs"))
                {
                    Directory.CreateDirectory(path + @"Logs");
                }
                using (TextWriter tw = File.AppendText(Path.Combine(path, @"Logs\\" + fileName + date + ".txt")))
                {
                    tw.WriteLine("Message:{0}{1}TimeStamp:{2}{3}",
                        logMessage,
                        Environment.NewLine,
                        DateTime.Now.ToString(),
                        Environment.NewLine);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}