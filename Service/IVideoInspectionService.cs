﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropertyLayers;
using System.IO;
using System;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVideoInspection" in both code and config file together.
    [ServiceContract]
    public interface IVideoInspectionService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadMediaFile")]
        bool UploadMediaFile(MediaFileInfo objMediaFileInfo);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InspectionBookingDetail/{inspectionId}")]
        List<InspectionBookingDetails> InspectionBookingDetail(string inspectionId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InspectionBookingDetails")]
        List<InspectionBookingDetails> InspectionBookingDetails(InspectionBookingDetails objDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetStatusCollection/{userType}")]
        List<StatusCollection> GetStatusCollection(string userType);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsurerInspectionDetail")]
        List<InspectionBookingDetails> InsurerInspectionDetail(InspectionBookingDetails objDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "MediaFileUrl/{inspectionId}")]
        List<MediaFileInfo> MediaFileUrl(string inspectionId);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetInspectionsDocument/{inspectionId}")]
        List<InspectionDocument> GetInspectionsDocument(string inspectionId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateStatus")]
        bool UpdateStatus(InspectionBookingDetails objDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsurerInspectionHistory/{inspectionId}")]
        List<InspectionBookingDetails> InsurerInspectionHistory(string inspectionId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdatePbInspectionStatus")]
        bool UpdatePbInspectionStatus(InspectionBookingDetails objDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InspectionRollback")]
        bool InspectionRollback(InspectionRollbackDetails objRollbackDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "PbInspectionHistory/{inspectionId}")]
        List<InspectionBookingDetails> PbInspectionHistory(string inspectionId);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetInsurerCollection")]
        List<InsurerCollection> GetInsurerCollection();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AddNewUser")]
        int AddNewUsers(AddNewUser objAddNewUser);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "ChangePassword")]
        int ChangePassword(ChangePassword objChangePassword);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "MasterSupplier/{ProductID}")]
        List<MasterSupplier> GetSupplierListByProduct(string ProductID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetHealthExUser")]
        List<AddNewUser> GetHealthExUser();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateUserInfo")]
        int UpdateUserInfo(UpdateUserInfo objUpdateUserInfo);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,  UriTemplate = "PDFInspectionBookingDetails")]
        Byte[] PDFInspectionBookingDetails(InspectionBookingDetails objDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetApplicationAccess")]
        ResultClass GetApplicationAccess(int UserID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetProducts")]
        ResultClass GetProducts();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "GetSuppliers")]
        [return: MessageParameter(Name = "result")]
        ResultClass GetSuppliers(int ProductId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetExUser")]
        ResultClass GetExUser();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetExUserTicket")]
        ResultClass GetExUserTicket();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "GetUserDetails")]
        [return: MessageParameter(Name = "result")]
        ResultClass GetUserDetails(int UserID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "SaveUserDetails")]
        [return: MessageParameter(Name = "result")]
        ResultClass SaveUserDetails(UserDetail Json);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "GetUserDetailsTicket")]
        [return: MessageParameter(Name = "result")]
        ResultClass GetUserDetailsTicket(int UserID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "SaveUserDetailsTicket")]
        [return: MessageParameter(Name = "result")]
        ResultClass SaveUserDetailsTicket(UserDetail Json);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ReplicateInspection")]
        [return: MessageParameter(Name = "result")]
        ResultClass ReplicateInspection(string InspectionID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "UpdateVehicleNo")]
        [return: MessageParameter(Name = "result")]
        ResultClass UpdateVehicleNo(string VehicleNumber, string InspectionID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "DeleteInspectionDocument")]
        [return: MessageParameter(Name = "result")]
        ResultClass DeleteInspectionDocument(string DocURL, string InspectionId);


        //[OperationContract]
        //[WebGet(UriTemplate = "Download2")]
        //Stream DownloadFile2();

        //[OperationContract]
        //[WebGet(UriTemplate = "Download1")]
        //Byte[] DownloadFile();
    }
}
