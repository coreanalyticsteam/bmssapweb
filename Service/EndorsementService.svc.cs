﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;
namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class EndorsementService : IEndorsementService
    {
        public EndorsementListDetails GetEndorsementListDetails(string LeadID)
        {
            EndorsementListDetails objEndorsementDetail = new EndorsementListDetails();
            IEndorsement bll = new Endorsement();
            objEndorsementDetail = bll.GetEndorsementListDetails(Convert.ToInt64(LeadID));
            return objEndorsementDetail;
        }

        public EndorsementDetail GetEndorsementDetails(string LeadID, string DetailsID, string Type)
        {
            EndorsementDetail objEndorsementDetail = new EndorsementDetail();
            IEndorsement bll = new Endorsement();
            objEndorsementDetail = bll.GetEndorsementDetails(Convert.ToInt64(LeadID), Convert.ToInt64(DetailsID), Convert.ToInt16(Type));
            return objEndorsementDetail;
        }
        public EndorsementAddFieldResponse UpdateDocumentDetails(ReqDocUploadRequest objReqDocUploadRequest)
        {
            EndorsementDetail objEndorsementDetail = new EndorsementDetail();
            IEndorsement bll = new Endorsement();
            return bll.UpdateDocumentDetails(objReqDocUploadRequest);
        }

        public string UpdateEndorsementCopyDetails(ReqDocUploadRequest objReqDocUploadRequest)
        {
            EndorsementDetail objEndorsementDetail = new EndorsementDetail();
            IEndorsement bll = new Endorsement();
            return bll.UpdateEndorsementCopyDetails(objReqDocUploadRequest);
        }


        public string RaiseEndorsement(EndorsementDetail RequestEndorsementList)
        {
            IEndorsement bll = new Endorsement();
            string res = bll.InsertCustomerEndorsement(RequestEndorsementList);
            return res;
        }

        public EndorsementAddFieldResponse AddNewEndorsementField(EndorsementFieldAddRequest NewRequestEndorsement)
        {
            IEndorsement bll = new Endorsement();

            return bll.AddNewEndorsementField(NewRequestEndorsement);
        }

        public string UpdateEndorsementFieldDetails(EndorsementFieldUpdateReq EndorsementFieldUpdate)
        {
            IEndorsement bll = new Endorsement();

            return bll.UpdateEndorsementFieldDetails(EndorsementFieldUpdate);
        }

        public EndorsementAddFieldResponse GetNewFieldDocs(EndorsementFieldAddRequest NewRequestEndorsement)
        {
            IEndorsement bll = new Endorsement();

            return bll.GetNewFieldDocs(NewRequestEndorsement);
        }


        public string UpdateEndorsementDetails(EndorsementDetail objEndorsementDetail)
        {
            IEndorsement bll = new Endorsement();
            string res = bll.UpdateEndorsementDetails(objEndorsementDetail);
            return res;
        }

        public int InsertEndorsementDocumentStatus(RequestEndorsementDocList objRequestEndorsementDocList)
        {
            IEndorsement bll = new Endorsement();
            return bll.InsertEndorsementDocumentStatus(objRequestEndorsementDocList);
        }

        public EndorsementDocList GetEndorsementDocList(EndorsementFieldUpdateReq EndorsementFieldUpdate)
        {
            IEndorsement bll = new Endorsement();
            return bll.GetEndorsementDocList(EndorsementFieldUpdate);
        }
        public List<FieldMasterData> GetFieldMasterDataList(string FieldID)
        {
            IEndorsement bll = new Endorsement();
            return bll.GetFieldMasterDataList(Convert.ToInt16(FieldID));
        }

        public List<EndorsementStatusdetailsResponse> GetEndorsementStatusdetails(string DetailsID)
        {
            IEndorsement bll = new Endorsement();
            return bll.GetEndorsementStatusdetails(Convert.ToInt64(DetailsID));
        }

        public List<EndorsementStatusdetailsResponse> RollbackEndorsement(EndorsementRollBackRequest objEndorsementRollBackRequest)
        {
            IEndorsement bll = new Endorsement();
            return bll.RollbackEndorsement(objEndorsementRollBackRequest);
        }

     
    }
}
