﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PropertyLayers;
using BusinessLogicLayer;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICCTechService" in both code and config file together.
    [ServiceContract]
    public interface ICCTechService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdateHealthExStatusForExternal/{NewLeadID}/{OldLeadID}/{SelectionID}/{Type}/{UserID}")]
        bool UpdateHealthExStatusForExternal(string NewLeadID, string OldLeadID, string SelectionID, string Type, string UserID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "MarkNewLeadForHealthEx")]
        bool MarkNewLeadForHealthEx(ReqMarkNewLead objReqMarkNewLead);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "InsertSupplierPlanNewHEx")]
        bool InsertSupplierPlanNewHEx(List<HealthExSelections> HealthExSelectionsList);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdatePaymentDetails")]
        bool UpdatePaymentDetails(PaymentDetailsData objPaymentDetailsData);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "CustomerMedicalDetails/{base64EncodedLeadID}")]
        List<MedicalDetails> CustomerMedicalDetails(string base64EncodedLeadID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GETPaymentDetailsForEx/{base64EncodedLeadID}")]
        List<PaymentDetailsForEx> GETPaymentDetailsForEx(string base64EncodedLeadID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetPBInsurerUnderwritingDetails/{ExID}")]
        ResPBInsurerUnderwriting GetPBInsurerUnderwritingDetails(string ExID);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "UpdatePremiumForSelection")]
        bool UpdatePremiumForSelection(UpdatePremiumReq objUpdatePremiumReq);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "GetCustomerDocUploadDetails/{ExID}")]
        ResCustomerDocUpload GetCustomerDocUploadDetails(string ExID);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "AddCustomerDouments")]
        int AddCustomerDouments(PostMemberDocumentDetails objPostMemberDocumentDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetHExLeadMemberDocDetails/{NewLeadID}/{SelectionID}")]
        LeadDocumentDetails GetHExLeadMemberDocDetails(string NewLeadID, string SelectionID);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "InsurerUnderwritingDecisionDetails/{LeadId}/{SelectionID}")]
        ResPBInsurerUnderwriting InsurerUnderwritingDecisionDetails(string LeadId, string SelectionID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "LogInsurerView")]
        void LogInsurerView(string Log);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
           UriTemplate = "AddInspectionDocuments")]
        int AddInspectionDocuments(InspectionDocumentDetails objInspectionDocumentDetails);
    }
}
