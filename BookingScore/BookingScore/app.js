

angular.module("HealthExchange", ["ngRoute", "ui.bootstrap"]).config(["$routeProvider",
  function ($routeProvider) {
      $routeProvider.
        when('/HealthExchange/PB/MarkHealthEx', {
            templateUrl: 'views/MarkHealthEx.html',
            controller: 'HealthExchangeCtrl'
        })

      .when('/HealthExchange/PB/UploadCaseToInsurer', {
          templateUrl: 'views/UploadCaseToInsurer.html',
          controller: 'UploadCaseToInsurerCtrl'
      })
       .when('/HealthExchange/PB/PBDashboard', {
           templateUrl: 'views/PBDashboard.html',
           controller: 'PBDashboardCtrl'
           //,
           //views: {
           //    'header': {
           //        templateUrl: 'views/PBHeader.html'
           //        //,                   controller: 'PBDashboardCtrl'
           //    }
           //}
       })
       .when('/HealthExchange/Insurer/InsurerUnderwriting', {
           templateUrl: 'views/InsurerUnderwriting.html',
           controller: 'InsurerUnderwritingCtrl'
       })
       .when('/HealthExchange/PB/InsurerUnderwritingResponse', {
           templateUrl: 'views/InsurerUnderwritingResponse.html',
           controller: 'InsurerUnderwritingResponseCtrl'
       })
      .when('/HealthExchange/Insurer/InsurerDashboard', {
          templateUrl: 'views/InsurerDashboard.html',
          controller: 'InsurerDashboardCtrl'
      })
      .when('/HealthExchange/Login', {
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl'
      })
      .when('/HealthExchange/Admin/AddNewUser', {
          templateUrl: 'views/AddNewUser/addnewuser.html',
          controller: 'UserMgmtCtrl'
      })
      .when('/HealthExchange/Admin/UserDetails', {
          templateUrl: 'views/UserDetails.html',
          controller: 'UserDetailsController'
      })

      .when('/HealthExchange/Admin/UpdateUserInfo', {
          templateUrl: 'views/UpdateUserInfo.html',
          controller: 'UpdateUserInfoCtrl'
      })

      .otherwise('/HealthExchange/Login', {
          templateUrl: 'views/login.html',
          controller: 'LoginCtrl'
      });
  }]);


