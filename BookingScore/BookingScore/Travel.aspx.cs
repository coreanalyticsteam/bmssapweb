﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BookingScore
{
    public partial class Travel : System.Web.UI.Page
    {
        static string connectionString = string.Empty;
        static string connectionStringLive = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            var x = Session["LoginData"];

            if (x == null)
            {
                Response.Redirect("Login.aspx");
            }

            else
            {
                connectionString = ConfigurationManager.ConnectionStrings["ConnectionName"].ConnectionString;
                connectionStringLive = ConfigurationManager.ConnectionStrings["ConnectionLive"].ConnectionString;

                lbldefault.Visible = false;
                lblhealth.Visible = false;

                DataSet ds = new DataSet();
                SqlDataAdapter _adp;
                BookingScore.Login.UserData data = (BookingScore.Login.UserData)Session["LoginData"];
                //  data =Session["LoginData"] as LoginData;
                using (SqlConnection sqlcon = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[BMS].[GetMenuRights]", sqlcon))
                    {
                        cmd.Parameters.AddWithValue("@UserID", data.UserID);
                        cmd.CommandType = CommandType.StoredProcedure;

                        _adp = new SqlDataAdapter(cmd);
                        _adp.Fill(ds);
                        List<BookingScore.Score.MenuList> ListML = new List<BookingScore.Score.MenuList>();
                        foreach (DataRow oDr in ds.Tables[0].Rows)
                        {
                            BookingScore.Score.MenuList oML = new BookingScore.Score.MenuList();
                            oML.MenuID = Convert.ToInt16(oDr["MenuID"]);
                            //    bool defaults = Convert.ToBoolean(oDr["Defaults"]);
                            ListML.Add(oML);
                        }
                        foreach (var i in ListML)
                        {
                            if (i.MenuID == 1)
                            {
                                lbldefault.Visible = true;

                            }
                            else if (i.MenuID == 2)
                            {
                                lblhealth.Visible = true;

                            }
                            else if (i.MenuID == 3)
                            {
                                continue;
                            }
                            else
                            {
                                lbldefault.Visible = false;
                                lblhealth.Visible = false;
                            }
                        }
                    }
                }

                if (!IsPostBack)
                {
                    GetCurrentScoreTravel();
                    GetScoreOfDayTravel();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name)))
                {
                    var msg = "Hi,";
                    msg += Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name);
                    welcome.Text = msg;
                }
            }
        }
        protected void LogOut(object sender, EventArgs e)
        {

            Response.Redirect("Login.aspx");
            // return loginstatus1;
        }

        private void GetCurrentScoreTravel()
        {
            DataSet ds = new DataSet();
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[BMS].[ActualScore_Travel]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            TravelNew.Text = dt.Rows[0]["NewActualCount"].ToString();
                        }
                    }
                }
            }
        }

        private void GetScoreOfDayTravel()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 ProductId,NewFrozenScore,RenewRolloverFrozenScore,TotalFrozenScore,BookingDateScore,CreatedBy,CreatedON,IsActive from BMS.BookingScoreDetails (nolock) where productid=3 AND IsActive=1", sqlcon))
                {
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtTravelNew.Text = reader["NewFrozenScore"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // log this exception or throw it up the StackTrace
                        // we do not need a finally-block to close the connection since it will be closed implicitely in an using-statement
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

     
        protected void ChangePassword(object sender, EventArgs e)
        {
            Response.Redirect("ChangePassword.aspx");
        }

        protected void DefaultBS(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void HealthBS(object sender, EventArgs e)
        {
            Response.Redirect("Health.aspx");
        }
    }
}
