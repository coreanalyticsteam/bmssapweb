﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BookingScore
{
    public partial class Health : System.Web.UI.Page
    {
        static string connectionString = string.Empty;
        static string connectionStringLive = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            var x = Session["LoginData"];

            if (x == null)
            {
                Response.Redirect("Login.aspx");
            }

            else
            {
                connectionString = ConfigurationManager.ConnectionStrings["ConnectionName"].ConnectionString;
                connectionStringLive = ConfigurationManager.ConnectionStrings["ConnectionLive"].ConnectionString;

                lbldefault.Visible = false;
                lbltravel.Visible = false;

                DataSet ds = new DataSet();
                SqlDataAdapter _adp;
                BookingScore.Login.UserData data = (BookingScore.Login.UserData)Session["LoginData"];
                //  data =Session["LoginData"] as LoginData;
                using (SqlConnection sqlcon = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[BMS].[GetMenuRights]", sqlcon))
                    {
                        cmd.Parameters.AddWithValue("@UserID", data.UserID);
                        cmd.CommandType = CommandType.StoredProcedure;

                        _adp = new SqlDataAdapter(cmd);
                        _adp.Fill(ds);
                        List<BookingScore.Score.MenuList> ListML = new List<BookingScore.Score.MenuList>();
                        foreach (DataRow oDr in ds.Tables[0].Rows)
                        {
                            BookingScore.Score.MenuList oML = new BookingScore.Score.MenuList();
                            oML.MenuID = Convert.ToInt16(oDr["MenuID"]);
                            //    bool defaults = Convert.ToBoolean(oDr["Defaults"]);
                            ListML.Add(oML);
                        }
                        foreach (var i in ListML)
                        {
                            if (i.MenuID == 1)
                            {
                                lbldefault.Visible = true;

                            }
                            else if (i.MenuID == 3)
                            {
                                lbltravel.Visible = true;

                            }
                            else if (i.MenuID==2)
                            {
                                continue;
                            }
                            else
                            {
                                lbldefault.Visible = false;
                                lbltravel.Visible = false;
                            }
                        }
                    }
                }

                if (!IsPostBack)
                {
                    GetHealthCurrentScore();
                    GetScoreOfDay();
                    GetCurrentScoreAttachment();                  
                    //GetScoreOfDayCI();
                    //GetScoreOfDayPA();
                    //GetScoreOfDaySTU();
                    //GetScoreOfDayTermOnHealth();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name)))
                {
                    var msg = "Hi,";
                    msg += Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name);
                    welcome.Text = msg;
                }
            }
        }
        protected void LogOut(object sender, EventArgs e)
        {

            Response.Redirect("Login.aspx");
            // return loginstatus1;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                txtTotalScore.Text = Convert.ToString(Convert.ToInt32(txtNew.Text) + Convert.ToInt32(txtRenew.Text));
                int i = CheckCurrentFrozenScore();
                if (i == 1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "ShowAlert();", true);
                    GetHealthCurrentScore();
                }
                else
                    InsertDayScore();
            }
        }

        private int CheckCurrentFrozenScore()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 ProductId,NewFrozenScore,RenewRolloverFrozenScore,TotalFrozenScore,BookingDateScore,CreatedBy,CreatedON,IsActive from BMS.BookingScoreDetails (nolock) where Productid=2 AND IsActive=1 AND BookingDateScore=@ScoreDate", sqlcon))
                {
                    cmd.Parameters.AddWithValue("@ScoreDate", System.DateTime.Today);
                    cmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                            return 1;
                        else
                            return 0;
                    }
                }
            }
        }

        private void GetHealthCurrentScore()
        {
            DataSet ds = new DataSet();
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[BMS].[ActualScore_Health]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            lblnew.Text = dt.Rows[0]["NewActualCount"].ToString();
                            lblnewape.Text = dt.Rows[0]["NewAPECount"].ToString();
                            lblrenewal.Text = dt.Rows[0]["RenewalActualCount"].ToString();
                            lblrenewalape.Text = dt.Rows[0]["RenewalAPECount"].ToString();
                            lbltotal.Text = dt.Rows[0]["TotalActualCount"].ToString();
                            lbltotalape.Text = dt.Rows[0]["TotalAPECount"].ToString();

                        }
                    }
                }
            }
        }


        private void GetCurrentScoreAttachment()
        {
            DataSet ds = new DataSet();
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[BMS].[ActualScore_Attachment]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            CI.Text = dt.Rows[0]["NewCI"].ToString();
                            PA.Text = dt.Rows[0]["NewPA"].ToString();
                            STU.Text = dt.Rows[0]["NewSTU"].ToString();
                            TermOnHealth.Text = dt.Rows[0]["NewTermOnHealth"].ToString();
                        }
                    }
                }
            }
        }
    

        private void GetScoreOfDay()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 ProductId,NewFrozenScore,RenewRolloverFrozenScore,TotalFrozenScore,BookingDateScore,CreatedBy,CreatedON,IsActive from BMS.BookingScoreDetails (nolock) where productid=2 AND BookingDateScore=@ScoreDate", sqlcon))
                {
                    cmd.Parameters.AddWithValue("@ScoreDate", System.DateTime.Today);
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtNew.Text = reader["NewFrozenScore"].ToString();
                                txtRenew.Text = reader["RenewRolloverFrozenScore"].ToString();
                                txtTotalScore.Text = reader["TotalFrozenScore"].ToString();
                                txtNew.Enabled = false;
                                txtRenew.Enabled = false;
                                txtTotalScore.Enabled = false;
                                btnSave.Enabled = false;
                            }
                        }
                        if (txtNew.Text == string.Empty)
                        {
                            txtNew.Enabled = true;
                            txtRenew.Enabled = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        // log this exception or throw it up the StackTrace
                        // we do not need a finally-block to close the connection since it will be closed implicitely in an using-statement
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

      

        private void GetScoreOfDayCI()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 CIFrozenScore from BMS.BookingScoreDetails (nolock) where productid=106 AND IsActive=1", sqlcon))
                {
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtCI.Text = reader["CIFrozenScore"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        private void GetScoreOfDayPA()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 PAMotorFrozenScore from BMS.BookingScoreDetails (nolock) where productid=118 AND IsActive=1", sqlcon))
                {
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtPA.Text = reader["PAFrozenScore"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        private void GetScoreOfDaySTU()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 STUFrozenScore from BMS.BookingScoreDetails (nolock) where productid=130 AND IsActive=1", sqlcon))
                {
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtSTU.Text = reader["STUFrozenScore"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        private void GetScoreOfDayTermOnHealth()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 TermOnHealthFrozenScore from BMS.BookingScoreDetails (nolock) where productid=7 AND IsActive=1", sqlcon))
                {
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtTermOnHealth.Text = reader["TermOnHealthFrozenScore"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        public static string GetUserIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            string ip = String.Empty;

            if (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            else if (!String.IsNullOrWhiteSpace(context.Request.UserHostAddress))
                ip = context.Request.UserHostAddress;

            if (ip == "::1")
                ip = "127.0.0.1";

            return ip;
        }

        private int InsertDayScore()
        {

            string strIPAddress = GetUserIPAddress();
            int ProductID = 2;
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("[BMS].[InsertBookingScoreDetails]", sqlcon))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductId", ProductID);
                    command.Parameters.AddWithValue("@NewFrozenScore", txtNew.Text);
                    command.Parameters.AddWithValue("@RenewRolloverFrozenScore", txtRenew.Text);
                    command.Parameters.AddWithValue("@TotalFrozenScore", txtTotalScore.Text);
                    command.Parameters.AddWithValue("@BookingDateScore", System.DateTime.Today);
                    command.Parameters.AddWithValue("@CreatedBy", 124);
                    command.Parameters.AddWithValue("@Remarks", strIPAddress);
                    command.Parameters.AddWithValue("@CurrentNew", hidCurrentNew.Value);
                    command.Parameters.AddWithValue("@CurrentRenewalRollover", hidCurrentRenewalRollover.Value);
                    try
                    {
                        sqlcon.Open();
                        int recordsAffected = command.ExecuteNonQuery();

                        btnSave.Enabled = false;

                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Great!!!Job Done Successfully');", true);
                    }
                    catch (SqlException)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('OOPs!!!Try after some time');", true);
                        btnSave.Enabled = true;

                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
            return 1;
        }

        protected void ChangePassword(object sender, EventArgs e)
        {
            Response.Redirect("ChangePassword.aspx");
        }

        protected void DefaultBS(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void TravelBS(object sender, EventArgs e)
        {
            Response.Redirect("Travel.aspx");
        }
    }
}