﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace BookingScore
{
    public partial class Login : System.Web.UI.Page
    {
        static string connectionString = string.Empty;
        static string connectionStringLive = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
           (Session["LoginData"]) = null ;
           
       

        }

        public class Menu
        {
            public int MenuID { get; set; }        
        }

        protected void Login1(object sender, EventArgs e)
        {
         
            
           
            LoginData objLoginData = new LoginData();
            objLoginData.LoginUserType = 1;
            objLoginData.Password = password.Text.Trim(); ;
            objLoginData.LoginID = username.Text.Trim(); ;
            string result = MakeRequest(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["Login"]), objLoginData, "POST", "application/json");
            var data = JsonConvert.DeserializeObject<UserData>(result);

            connectionString = ConfigurationManager.ConnectionStrings["ConnectionName"].ConnectionString;
            connectionStringLive = ConfigurationManager.ConnectionStrings["ConnectionLive"].ConnectionString;

            if (data.LoginStatus != 0 && data.UserType==1)
            {
                Session["LoginData"] = data;

                DataSet ds = new DataSet();
                SqlDataAdapter _adp;
                using (SqlConnection sqlcon = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[BMS].[GetMenuRights]", sqlcon))
                    {
                        cmd.Parameters.AddWithValue("@UserID",data.UserID);
                        cmd.CommandType = CommandType.StoredProcedure;

                        _adp = new SqlDataAdapter(cmd);
                        _adp.Fill(ds);
                        Menu objmenu = new Menu();
                        foreach (DataRow oDr in ds.Tables[0].Rows)
                        {
                            objmenu.MenuID = Convert.ToInt32(oDr["MenuID"]);
                            bool defaults = Convert.ToBoolean(oDr["Defaults"]);
                            if (objmenu.MenuID != 0 && defaults != false)
                            {
                                switch (objmenu.MenuID)
                                {
                                    case 1:
                                        Response.Redirect("Default.aspx");
                                        break;
                                    case 2:
                                        Response.Redirect("Health.aspx");
                                        break;
                                    case 3:
                                        Response.Redirect("Travel.aspx");
                                        break;
                                    default:
                                        Response.Redirect("Default.aspx");
                                        break;
                                }

                            }
                        }

                    }
                }
                


            }
            else
            {
                error.Text = "Invalid User";
            }
            
          
        }
        public class LoginData
        {
            public string LoginID { get; set; }
            public string Password { get; set; }
            public int LoginUserType { get; set; }
        }



        public class UserData
        {
            public object EmailID { get; set; }
            public int InsurerID { get; set; }
            public object InsurerName { get; set; }
            public object LoginID { get; set; }
            public int LoginStatus { get; set; }
            public object MobileNo { get; set; }
            public object Name { get; set; }
            public object Token { get; set; }
            public int UserID { get; set; }
            public int UserRole { get; set; }
            public int UserType { get; set; }
        }



        public string MakeRequest(string requestUrl, object JSONRequest, string JSONmethod, string JSONContentType)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {


                oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                    "\r\n requestUrl=" + requestUrl.ToString() +
                    "\r\n JSONRequest=" + JsonConvert.SerializeObject(JSONRequest) +
                    "\r\n JSONmethod=" + JSONmethod.ToString() +
                    "\r\n JSONContentType=" + JSONContentType);

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                request.Timeout = Convert.ToInt32(100000);
                request.Method = JSONmethod;
                request.ContentType = JSONContentType;// "application/json";

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                if (JSONmethod == "POST")
                {
                    byte[] bytes = encoding.GetBytes(JsonConvert.SerializeObject(JSONRequest));

                    request.ContentLength = bytes.Length;

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        // Send the data.
                        requestStream.Write(bytes, 0, bytes.Length);
                        
                    }
                }

                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string objRespons = responseReader.ReadToEnd();
                            oStringBuilder.Append("\r\n objRespons=" + objRespons.ToString());
                           
                           
                            return objRespons;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                oStringBuilder.Append("\r\n Error=" + ex.Message);
                return null;
                //"\r\n \r\n \r\n "
            }

        }
    }
}