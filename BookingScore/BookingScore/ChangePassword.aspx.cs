﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BookingScore
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        static string connectionString = string.Empty;
        static string connectionStringLive = string.Empty;
        
        protected void Page_Load(object sender, EventArgs e)
         {
            var x = Session["LoginData"];
            
            if (x ==null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                connectionString = ConfigurationManager.ConnectionStrings["ConnectionName"].ConnectionString;
                connectionStringLive = ConfigurationManager.ConnectionStrings["ConnectionLive"].ConnectionString;
                userID.Value = Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).UserID);
                if (!IsPostBack)
                {
                    //GetCurrentScore();
                    //GetScoreOfDay();
                    //GetCurrentScoreTW();
                    //GetScoreOfDayTW();
                    //GetCurrentScorePA();
                    //GetScoreOfDayPAMotorTP();
                    //GetScoreOfDayPATW();
                }
                 if (!string.IsNullOrEmpty(Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name)))
                {
                    var msg = "Hi,";
                    msg += Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name);
                    welcome.Text = msg;
                }
            }
        }
        protected void LogOut(object sender, EventArgs e)
        {
           
            Response.Redirect("Login.aspx");
           // return loginstatus1;
        }

        protected void ChangePassword1(object sender, EventArgs e)
        {
            if (OldPassword.Value.Length == 0)
            {
                Msg.Text =  "old password can not be null";
                return;
            }
            if (NewPassword.Value.Length == 0)
            {
                Msg.Text = "new password can not be null";
                return;
            }
            if (OldPassword.Value == NewPassword.Value)
            {
               Msg.Text = "old and new password cant be same";
                return;
            }
            if (ConfirmNewPassword.Value.ToString() != NewPassword.Value.ToString())
            {
                Msg.Text = "New Password does not match";
                return;
            }
           
            
            BookingScore.Proxy.ChangePasswordClass oCP = new BookingScore.Proxy.ChangePasswordClass();
            oCP.OldPassword = OldPassword.Value.ToString();
            oCP.NewPassword = NewPassword.Value.ToString();
            oCP.UserId = Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).UserID);
            string result = MakeRequest(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CCTechService"])+"ChangePassword", oCP, "POST", "application/json");
            var data = JsonConvert.DeserializeObject<int>(result);
            //Response.Write("<script>alert(" + data + ");</script>");
            if (data.ToString() == "0")
            {
                Msg.Text = "Successfully Changed";
                //OldPassword.Value = "";
                //NewPassword.Value = "";
                //ConfirmNewPassword.Value = "";
            }
            else
            {
                Msg.Text = "Old Password does not match";
            }

        }
         public string MakeRequest(string requestUrl, object JSONRequest, string JSONmethod, string JSONContentType)
        {
            StringBuilder oStringBuilder = new StringBuilder();
            try
            {


                oStringBuilder.Append("\r\n Time=" + DateTime.Now +
                    "\r\n requestUrl=" + requestUrl.ToString() +
                    "\r\n JSONRequest=" + JsonConvert.SerializeObject(JSONRequest) +
                    "\r\n JSONmethod=" + JSONmethod.ToString() +
                    "\r\n JSONContentType=" + JSONContentType);

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                request.Timeout = Convert.ToInt32(100000);
                request.Method = JSONmethod;
                request.ContentType = JSONContentType;// "application/json";

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                if (JSONmethod == "POST")
                {
                    byte[] bytes = encoding.GetBytes(JsonConvert.SerializeObject(JSONRequest));

                    request.ContentLength = bytes.Length;

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        // Send the data.
                        requestStream.Write(bytes, 0, bytes.Length);
                        
                    }
                }

                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            string objRespons = responseReader.ReadToEnd();
                            oStringBuilder.Append("\r\n objRespons=" + objRespons.ToString());
                           
                           
                            return objRespons;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                oStringBuilder.Append("\r\n Error=" + ex.Message);
                return null;
                //"\r\n \r\n \r\n "
            }

        }
    
      
    }
}