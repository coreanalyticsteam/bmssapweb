﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageUser.aspx.cs" Inherits="BookingScore.ManageUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/style2.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       <script src="//kendo.cdn.telerik.com/2016.3.1028/js/kendo.all.min.js"></script>

    <link rel="stylesheet" href="//kendo.cdn.telerik.com/2016.3.1028/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="//kendo.cdn.telerik.com/2016.3.1028/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="//kendo.cdn.telerik.com/2016.3.1028/styles/kendo.material.mobile.min.css" />

    <title> Booking Score</title>
    <meta http-equiv="refresh" content="120" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
                   body{background-color:#f2f2f2; padding:0; margin:0;}
            html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {border:0 none;font:inherit;margin:0;padding:0;vertical-align:baseline;list-style:none; font-family: "Lato",sans-serif;}
            article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {display: block;}
            button{ cursor:pointer;}
            .clearfix{ clear:both;}

            h1{ font-size:15px; margin:20px 0 0 0; padding:0; text-align:left; }
            #main_container{ width:1088px; margin:0 auto;}
            .inner_block{ width:100%; display:table; margin:0 auto; padding:30px 0; text-align:center; margin-top:20px;} 
            .score_block{ width:30%; margin:0 10px 0 0; padding:0; border:1px solid #ddd;display: inline-block; border-radius:5px; background:#fff; box-shadow:1px 2px 5px -1px #a0a0a0; vertical-align: top;}
            .product_head{background: #039be5; padding:15px 0; color: #fff;display: block;border-radius: 4px 4px 0 0;border: 1px solid #048acc;border-bottom: none;}
            .rating_box{width: 100%;display: block;margin: 0;padding: 0;background: #efefef;box-sizing: border-box;}
            ul.table_heading{display:table; width:100%; margin:0; padding:0;}
            ul.table_heading li{margin: 0;padding: 12px 0;display: table-cell;text-align: center;font-size: 13px;font-family: lato;color: #696969;width: 33.33333333333%; border-right: 1px solid #ddd; font-weight: 600;}
            ul.table_heading li:last-child{ border-right:none;}

            ul.table_data{display:table; width:100%; margin:0; padding:0; background:#fff;}
            ul.table_data li{margin: 0;padding: 12px 0;display: table-cell;text-align: center;font-size: 13px;font-family: lato;color: #696969;text-transform: uppercase;width: 33.33333333333%; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;}
            ul.table_data li:last-child{ border-right:none;}

            .form_hd{display: block; padding:0;}
            .data_hd{padding: 10px 0;color: #909090; background: #f5f5f5; display:block;}
            ul.form_box{width: 100%; margin: 0; padding:15px 15px 20px 15px; display: block; box-sizing: border-box;}
            ul.form_box li{width:100%; margin:15px 0 0 0; padding:0; display:inline-block;}
            ul.form_box li label{width: 40%;margin: 0; padding: 0;display: inline-block;font-size: 14px;text-align: left;color: #aeaeae;float: left;}
            ul.form_box li span{width: 60%; margin: 0; float: left; padding: 0; box-sizing: border-box; margin: 0;     text-align: left; }
            ul.form_box li span input[type="text"]{   padding: 9px 6px; display: inline; font-size: 14px;color: #aeaeae;border-radius: 4px;border: 1px solid #c5c5c5;width: 100%; box-sizing: border-box;}
            .login_button{  border: 1px solid #0395dc; border-radius: 4px; color: #595FFF; font-size: 16px; min-width: 140px; padding: 6px 5px; background: #039be5; text-align: center;   position: relative; min-width: 100px; box-shadow: 1px 2px 5px -1px #a0a0a0;}
            .submit_button{  border: 1px solid #0395dc; border-radius: 4px; color: #ffffff; font-size: 16px; min-width: 140px; padding: 6px 5px; background: #039be5; text-align: center;   position: relative; min-width: 100px; box-shadow: 1px 2px 5px -1px #a0a0a0;}
            .submit_block{ padding:10px 0; display:block;}
                    @media screen and (max-width:1024px) {
	            .score_block{width: 100%;margin: 0 0 20px 0; padding: 0; box-sizing: border-box;}
	            .inner_block{ width:100%;}
	            h1{ margin-left:0;}
	            #main_container {width: 95%;margin: 0 auto; margin: 10px;box-sizing: border-box;}
                .login_block span em.login_icon {    background: url(../images/sprite.png) no-repeat -12px -80px;    width: 19px;    height: 21px;    float: left;}
                .container_block { box-sizing: border-box; margin: 0 auto; padding:0; width: 100%; margin-top: 20px; margin-bottom: 20px;}
	
	            }
</style>
    <script type="text/javascript">
        function ConfirmClick() {
            var today = new Date();
            var h = today.getHours();
            var today = new Date();
            var h = today.getHours();
            var newScore = document.getElementById('txtNew').value;
            var renewScore = document.getElementById('txtRenewRollover').value;
            if (h >= 16) {
                if (newScore <= 0) {
                    alert("Please enter positive value for New Score");
                    document.getElementById('txtNew').focus();
                    return false;
                }
                else if (newScore < parseInt(hidCurrentNew.value)) {
                    alert("New Input Score cannot be less than Current New Score");
                    document.getElementById('txtNew').focus();
                    return false;
                }
                if (renewScore <= 0) {
                    alert("Please enter positive value for Renewal+Rollover Score");
                    document.getElementById('txtRenewRollover').focus();
                    return false;
                }
                else if (renewScore < parseInt(hidCurrentRenewalRollover.value)) {
                    alert("Renew+Rollover Input Score cannot be less than Current Renew+Rollover Score");
                    document.getElementById('txtRenewRollover').focus();
                    return false;
                }
                var x = confirm("Are you sure you want to freeze the score now?");
                if (x) {
                    return true;
                }
                else
                    return false;
            }
            else {
                alert('Oh Sorry!!!You cannot freeze before 6 PM');
                return false;
            }
        }

        function ShowAlert() {
            alert('You are already done, Thank you!!!');
        }
        function toggleClick() {
            $(".user_list").toggle();
        };

</script>
  
      
</head>
    
    <body>
         
        <form id="form1"   runat="server">
                  
            <div class="header">
                        
        <div class="inner_container">
            <div class="logo"></div>


            <div class="login_block" >
            
               <div class="login_block" ng-if="UserDetails != null">
            <asp:label style="text-align:right; color:#fff; font-size:15px; " id="welcome" runat="server"></asp:label>
            <span id="show" onclick="toggleClick()">
                <em class="login_icon"></em>
                <div class="user_list">
                    <em class="arrow_box"></em>
                    <ul class="change_list">
                        <li ><em class=""></em><label>Dashboard</label></li>

                        <li ><label>Change Password</label></li>
                        <li ><em class=""></em><label>Manage Users</label></li>

                        <%--<li ><em class="logout"></em><label OnClick="LogOut" runat="server"  >Logout</label></li>--%>
                        <li ><%--<button type="button" class="logout_button"  OnClick="LogOut" text="LogOut" runat="server">Logout</button>--%>
                          <em class="logout"></em><label>  <asp:LinkButton runat="server"  OnClick="LogOut" > Logout </asp:LinkButton></label>
                        </li>
                    </ul>
                </div>
            </span>

                <%--<asp:button type="button" class="logout_button"  OnClick="LogOut" text="LogOut" runat="server"></asp:button>--%>
                   </div>


            
            
           


            
               
                     
        </div>
    </div>
           
           
           
             
            <div id="main_container">
               <h1 style="color:#000000">BOOKING SCORE  :  <%= DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss") %></h1>
                <div class="inner_block">
                  
                   
                 
                
                    
                   
                    
                    <div class="score_block">
                        <span class="product_head">Motor</span>
                        <div class="rating_box">
                            <ul class="table_heading">               
                                <li>New</li>
                                <li>Renew Rollover</li>
                                <li>Total</li>
                            </ul>
                            <ul class="table_data">
                                <li><asp:label id="new0" runat="server"></asp:label></li>
                                <li><asp:label id="renewrollover" runat="server"></asp:label></li>
                                <li><asp:label id="total" runat="server"></asp:label></li>
                            </ul>
                    </div>
                    <div class="form_hd">
                        <span class="data_hd">Input Car Score</span>    
                        <ul class="form_box">
                            <li><label>New</label> <span> <asp:TextBox ID="txtNew" runat="server" /></span></li>
                            <li><label>Renewal+Rollover</label> <span><asp:TextBox ID="txtRenewRollover" runat="server" /></span></li>
                            <li><label>Total Score</label> <span><asp:TextBox ID="txtTotalScore" runat="server" /></span></li>
                            <li><label>&nbsp;</label> <span> <asp:Button Text="Submit" runat="server" id="btnSave" class="submit_button" OnClick="btnSave_Click"  OnClientClick="return ConfirmClick()"/></br></span></li>
<%--                            <asp:Button type="button" id="btnSave" class="submit_button" Text="Submit" runat="server" OnClick="btnSave_Click" OnClientClick="return ConfirmClick()></asp:Button>--%>
                        </ul>    
   
                    </div>
                </div>

                    <div class="score_block">
                        <span class="product_head">TW</span>
                        <div class="rating_box">
                            <ul class="table_heading">               
                                <li>New</li>
                                <li>Renew Rollover</li>
                                <li>Total</li>
                            </ul>
                            <ul class="table_data">
                                <li><asp:label id="new1" runat="server"></asp:label></li>
                                <li><asp:label id="renewrollover1" runat="server"></asp:label></li>
                                <li><asp:label id="total1" runat="server"></asp:label></li>
                            </ul>
                        </div>
                        <div class="form_hd">
                            <span class="data_hd">TW Score at 7pm as below</span>    
                            <ul class="form_box">
                                <li><label>New</label> <span><asp:TextBox ID="txtTWNew" runat="server" Enabled="false"/></span></li>
                                <li><label>Renewal+Rollover</label> <span><asp:TextBox ID="txtTWRenew" runat="server"  Enabled="false"/></span></li>
                                <li><label>Total Score</label> <span><asp:TextBox ID="txtTWTotal" runat="server" Enabled="false"/></span></li>
                                <li><label>&nbsp;</label> <span></br></br></span></li>

                            </ul>    
   
                        </div>
                    </div>

                    <div class="score_block">
                        <span class="product_head">PA</span>
                        <div class="rating_box">
                            <ul class="table_heading">               
                                <li>PA Motor</li>
                                <li>PA TW</li>
                                <li>Motor TP</li>
                            </ul>
                            <ul class="table_data">
                                <li><asp:label id="PAMotor" runat="server"></asp:label></li>
                                <li><asp:label id="PATW" runat="server"></asp:label></li>
                                <li><asp:label id="MotorTP" runat="server"></asp:label></li>
                            </ul>
                        </div>
                        <div class="form_hd">
                            <span class="data_hd">Closing Details</span>    
                            <ul class="form_box">
                            <li><label>PA Motor</label> <span><asp:TextBox ID="txtPAMotor" runat="server" Enabled="false"/></span></li>
                            <li><label>PA TW</label> <span><asp:TextBox ID="txtPATW" runat="server"  Enabled="false"/></span></li>
                                <li><label>Motor TP</label> <span><asp:TextBox ID="txtMotorTP" runat="server"  Enabled="false"/></span></li>
                            <li><label>&nbsp;</label> <span></br></br></span></li>
                            </ul>       
                        </div>
                    </div>

                    <asp:HiddenField ID="hidCurrentNew" runat="server" />
                    <asp:HiddenField ID="hidCurrentRenewalRollover" runat="server" />     
                                 
                </div> 
            </div>
                 
        </form>
                 
   <script>
     
   </script>
    </body>
</html>
