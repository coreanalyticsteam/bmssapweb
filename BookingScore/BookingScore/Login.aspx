﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BookingScore.Login" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Booking Score</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,100,300,700' rel='stylesheet' type='text/css' />

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" type="text/css" rel="stylesheet" />





</head>
<body>
    <form runat="server">
 <%--   ng-app="HealthExchange"--%>
    <div class="header">
        <div class="inner_container">
            <div class="logo"></div>


        </div>
    </div>
    <div id="main_container">
    
        <div class="login_container">
            <div class="login_heading">Login</div>
            <div class="login_inner_box">
                <ul class="logininput">
                    <li>
                        <label><em class="user_icon"></em></label><font>Username</font>
                        <asp:label runat="server"><asp:textbox name="" id="username" type="text" value="" placeholder="User Name" runat="server"/></asp:label>
                     
                    </li>
                    <li>
                        <label><em class="password_icon"></em></label><font>Password</font>
                        <asp:label runat="server"><asp:textbox name="" id="password" type="password" value="" placeholder="Password" runat="server"/></asp:label>
                     
                    </li>
                </ul>
                </ br>
               
                <div class="login_button"> <asp:button type="button" OnClick="Login1" Text="Sign In" runat="server"/></div>
                <div style="color:red"><asp:Label ID="error" runat="server"></asp:Label></div>
            </div>
        </div>
    </div>
        </form>
    </body>
</html>


