﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookingScore
{
    public class Proxy
    {
        
        public class ChangePasswordClass
        {
            public string OldPassword { get; set; }

            public string NewPassword { get; set; }

            public string UserId { get; set; }
        }
    }
}