﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BookingScore
{
    public partial class ManageUser : System.Web.UI.Page
    {
        static string connectionString = string.Empty;
        static string connectionStringLive = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
         {
            var x = Session["LoginData"];

            if (x ==null)
            {
                Response.Redirect("Login.aspx");
            }
           
            else
            {
                connectionString = ConfigurationManager.ConnectionStrings["ConnectionName"].ConnectionString;
                connectionStringLive = ConfigurationManager.ConnectionStrings["ConnectionLive"].ConnectionString;

                if (!IsPostBack)
                {
                    GetCurrentScore();
                    GetScoreOfDay();
                    GetCurrentScoreTW();
                    GetScoreOfDayTW();
                    GetCurrentScorePA();
                    GetScoreOfDayPAMotorTP();
                    GetScoreOfDayPATW();
                }
                 if (!string.IsNullOrEmpty(Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name)))
                {
                    var msg = "Hi,";
                    msg += Convert.ToString(((BookingScore.Login.UserData)(Session["LoginData"])).Name);
                    welcome.Text = msg;
                }
            }
        }
        protected void LogOut(object sender, EventArgs e)
        {
           
            Response.Redirect("Login.aspx");
           // return loginstatus1;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                txtTotalScore.Text = Convert.ToString(Convert.ToInt32(txtNew.Text) + Convert.ToInt32(txtRenewRollover.Text));
                int i = CheckCurrentFrozenScore();
                if (i == 1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "ShowAlert();", true);
                    GetCurrentScore();
                }
                else
                    InsertDayScore();
            }
        }

        private int CheckCurrentFrozenScore()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 ProductId,NewFrozenScore,RenewRolloverFrozenScore,TotalFrozenScore,BookingDateScore,CreatedBy,CreatedON,IsActive from BMS.BookingScoreDetails (nolock) where Productid=117 AND IsActive=1 AND BookingDateScore=@ScoreDate", sqlcon))
                {
                    cmd.Parameters.AddWithValue("@ScoreDate", System.DateTime.Today);
                    cmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                            return 1;
                        else
                            return 0;
                    }
                }
            }
        }

        private void GetCurrentScore()
        {
            DataSet ds = new DataSet();
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[BMS].[ActualScore_Motor]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            new0.Text = dt.Rows[0]["NewActualCount"].ToString();
                            renewrollover.Text = dt.Rows[0]["RenewalRolloverActualCount"].ToString();
                            total.Text = dt.Rows[0]["TotalActualCount"].ToString();
                           
                        }
                    }
                }
            }
        }

        private void GetCurrentScoreTW()
        {
            DataSet ds = new DataSet();
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[BMS].[ActualScore_TW]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            new1.Text = dt.Rows[0]["NewActualCount"].ToString();
                            renewrollover1.Text = dt.Rows[0]["RenewalRolloverActualCount"].ToString();
                            total1.Text = dt.Rows[0]["TotalActualCount"].ToString();
                    
                        }
                    }
                }
            }
        }

        private void GetCurrentScorePA()
        {
            DataSet ds = new DataSet();
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[BMS].[ActualScore_PA]", sqlcon))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();

                        da1.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            PAMotor.Text = dt.Rows[0]["NewPAMotor"].ToString();
                            PATW.Text = dt.Rows[0]["NewPATW"].ToString();
                            MotorTP.Text = dt.Rows[0]["NewMotorTP"].ToString();                          
                        }
                    }
                }
            }
        }

        private void GetScoreOfDay()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 ProductId,NewFrozenScore,RenewRolloverFrozenScore,TotalFrozenScore,BookingDateScore,CreatedBy,CreatedON,IsActive from BMS.BookingScoreDetails (nolock) where productid=117 AND BookingDateScore=@ScoreDate", sqlcon))
                {
                    cmd.Parameters.AddWithValue("@ScoreDate", System.DateTime.Today);
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtNew.Text = reader["NewFrozenScore"].ToString();
                                txtRenewRollover.Text = reader["RenewRolloverFrozenScore"].ToString();
                                txtTotalScore.Text = reader["TotalFrozenScore"].ToString();
                                txtNew.Enabled = false;
                                txtRenewRollover.Enabled = false;
                                txtTotalScore.Enabled = false;
                                btnSave.Enabled = false;
                            }
                        }
                        if (txtNew.Text == string.Empty)
                        {
                            txtNew.Enabled = true;
                            txtRenewRollover.Enabled = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        // log this exception or throw it up the StackTrace
                        // we do not need a finally-block to close the connection since it will be closed implicitely in an using-statement
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        private void GetScoreOfDayTW()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 ProductId,NewFrozenScore,RenewRolloverFrozenScore,TotalFrozenScore,BookingDateScore,CreatedBy,CreatedON,IsActive from BMS.BookingScoreDetails (nolock) where productid=114 AND IsActive=1", sqlcon))
                {
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtTWNew.Text = reader["NewFrozenScore"].ToString();
                                txtTWRenew.Text = reader["RenewRolloverFrozenScore"].ToString();
                                txtTWTotal.Text = reader["TotalFrozenScore"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // log this exception or throw it up the StackTrace
                        // we do not need a finally-block to close the connection since it will be closed implicitely in an using-statement
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        private void GetScoreOfDayPAMotorTP()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 PAMotorFrozenScore,BajajTPMotorScore from BMS.BookingScoreDetails (nolock) where productid=117 AND IsActive=1", sqlcon))
                {
                  //  Select top 1 PAMotorFrozenScore,BajajTPMotorScore from BMS.BookingScoreDetails (nolock) where productid=117 AND IsActive=1
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtPAMotor.Text = reader["PAMotorFrozenScore"].ToString();
                                txtMotorTP.Text = reader["BajajTPMotorScore"].ToString();                                
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // log this exception or throw it up the StackTrace
                        // we do not need a finally-block to close the connection since it will be closed implicitely in an using-statement
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        private void GetScoreOfDayPATW()
        {
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select top 1 PATWFrozenScore from BMS.BookingScoreDetails (nolock) where productid=114 AND IsActive=1", sqlcon))
                {
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        sqlcon.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                txtPATW.Text = reader["PATWFrozenScore"].ToString();                                
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // log this exception or throw it up the StackTrace
                        // we do not need a finally-block to close the connection since it will be closed implicitely in an using-statement
                        throw;
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
        }

        public static string GetUserIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            string ip = String.Empty;

            if (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            else if (!String.IsNullOrWhiteSpace(context.Request.UserHostAddress))
                ip = context.Request.UserHostAddress;

            if (ip == "::1")
                ip = "127.0.0.1";

            return ip;
        }        

        private int InsertDayScore()
        {

            string strIPAddress = GetUserIPAddress();
            using (SqlConnection sqlcon = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("[BMS].[UnassignedBookingsReport]", sqlcon))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductId", 117);
                    command.Parameters.AddWithValue("@NewFrozenScore", txtNew.Text);
                    command.Parameters.AddWithValue("@RenewRolloverFrozenScore", txtRenewRollover.Text);
                    command.Parameters.AddWithValue("@TotalFrozenScore", txtTotalScore.Text);
                    command.Parameters.AddWithValue("@BookingDateScore", System.DateTime.Today);
                    command.Parameters.AddWithValue("@CreatedBy", 124);
                    command.Parameters.AddWithValue("@Remarks", strIPAddress);
                    command.Parameters.AddWithValue("@CurrentNew", hidCurrentNew.Value);
                    command.Parameters.AddWithValue("@CurrentRenewalRollover", hidCurrentRenewalRollover.Value);
                    try
                    {
                        sqlcon.Open();
                        int recordsAffected = command.ExecuteNonQuery();
                        
                        btnSave.Enabled = false;
                       
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Great!!!Job Done Successfully');", true);
                    }
                    catch (SqlException)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('OOPs!!!Try after some time');", true);
                        btnSave.Enabled = true;
                       
                    }
                    finally
                    {
                        sqlcon.Close();
                    }
                }
            }
            return 1;
        }

      
    }
}