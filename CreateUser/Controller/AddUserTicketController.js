﻿myApp.controller('AddUserTicketController', function ($scope, AjaxFactory, $stateParams, $state) {
    $scope.UserMgmtData = UserMgmtData;
    $scope.User = {};
    $scope.AddUser = {};
    $scope.SupplierList = [];
    //var GetMaxBupa = 
    // alert($stateParams.Id)
    if ($stateParams.Id == 0) {
        $scope.NewUser = true;
    }
    else {
        $scope.NewUser = false;
    }
    if ($stateParams.Id >= 0) {
        
        AjaxFactory.postData(config.serviceURL + 'GetUserDetailsTicket', { UserID: $stateParams.Id }, function (data) {
            //console.log(data)
            $scope.User = JSON.parse(data.data.result.Success);
          
        });
    }
    AjaxFactory.post(config.serviceURL + 'GetProducts', function (data) {
        console.log(data.data.Success);
        $scope.Products = JSON.parse(data.data.Success);

    });
    $scope.GetInsurerName = function (data) {
        AjaxFactory.postData(config.serviceURL + 'GetSuppliers', {ProductId: data.ProductID}, function (data) {
            $scope.SupplierList = JSON.parse(data.data.result.Success);
            console.log($scope.SupplierList);
        });
    }
    $scope.AddNewUsers = function () {
        $scope.User.CreatedBy = parseInt(localStorage.getItem("CreatedBy"));
        if ($scope.User.InsurerUserMapping == undefined) {
            $scope.User.InsurerUserMapping = [];
            alert("Please add minimum 1 Product and insurer;")
            return false;
        }
        if ($scope.User.InsurerUserMapping.length == 0) {
            alert("Please add minimum 1 Product and insurer;")
            return false;
        }
        AjaxFactory.postData(config.serviceURL + 'SaveUserDetailsTicket', { Json: $scope.User }, function (data) {
           // console.log(data)
            if (data.data.result.Error == '') {
                alert("Successfully Saved");
                if ($stateParams.Id == 0) {
                    $state.go('AddUserTicket', { 'Id': JSON.parse(data.data.result.Success).UserId });
                }
                else {
                    $scope.User = JSON.parse(data.data.result.Success);
                }
            }
            else {
                alert(data.data.result.Error);
            }
            // console.log($scope.SupplierList);
        });
    }
    $scope.InsurerSelected = function (data) {

    }
    $scope.Back = function () {
        $state.go('DashboardTicket', { 'Id': parseInt(localStorage.getItem("CreatedBy")) });
    }
    $scope.addToList = function ()
    {
        debugger;
        if ($scope.AddUser.Product == undefined) {
            alert("Please Select Product first")
            return false;
        }
        if ($scope.AddUser.Product.ProductID == undefined) {
            alert("Please Select Product first")
            return false;
        }
        if ($scope.AddUser.Insurer == undefined) {
            alert("Please Select Insurer first")
            return false;
        }
        if ($scope.AddUser.Insurer.ID == undefined) {
            alert("Please Select Insurer first")
            return false;
        }
        if ($scope.User.InsurerUserMapping == undefined) {
            $scope.User.InsurerUserMapping = [];
        }
        var NewData = { 'SNO': 0, 'MappingId': 0, 'SupplierName': $scope.AddUser.Insurer.SupplierName, 'InsurerID': $scope.AddUser.Insurer.ID, 'ProductId': $scope.AddUser.Product.ProductID, 'ProductName': $scope.AddUser.Product.Product, 'IsActive': true }
        console.log(NewData);
        for (var a = 0; a < $scope.User.InsurerUserMapping.length; a++) {
           // alert(a);
            if ($scope.User.InsurerUserMapping[a].InsurerID == NewData.InsurerID && $scope.User.InsurerUserMapping[a].ProductId == NewData.ProductId) {
                alert("Already exists.Please Check");
                return false;
            }
        }
        $scope.User.InsurerUserMapping.push(NewData);
        $scope.AddUser = {};
    }
    $scope.Delete = function (dataindex) {
        $scope.User.InsurerUserMapping.splice(dataindex,1)
    }
    
});