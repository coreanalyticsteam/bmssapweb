﻿myApp.controller('DashboardTicketController', function ($scope, AjaxFactory, $stateParams, $state) {
    
   // alert("Hello");
    //var GetMaxBupa = 
    localStorage.setItem("CreatedBy", $stateParams.Id)
   // console.log('$stateParams.Id : '+$stateParams.Id)
    AjaxFactory.post(config.serviceURL + 'GetExUserTicket', function (data) {
        console.log(JSON.parse(data.data.Success));
        $scope.users = JSON.parse(data.data.Success);
    });
    $scope.users = []; //declare an empty array
   

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.EditUser = function (data) {
        $state.go('AddUserTicket', { 'Id': data.UserID });
    }
    $scope.AddUser = function () {
        $state.go('AddUserTicket', { 'Id': 0 });
    }
});