﻿myApp.controller('AddUserController', function ($scope, AjaxFactory, $stateParams, $state) {
    $scope.UserMgmtData = UserMgmtData;
    $scope.User = {};
    $scope.SupplierList = [];
    debugger;
    //var GetMaxBupa = 
    // alert($stateParams.Id)
    if ($stateParams.Id == 0) {
        $scope.NewUser = true;
    }
    else {
        $scope.NewUser = false;
    }
    if ($stateParams.Id >= 0) {
        
        AjaxFactory.postData(config.serviceURL + 'GetUserDetails', { UserID: $stateParams.Id }, function (data) {
            //console.log(data)
            $scope.User = JSON.parse(data.data.result.Success);
            if ($scope.User.UserType.ID == 2) {
                $scope.GetInsurerName($scope.User.Product);
            }
           // console.log($scope.SupplierList);
        });
    }
    AjaxFactory.post(config.serviceURL + 'GetProducts', function (data) {
        console.log(data.data.Success);
        $scope.Products = JSON.parse(data.data.Success);

    });
    $scope.GetInsurerName = function (data) {
        AjaxFactory.postData(config.serviceURL + 'GetSuppliers', {ProductId: data.ProductID}, function (data) {
            $scope.SupplierList = JSON.parse(data.data.result.Success);
            console.log($scope.SupplierList);
        });
    }
    $scope.AddNewUsers = function () {
        debugger;
        AjaxFactory.postData(config.serviceURL + 'SaveUserDetails', { Json: $scope.User }, function (data) {
           // console.log(data)
            if (data.data.result.Error == '') {
                alert("Successfully Saved");
                if ($stateParams.Id == 0) {
                    $state.go('AddUser', { 'Id': JSON.parse(data.data.result.Success).UserId });
                }
                else {
                    $scope.User = JSON.parse(data.data.result.Success);
                }
            }
            else {
                alert(data.data.result.Error);
            }
            // console.log($scope.SupplierList);
        });
    }
    $scope.InsurerSelected = function (data) {

    }
    //$scope.User.Default = 1;
    $scope.MenuCheck = function (data) {
        //alert($scope.User.Default)
        debugger;
        if ($scope.User.Default == data.MenuID && !data.IsChecked) {
            $scope.User.Default = undefined;
        }
        var r = 0;
        for (var a in $scope.User.MenuRights) {
            if ($scope.User.MenuRights[a].IsChecked) {
                r = parseInt(a) + 1;
                break;
            }
        }
        if (r != 0 && ($scope.User.Default == undefined || $scope.User.Default == 0) && data.IsChecked) {
            $scope.User.Default = data.MenuID;
            $scope.$apply();
        }
        else if (r != 0 && ($scope.User.Default == undefined || $scope.User.Default == 0) && !data.IsChecked) {
            $scope.User.Default = r;
            $scope.$apply();
        }

        if (($scope.User.Default == undefined || $scope.User.Default == 0) && r != 0) {
            $scope.User.Default = r;
            $scope.$apply();
        }
    }
    $scope.clearAll = function (data) {
        debugger;
        if (!data) {
            for (var a in $scope.User.MenuRights) {
                $scope.User.MenuRights[a].IsChecked = false;
                
            }
            $scope.User.Default = undefined;
        }
        
    };
    
});