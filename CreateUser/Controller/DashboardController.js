﻿myApp.controller('DashboardController', function ($scope, AjaxFactory, $stateParams, $state) {

    //var GetMaxBupa = 
    AjaxFactory.post(config.serviceURL + 'GetExUser', function (data) {
        console.log(JSON.parse(data.data.Success));
        $scope.users = JSON.parse(data.data.Success);
    });
    $scope.users = []; //declare an empty array
   

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.EditUser = function (data) {
        $state.go('AddUser', { 'Id': data.UserID });
    }
    $scope.AddUser = function () {
        $state.go('AddUser', { 'Id': 0 });
    }
});