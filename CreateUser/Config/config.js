﻿myApp.config(function ($stateProvider) {
    $stateProvider
    .state('Dashboard', {
        name: 'Dashboard',
        url: '/Dashboard',
        templateUrl: 'View/Dashboard.html',
        controller:'DashboardController'
    })
    .state('AddUser', {
        name: 'AddUser',
        url: '/AddUser/:Id',
        templateUrl: 'View/AddUser.html',
        controller: 'AddUserController'
    })
    .state('DashboardTicket', {
        name: 'DashboardTicket',
        url: '/DashboardTicket/:Id',
        templateUrl: 'View/DashboardTicket.html',
        controller: 'DashboardTicketController'
    })
    .state('AddUserTicket', {
        name: 'AddUserTicket',
        url: '/AddUserTicket/:Id',
        templateUrl: 'View/AddUserTicket.html',
        controller: 'AddUserTicketController'
    })
    ;
});
