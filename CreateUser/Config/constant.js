﻿var config = {
    serviceURL: "http://localhost:55235/VideoInspectionService.svc/"
};
var UserMgmtData = {
    "UserRoleName": [{ "ID": 1, "Name": "Admin" },
                      { "ID": 2, "Name": "Agent" }],

    "UserTypeName": [{ "ID": 1, "Name": "PB User" },
                   { "ID": 2, "Name": "Insurer" }],

    //"ApplicationType": [{ "ID": 1, "Name": "HealthEx" }, { "ID": 2, "Name": "Inspection" }, { "ID": 3, "Name": "Agent Score" }],
    //"Products": [{ "ID": 2, "Name": "Health" }, { "ID": 117, "Name": "Motor" }],
    "ButtonLabel": { "Register": "Register", "Update": "Update", "Edit": "Edit", "Submit": "Submit" }


};