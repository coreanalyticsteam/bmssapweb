﻿
myApp.factory('AjaxFactory', function ($http, $location) {
    var postData = function (url, data, callBackFn) {
        var token = '';
        var UserId = '';
        if (JSON.parse(localStorage.getItem('UserDetails'))) {
            token = JSON.parse(localStorage.getItem('UserDetails')).Token;
            UserId = JSON.parse(localStorage.getItem('UserDetails')).UserID;
        }
       
        $http({
            method: 'POST', url: url, headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(token + "~" + UserId),
                "UserID": UserId
            }, data: JSON.stringify(data)
        })
           .then(function successCallback(response) {
               callBackFn(response);
           }, function errorCallBack(response) {
               console.log(response);
              // ErrorMsg();
               return null;
           });

    };
    var post = function (url, callBackFn) {
        var token = '';
        var UserId = '';
        if (JSON.parse(localStorage.getItem('UserDetails'))) {
            token = JSON.parse(localStorage.getItem('UserDetails')).Token;
            UserId = JSON.parse(localStorage.getItem('UserDetails')).UserID;
        }
        $http({
            method: 'POST', url: url, headers: {
                "Content-Type": "application/json; charset=utf-8",
                "auth-token": btoa(token + "~" + UserId),
                "UserID": UserId
            }
        })
           .then(function successCallback(response) {
               callBackFn(response);
           }, function errorCallBack(response) {
               console.log(response);
             //  ErrorMsg();
               return null;
           });

    };
    return {
        postData: postData,
        post: post
        //checkToken: checkToken
    };
});

